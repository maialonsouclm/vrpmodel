Instances\Inst10P1Du_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000030
camion 1, TRUE-heu 0.000057
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 6973.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 282 rows and 52 columns.
MIP Presolve modified 774 coefficients.
Reduced MIP has 400 rows, 1370 columns, and 10068 nonzeros.
Reduced MIP has 1170 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (8.46 ticks)
Clique table members: 121.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.02 sec. (14.34 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         6973.0000        0.0000           100.00%
      0     0     4272.5619    83     6973.0000     4272.5619      642   38.73%
      0     0     5177.9214    83     6973.0000      Cuts: 51      772   25.74%
      0     0     5264.6986    83     6973.0000      Cuts: 30      919   24.50%
      0     0     5330.4390    83     6973.0000      Cuts: 51     1262   23.56%
      0     0     5440.8000    83     6973.0000      Cuts: 41     1371   21.97%
      0     0     5684.8288    83     6973.0000      Cuts: 42     1554   18.47%
      0     0     5687.0887    83     6973.0000      Cuts: 57     1617   18.44%
      0     0     5691.7252    83     6973.0000      Cuts: 36     1700   18.37%
      0     0     5694.9268    83     6973.0000      Cuts: 33     1750   18.33%
      0     0     5705.3571    83     6973.0000      Cuts: 29     1846   18.18%
      0     0     5753.5400    83     6973.0000      Cuts: 21     2054   17.49%
      0     0     5785.0442    83     6973.0000      Cuts: 43     2133   17.04%
      0     0     5834.1026    83     6973.0000      Cuts: 51     2192   16.33%
      0     0     5840.4092    83     6973.0000      Cuts: 45     2237   16.24%
      0     0     5994.5299    83     6973.0000      Cuts: 41     2385   14.03%
      0     0     6002.8150    83     6973.0000      Cuts: 39     2452   13.91%
      0     0     6025.5137    83     6973.0000      Cuts: 29     2483   13.59%
      0     0     6082.7423    83     6973.0000      Cuts: 27     2637   12.77%
      0     0     6089.0000    83     6973.0000      Cuts: 24     2679   12.68%
      0     0     6090.6222    83     6973.0000      Cuts: 35     2741   12.65%
      0     0     6094.4469    83     6973.0000      Cuts: 42     2804   12.60%
      0     0     6096.5714    83     6973.0000      Cuts: 42     2879   12.57%
      0     0     6097.2016    83     6973.0000      Cuts: 27     2936   12.56%
      0     0     6097.4899    83     6973.0000       Cuts: 8     2949   12.56%
Dentro lazy
camion 0, TRUE-heu 0.000090
camion 1, TRUE-heu 0.000116
Salgo lazy
*     0+    0                         6793.0000     6097.4899            10.24%
      0     0  -1.00000e+75     0     6793.0000     6097.4899     2949   10.24%
      0     2     6097.4899    72     6793.0000     6097.4899     2949   10.24%                        0             0
Elapsed time = 1.70 sec. (537.23 ticks, tree = 0.02 MB, solutions = 2)
Dentro lazy
camion 0, TRUE-heu 0.000142
camion 1, TRUE-heu 0.000190
Salgo lazy
*     3+    2                         6356.0000     6164.1081             3.02%

GUB cover cuts applied:  8
Cover cuts applied:  2
Flow cuts applied:  6
Mixed integer rounding cuts applied:  11
Zero-half cuts applied:  37
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    1.34 sec. (536.80 ticks)
Parallel b&c, 8 threads:
  Real time             =    2.08 sec. (54.04 ticks)
  Sync time (average)   =    1.50 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.42 sec. (590.85 ticks)
3.421000 

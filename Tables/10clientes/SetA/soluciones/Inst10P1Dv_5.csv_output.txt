Instances\Inst10P1Dv_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000031
camion 1, TRUE-heu 0.000054
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 6751.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 287 rows and 52 columns.
MIP Presolve modified 646 coefficients.
Reduced MIP has 397 rows, 1370 columns, and 9942 nonzeros.
Reduced MIP has 1170 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (7.98 ticks)
Clique table members: 121.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.02 sec. (11.96 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         6751.0000        0.0000           100.00%
      0     0     4192.9707    79     6751.0000     4192.9707      563   37.89%
      0     0     5177.9333    79     6751.0000      Cuts: 20      727   23.30%
      0     0     5266.9780    79     6751.0000      Cuts: 27     1036   21.98%
      0     0     5326.4708    79     6751.0000      Cuts: 58     1101   21.10%
      0     0     5336.7180    79     6751.0000      Cuts: 37     1149   20.95%
      0     0     5456.3692    79     6751.0000      Cuts: 34     1199   19.18%
      0     0     5470.8702    79     6751.0000      Cuts: 29     1300   18.96%
      0     0     5503.9677    79     6751.0000      Cuts: 41     1344   18.47%
      0     0     5683.0991    79     6751.0000      Cuts: 39     1555   15.82%
      0     0     5688.2026    79     6751.0000      Cuts: 60     1649   15.74%
      0     0     5696.7027    79     6751.0000      Cuts: 46     1699   15.62%
      0     0     5721.4762    79     6751.0000      Cuts: 26     1870   15.25%
      0     0     5724.3223    79     6751.0000      Cuts: 50     1966   15.21%
      0     0     5753.6030    79     6751.0000      Cuts: 40     2215   14.77%
      0     0     5805.4096    79     6751.0000      Cuts: 49     2465   14.01%
      0     0     5849.7175    79     6751.0000      Cuts: 58     2543   13.35%
      0     0     5982.8333    79     6751.0000      Cuts: 32     2613   11.38%
      0     0     6082.1463    79     6751.0000      Cuts: 42     2763    9.91%
      0     0     6085.1671    79     6751.0000      Cuts: 48     2853    9.86%
      0     0     6090.3774    79     6751.0000      Cuts: 37     2926    9.79%
      0     0     6091.8462    79     6751.0000      Cuts: 30     2976    9.76%
      0     0     6094.7803    79     6751.0000      Cuts: 41     3100    9.72%
      0     0     6097.4783    79     6751.0000      Cuts: 31     3217    9.68%
      0     0     6100.8846    79     6751.0000      Cuts: 44     3265    9.63%
      0     0     6100.8846    79     6751.0000      Cuts: 19     3317    9.63%
Dentro lazy
camion 0, TRUE-heu 0.000085
camion 1, TRUE-heu 0.000109
Salgo lazy
*     0+    0                         6738.0000     6100.8846             9.46%
      0     0  -1.00000e+75     0     6738.0000     6100.8846     3317    9.46%
Dentro lazy
camion 0, TRUE-heu 0.000133
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 9358 rows and 34 columns.
MIP Presolve modified 1288 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 877 rows, 365 columns, and 26351 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.25 sec. (259.87 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 877 rows, 365 columns, and 26351 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (38.29 ticks)
Clique table members: 1062.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (10.97 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    66                      0.0000      162         
      0     0        0.0000    66                  Cliques: 3      221         
      0     0        0.0000    66                    Cuts: 83      372         
      0     0        0.0000    66                     Cuts: 8      445         
      0     0        cutoff                                        445         
Elapsed time = 1.89 sec. (1218.64 ticks, tree = 0.01 MB, solutions = 0)

Clique cuts applied:  6
Zero-half cuts applied:  5

Root node processing (before b&c):
  Real time             =    1.89 sec. (1218.72 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.89 sec. (1218.72 ticks)
1.938000 
FALSE-modelo
 Salgo lazy
      0     2     6100.8846    55     6738.0000     6100.8846     3317    9.46%                        0             0
Elapsed time = 6.20 sec. (464.41 ticks, tree = 0.02 MB, solutions = 2)
Dentro lazy
camion 0, TRUE-heu 0.000195
camion 1, TRUE-heu 0.000217
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000241
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 9360 rows and 34 columns.
MIP Presolve modified 1590 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 875 rows, 365 columns, and 28968 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.39 sec. (390.72 ticks)
Tried aggregator 1 time.
Detecting symmetries...
MIP Presolve modified 1 coefficients.
Reduced MIP has 875 rows, 365 columns, and 28968 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (47.18 ticks)
Clique table members: 1121.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (8.22 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    65                      0.0000      113         
      0     0        0.0000    65                  Cliques: 2      166         
      0     0        0.0000    65                    Cuts: 73      304         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      336    0.00%
Elapsed time = 1.63 sec. (1006.00 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  11
Zero-half cuts applied:  14
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    1.63 sec. (1006.10 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.63 sec. (1006.10 ticks)
1.625000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000293
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 9360 rows and 34 columns.
MIP Presolve modified 1590 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 875 rows, 365 columns, and 28968 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.39 sec. (390.72 ticks)
Tried aggregator 1 time.
Detecting symmetries...
MIP Presolve modified 1 coefficients.
Reduced MIP has 875 rows, 365 columns, and 28968 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (47.18 ticks)
Clique table members: 1121.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (8.22 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    65                      0.0000      113         
      0     0        0.0000    65                  Cliques: 2      166         
      0     0        0.0000    65                    Cuts: 73      304         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      336    0.00%
Elapsed time = 1.61 sec. (1006.00 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  11
Zero-half cuts applied:  14
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    1.61 sec. (1006.10 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.61 sec. (1006.10 ticks)
1.610000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, *     7+    2                         6684.0000     6190.4000             7.38%
TRUE-heu 0.000348
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 9358 rows and 34 columns.
MIP Presolve modified 1288 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 877 rows, 365 columns, and 26351 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.28 sec. (259.87 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 877 rows, 365 columns, and 26351 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (38.29 ticks)
Clique table members: 1062.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (10.97 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    66                      0.0000      162         
      0     0        0.0000    66                  Cliques: 3      221         
      0     0        0.0000    66                    Cuts: 83      372         
      0     0        0.0000    66                     Cuts: 8      445         
      0     0        cutoff                                        445         
Elapsed time = 2.23 sec. (1218.64 ticks, tree = 0.01 MB, solutions = 0)

Clique cuts applied:  6
Zero-half cuts applied:  5

Root node processing (before b&c):
  Real time             =    2.23 sec. (1218.72 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.23 sec. (1218.72 ticks)
2.266000 
FALSE-modelo
 Salgo lazy
*    10+    4                         6356.0000     6190.4000             2.61%
*    12+    3                         6356.0000     6190.4000             2.61%

GUB cover cuts applied:  3
Cover cuts applied:  8
Flow cuts applied:  5
Mixed integer rounding cuts applied:  5
Zero-half cuts applied:  27
Gomory fractional cuts applied:  1
User cuts applied:  2

Root node processing (before b&c):
  Real time             =    3.91 sec. (463.96 ticks)
Parallel b&c, 8 threads:
  Real time             =    9.31 sec. (100.56 ticks)
  Sync time (average)   =    4.90 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   13.22 sec. (564.52 ticks)
13.218000 

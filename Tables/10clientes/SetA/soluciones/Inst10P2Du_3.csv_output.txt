Instances\Inst10P2Du_3.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000031
camion 1, TRUE-heu 0.000050
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 6973.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 266 rows and 52 columns.
MIP Presolve modified 2054 coefficients.
Reduced MIP has 426 rows, 2330 columns, and 19170 nonzeros.
Reduced MIP has 2130 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (13.97 ticks)
Clique table members: 136.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.08 sec. (45.26 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         6973.0000        0.0000           100.00%
      0     0     4165.4678    94     6973.0000     4165.4678     1200   40.26%
      0     0     5149.9000    94     6973.0000      Cuts: 17     1701   26.15%
      0     0     5157.4004    94     6973.0000      Cuts: 64     2251   26.04%
      0     0     5247.6218    94     6973.0000      Cuts: 63     2541   24.74%
      0     0     5314.0786    94     6973.0000      Cuts: 82     2723   23.79%
      0     0     5659.2000    94     6973.0000      Cuts: 70     3699   18.84%
Dentro lazy
camion 0, TRUE-heu 0.000088
camion 1, TRUE-heu 0.000108
Salgo lazy
*     0+    0                         6904.0000     5659.2000            18.03%
      0     0  -1.00000e+75     0     6904.0000     5659.2000     3699   18.03%
      0     0     5664.7517    94     6904.0000      Cuts: 81     4263   17.95%
      0     0     5668.5000    94     6904.0000      Cuts: 73     4475   17.90%
      0     0     5678.9357    94     6904.0000      Cuts: 19     4825   17.74%
      0     0     5699.4125    94     6904.0000      Cuts: 67     5088   17.45%
      0     0     5704.3533    94     6904.0000      Cuts: 65     5291   17.38%
      0     0     5708.9520    94     6904.0000      Cuts: 42     5439   17.31%
      0     0     5748.7290    94     6904.0000      Cuts: 57     5794   16.73%
      0     0     5825.7095    94     6904.0000      Cuts: 59     5891   15.62%
      0     0     5829.4035    94     6904.0000      Cuts: 41     5996   15.56%
      0     0     5835.1186    94     6904.0000      Cuts: 32     6190   15.48%
      0     0     5838.8438    94     6904.0000      Cuts: 66     6289   15.43%
      0     0     5905.3273    94     6904.0000      Cuts: 46     6474   14.47%
      0     0     5942.5000    94     6904.0000      Cuts: 59     6747   13.93%
      0     0     5952.1667    94     6904.0000      Cuts: 69     6840   13.79%
      0     0     5968.9339    94     6904.0000      Cuts: 45     7021   13.54%
      0     0     6056.3941    94     6904.0000      Cuts: 51     7354   12.28%
      0     0     6057.3915    94     6904.0000      Cuts: 52     7412   12.26%
      0     0     6061.0039    94     6904.0000      Cuts: 20     7483   12.21%
      0     0     6061.1571    94     6904.0000      Cuts: 32     7522   12.21%
Dentro lazy
camion 0, TRUE-heu 0.000170
camion 1, TRUE-heu 0.000192
Salgo lazy
*     0+    0                         6604.0000     6061.1571             8.22%
      0     0  -1.00000e+75     0     6604.0000     6061.1571     7522    8.22%
      0     2     6061.1571   102     6604.0000     6061.1571     7522    8.22%                        0             0
Elapsed time = 5.05 sec. (1067.05 ticks, tree = 0.02 MB, solutions = 3)
Dentro lazy
camion 0, TRUE-heu 0.000225
camion 1, TRUE-heu 0.000253
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000295
camion 1, TRUE-heu 0.000319
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000345
camion 1, TRUE-heu 0.000367
Salgo lazy
*     4+    3                         6506.0000     6138.0200             5.66%
Dentro lazy
camion 0, TRUE-heu 0.000397
camion 1, TRUE-heu 0.000422
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000449
camion 1, TRUE-heu 0.000480
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000513
camion 1, TRUE-heu 0.000548
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000588
camion 1, TRUE-heu 0.000616
Salgo lazy
*    78+    7                         6488.0000     6138.2813             5.39%
*    83+    4                         6475.0000     6138.2813             5.20%
    148    41     6421.9097    37     6475.0000     6138.2813    10997    5.20%         X_0_6_0 D    326    318     11
    634     8     6433.5820    42     6475.0000     6370.0188    24045    1.62%      Z_0_6_1_20 D    688    680     21

GUB cover cuts applied:  21
Cover cuts applied:  61
Flow cuts applied:  29
Mixed integer rounding cuts applied:  9
Zero-half cuts applied:  28
Lift and project cuts applied:  3
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    4.78 sec. (1046.40 ticks)
Parallel b&c, 8 threads:
  Real time             =    3.56 sec. (558.73 ticks)
  Sync time (average)   =    2.64 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    8.34 sec. (1605.13 ticks)
8.360000 

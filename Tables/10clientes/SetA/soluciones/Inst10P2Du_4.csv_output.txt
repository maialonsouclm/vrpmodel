Instances\Inst10P2Du_4.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000117
camion 1, TRUE-heu 0.000194
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 6973.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 266 rows and 52 columns.
MIP Presolve modified 2054 coefficients.
Reduced MIP has 426 rows, 2330 columns, and 19170 nonzeros.
Reduced MIP has 2130 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (13.97 ticks)
Clique table members: 136.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.06 sec. (45.50 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         6973.0000        0.0000           100.00%
      0     0     4165.1922   104     6973.0000     4165.1922     1158   40.27%
      0     0     5155.8506   104     6973.0000      Cuts: 28     2573   26.06%
      0     0     5246.1590   104     6973.0000      Cuts: 58     2649   24.76%
      0     0     5323.9125   104     6973.0000      Cuts: 70     2802   23.65%
      0     0     5659.2000   104     6973.0000      Cuts: 71     3062   18.84%
      0     0     5660.7615   104     6973.0000      Cuts: 58     3801   18.82%
Dentro lazy
camion 0, TRUE-heu 0.000224
camion 1, TRUE-heu 0.000246
Salgo lazy
*     0+    0                         6928.0000     5660.7615            18.29%
      0     0  -1.00000e+75     0     6928.0000     5660.7615     3801   18.29%
      0     0     5665.5000   104     6928.0000      Cuts: 70     4115   18.22%
      0     0     5668.8340   104     6928.0000      Cuts: 46     4388   18.18%
      0     0     5686.3498   104     6928.0000      Cuts: 39     4544   17.92%
      0     0     5699.2583   104     6928.0000      Cuts: 39     4713   17.74%
      0     0     5717.5352   104     6928.0000      Cuts: 50     5210   17.47%
      0     0     5729.2858   104     6928.0000      Cuts: 56     5366   17.30%
      0     0     5747.3157   104     6928.0000      Cuts: 66     5750   17.04%
      0     0     5757.0904   104     6928.0000      Cuts: 50     5888   16.90%
      0     0     5758.3455   104     6928.0000      Cuts: 57     5929   16.88%
      0     0     5783.1361   104     6928.0000      Cuts: 16     5996   16.53%
      0     0     5814.7336   104     6928.0000      Cuts: 24     6067   16.07%
      0     0     5830.6222   104     6928.0000      Cuts: 39     6154   15.84%
      0     0     5890.5538   104     6928.0000      Cuts: 43     6230   14.97%
      0     0     5907.5951   104     6928.0000      Cuts: 51     6302   14.73%
      0     0     6055.4522   104     6928.0000      Cuts: 36     6663   12.59%
      0     0     6060.9542   104     6928.0000      Cuts: 63     6749   12.52%
      0     0     6066.1264   104     6928.0000      Cuts: 24     6791   12.44%
      0     0     6068.5848   104     6928.0000      Cuts: 30     6856   12.40%
      0     0     6073.3751   104     6928.0000      Cuts: 34     6936   12.34%
      0     0     6083.6318   104     6928.0000      Cuts: 46     7029   12.19%
      0     0     6084.0147   104     6928.0000      Cuts: 35     7100   12.18%
      0     0     6084.8213   104     6928.0000      Cuts: 11     7140   12.17%
      0     2     6084.8213    90     6928.0000     6084.8213     7140   12.17%                        0             0
Elapsed time = 2.36 sec. (946.29 ticks, tree = 0.02 MB, solutions = 2)
Dentro lazy
camion 0, TRUE-heu 0.000277
camion 1, TRUE-heu 0.000301
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000330
camion 1, TRUE-heu 0.000352
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000382
camion 1, TRUE-heu 0.000404
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000431
camion 1, TRUE-heu 0.000454
Salgo lazy
*    10+    3                         6506.0000     6211.7723             4.52%
Dentro lazy
camion 0, TRUE-heu 0.000477
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7367 rows and 34 columns.
MIP Presolve modified 4107 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2874 rows, 717 columns, and 63231 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.45 sec. (460.00 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2874 rows, 717 columns, and 63231 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (90.54 ticks)
Clique table members: 2812.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.16 sec. (74.09 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   156                      0.0000      492         
      0     0        0.0000   156                  Cliques: 2      538         
      0     0        0.0000   156                    Cuts: 30      602         
      0     0        0.0000   156                    Cuts: 18      735         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      735    0.00%
Elapsed time = 3.83 sec. (2298.06 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  3
Zero-half cuts applied:  3
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    3.83 sec. (2298.28 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.83 sec. (2298.28 ticks)
3.828000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000543
camion 1, TRUE-heu 0.000569
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000601
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7367 rows and 34 columns.
MIP Presolve modified 4107 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2874 rows, 717 columns, and 63231 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.42 sec. (460.54 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2874 rows, 717 columns, and 63231 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (90.61 ticks)
Clique table members: 2812.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.08 sec. (52.13 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   139                      0.0000      433         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1746    0.00%
Elapsed time = 2.92 sec. (2026.83 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  97

Root node processing (before b&c):
  Real time             =    2.92 sec. (2027.06 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.92 sec. (2027.06 ticks)
2.922000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000656
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7367 rows and 34 columns.
MIP Presolve modified 4107 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2874 rows, 717 columns, and 63231 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.44 sec. (460.54 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2874 rows, 717 columns, and 63231 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (90.61 ticks)
Clique table members: 2812.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.13 sec. (52.13 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   139                      0.0000      433         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1746    0.00%
Elapsed time = 2.80 sec. (2026.83 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  97

Root node processing (before b&c):
  Real time             =    2.80 sec. (2027.06 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.80 sec. (2027.06 ticks)
2.829000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000718
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7367 rows and 34 columns.
MIP Presolve modified 4107 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2874 rows, 717 columns, and 63231 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.45 sec. (460.00 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2874 rows, 717 columns, and 63231 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (90.54 ticks)
Clique table members: 2812.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.13 sec. (74.09 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   156                      0.0000      492         
      0     0        0.0000   156                  Cliques: 2      538         
      0     0        0.0000   156                    Cuts: 30      602         
      0     0        0.0000   156                    Cuts: 18      735         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      735    0.00%
Elapsed time = 3.30 sec. (2298.06 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  3
Zero-half cuts applied:  3
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    3.30 sec. (2298.28 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.30 sec. (2298.28 ticks)
3.297000 
TRUE-modelo
Salgo lazy
Dentro lazy
*    16+    5                         6356.0000     6211.7723             2.27%
camion 0, TRUE-heu 0.000779
camion 1, TRUE-heu 0.000807
Salgo lazy

GUB cover cuts applied:  26
Cover cuts applied:  5
Flow cuts applied:  11
Mixed integer rounding cuts applied:  12
Zero-half cuts applied:  21
Lift and project cuts applied:  1
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    2.01 sec. (927.11 ticks)
Parallel b&c, 8 threads:
  Real time             =   14.45 sec. (225.78 ticks)
  Sync time (average)   =    4.57 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   16.47 sec. (1152.88 ticks)
16.468000 

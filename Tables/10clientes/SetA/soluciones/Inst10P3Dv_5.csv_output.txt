Instances\Inst10P3Dv_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000025
camion 1, TRUE-heu 0.000046
camion 2, TRUE-heu 0.000063
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7982.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 399 rows and 126 columns.
MIP Presolve modified 5992 coefficients.
Reduced MIP has 654 rows, 6327 columns, and 54275 nonzeros.
Reduced MIP has 6027 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (41.17 ticks)
Clique table members: 219.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.13 sec. (145.14 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7982.0000        0.0000           100.00%
      0     0     4107.0737   106     7982.0000     4107.0737     2008   48.55%
      0     0     4878.8195   106     7982.0000      Cuts: 60     2605   38.88%
      0     0     5156.2915   106     7982.0000     Cuts: 106     4342   35.40%
      0     0     5382.2283   106     7982.0000     Cuts: 132     5691   32.57%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 14561 rows and 34 columns.
MIP Presolve modified 4749 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2891 rows, 1069 columns, and 111288 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 1.00 sec. (1408.93 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2891 rows, 1069 columns, and 111288 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.19 sec. (159.63 ticks)
Clique table members: 2826.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.16 sec. (136.09 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   239                      0.0000      780         
      0     0        cutoff                                       1594         
Elapsed time = 4.67 sec. (6121.81 ticks, tree = 0.01 MB, solutions = 0)

Clique cuts applied:  179

Root node processing (before b&c):
  Real time             =    4.67 sec. (6122.17 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.67 sec. (6122.17 ticks)
4.687000 
FALSE-modelo
 Salgo lazy
      0     0     5409.4236   106     7982.0000     Cuts: 110     7173   32.23%
      0     0     5456.6450   106     7982.0000      Cuts: 96     8602   31.64%
      0     0     5513.2834   106     7982.0000      Cuts: 93     9717   30.93%
      0     0     5569.3403   106     7982.0000     Cuts: 132    11109   30.23%
      0     0     5673.6782   106     7982.0000      Cuts: 86    12869   28.92%
      0     0     5738.8041   106     7982.0000      Cuts: 94    14469   28.10%
      0     0     5773.0449   106     7982.0000      Cuts: 98    16052   27.67%
      0     0     5785.5107   106     7982.0000      Cuts: 81    16565   27.52%
      0     0     5837.5098   106     7982.0000      Cuts: 90    18502   26.87%
      0     0     5863.4326   106     7982.0000     Cuts: 117    19396   26.54%
      0     0     5873.1392   106     7982.0000     Cuts: 126    19937   26.42%
      0     0     5878.4374   106     7982.0000      Cuts: 91    20559   26.35%
      0     0     5883.6982   106     7982.0000     Cuts: 108    21239   26.29%
      0     0     5895.0144   106     7982.0000     Cuts: 131    21923   26.15%
      0     0     5899.6077   106     7982.0000     Cuts: 106    22519   26.09%
      0     0     5914.8835   106     7982.0000      Cuts: 95    23220   25.90%
      0     0     5914.8835   106     7982.0000     Cuts: 114    23502   25.90%
      0     0     5915.2451   106     7982.0000      Cuts: 62    23815   25.89%
Dentro lazy
camion 0, TRUE-heu 0.000109
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7729 rows and 34 columns.
MIP Presolve modified 4188 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2522 rows, 1005 columns, and 86352 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.97 sec. (1378.53 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2522 rows, 1005 columns, and 86352 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (123.96 ticks)
Clique table members: 2458.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.14 sec. (96.30 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   186                      0.0000      636         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1776    0.00%
Elapsed time = 3.27 sec. (3420.76 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  134

Root node processing (before b&c):
  Real time             =    3.27 sec. (3421.07 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.27 sec. (3421.07 ticks)
3.266000 
TRUE-modelo
Salgo lazy
*     0+    0                         7680.0000     5915.2451            22.98%
Dentro lazy
camion 0, TRUE-heu 0.000154
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4940 rows and 34 columns.
MIP Presolve modified 7474 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2430 rows, 973 columns, and 74606 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.42 sec. (595.40 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2430 rows, 973 columns, and 74606 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (107.56 ticks)
Clique table members: 2367.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.75 sec. (937.28 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.75 sec. (937.28 ticks)
0.766000 
TRUE-modelo
Salgo lazy
*     0+    0                         7420.0000     5915.2451            20.28%
      0     0  -1.00000e+75     0     7420.0000     5915.2451    23815   20.28%
      0     2     5915.2451   196     7420.0000     5915.2451    23815   20.28%                        0             0
Elapsed time = 15.48 sec. (7846.94 ticks, tree = 0.02 MB, solutions = 3)
      1     3     5973.9587   123     7420.0000     5985.5113    24693   19.33%              Y2 D      8      0      1
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4938 rows and 34 columns.
MIP Presolve modified 4357 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2435 rows, 1069 columns, and 84873 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.50 sec. (618.49 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2435 rows, 1069 columns, and 84873 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (124.00 ticks)
Clique table members: 2370.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.14 sec. (92.12 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   215                      0.0000      663         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      663    0.00%
Elapsed time = 2.84 sec. (2945.96 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    2.84 sec. (2946.25 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.84 sec. (2946.25 ticks)
2.843000 
TRUE-modelo
camion 1, TRUE-heu 0.000221
Salgo lazy
Dentro lazy
      3     5     6165.5800   110     7420.0000     5990.5849    25267   19.26%         X_0_5_0 D     24     16      3
camion 0, TRUE-heu 0.000244
camion 1, TRUE-heu 0.000266
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7788 rows and 34 columns.
MIP Presolve modified 4355 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2465 rows, 1069 columns, and 91342 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.58 sec. (818.93 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2465 rows, 1069 columns, and 91342 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (132.75 ticks)
Clique table members: 2400.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (114.34 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   213                      0.0000      763         
      0     0        0.0000   213                Cliques: 456     2148         
      0     0        0.0000   213                Cliques: 201     3410         
      0     0        0.0000   213                   Cuts: 160     4563         
      0     0        cutoff                                       4563         
Elapsed time = 11.17 sec. (8117.79 ticks, tree = 0.01 MB, solutions = 0)

Clique cuts applied:  162
Zero-half cuts applied:  1

Root node processing (before b&c):
  Real time             =   11.19 sec. (8118.09 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   11.19 sec. (8118.09 ticks)
11.188000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000312
camion 2, TRUE-heu 0.000335
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000364
camion 1, TRUE-heu 0.000389
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5393 rows and 34 columns.
MIP Presolve modified 3750 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1981 rows, 1069 columns, and 80981 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.92 sec. (1256.98 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1981 rows, 1069 columns, and 80981 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (115.06 ticks)
Clique table members: 1917.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (48.95 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   178                      0.0000      366         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1572    0.00%
Elapsed time = 3.67 sec. (4211.45 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  349

Root node processing (before b&c):
  Real time             =    3.67 sec. (4211.77 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.67 sec. (4211.77 ticks)
3.735000 
TRUE-modelo
camion 1, TRUE-heu 0.000433
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000458
camion 1, TRUE-heu 0.000496
Salgo lazy
      7     9     6235.5567   106     7420.0000     5990.5849    25677   19.26%         X_0_5_1 D     56     48      7
*     9+    3                         6944.0000     5990.5849            13.73%
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5393 rows and 34 columns.
MIP Presolve modified 3749 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1981 rows, 1069 columns, and 80950 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.92 sec. (1256.85 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1981 rows, 1069 columns, and 80950 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (115.10 ticks)
Clique table members: 1917.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.08 sec. (56.45 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   178                      0.0000      437         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      437    0.00%
Elapsed time = 2.64 sec. (2869.44 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    2.64 sec. (2869.72 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.64 sec. (2869.72 ticks)
2.657000 
TRUE-modelo
camion 1, TRUE-heu 0.000544
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4938 rows and 34 columns.
MIP Presolve modified 4357 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2435 rows, 1069 columns, and 84873 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.45 sec. (613.58 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2435 rows, 1069 columns, and 84873 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (123.93 ticks)
Clique table members: 2370.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (90.75 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   206                      0.0000      603         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      746    0.00%
Elapsed time = 3.22 sec. (3282.75 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  169

Root node processing (before b&c):
  Real time             =    3.23 sec. (3283.06 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.23 sec. (3283.06 ticks)
3.234000 
TRUE-modelo
camion 1, TRUE-heu 0.000587
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000643
camion 1, TRUE-heu 0.000663
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000686
camion 1, TRUE-heu 0.000706
Salgo lazy
Dentro lazy
camion 0,      17    18     6836.6446    70     6944.0000     5990.5849    26476   13.73%         X_1_7_3 U     61     45      9
TRUE-heu 0.000728
camion 1, TRUE-heu 0.000754
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000776
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5395 rows and 34 columns.
MIP Presolve modified 3730 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1979 rows, 1069 columns, and 78335 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.89 sec. (1229.09 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1979 rows, 1069 columns, and 78335 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (109.89 ticks)
Clique table members: 1917.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (49.97 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   176                      0.0000      391         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      391    0.00%
Elapsed time = 3.48 sec. (3665.78 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    3.48 sec. (3666.05 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.48 sec. (3666.05 ticks)
3.500000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000819
camion 1, TRUE-heu 0.000840
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000870
camion 1, TRUE-heu 0.000892
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000914
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5395 rows and 34 columns.
MIP Presolve modified 3729 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1979 rows, 1069 columns, and 78336 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.83 sec. (1226.70 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1979 rows, 1069 columns, and 78336 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (110.48 ticks)
Clique table members: 1917.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.06 sec. (52.82 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   169                      0.0000      391         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      391    0.00%
Elapsed time = 3.38 sec. (3068.18 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    3.38 sec. (3068.45 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.38 sec. (3068.45 ticks)
3.390000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000958
camion 1, TRUE-heu 0.000983
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.001010
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4940 rows and 34 columns.
MIP Presolve modified 4357 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2433 rows, 1069 columns, and 82825 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.41 sec. (622.98 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2433 rows, 1069 columns, and 82825 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (119.60 ticks)
Clique table members: 2370.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.14 sec. (92.56 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   210                      0.0000      608         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      629    0.00%
Elapsed time = 3.55 sec. (4102.62 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  22

Root node processing (before b&c):
  Real time             =    3.55 sec. (4102.90 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.55 sec. (4102.90 ticks)
3.546000 
TRUE-modelo
Salgo lazy
     32    27     6682.0467    57     6944.0000     5990.5849    27785   13.73%         X_1_7_9 D     60     52     11
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5419 rows and 34 columns.
MIP Presolve modified 3783 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1954 rows, 1037 columns, and 71204 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.45 sec. (622.64 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1954 rows, 1037 columns, and 71204 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (105.00 ticks)
Clique table members: 1890.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.08 sec. (55.73 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   168                      0.0000      406         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      742    0.00%
Elapsed time = 2.36 sec. (2632.83 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  2

Root node processing (before b&c):
  Real time             =    2.36 sec. (2633.07 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.36 sec. (2633.07 ticks)
2.360000 
TRUE-modelo
camion 1, TRUE-heu 0.001093
Salgo lazy
     43    34     6678.3273   104     6944.0000     5990.5849    30379   13.73%         X_0_0_3 D    117    109     11
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7735 rows and 34 columns.
MIP Presolve modified 3799 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2517 rows, 1037 columns, and 87155 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 1.00 sec. (1415.30 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2517 rows, 1037 columns, and 87155 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (124.44 ticks)
Clique table members: 2453.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.16 sec. (82.09 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   176                      0.0000      564         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1111    0.00%
Elapsed time = 3.80 sec. (3736.39 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  188

Root node processing (before b&c):
  Real time             =    3.80 sec. (3736.71 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.80 sec. (3736.71 ticks)
3.828000 
TRUE-modelo
camion 1, TRUE-heu 0.001148
Salgo lazy
*    60+    2                         6842.0000     5990.5849            12.44%
     61    37     6715.9656   120     6842.0000     5990.5849    33155   12.44%         X_1_8_0 D    124    116     11
*    90+    2                         6837.0000     5990.5849            12.38%
     90    46     6690.7186    77     6837.0000     5990.5849    35563   12.38%         X_1_4_5 D    156    140     13
    125    70     6230.1349    58     6837.0000     6019.3772    40840   11.96%         X_0_1_9 D     54     46      7
    364   173     6662.0000    53     6837.0000     6177.0970    62160    9.65%         X_1_2_9 D     58     50     13
Elapsed time = 60.25 sec. (11180.85 ticks, tree = 0.43 MB, solutions = 22)
Dentro lazy
camion 0, TRUE-heu 0.001180
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4940 rows and 34 columns.
MIP Presolve modified 4357 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2433 rows, 1069 columns, and 82825 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.42 sec. (627.05 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2433 rows, 1069 columns, and 82825 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (119.57 ticks)
Clique table members: 2370.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.78 sec. (993.35 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.78 sec. (993.35 ticks)
0.781000 
TRUE-modelo
Salgo lazy
*   604   282      integral     0     6731.0000     6177.0970    81955    8.23%         X_0_0_2 D    864    848     18
Dentro lazy
camion 0, TRUE-heu 0.001243
camion 1, TRUE-heu 0.001263
Salgo lazy
    864   379     6617.5434    54     6731.0000     6306.3000   110725    6.31%         X_0_6_0 D   1001    993     25
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4939 rows and 34 columns.
MIP Presolve modified 4294 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2433 rows, 1037 columns, and 81232 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.44 sec. (631.41 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2433 rows, 1037 columns, and 81232 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (117.99 ticks)
Clique table members: 2369.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (91.96 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   196                      0.0000      626         
      0     0        0.0000   196                 Cliques: 23      848         
      0     0        0.0000   196                   Cuts: 130     1379         
      0     0        0.0000   196                 Cliques: 92     1835         
Detecting symmetries...
      0     2        0.0000    29                      0.0000     1835         
Elapsed time = 4.56 sec. (3360.85 ticks, tree = 0.02 MB, solutions = 0)
*    11     9      integral     0        0.0000        0.0000     4241    0.00%

Clique cuts applied:  67

Root node processing (before b&c):
  Real time             =    2.64 sec. (3343.50 ticks)
Parallel b&c, 12 threads:
  Real time             =    6.00 sec. (179.96 ticks)
  Sync time (average)   =    4.54 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    8.64 sec. (3523.46 ticks)
8.657000 
TRUE-modelo
camion 2, TRUE-heu 0.001312
Salgo lazy
   1218   589     6593.0316    51     6731.0000     6364.7893   143502    5.44%         X_1_2_9 D   1070   1062     18
Dentro lazy
camion 0, TRUE-heu 0.001356
camion 2, TRUE-heu 0.001377
Salgo lazy
*  1276   584      integral     0     6506.0000     6369.1849   145079    2.10%
   1542   212        cutoff           6506.0000     6410.8557   189580    1.46%         X_1_8_9 U   1034   1804     12

GUB cover cuts applied:  31
Cover cuts applied:  361
Flow cuts applied:  19
Mixed integer rounding cuts applied:  30
Zero-half cuts applied:  27
Gomory fractional cuts applied:  4
User cuts applied:  2

Root node processing (before b&c):
  Real time             =   14.94 sec. (7735.08 ticks)
Parallel b&c, 8 threads:
  Real time             =   61.50 sec. (7697.69 ticks)
  Sync time (average)   =   17.30 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   76.44 sec. (15432.77 ticks)
76.453000 

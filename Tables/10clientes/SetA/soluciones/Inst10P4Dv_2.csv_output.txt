Instances\Inst10P4Dv_2.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000026
camion 1, TRUE-heu 0.000065
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 6782.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 272 rows and 52 columns.
MIP Presolve modified 1926 coefficients.
Reduced MIP has 422 rows, 2330 columns, and 19002 nonzeros.
Reduced MIP has 2130 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (13.90 ticks)
Clique table members: 136.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.05 sec. (47.13 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         6782.0000        0.0000           100.00%
      0     0     4080.6838   106     6782.0000     4080.6838     1355   39.83%
      0     0     5177.2000   106     6782.0000      Cuts: 22     1802   23.66%
      0     0     5263.0039   106     6782.0000      Cuts: 53     1980   22.40%
      0     0     5435.0477   106     6782.0000      Cuts: 70     2303   19.86%
      0     0     5680.0000   106     6782.0000      Cuts: 54     2549   16.25%
      0     0     5688.1084   106     6782.0000      Cuts: 70     3099   16.13%
Dentro lazy
camion 0, TRUE-heu 0.000088
camion 1, TRUE-heu 0.000102
Salgo lazy
*     0+    0                         6713.0000     5688.1084            15.27%
      0     0  -1.00000e+75     0     6713.0000     5688.1084     3099   15.27%
      0     0     5724.1018   106     6713.0000      Cuts: 69     3348   14.73%
      0     0     5724.3993   106     6713.0000      Cuts: 54     3397   14.73%
      0     0     5756.3444   106     6713.0000      Cuts: 37     3597   14.25%
      0     0     5822.8737   106     6713.0000      Cuts: 51     3927   13.26%
      0     0     5921.9167   106     6713.0000      Cuts: 48     4075   11.78%
      0     0     5925.1184   106     6713.0000      Cuts: 44     4125   11.74%
      0     0     5975.7734   106     6713.0000      Cuts: 17     4377   10.98%
      0     0     6084.1222   106     6713.0000      Cuts: 82     4604    9.37%
      0     0     6089.5626   106     6713.0000      Cuts: 67     4701    9.29%
      0     0     6095.6957   106     6713.0000      Cuts: 53     4803    9.20%
      0     0     6098.9384   106     6713.0000      Cuts: 43     4888    9.15%
      0     0     6099.8907   106     6713.0000      Cuts: 36     4942    9.13%
      0     2     6099.8907   110     6713.0000     6099.8907     4942    9.13%                        0             0
Elapsed time = 3.19 sec. (868.33 ticks, tree = 0.02 MB, solutions = 2)
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 8123 rows and 34 columns.
MIP Presolve modified 2834 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2119 rows, 685 columns, and 57399 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.67 sec. (914.01 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2119 rows, 685 columns, and 57399 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (81.29 ticks)
Clique table members: 2057.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.08 sec. (69.12 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   156                      0.0000      553         
      0     0        0.0000   156                 Cliques: 26      770         
      0     0        0.0000   156                   Cuts: 109     1029         
Detecting symmetries...
      0     2        0.0000    11                      0.0000     1029         
Elapsed time = 4.41 sec. (2203.73 ticks, tree = 0.02 MB, solutions = 0)
*    27    17      integral     0        0.0000        0.0000     5430    0.00%
     38    10        0.0000   125        0.0000        0.0000     2152    0.00%

Clique cuts applied:  28
Cover cuts applied:  1
Zero-half cuts applied:  3

Root node processing (before b&c):
  Real time             =    2.28 sec. (2202.36 ticks)
Parallel b&c, 12 threads:
  Real time             =    7.14 sec. (344.17 ticks)
  Sync time (average)   =    5.46 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    9.42 sec. (2546.53 ticks)
9.468000 
TRUE-modelo
camion 1, TRUE-heu 0.000151
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000182
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5365 rows and 34 columns.
MIP Presolve modified 3135 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1997 rows, 685 columns, and 48514 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.28 sec. (350.45 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1997 rows, 685 columns, and 48514 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (70.56 ticks)
Clique table members: 1935.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.05 sec. (39.82 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   128                      0.0000      333         
      0     0        0.0000   128                 Cliques: 32      574         
      0     0        0.0000   128                   Cuts: 275     1521         
Detecting symmetries...
      0     2        0.0000    27                      0.0000     1521         
Elapsed time = 5.92 sec. (2763.23 ticks, tree = 0.02 MB, solutions = 0)
   1027    65        0.0000    35                      0.0000    37520         
   1663    79        0.0000    28                      0.0000    75326         
   2720   384    infeasible                            0.0000   115217         
   3857   569        0.0000     6                      0.0000   155333         
   4427   644    infeasible                            0.0000   187807         
   4672   669        0.0000    55                      0.0000   214272         
   4995   717        0.0000    39                      0.0000   242233         
   5315   831        0.0000    79                      0.0000   276397         
   5602   931        0.0000    51                      0.0000   300097         
   7416   811    infeasible                            0.0000   435156         
Elapsed time = 20.17 sec. (5866.93 ticks, tree = 1.01 MB, solutions = 0)

Performing restart 1

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 457 rows and 84 columns.
MIP Presolve modified 1111 coefficients.
Reduced MIP has 1459 rows, 601 columns, and 30474 nonzeros.
Reduced MIP has 601 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (74.11 ticks)
Tried aggregator 1 time.
MIP Presolve modified 23 coefficients.
Reduced MIP has 1457 rows, 601 columns, and 30451 nonzeros.
Reduced MIP has 601 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (39.93 ticks)
Represolve time = 0.13 sec. (138.68 ticks)
   7685     0        0.0000   191                     Cuts: 8   468565         
   7685     0        0.0000   191                   Cuts: 174   468718         
   7685     0        0.0000   191                    Cuts: 28   468974         
   7685     2        0.0000    90                      0.0000   469282         
   9233   269    infeasible                            0.0000   577806         
  10476   345        0.0000    65                      0.0000   733419         
  11559   292    infeasible                            0.0000   870798         
  12749   133        0.0000    82                      0.0000  1010134         
  13448    99        0.0000   184                      0.0000  1123377         
  14043   324    infeasible                            0.0000  1219965         

Performing restart 2

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 358 rows and 65 columns.
MIP Presolve modified 321 coefficients.
Reduced MIP has 1076 rows, 536 columns, and 20260 nonzeros.
Reduced MIP has 536 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (37.96 ticks)
Tried aggregator 1 time.
Reduced MIP has 1076 rows, 536 columns, and 20260 nonzeros.
Reduced MIP has 536 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (22.75 ticks)
Represolve time = 0.13 sec. (98.19 ticks)
  14338     0        0.0000   163                  Cliques: 5  1301618         
  14338     0        0.0000   163                    Cuts: 67  1301768         
  14338     2        0.0000    17                      0.0000  1301768         
  15289   273        0.0000    86                      0.0000  1424873         
  16175   381    infeasible                            0.0000  1553223         
Elapsed time = 66.51 sec. (18414.18 ticks, tree = 0.33 MB, solutions = 0)
  16894   262        0.0000   127                      0.0000  1702677         
  17507    36    infeasible                            0.0000  1831329         
  18003    44        0.0000    85                      0.0000  1956867         
  18461    18    infeasible                            0.0000  2053714         

Clique cuts applied:  302
Cover cuts applied:  3
Mixed integer rounding cuts applied:  2
Zero-half cuts applied:  5
Gomory fractional cuts applied:  3

Root node processing (before b&c):
  Real time             =    4.16 sec. (2748.12 ticks)
Parallel b&c, 12 threads:
  Real time             =   80.55 sec. (19534.25 ticks)
  Sync time (average)   =   44.04 sec.
  Wait time (average)   =    0.11 sec.
                          ------------
Total (root+branch&cut) =   84.70 sec. (22282.37 ticks)
84.750000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 8171 rows and 34 columns.
MIP Presolve modified 2803 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2071 rows, 685 columns, and 59078 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.66 sec. (907.53 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2071 rows, 685 columns, and 59078 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (84.06 ticks)
Clique table members: 2009.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.05 sec. (45.13 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   117                      0.0000      455         
      0     0        0.0000   117                 Cliques: 91     1080         
      0     0        0.0000   117                   Cuts: 104     1502         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1502    0.00%
Elapsed time = 5.13 sec. (4142.38 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  19

Root node processing (before b&c):
  Real time             =    5.13 sec. (4142.58 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    5.13 sec. (4142.58 ticks)
5.172000 
TRUE-modelo
camion 1, TRUE-heu 0.000255
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 8171 rows and 34 columns.
MIP Presolve modified 2803 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2071 rows, 685 columns, and 59078 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.63 sec. (907.53 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2071 rows, 685 columns, and 59078 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (84.06 ticks)
Clique table members: 2009.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.05 sec. (45.13 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   117                      0.0000      455         
      0     0        0.0000   117                 Cliques: 91     1080         
      0     0        0.0000   117                   Cuts: 104     1502         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1502    0.00%
Elapsed time = 4.72 sec. (4142.38 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  19

Root node processing (before b&c):
  Real time             =    4.72 sec. (4142.58 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.72 sec. (4142.58 ticks)
4.719000 
TRUE-modelo
camion 1, TRUE-heu 0.000295
Salgo lazy
*    44+    4                         6410.0000     6207.8333             3.15%

GUB cover cuts applied:  21
Cover cuts applied:  20
Flow cuts applied:  15
Mixed integer rounding cuts applied:  11
Zero-half cuts applied:  36
Lift and project cuts applied:  1
User cuts applied:  1

Root node processing (before b&c):
  Real time             =    2.98 sec. (854.56 ticks)
Parallel b&c, 8 threads:
  Real time             =  106.20 sec. (233.12 ticks)
  Sync time (average)   =   52.89 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  109.19 sec. (1087.68 ticks)
109.187000 

Instances\Inst10P1Du_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000030
camion 1, TRUE-heu 0.000052
camion 2, TRUE-heu 0.000079
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7982.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 402 rows and 126 columns.
MIP Presolve modified 2152 coefficients.
Reduced MIP has 611 rows, 2487 columns, and 19589 nonzeros.
Reduced MIP has 2187 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (17.08 ticks)
Clique table members: 179.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.02 sec. (31.43 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7982.0000        0.0000           100.00%
      0     0     4416.8014    87     7982.0000     4416.8014      677   44.67%
      0     0     4875.9844    87     7982.0000      Cuts: 70      898   38.91%
      0     0     4941.3052    87     7982.0000      Cuts: 48     1042   38.09%
      0     0     5099.0553    87     7982.0000      Cuts: 64     1143   36.12%
      0     0     5208.5730    87     7982.0000      Cuts: 55     1298   34.75%
      0     0     5268.1559    87     7982.0000      Cuts: 34     1439   34.00%
      0     0     5288.5924    87     7982.0000      Cuts: 51     1517   33.74%
      0     0     5303.9893    87     7982.0000      Cuts: 56     1597   33.55%
      0     0     5335.6954    87     7982.0000      Cuts: 51     1733   33.15%
      0     0     5430.1055    87     7982.0000      Cuts: 31     1853   31.97%
      0     0     5441.6918    87     7982.0000      Cuts: 69     1987   31.83%
Dentro lazy
camion 0, TRUE-heu 0.000113
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5785 rows and 34 columns.
MIP Presolve modified 2066 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1568 rows, 365 columns, and 31334 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.41 sec. (422.62 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1568 rows, 365 columns, and 31334 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (45.27 ticks)
Clique table members: 1753.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (11.56 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    55                      0.0000      148         
Detecting symmetries...
      0     2        0.0000    32                      0.0000      148         
Elapsed time = 2.66 sec. (802.35 ticks, tree = 0.02 MB, solutions = 0)
    469     1    infeasible                            0.0000    24559         
    549     3    infeasible                            0.0000    30425         

Clique cuts applied:  13
Cover cuts applied:  6
Zero-half cuts applied:  2

Root node processing (before b&c):
  Real time             =    1.12 sec. (801.54 ticks)
Parallel b&c, 12 threads:
  Real time             =   45.61 sec. (501.59 ticks)
  Sync time (average)   =   43.12 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   46.73 sec. (1303.12 ticks)
46.734000 
FALSE-modelo
 Salgo lazy
      0     0     5485.4684    87     7982.0000      Cuts: 57     2221   31.28%
      0     0     5494.1596    87     7982.0000      Cuts: 57     2315   31.17%
      0     0     5494.7661    87     7982.0000      Cuts: 38     2355   31.16%
      0     0     5505.8448    87     7982.0000      Cuts: 18     2550   31.02%
      0     0     5515.2736    87     7982.0000     Cuts: 108     2657   30.90%
      0     0     5523.1435    87     7982.0000      Cuts: 31     2736   30.81%
      0     0     5561.0558    87     7982.0000      Cuts: 45     2877   30.33%
      0     0     5580.6359    87     7982.0000      Cuts: 84     3039   30.08%
      0     0     5595.0409    87     7982.0000      Cuts: 73     3443   29.90%
      0     0     5616.1120    87     7982.0000     Cuts: 120     3625   29.64%
      0     0     5622.3915    87     7982.0000      Cuts: 86     3728   29.56%
      0     0     5624.0877    87     7982.0000      Cuts: 57     3816   29.54%
      0     0     5639.4164    87     7982.0000      Cuts: 56     3953   29.35%
      0     0     5650.5331    87     7982.0000      Cuts: 64     4128   29.21%
      0     0     5662.6605    87     7982.0000      Cuts: 45     4228   29.06%
      0     0     5664.1760    87     7982.0000      Cuts: 65     4307   29.04%
      0     0     5671.1039    87     7982.0000      Cuts: 32     4379   28.95%
      0     0     5671.4891    87     7982.0000      Cuts: 35     4447   28.95%
Dentro lazy
camion 0, TRUE-heu 0.000174
camion 1, TRUE-heu 0.000199
Salgo lazy
*     0+    0                         6798.0000     5671.4891            16.57%
      0     0  -1.00000e+75     0     6798.0000     5671.4891     4447   16.57%
      0     2     5671.4891   125     6798.0000     5671.4891     4447   16.57%                        0             0
Elapsed time = 49.34 sec. (1225.01 ticks, tree = 0.02 MB, solutions = 2)
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (87.94 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.04 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (12.92 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    63                      0.0000      179         
      0     0        0.0000    63                 Cliques: 10      260         
      0     0        0.0000    63                    Cuts: 96      367         
      0     0        0.0000    63                  Cliques: 6      479         
Detecting symmetries...
      0     2        0.0000     8                      0.0000      479         
Elapsed time = 3.20 sec. (669.13 ticks, tree = 0.02 MB, solutions = 0)
    438     4        0.0000    53                      0.0000    26354         
    592     9    infeasible                            0.0000    38818         
    702     7    infeasible                            0.0000    48987         
    759     2    infeasible                            0.0000    53592         

Clique cuts applied:  45
Cover cuts applied:  1
Zero-half cuts applied:  15

Root node processing (before b&c):
  Real time             =    1.36 sec. (668.36 ticks)
Parallel b&c, 12 threads:
  Real time             =   60.92 sec. (1013.18 ticks)
  Sync time (average)   =   57.38 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   62.28 sec. (1681.53 ticks)
62.281000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (87.94 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.04 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.05 sec. (12.92 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    63                      0.0000      179         
      0     0        0.0000    63                 Cliques: 10      260         
      0     0        0.0000    63                    Cuts: 96      367         
      0     0        0.0000    63                  Cliques: 6      479         
Detecting symmetries...
      0     2        0.0000     8                      0.0000      479         
Elapsed time = 2.88 sec. (669.13 ticks, tree = 0.02 MB, solutions = 0)
    438     4        0.0000    53                      0.0000    26354         
    592     9    infeasible                            0.0000    38818         
    702     7    infeasible                            0.0000    48987         
    759     2    infeasible                            0.0000    53592         

Clique cuts applied:  45
Cover cuts applied:  1
Zero-half cuts applied:  15

Root node processing (before b&c):
  Real time             =    1.17 sec. (668.36 ticks)
Parallel b&c, 12 threads:
  Real time             =   65.16 sec. (1013.18 ticks)
  Sync time (average)   =   61.76 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   66.33 sec. (1681.53 ticks)
66.328000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (87.91 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.03 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (9.90 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    56                      0.0000      140         
      0     0        0.0000    56                    Cuts: 29      290         
      0     0        0.0000    56                    Cuts: 37      540         
Detecting symmetries...
      0     2        0.0000    27                      0.0000      540         
Elapsed time = 3.91 sec. (846.28 ticks, tree = 0.02 MB, solutions = 0)
      8     3        0.0000    76                      0.0000     2204         
    278     8        0.0000    26                      0.0000    19030         
    545     5        0.0000    42                      0.0000    34451         
    628     5    infeasible                            0.0000    41087         

Clique cuts applied:  65
Zero-half cuts applied:  8

Root node processing (before b&c):
  Real time             =    1.53 sec. (845.46 ticks)
Parallel b&c, 12 threads:
  Real time             =   84.69 sec. (997.12 ticks)
  Sync time (average)   =   81.03 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   86.22 sec. (1842.58 ticks)
86.219000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000337
camion 1, TRUE-heu 0.000361
Salgo lazy
     15    12     5928.2475    70     6798.0000     5726.9747     6052   15.76%         X_1_0_8 D     15      7      2
*    20+    7                         6573.0000     5726.9747            12.87%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (87.94 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.04 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.05 sec. (12.92 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    63                      0.0000      179         
      0     0        0.0000    63                 Cliques: 10      260         
      0     0        0.0000    63                    Cuts: 96      367         
      0     0        0.0000    63                  Cliques: 6      479         
Detecting symmetries...
      0     2        0.0000     8                      0.0000      479         
Elapsed time = 3.27 sec. (669.13 ticks, tree = 0.02 MB, solutions = 0)
    438     4        0.0000    53                      0.0000    26354         
    592     9    infeasible                            0.0000    38818         
    702     7    infeasible                            0.0000    48987         
    759     2    infeasible                            0.0000    53592         

Clique cuts applied:  45
Cover cuts applied:  1
Zero-half cuts applied:  15

Root node processing (before b&c):
  Real time             =    1.33 sec. (668.36 ticks)
Parallel b&c, 12 threads:
  Real time             =   63.28 sec. (1013.18 ticks)
  Sync time (average)   =   59.88 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   64.61 sec. (1681.53 ticks)
64.625000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000431
camion 2, TRUE-heu 0.000459
Salgo lazy
*    72+   12                         6548.0000     5730.0581            12.49%
Dentro lazy
camion 0, TRUE-heu 0.000495
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (87.78 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.05 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (11.93 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    57                      0.0000      170         
      0     0        0.0000    57                    Cuts: 12      260         
      0     0        0.0000    57                    Cuts: 39      340         
      0     0        0.0000    57                    Cuts: 90      573         
Detecting symmetries...
      0     2        0.0000    40                      0.0000      573         
Elapsed time = 3.25 sec. (820.15 ticks, tree = 0.02 MB, solutions = 0)
      6     3        0.0000    84                      0.0000     1879         
    413    21    infeasible                            0.0000    24738         
    656     2        0.0000    50                      0.0000    41368         
    711     5        0.0000    44                      0.0000    45633         
    818     6        0.0000    42                      0.0000    52340         

Clique cuts applied:  68
Zero-half cuts applied:  12

Root node processing (before b&c):
  Real time             =    1.36 sec. (819.30 ticks)
Parallel b&c, 12 threads:
  Real time             =   73.13 sec. (1266.65 ticks)
  Sync time (average)   =   69.39 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   74.48 sec. (2085.95 ticks)
74.500000 
FALSE-modelo
 Salgo lazy
    355   113     6526.2000    16     6548.0000     5752.2458    17246   12.15%         X_0_3_1 U    181    173     16
Dentro lazy
camion 0, TRUE-heu 0.000563
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (87.78 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.05 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (11.93 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    57                      0.0000      170         
      0     0        0.0000    57                    Cuts: 12      260         
      0     0        0.0000    57                    Cuts: 39      340         
      0     0        0.0000    57                    Cuts: 90      573         
Detecting symmetries...
      0     2        0.0000    40                      0.0000      573         
Elapsed time = 3.89 sec. (820.15 ticks, tree = 0.02 MB, solutions = 0)
      6     3        0.0000    84                      0.0000     1879         
    413    21    infeasible                            0.0000    24738         
    656     2        0.0000    50                      0.0000    41368         
    711     5        0.0000    44                      0.0000    45633         
    818     6        0.0000    42                      0.0000    52340         

Clique cuts applied:  68
Zero-half cuts applied:  12

Root node processing (before b&c):
  Real time             =    1.48 sec. (819.30 ticks)
Parallel b&c, 12 threads:
  Real time             =   72.66 sec. (1266.65 ticks)
  Sync time (average)   =   68.78 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   74.14 sec. (2085.95 ticks)
74.141000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000619
camion 2, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (87.78 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.05 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (11.93 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    57                      0.0000      170         
      0     0        0.0000    57                    Cuts: 12      260         
      0     0        0.0000    57                    Cuts: 39      340         
      0     0        0.0000    57                    Cuts: 90      573         
Detecting symmetries...
      0     2        0.0000    40                      0.0000      573         
Elapsed time = 3.47 sec. (820.15 ticks, tree = 0.02 MB, solutions = 0)
      6     3        0.0000    84                      0.0000     1879         
    413    21    infeasible                            0.0000    24738         
    656     2        0.0000    50                      0.0000    41368         
    711     5        0.0000    44                      0.0000    45633         
    818     6        0.0000    42                      0.0000    52340         

Clique cuts applied:  68
Zero-half cuts applied:  12

Root node processing (before b&c):
  Real time             =    1.23 sec. (819.30 ticks)
Parallel b&c, 12 threads:
  Real time             =   72.34 sec. (1266.65 ticks)
  Sync time (average)   =   68.30 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   73.58 sec. (2085.95 ticks)
73.578000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000688
camion 2, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (87.78 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.05 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (11.93 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    57                      0.0000      170         
      0     0        0.0000    57                    Cuts: 12      260         
      0     0        0.0000    57                    Cuts: 39      340         
      0     0        0.0000    57                    Cuts: 90      573         
Detecting symmetries...
      0     2        0.0000    40                      0.0000      573         
Elapsed time = 2.86 sec. (820.15 ticks, tree = 0.02 MB, solutions = 0)
      6     3        0.0000    84                      0.0000     1879         
    413    21    infeasible                            0.0000    24738         
    656     2        0.0000    50                      0.0000    41368         
    711     5        0.0000    44                      0.0000    45633         
    818     6        0.0000    42                      0.0000    52340         

Clique cuts applied:  68
Zero-half cuts applied:  12

Root node processing (before b&c):
  Real time             =    1.27 sec. (819.30 ticks)
Parallel b&c, 12 threads:
  Real time             =   72.55 sec. (1266.65 ticks)
  Sync time (average)   =   69.14 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   73.81 sec. (2085.95 ticks)
73.813000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000746
camion 2, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (87.78 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.05 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (11.93 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    57                      0.0000      170         
      0     0        0.0000    57                    Cuts: 12      260         
      0     0        0.0000    57                    Cuts: 39      340         
      0     0        0.0000    57                    Cuts: 90      573         
Detecting symmetries...
      0     2        0.0000    40                      0.0000      573         
Elapsed time = 3.86 sec. (820.15 ticks, tree = 0.02 MB, solutions = 0)
      6     3        0.0000    84                      0.0000     1879         
    413    21    infeasible                            0.0000    24738         
    656     2        0.0000    50                      0.0000    41368         
    711     5        0.0000    44                      0.0000    45633         
    818     6        0.0000    42                      0.0000    52340         

Clique cuts applied:  68
Zero-half cuts applied:  12

Root node processing (before b&c):
  Real time             =    1.81 sec. (819.30 ticks)
Parallel b&c, 12 threads:
  Real time             =   72.16 sec. (1266.65 ticks)
  Sync time (average)   =   68.21 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   73.97 sec. (2085.95 ticks)
73.969000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000814
camion 1, TRUE-heu 0.000837
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (87.84 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.04 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (9.94 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    55                      0.0000      137         
      0     0        0.0000    55                    Cuts: 17      191         
      0     0        0.0000    55                    Cuts: 71      309         
      0     0        0.0000    55                    Cuts: 23      397         
Detecting symmetries...
      0     2        0.0000    46                      0.0000      397         
Elapsed time = 3.38 sec. (720.58 ticks, tree = 0.02 MB, solutions = 0)
      6     3        0.0000    85                      0.0000     1791         
    353     8    infeasible                            0.0000    20970         
    452     4    infeasible                            0.0000    34266         
    523     3    infeasible                            0.0000    41135         
    584     3        0.0000    42                      0.0000    46106         

Clique cuts applied:  57
Zero-half cuts applied:  30

Root node processing (before b&c):
  Real time             =    1.31 sec. (719.74 ticks)
Parallel b&c, 12 threads:
  Real time             =   86.59 sec. (1359.91 ticks)
  Sync time (average)   =   82.79 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   87.91 sec. (2079.65 ticks)
87.906000 
FALSE-modelo
 Salgo lazy
    890   293     6536.0571    60     6548.0000     5854.3796    36137   10.59%         X_0_1_4 D    903    895     15
Dentro lazy
camion 0, TRUE-heu 0.000904
camion 1, TRUE-heu 0.000931
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000965
camion 1, TRUE-heu 0.000991
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.001021
camion 1, TRUE-heu 0.001049
Salgo lazy
Dentro lazy
camion 0, *   916+  272                         6475.0000     5862.6335             9.46%
FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 2497 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (87.84 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1957 rows, 365 columns, and 21105 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (31.04 ticks)
Clique table members: 1895.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.05 sec. (9.94 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    55                      0.0000      137         
      0     0        0.0000    55                    Cuts: 17      191         
      0     0        0.0000    55                    Cuts: 71      309         
      0     0        0.0000    55                    Cuts: 23      397         
Detecting symmetries...
      0     2        0.0000    46                      0.0000      397         
Elapsed time = 4.14 sec. (720.58 ticks, tree = 0.02 MB, solutions = 0)
      6     3        0.0000    85                      0.0000     1791         
    353     8    infeasible                            0.0000    20970         
    452     4    infeasible                            0.0000    34266         
    523     3    infeasible                            0.0000    41135         
    584     3        0.0000    42                      0.0000    46106         

Clique cuts applied:  57
Zero-half cuts applied:  30

Root node processing (before b&c):
  Real time             =    1.81 sec. (719.74 ticks)
Parallel b&c, 12 threads:
  Real time             =   86.06 sec. (1359.91 ticks)
  Sync time (average)   =   82.22 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   87.88 sec. (2079.65 ticks)
87.875000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.001150
camion 1, TRUE-heu 0.001178
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.001213
camion 1, TRUE-heu 0.001241
Salgo lazy
   1452    34     6465.9254    49     6475.0000     6426.9882    57055    0.74%      Z_2_2_1_19 D   1232   1224     19

GUB cover cuts applied:  9
Cover cuts applied:  147
Flow cuts applied:  21
Mixed integer rounding cuts applied:  18
Zero-half cuts applied:  38
Gomory fractional cuts applied:  1
User cuts applied:  12

Root node processing (before b&c):
  Real time             =   48.92 sec. (1208.28 ticks)
Parallel b&c, 8 threads:
  Real time             =  829.83 sec. (998.42 ticks)
  Sync time (average)   =  266.29 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  878.75 sec. (2206.69 ticks)
878.766000 

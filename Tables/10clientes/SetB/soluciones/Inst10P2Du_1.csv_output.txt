Instances\Inst10P2Du_1.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000023
camion 1, TRUE-heu 0.000046
camion 2, TRUE-heu 0.000110
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 8085.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 399 rows and 126 columns.
MIP Presolve modified 4072 coefficients.
Reduced MIP has 634 rows, 4407 columns, and 36995 nonzeros.
Reduced MIP has 4107 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (28.22 ticks)
Clique table members: 199.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.14 sec. (124.07 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         8085.0000        0.0000           100.00%
      0     0     4191.7092    94     8085.0000     4191.7092     1450   48.15%
      0     0     4707.5507    94     8085.0000      Cuts: 77     1692   41.77%
      0     0     4891.0553    94     8085.0000     Cuts: 129     1976   39.50%
      0     0     5066.5406    94     8085.0000      Cuts: 89     2313   37.33%
      0     0     5385.5276    94     8085.0000      Cuts: 69     3063   33.39%
      0     0     5502.6039    94     8085.0000      Cuts: 60     3233   31.94%
      0     0     5528.7528    94     8085.0000      Cuts: 85     3324   31.62%
      0     0     5536.3140    94     8085.0000      Cuts: 42     3456   31.52%
      0     0     5631.7309    94     8085.0000      Cuts: 33     4125   30.34%
      0     0     5650.1592    94     8085.0000     Cuts: 103     4706   30.12%
      0     0     5667.5575    94     8085.0000      Cuts: 69     5081   29.90%
      0     0     5781.8302    94     8085.0000      Cuts: 86     5581   28.49%
      0     0     5854.1309    94     8085.0000      Cuts: 98     6522   27.59%
      0     0     5898.2862    94     8085.0000     Cuts: 141     6744   27.05%
      0     0     5954.5125    94     8085.0000      Cuts: 93     7143   26.35%
      0     0     6065.1685    94     8085.0000      Cuts: 65     7588   24.98%
      0     0     6076.0905    94     8085.0000      Cuts: 87     7858   24.85%
      0     0     6081.3354    94     8085.0000      Cuts: 43     7990   24.78%
      0     0     6092.3694    94     8085.0000      Cuts: 86     8377   24.65%
      0     0     6105.6829    94     8085.0000      Cuts: 59     8628   24.48%
      0     0     6118.9694    94     8085.0000      Cuts: 81     8897   24.32%
      0     0     6119.0631    94     8085.0000      Cuts: 77     9021   24.32%
      0     0     6119.0636    94     8085.0000      Cuts: 27     9097   24.32%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5394 rows and 34 columns.
MIP Presolve modified 3106 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1968 rows, 685 columns, and 48990 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.38 sec. (409.38 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1968 rows, 685 columns, and 48990 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (70.89 ticks)
Clique table members: 1906.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.06 sec. (50.10 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   124                      0.0000      435         
      0     0        0.0000   124                Cliques: 130     1338         
      0     0        0.0000   124                  Cliques: 7     1358         
      0     0        0.0000   124                    Cuts: 28     1436         
Detecting symmetries...
      0     2        0.0000     8                      0.0000     1436         
Elapsed time = 6.00 sec. (2443.98 ticks, tree = 0.02 MB, solutions = 0)
    497   164        0.0000    16                      0.0000    29782         
   1289   232    infeasible                            0.0000    63808         
   1903   359        0.0000    37                      0.0000   106900         
   2246   410        0.0000    24                      0.0000   135552         
   2623   473        0.0000    56                      0.0000   171039         
   2942   421    infeasible                            0.0000   206590         
   3279   383        0.0000    39                      0.0000   237538         
   3689   403        0.0000    58                      0.0000   283529         
   3968   589        0.0000    69                      0.0000   314651         
   4914   503    infeasible                            0.0000   434681         
Elapsed time = 19.91 sec. (5550.08 ticks, tree = 0.21 MB, solutions = 0)
   5750   265    infeasible                            0.0000   575487         
   6540   360        0.0000   100                      0.0000   687034         
   7525   486    infeasible                            0.0000   798884         

Performing restart 1

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 36 rows and 28 columns.
MIP Presolve modified 1743 coefficients.
Reduced MIP has 1818 rows, 657 columns, and 42351 nonzeros.
Reduced MIP has 657 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (103.72 ticks)
Tried aggregator 1 time.
MIP Presolve eliminated 2 rows and 0 columns.
MIP Presolve modified 13 coefficients.
Reduced MIP has 1815 rows, 657 columns, and 42264 nonzeros.
Reduced MIP has 657 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (65.10 ticks)
Represolve time = 0.28 sec. (203.09 ticks)
   7543     0        0.0000   164                  Cliques: 1   833704         
   7543     0        0.0000   164                   Cuts: 136   833883         
   7543     0        0.0000   164                     Cuts: 8   834075         
   7543     2        0.0000    27                      0.0000   834075         
   8013   142        0.0000    54                      0.0000   904921         
   8965   331        0.0000   126                      0.0000  1019909         
   9608   383    infeasible                            0.0000  1124949         
  10197   413    infeasible                            0.0000  1238330         
  10823   227    infeasible                            0.0000  1384061         
  11442   195        0.0000   116                      0.0000  1497629         
Elapsed time = 54.19 sec. (16239.82 ticks, tree = 0.08 MB, solutions = 0)
  12104    95        0.0000    67                      0.0000  1600841         
  12640    42        0.0000    82                      0.0000  1722203         
  12887    11    infeasible                            0.0000  1797505         
  12919     0    infeasible                                    1806620         

Clique cuts applied:  188
Cover cuts applied:  2
Mixed integer rounding cuts applied:  4
Zero-half cuts applied:  10
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    4.22 sec. (2432.22 ticks)
Parallel b&c, 12 threads:
  Real time             =   69.63 sec. (17668.71 ticks)
  Sync time (average)   =   33.21 sec.
  Wait time (average)   =    0.10 sec.
                          ------------
Total (root+branch&cut) =   73.84 sec. (20100.93 ticks)
73.859000 
FALSE-modelo
 Salgo lazy
      0     2     6119.0636   167     8085.0000     6119.0636     9097   24.32%                        0             0
Elapsed time = 79.19 sec. (2783.34 ticks, tree = 0.02 MB, solutions = 1)
      2     2        cutoff           8085.0000     6178.4335    10702   23.58%         X_0_7_0 U     16      8      2
      3     3     6218.2496   166     8085.0000     6178.4335     9347   23.58%         X_0_0_7 N      7      0      1
      5     4     6315.4250   148     8085.0000     6178.4335    11529   23.58%         X_0_7_0 N     24      7      2
      9     8     6417.8405   116     8085.0000     6178.4335    13845   23.58%         X_0_9_3 U     39     31      5
     19    15     6278.1127   145     8085.0000     6249.1271    17800   22.71%         X_0_3_7 D     11      3      5
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5367 rows and 34 columns.
MIP Presolve modified 3197 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1996 rows, 717 columns, and 54096 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.48 sec. (522.11 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1996 rows, 717 columns, and 54096 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (77.98 ticks)
Clique table members: 1934.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.06 sec. (52.45 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   139                      0.0000      428         
      0     0        0.0000   139                Cliques: 103     1322         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1817    0.00%
Elapsed time = 5.38 sec. (3633.23 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  122

Root node processing (before b&c):
  Real time             =    5.38 sec. (3633.43 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    5.38 sec. (3633.43 ticks)
5.391000 
TRUE-modelo
camion 1, TRUE-heu 0.000218
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4941 rows and 34 columns.
MIP Presolve modified 3654 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2421 rows, 717 columns, and 53718 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.30 sec. (343.63 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2421 rows, 717 columns, and 53718 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (78.06 ticks)
Clique table members: 2359.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (46.56 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   133                      0.0000      324         
      0     0        0.0000   133                 Cliques: 97      607         
      0     0        0.0000   133                   Cuts: 236     1759         
      0     0        0.0000   133                 Cliques: 31     2584         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     2584    0.00%
Elapsed time = 8.19 sec. (4881.81 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  71

Root node processing (before b&c):
  Real time             =    8.19 sec. (4882.00 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    8.19 sec. (4882.00 ticks)
8.187000 
TRUE-modelo
camion 1, TRUE-heu 0.000277
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5237 rows and 34 columns.
MIP Presolve modified 2882 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2126 rows, 717 columns, and 56214 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.69 sec. (821.62 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2126 rows, 717 columns, and 56214 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (80.47 ticks)
Clique table members: 2064.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (47.63 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   129                      0.0000      408         
      0     0        0.0000   129                 Cliques: 28      519         
      0     0        0.0000   129                   Cuts: 114      892         
      0     0        0.0000   129                    Cuts: 53     1051         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1051    0.00%
Elapsed time = 4.02 sec. (3699.97 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  40
Zero-half cuts applied:  3
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    4.03 sec. (3700.17 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.03 sec. (3700.17 ticks)
4.047000 
TRUE-modelo
camion 2, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2995 rows and 34 columns.
MIP Presolve modified 3073 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1966 rows, 653 columns, and 40005 nonzeros.
Reduced MIP has 653 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.20 sec. (207.53 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1966 rows, 653 columns, and 40005 nonzeros.
Reduced MIP has 653 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (57.02 ticks)
Clique table members: 1904.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.05 sec. (30.33 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   108                      0.0000      307         
      0     0        0.0000   108                Cliques: 103     1127         
      0     0        0.0000   108                    Cuts: 25     1397         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     2265    0.00%
Elapsed time = 3.69 sec. (2181.38 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  180
Zero-half cuts applied:  51

Root node processing (before b&c):
  Real time             =    3.70 sec. (2181.58 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.70 sec. (2181.58 ticks)
3.704000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4941 rows and 34 columns.
MIP Presolve modified 3654 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2421 rows, 717 columns, and 53718 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.30 sec. (341.99 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2421 rows, 717 columns, and 53718 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (78.13 ticks)
Clique table members: 2359.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.13 sec. (56.22 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   146                      0.0000      444         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1348    0.00%
Elapsed time = 2.25 sec. (1682.83 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  145

Root node processing (before b&c):
  Real time             =    2.25 sec. (1683.03 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.25 sec. (1683.03 ticks)
2.250000 
TRUE-modelo
camion 1, TRUE-heu 0.000412
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4941 rows and 34 columns.
MIP Presolve modified 3590 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2420 rows, 685 columns, and 51165 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.31 sec. (316.46 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2420 rows, 685 columns, and 51165 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (74.43 ticks)
Clique table members: 2358.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.06 sec. (51.03 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   136                      0.0000      372         
Detecting symmetries...
      0     2        0.0000    11                      0.0000      372         
Elapsed time = 3.56 sec. (1567.82 ticks, tree = 0.02 MB, solutions = 0)
    516   132        0.0000    29                      0.0000    26347         
   1160   173    infeasible                            0.0000    54829         
   1823   216        0.0000    19                      0.0000    82657         
   2524   279        0.0000    24                      0.0000   118873         
   3120   436        0.0000    28                      0.0000   158300         
   3506   376    infeasible                            0.0000   194268         
   4050   345        0.0000    27                      0.0000   222003         
   4656   355        0.0000    39                      0.0000   253745         
   5313   370    infeasible                            0.0000   289081         
   7019   519        0.0000    31                      0.0000   418761         
Elapsed time = 21.55 sec. (4672.65 ticks, tree = 0.47 MB, solutions = 0)

Performing restart 1

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 2 rows and 0 columns.
MIP Presolve modified 211 coefficients.
Reduced MIP has 2418 rows, 685 columns, and 51105 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (83.60 ticks)
Represolve time = 0.13 sec. (95.71 ticks)
   7690     0        0.0000   183                    Cuts: 30   500565         
   7690     2        0.0000    10                      0.0000   500565         
   8936   334    infeasible                            0.0000   588990         
  10355   348        0.0000    70                      0.0000   694785         
  12125   126        0.0000    90                      0.0000   822665         
  13353    98    infeasible                            0.0000   942862         
  14570   120        0.0000    64                      0.0000  1040154         
  15345    44        0.0000    74                      0.0000  1126324         

Clique cuts applied:  73
Cover cuts applied:  4
Mixed integer rounding cuts applied:  2

Root node processing (before b&c):
  Real time             =    1.95 sec. (1566.64 ticks)
Parallel b&c, 12 threads:
  Real time             =   65.25 sec. (12399.13 ticks)
  Sync time (average)   =   36.97 sec.
  Wait time (average)   =    0.03 sec.
                          ------------
Total (root+branch&cut) =   67.20 sec. (13965.77 ticks)
67.203000 
FALSE-modelo
 Salgo lazy
*    45+    7                         6596.0000     6249.1271             5.26%
     45    26     6343.0707   129     6596.0000     6249.1271    21475    5.26%         X_1_2_7 D     44     36      8
     89    58     6458.7778    41     6596.0000     6249.1271    27389    5.26%         X_2_4_2 D    246    238     29
    132    69        cutoff           6596.0000     6249.1271    32492    5.26%         X_0_4_9 U     73     49     10
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5365 rows and 34 columns.
MIP Presolve modified 3135 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1997 rows, 685 columns, and 48484 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.33 sec. (345.60 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1997 rows, 685 columns, and 48484 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (70.47 ticks)
Clique table members: 1935.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.08 sec. (51.94 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   129                      0.0000      452         
      0     0        0.0000   129                Cliques: 188     1695         
      0     0        0.0000   129                 Cliques: 28     1871         
      0     0        0.0000   129                   Cuts: 131     2696         
Detecting symmetries...
      0     2        0.0000    40                      0.0000     2696         
Elapsed time = 9.08 sec. (3760.35 ticks, tree = 0.02 MB, solutions = 0)
     15     7        0.0000    82                      0.0000     5505         
     64     6        0.0000   125                      0.0000     9396         
    344    76        0.0000    25                      0.0000    34433         
   1086   235        0.0000    14                      0.0000    58911         
   1921   399        0.0000    32                      0.0000    88753         
   2309   430    infeasible                            0.0000   113666         
   2716   461    infeasible                            0.0000   143436         
   3059   530        0.0000    14                      0.0000   167345         
   3338   524        0.0000    99                      0.0000   190464         
   4475   596    infeasible                            0.0000   304905         
Elapsed time = 40.78 sec. (6885.45 ticks, tree = 3.53 MB, solutions = 0)
   4995   598        0.0000    14                      0.0000   389428         
   6208   428    infeasible                            0.0000   501965         
   7800   459        0.0000    84                      0.0000   596298         

Performing restart 1

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 446 rows and 84 columns.
MIP Presolve modified 948 coefficients.
Reduced MIP has 1484 rows, 601 columns, and 31088 nonzeros.
Reduced MIP has 601 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (74.76 ticks)
Tried aggregator 1 time.
MIP Presolve modified 16 coefficients.
Reduced MIP has 1483 rows, 601 columns, and 31072 nonzeros.
Reduced MIP has 601 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (40.68 ticks)
Represolve time = 0.20 sec. (147.35 ticks)
   7878     0        0.0000   174                    Cuts: 52   641060         
   7878     2        0.0000    20                      0.0000   641060         
   9609   483    infeasible                            0.0000   751442         
  11018   679        0.0000    17                      0.0000   888016         
  12891   780    infeasible                            0.0000  1031015         
  14584   621        0.0000    51                      0.0000  1178544         
  16229   497        0.0000    33                      0.0000  1300632         
  17413   424    infeasible                            0.0000  1448118         
Elapsed time = 81.80 sec. (17146.29 ticks, tree = 0.18 MB, solutions = 0)

Performing restart 2

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 338 rows and 53 columns.
MIP Presolve modified 23 coefficients.
Reduced MIP has 1145 rows, 548 columns, and 22239 nonzeros.
Reduced MIP has 548 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (39.27 ticks)
Tried aggregator 1 time.
Reduced MIP has 1145 rows, 548 columns, and 22239 nonzeros.
Reduced MIP has 548 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (25.34 ticks)
Represolve time = 0.17 sec. (109.73 ticks)
  17918     0        0.0000   156                    Cuts: 15  1547390         
  17918     0        0.0000   156                   Cuts: 149  1547645         
  17918     2        0.0000    24                      0.0000  1547645         
  19368   324        0.0000    75                      0.0000  1680236         
  20715   520    infeasible                            0.0000  1837069         
  21967   652    infeasible                            0.0000  1991350         
  23221   551        0.0000    91                      0.0000  2142729         
  24525   427    infeasible                            0.0000  2286567         
  25835   134        0.0000   113                      0.0000  2430928         
  26476    17        0.0000    72                      0.0000  2544092         
  26779    16        0.0000    50                      0.0000  2609269         
  26912    11    infeasible                            0.0000  2652387         
Elapsed time = 135.33 sec. (28535.53 ticks, tree = 0.04 MB, solutions = 0)
  27010     9        0.0000   149                      0.0000  2695300         

Clique cuts applied:  282
Cover cuts applied:  3
Flow cuts applied:  1
Mixed integer rounding cuts applied:  7
Zero-half cuts applied:  11
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    6.64 sec. (3737.47 ticks)
Parallel b&c, 12 threads:
  Real time             =  144.88 sec. (25849.28 ticks)
  Sync time (average)   =   87.36 sec.
  Wait time (average)   =    0.25 sec.
                          ------------
Total (root+branch&cut) =  151.52 sec. (29586.76 ticks)
151.516000 
FALSE-modelo
 Salgo lazy
    222   115     6397.1318    90     6596.0000     6277.9469    38595    4.82%        X_2_10_6 D    487    479     50
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5378 rows and 34 columns.
MIP Presolve modified 2966 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1985 rows, 717 columns, and 55836 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.77 sec. (830.02 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1985 rows, 717 columns, and 55836 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (78.65 ticks)
Clique table members: 1923.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (56.65 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   147                      0.0000      454         
      0     0        0.0000   147                Cliques: 133      915         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1082    0.00%
Elapsed time = 4.19 sec. (2638.20 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  61

Root node processing (before b&c):
  Real time             =    4.19 sec. (2638.40 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.19 sec. (2638.40 ticks)
4.203000 
TRUE-modelo
camion 2, TRUE-heu 0.000534
Salgo lazy
*   594   280      integral     0     6506.0000     6296.2092    65595    3.22%
    676   279        cutoff           6506.0000     6307.3333    82093    3.05%         X_2_8_6 D    627    619     30
Elapsed time = 331.23 sec. (6063.88 ticks, tree = 0.83 MB, solutions = 6)
   1149   325     6460.6630    64     6506.0000     6369.7760   125070    2.09%         X_0_2_9 D   1146   1138     12

GUB cover cuts applied:  69
Cover cuts applied:  235
Flow cuts applied:  49
Mixed integer rounding cuts applied:  23
Zero-half cuts applied:  49
Gomory fractional cuts applied:  2
User cuts applied:  3

Root node processing (before b&c):
  Real time             =   78.83 sec. (2727.15 ticks)
Parallel b&c, 8 threads:
  Real time             =  256.20 sec. (4948.34 ticks)
  Sync time (average)   =   80.05 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  335.03 sec. (7675.48 ticks)
335.047000 

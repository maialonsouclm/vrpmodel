Instances\Inst10P3Du_2.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000029
camion 1, TRUE-heu 0.000070
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 6926.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 266 rows and 52 columns.
MIP Presolve modified 3974 coefficients.
Reduced MIP has 456 rows, 4250 columns, and 36450 nonzeros.
Reduced MIP has 4050 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (25.95 ticks)
Clique table members: 166.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.13 sec. (125.34 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         6926.0000        0.0000           100.00%
      0     0     4138.6350   109     6926.0000     4138.6350     1708   40.24%
      0     0     5150.7452   109     6926.0000      Cuts: 32     2929   25.63%
      0     0     5248.8529   109     6926.0000      Cuts: 80     3954   24.22%
      0     0     5432.4469   109     6926.0000      Cuts: 79     4631   21.56%
      0     0     5570.9687   109     6926.0000      Cuts: 77     5086   19.56%
      0     0     5683.6701   109     6926.0000      Cuts: 66     5925   17.94%
      0     0     5734.2221   109     6926.0000      Cuts: 66     6857   17.21%
      0     0     5783.7007   109     6926.0000      Cuts: 57     7178   16.49%
      0     0     5842.2202   109     6926.0000      Cuts: 59     7389   15.65%
      0     0     6072.0551   109     6926.0000      Cuts: 48     7692   12.33%
      0     0     6073.9684   109     6926.0000      Cuts: 38     7737   12.30%
      0     0     6075.6897   109     6926.0000      Cuts: 29     7826   12.28%
      0     0     6091.5156   109     6926.0000      Cuts: 26     7967   12.05%
      0     0     6093.6465   109     6926.0000      Cuts: 35     8060   12.02%
      0     0     6096.3911   109     6926.0000      Cuts: 25     8158   11.98%
      0     0     6097.3392   109     6926.0000      Cuts: 16     8241   11.96%
      0     0     6097.3732   109     6926.0000      Cuts: 27     8322   11.96%
      0     0     6097.3732   109     6926.0000       Cuts: 7     8333   11.96%
      0     0     6097.3732   109     6926.0000      Cuts: 31     8401   11.96%
Dentro lazy
camion 0, TRUE-heu 0.000104
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4940 rows and 34 columns.
MIP Presolve modified 4357 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2433 rows, 1069 columns, and 82825 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.58 sec. (648.44 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2433 rows, 1069 columns, and 82825 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (119.74 ticks)
Clique table members: 2370.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.17 sec. (97.52 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   201                      0.0000      680         
      0     0        0.0000   201                Cliques: 132     1330         
      0     0        0.0000   201                   Cuts: 303     2845         
      0     0        0.0000   201                 Cliques: 53     3690         
Detecting symmetries...
      0     2        0.0000   107                      0.0000     3690         
Elapsed time = 15.84 sec. (9606.00 ticks, tree = 0.02 MB, solutions = 0)
      2     3        0.0000   110                      0.0000     3863         
     12     8        0.0000   119                      0.0000     6615         
     70     6    infeasible                            0.0000    18477         
    103     5        0.0000   146                      0.0000    26614         
    164     7    infeasible                            0.0000    33841         
    198     7        0.0000   170                      0.0000    47456         
    212     8        0.0000   146                      0.0000    56449         
    223     9        0.0000   131                      0.0000    61046         
    248     8        0.0000   114                      0.0000    65295         
    453     9    infeasible                            0.0000   107567         
Elapsed time = 54.69 sec. (13072.54 ticks, tree = 0.06 MB, solutions = 0)
    549     3    infeasible                            0.0000   132337         
    584     2        0.0000   210                      0.0000   140257         
    628     1    infeasible                            0.0000   147827         

Clique cuts applied:  295

Root node processing (before b&c):
  Real time             =   13.55 sec. (9567.87 ticks)
Parallel b&c, 12 threads:
  Real time             =   68.53 sec. (6506.92 ticks)
  Sync time (average)   =   61.98 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   82.08 sec. (16074.79 ticks)
82.078000 
FALSE-modelo
 Salgo lazy
      0     2     6097.3732   172     6926.0000     6097.3732     8401   11.96%                        0             0
Elapsed time = 86.88 sec. (2294.77 ticks, tree = 0.02 MB, solutions = 1)
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4939 rows and 34 columns.
MIP Presolve modified 4166 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2431 rows, 973 columns, and 75998 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.52 sec. (572.23 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2431 rows, 973 columns, and 75998 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (110.66 ticks)
Clique table members: 2367.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    1.00 sec. (900.87 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.00 sec. (900.87 ticks)
1.000000 
TRUE-modelo
camion 1, TRUE-heu 0.000190
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000219
camion 1, TRUE-heu 0.000243
Salgo lazy
*     4+    3                         6913.0000     6196.8889            10.36%
      4     5     6350.4333    92     6913.0000     6196.8889     9638   10.36%         X_0_7_0 D      5     16      3
*     8+    2                         6587.0000     6196.8889             5.92%
     17    11     6419.8333    61     6587.0000     6350.4333    13666    3.59%         X_1_4_9 U     38     30      6
Dentro lazy
camion 0, TRUE-heu 0.000276
camion 1, TRUE-heu 0.000314
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000347
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4940 rows and 34 columns.
MIP Presolve modified 4357 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2433 rows, 1069 columns, and 82825 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.55 sec. (636.30 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2433 rows, 1069 columns, and 82825 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (119.51 ticks)
Clique table members: 2370.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.16 sec. (96.91 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   213                      0.0000      643         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1074    0.00%
Elapsed time = 4.39 sec. (4136.23 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  11

Root node processing (before b&c):
  Real time             =    4.39 sec. (4136.51 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.39 sec. (4136.51 ticks)
4.407000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2993 rows and 34 columns.
MIP Presolve modified 3777 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1979 rows, 1005 columns, and 65025 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.41 sec. (419.14 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1979 rows, 1005 columns, and 65025 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (93.43 ticks)
Clique table members: 1915.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (58.80 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   174                      0.0000      450         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1327    0.00%
Elapsed time = 2.70 sec. (1929.57 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  301

Root node processing (before b&c):
  Real time             =    2.72 sec. (1929.84 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.72 sec. (1929.84 ticks)
2.734000 
TRUE-modelo
camion 1, TRUE-heu 0.000437
Salgo lazy
     48    20     6507.3333    52     6587.0000     6355.6812    18924    3.51%         X_1_4_9 U     61     53      8
*    52+    7                         6545.0000     6355.6812             2.89%
Dentro lazy
camion 0, TRUE-heu 0.000466
camion 1, TRUE-heu 0.000491
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2993 rows and 34 columns.
MIP Presolve modified 3777 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1979 rows, 1005 columns, and 65025 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.38 sec. (419.78 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1979 rows, 1005 columns, and 65025 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (93.54 ticks)
Clique table members: 1915.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.14 sec. (64.72 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   184                      0.0000      476         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      489    0.00%
Elapsed time = 1.95 sec. (1422.91 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  10

Root node processing (before b&c):
  Real time             =    1.95 sec. (1423.14 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.95 sec. (1423.14 ticks)
1.953000 
TRUE-modelo
camion 1, TRUE-heu 0.000542
Salgo lazy
*    59    16      integral     0     6475.0000     6355.6812    16502    1.84%
    112     8     6455.6711    85     6475.0000     6355.6812    28729    1.84%         X_0_8_6 D     34     26      6

GUB cover cuts applied:  48
Cover cuts applied:  17
Flow cuts applied:  19
Mixed integer rounding cuts applied:  10
Zero-half cuts applied:  34
Gomory fractional cuts applied:  1
User cuts applied:  1

Root node processing (before b&c):
  Real time             =   86.52 sec. (2253.96 ticks)
Parallel b&c, 8 threads:
  Real time             =   13.75 sec. (1206.74 ticks)
  Sync time (average)   =    6.26 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  100.27 sec. (3460.70 ticks)
100.281000 

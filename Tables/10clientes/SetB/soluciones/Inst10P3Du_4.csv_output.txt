Instances\Inst10P3Du_4.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000026
camion 1, TRUE-heu 0.000051
camion 2, TRUE-heu 0.000074
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 8085.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 399 rows and 126 columns.
MIP Presolve modified 5992 coefficients.
Reduced MIP has 654 rows, 6327 columns, and 54275 nonzeros.
Reduced MIP has 6027 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (41.10 ticks)
Clique table members: 219.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.17 sec. (163.10 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         8085.0000        0.0000           100.00%
      0     0     4138.6350   144     8085.0000     4138.6350       15   48.81%
      0     0     4881.4653   144     8085.0000      Cuts: 59     1557   39.62%
      0     0     5025.8460   144     8085.0000      Cuts: 87     2128   37.84%
      0     0     5212.7354   144     8085.0000      Cuts: 75     4985   35.53%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4939 rows and 34 columns.
MIP Presolve modified 4475 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2434 rows, 1069 columns, and 83849 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.67 sec. (682.06 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2434 rows, 1069 columns, and 83849 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (121.82 ticks)
Clique table members: 2370.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (91.31 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   209                      0.0000      631         
      0     0        0.0000   209                Cliques: 595     2111         
      0     0        0.0000   209                 Cliques: 28     2931         
      0     0        0.0000   209                   Cuts: 134     3994         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     3994    0.00%
Elapsed time = 8.44 sec. (6299.31 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  131
Zero-half cuts applied:  1

Root node processing (before b&c):
  Real time             =    8.45 sec. (6299.61 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    8.45 sec. (6299.61 ticks)
8.469000 
TRUE-modelo
camion 1, TRUE-heu 0.000169
Salgo lazy
*     0+    0                         7349.0000     5212.7354            29.07%
      0     0  -1.00000e+75     0     7349.0000     5212.7354     4985   29.07%
      0     0     5507.8393   144     7349.0000     Cuts: 106     6011   25.05%
      0     0     5541.0244   144     7349.0000      Cuts: 54     6264   24.60%
      0     0     5589.7470   144     7349.0000      Cuts: 61     6654   23.94%
      0     0     5632.4574   144     7349.0000     Cuts: 100     8311   23.36%
      0     0     5641.6698   144     7349.0000     Cuts: 109     9975   23.23%
      0     0     5654.3175   144     7349.0000     Cuts: 121    10851   23.06%
      0     0     5684.6163   144     7349.0000     Cuts: 117    11561   22.65%
      0     0     5699.2133   144     7349.0000     Cuts: 122    12600   22.45%
      0     0     5704.6219   144     7349.0000      Cuts: 78    12835   22.38%
      0     0     5714.8858   144     7349.0000      Cuts: 47    13059   22.24%
      0     0     5720.3037   144     7349.0000      Cuts: 67    13391   22.16%
      0     0     5725.9618   144     7349.0000     Cuts: 102    13736   22.09%
      0     0     5872.7029   144     7349.0000      Cuts: 91    14408   20.09%
      0     0     5904.8458   144     7349.0000     Cuts: 110    14962   19.65%
      0     0     5951.7963   144     7349.0000     Cuts: 111    16227   19.01%
      0     0     5960.6814   144     7349.0000     Cuts: 140    17111   18.89%
      0     0     5967.7398   144     7349.0000     Cuts: 106    17641   18.80%
      0     0     5987.4341   144     7349.0000     Cuts: 103    18682   18.53%
      0     0     5992.3829   144     7349.0000      Cuts: 87    19100   18.46%
      0     0     6003.5071   144     7349.0000     Cuts: 118    19580   18.31%
      0     0     6006.0786   144     7349.0000      Cuts: 77    20176   18.27%
      0     0     6006.5513   144     7349.0000     Cuts: 103    20559   18.27%
      0     0     6009.7157   144     7349.0000      Cuts: 60    21413   18.22%
      0     0     6012.7521   144     7349.0000      Cuts: 85    22024   18.18%
      0     0     6013.4718   144     7349.0000      Cuts: 79    22373   18.17%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2993 rows and 34 columns.
MIP Presolve modified 3713 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1978 rows, 973 columns, and 62861 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.36 sec. (398.88 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1978 rows, 973 columns, and 62861 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (90.54 ticks)
Clique table members: 1914.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.08 sec. (62.38 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   183                      0.0000      483         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      483    0.00%
Elapsed time = 2.20 sec. (1943.47 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    2.20 sec. (1943.69 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.20 sec. (1943.69 ticks)
2.218000 
TRUE-modelo
camion 2, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5421 rows and 34 columns.
MIP Presolve modified 3783 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1952 rows, 1037 columns, and 68584 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.59 sec. (617.80 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1952 rows, 1037 columns, and 68584 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (99.44 ticks)
Clique table members: 1890.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (55.82 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   188                      0.0000      437         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      437    0.00%
Elapsed time = 2.38 sec. (2038.27 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    2.38 sec. (2038.50 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.38 sec. (2038.50 ticks)
2.390000 
TRUE-modelo
Salgo lazy
*     0+    0                         7018.0000     6013.4718            14.31%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4938 rows and 34 columns.
MIP Presolve modified 4366 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2435 rows, 1069 columns, and 84873 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.67 sec. (678.14 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2435 rows, 1069 columns, and 84873 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.17 sec. (124.12 ticks)
Clique table members: 2370.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.19 sec. (81.53 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   208                      0.0000      560         
      0     0        0.0000   208                Cliques: 485     2468         
      0     0        0.0000   208                Cliques: 158     3952         
      0     0        cutoff                                       5789         
Elapsed time = 11.08 sec. (9541.63 ticks, tree = 0.01 MB, solutions = 0)

Clique cuts applied:  349

Root node processing (before b&c):
  Real time             =   11.08 sec. (9541.94 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   11.08 sec. (9541.94 ticks)
11.078000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4939 rows and 34 columns.
MIP Presolve modified 4451 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2434 rows, 1069 columns, and 83849 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.66 sec. (688.39 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2434 rows, 1069 columns, and 83849 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (121.84 ticks)
Clique table members: 2370.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.16 sec. (99.83 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   211                      0.0000      666         
      0     0        0.0000   211                Cliques: 225     1405         
      0     0        0.0000   211                 Cliques: 27     2149         
      0     0        0.0000   211                   Cuts: 117     2828         
Detecting symmetries...
      0     2        0.0000    42                      0.0000     2828         
Elapsed time = 11.41 sec. (6446.34 ticks, tree = 0.02 MB, solutions = 0)
      5     4        0.0000   135                      0.0000     3746         
    132    75        0.0000    44                      0.0000    18026         
    579   250        0.0000    51                      0.0000    43609         
   1180   432        0.0000    59                      0.0000    66507         
   1857   573        0.0000    49                      0.0000    92691         
   2432   767        0.0000    63                      0.0000   121014         
   3038   918    infeasible                            0.0000   150421         
   3635  1088    infeasible                            0.0000   178487         
   4262  1125        0.0000    49                      0.0000   209213         

Performing restart 1

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 117 rows and 46 columns.
MIP Presolve modified 2662 coefficients.
Reduced MIP has 2171 rows, 1023 columns, and 69206 nonzeros.
Reduced MIP has 1023 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (147.76 ticks)
Tried aggregator 1 time.
MIP Presolve modified 68 coefficients.
Reduced MIP has 2170 rows, 1023 columns, and 69186 nonzeros.
Reduced MIP has 1023 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (102.26 ticks)
Represolve time = 0.45 sec. (306.24 ticks)
   5968     0        0.0000   277                 Cliques: 29   305679         
   5968     0        0.0000   277                   Cuts: 130   306617         
   5968     0        0.0000   277                 Cliques: 29   307501         
   5968     2        0.0000    31                      0.0000   307501         
Elapsed time = 32.17 sec. (12736.89 ticks, tree = 0.02 MB, solutions = 0)
   7608   593    infeasible                            0.0000   346053         
   9242  1157        0.0000    57                      0.0000   440624         
  10821  1508        0.0000    75                      0.0000   539333         
  12247  1810    infeasible                            0.0000   637572         
  14303  1997    infeasible                            0.0000   730566         
  16095  2313        0.0000    34                      0.0000   817286         
  17976  2427    infeasible                            0.0000   925955         
  19471  2239    infeasible                            0.0000  1008134         
  20928  2031    infeasible                            0.0000  1115880         
  22720  1789    infeasible                            0.0000  1221474         
Elapsed time = 72.58 sec. (22279.34 ticks, tree = 17.31 MB, solutions = 0)
  24346  1516    infeasible                            0.0000  1306067         
  25640  1319        0.0000   140                      0.0000  1421118         
  26488  1401    infeasible                            0.0000  1509923         
  27481  1302    infeasible                            0.0000  1611128         
  29088  1124    infeasible                            0.0000  1691629         
  30922   678        0.0000    67                      0.0000  1792771         
  32520   460        0.0000    50                      0.0000  1900675         
  33956   232    infeasible                            0.0000  1983538         
  35364    41        0.0000    56                      0.0000  2113145         

Clique cuts applied:  284
Cover cuts applied:  16
Mixed integer rounding cuts applied:  10
Zero-half cuts applied:  8
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    9.09 sec. (6418.77 ticks)
Parallel b&c, 12 threads:
  Real time             =   94.28 sec. (24946.13 ticks)
  Sync time (average)   =   37.93 sec.
  Wait time (average)   =    0.25 sec.
                          ------------
Total (root+branch&cut) =  103.38 sec. (31364.90 ticks)
103.375000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000327
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2993 rows and 34 columns.
MIP Presolve modified 3873 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1979 rows, 1005 columns, and 65025 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.39 sec. (418.64 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1979 rows, 1005 columns, and 65025 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (93.23 ticks)
Clique table members: 1915.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (62.29 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   183                      0.0000      455         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1068    0.00%
Elapsed time = 2.25 sec. (2113.22 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  173

Root node processing (before b&c):
  Real time             =    2.25 sec. (2113.47 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.25 sec. (2113.47 ticks)
2.266000 
TRUE-modelo
Salgo lazy
*     0+    0                         6793.0000     6013.4718            11.48%
      0     0  -1.00000e+75     0     6793.0000     6013.4718    22373   11.48%
      0     2     6013.4718   163     6793.0000     6013.4718    22373   11.48%                        0             0
Elapsed time = 152.00 sec. (10562.42 ticks, tree = 0.02 MB, solutions = 4)
      1     3     6216.6673    97     6793.0000     6013.4990    23257   11.48%         X_2_7_0 U      7      0      1
      4     5     6128.1630   176     6793.0000     6060.8551    25640   10.78%         X_1_0_7 D     16      8      2
     28    18     6599.8075    49     6793.0000     6128.1630    31051    9.79%         X_0_5_2 D     17      9      7
     83    47        cutoff           6793.0000     6128.1630    35863    9.79%         X_2_6_7 U    100     92     14
    133    70     6550.1419    53     6793.0000     6128.1630    38209    9.79%         X_2_6_7 D    209    201     14
    165    82     6753.5556    52     6793.0000     6128.1630    42571    9.79%         X_0_0_7 D     77     61      9
    211    89     6784.0000     2     6793.0000     6130.0121    55384    9.76%      Z_0_10_4_5 D    478    470     10
Dentro lazy
camion 0, TRUE-heu 0.000430
camion 1, TRUE-heu 0.000460
Salgo lazy
*   273+   85                         6784.0000     6130.0121             9.64%
    283    75        cutoff           6784.0000     6130.0121    60911    9.64%         X_0_5_0 U    322     76     12
    342    83     6356.4735   140     6784.0000     6130.0121    59210    9.64%        X_0_10_4 D    184    176     20
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2993 rows and 34 columns.
MIP Presolve modified 3777 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1979 rows, 1005 columns, and 65025 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.39 sec. (419.68 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1979 rows, 1005 columns, and 65025 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (93.45 ticks)
Clique table members: 1915.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.77 sec. (676.12 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.77 sec. (676.12 ticks)
0.844000 
TRUE-modelo
camion 2, TRUE-heu 0.000520
Salgo lazy
*   420    63      integral     0     6475.0000     6130.0121    80319    5.33%      Z_2_9_5_17 D    319    311     17
Elapsed time = 157.44 sec. (13374.04 ticks, tree = 0.25 MB, solutions = 6)
    542    13     6426.0000    55     6475.0000     6392.5844   105591    1.27%         X_1_5_0 N    448     83     17

GUB cover cuts applied:  22
Cover cuts applied:  154
Flow cuts applied:  20
Mixed integer rounding cuts applied:  18
Zero-half cuts applied:  26
Gomory fractional cuts applied:  3
User cuts applied:  2

Root node processing (before b&c):
  Real time             =  151.75 sec. (10449.94 ticks)
Parallel b&c, 8 threads:
  Real time             =    8.09 sec. (4160.65 ticks)
  Sync time (average)   =    3.51 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  159.84 sec. (14610.59 ticks)
159.844000 

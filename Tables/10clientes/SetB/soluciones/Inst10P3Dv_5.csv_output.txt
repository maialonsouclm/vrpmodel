Instances\Inst10P3Dv_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000027
camion 1, TRUE-heu 0.000049
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 6926.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 267 rows and 52 columns.
MIP Presolve modified 3974 coefficients.
Reduced MIP has 455 rows, 4250 columns, and 36408 nonzeros.
Reduced MIP has 4050 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (27.91 ticks)
Clique table members: 166.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.08 sec. (77.42 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         6926.0000        0.0000           100.00%
      0     0     4407.9054   124     6926.0000     4407.9054     1111   36.36%
      0     0     4840.3292   124     6926.0000      Cuts: 68     1482   30.11%
      0     0     4870.9331   124     6926.0000      Cuts: 71     1804   29.67%
      0     0     5128.9726   124     6926.0000      Cuts: 84     2627   25.95%
      0     0     5191.4007   124     6926.0000      Cuts: 76     3420   25.04%
      0     0     5200.1665   124     6926.0000      Cuts: 97     3703   24.92%
      0     0     5210.0052   124     6926.0000      Cuts: 72     4280   24.78%
      0     0     5215.3604   124     6926.0000      Cuts: 69     4562   24.70%
      0     0     5308.7014   124     6926.0000      Cuts: 64     4940   23.35%
      0     0     5333.6611   124     6926.0000      Cuts: 75     5259   22.99%
      0     0     5361.3444   124     6926.0000      Cuts: 99     5533   22.59%
      0     0     5379.9086   124     6926.0000      Cuts: 62     5756   22.32%
      0     0     5380.6903   124     6926.0000      Cuts: 84     5995   22.31%
      0     0     5380.6903   124     6926.0000      Cuts: 54     6231   22.31%
      0     0     5399.8679   124     6926.0000      Cuts: 61     6642   22.03%
      0     0     5399.8679   124     6926.0000      Cuts: 86     6898   22.03%
Dentro lazy
camion 0, TRUE-heu 0.000078
camion 1, TRUE-heu 0.000112
Salgo lazy
*     0+    0                         6913.0000     5399.8679            21.89%
      0     0  -1.00000e+75     0     6913.0000     5399.8679     6898   21.89%
      0     2     5399.8679   144     6913.0000     5399.8679     6898   21.89%                        0             0
Elapsed time = 4.91 sec. (2212.57 ticks, tree = 0.02 MB, solutions = 2)
      3     5     5483.8159   119     6913.0000     5451.0795     7542   21.15%         X_0_7_1 D     24     16      3
     17    14     6235.3937   136     6913.0000     5451.0795     8982   21.15%         X_1_4_9 D     23     15      3
Dentro lazy
camion 0, TRUE-heu 0.000143
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2993 rows and 34 columns.
MIP Presolve modified 4609 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1980 rows, 1037 columns, and 67189 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.41 sec. (452.70 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1980 rows, 1037 columns, and 67189 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (98.88 ticks)
Clique table members: 1916.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (68.26 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   179                      0.0000      495         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      495    0.00%
Elapsed time = 2.56 sec. (1958.45 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    2.56 sec. (1958.70 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.56 sec. (1958.70 ticks)
2.594000 
TRUE-modelo
Salgo lazy
     45    35     6842.9783    47     6913.0000     5451.0795    10877   21.15%              Y1 U    119    111     13
*    62+    5                         6874.0000     5542.2525            19.37%
     80    55     6002.6870   107     6874.0000     5542.2525    15567   19.37%              Y1 U    159    151      7
    150    94     6399.0882    74     6874.0000     5542.2525    25853   19.37%         X_0_1_4 D    222    206     22
    230   134     6462.7281    89     6874.0000     5542.2525    32748   19.37%         X_1_0_3 D    137    121     20
Dentro lazy
camion 0, TRUE-heu 0.000206
camion 1, TRUE-heu 0.000276
Salgo lazy
    329   212     6650.2604    99     6874.0000     5542.2525    45265   19.37%         X_1_4_2 D    276    268     32
*   370+  203                         6738.0000     5571.1512            17.32%
    417   255     6604.4524    50     6738.0000     5571.1512    52572   17.32%         X_1_3_7 D    402    394      8
    521   321     6552.5251    99     6738.0000     5571.1512    64490   17.32%         X_1_4_9 D    529    521     12
Dentro lazy
camion 0, TRUE-heu 0.000346
camion 1, TRUE-heu 0.000375
Salgo lazy
*   859   432      integral     0     6657.0000     5748.7675    90160   13.64%
    940   482     6637.1597    51     6657.0000     5923.5876   110274   11.02%         X_1_3_7 U    773    765     17
Elapsed time = 13.69 sec. (5403.15 ticks, tree = 1.17 MB, solutions = 5)
Dentro lazy
camion 0, TRUE-heu 0.000406
camion 1, TRUE-heu 0.000437
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000468
camion 1, TRUE-heu 0.000497
Salgo lazy
*  1405   546      integral     0     6612.0000     6245.6988   179676    5.54%
   1426   563     6522.2923    44     6612.0000     6245.6988   177444    5.54%         X_1_0_7 D   1505   1497     19
Dentro lazy
camion 0, TRUE-heu 0.000532
camion 1, TRUE-heu 0.000563
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000594
camion 1, TRUE-heu 0.000623
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000655
camion 1, TRUE-heu 0.000684
Salgo lazy
*  1806   519      integral     0     6530.0000     6372.9717   208410    2.40%
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4939 rows and 34 columns.
MIP Presolve modified 4294 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2433 rows, 1037 columns, and 81232 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.61 sec. (636.89 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2433 rows, 1037 columns, and 81232 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (118.43 ticks)
Clique table members: 2369.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.17 sec. (95.13 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   217                      0.0000      677         
      0     0        0.0000   217                 Cliques: 20     1005         
      0     0        0.0000   217                   Cuts: 179     1795         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1795    0.00%
Elapsed time = 5.08 sec. (3820.42 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  66
Zero-half cuts applied:  3
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    5.08 sec. (3820.71 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    5.08 sec. (3820.71 ticks)
5.078000 
TRUE-modelo
camion 1, TRUE-heu 0.000743
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 4939 rows and 34 columns.
MIP Presolve modified 4294 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2433 rows, 1037 columns, and 81232 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.59 sec. (649.81 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2433 rows, 1037 columns, and 81232 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (118.30 ticks)
Clique table members: 2369.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.16 sec. (95.81 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   220                      0.0000      632         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1332    0.00%
Elapsed time = 3.61 sec. (2993.85 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  249

Root node processing (before b&c):
  Real time             =    3.63 sec. (2994.15 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.63 sec. (2994.15 ticks)
3.640000 
TRUE-modelo
camion 1, TRUE-heu 0.000799
Salgo lazy
*  1888   537      integral     0     6506.0000     6396.5051   224813    1.68%
Dentro lazy
camion 0, TRUE-heu 0.000830
camion 1, TRUE-heu 0.000857
Salgo lazy

GUB cover cuts applied:  20
Cover cuts applied:  357
Flow cuts applied:  15
Mixed integer rounding cuts applied:  30
Zero-half cuts applied:  21
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    4.50 sec. (2174.05 ticks)
Parallel b&c, 8 threads:
  Real time             =   23.03 sec. (5036.01 ticks)
  Sync time (average)   =   10.79 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   27.53 sec. (7210.06 ticks)
27.531000 

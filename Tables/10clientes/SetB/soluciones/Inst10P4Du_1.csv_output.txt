Instances\Inst10P4Du_1.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000029
camion 1, TRUE-heu 0.000072
camion 2, TRUE-heu 0.000116
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 8085.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 399 rows and 126 columns.
MIP Presolve modified 4072 coefficients.
Reduced MIP has 634 rows, 4407 columns, and 36995 nonzeros.
Reduced MIP has 4107 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (27.56 ticks)
Clique table members: 199.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.13 sec. (132.54 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         8085.0000        0.0000           100.00%
      0     0     4145.1813   100     8085.0000     4145.1813       15   48.73%
      0     0     5023.6037   100     8085.0000      Cuts: 81     2108   37.87%
      0     0     5140.9217   100     8085.0000     Cuts: 100     2270   36.41%
      0     0     5219.0830   100     8085.0000      Cuts: 96     2547   35.45%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 5237 rows and 34 columns.
MIP Presolve modified 2882 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2126 rows, 717 columns, and 56214 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.77 sec. (821.88 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2126 rows, 717 columns, and 56214 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (80.57 ticks)
Clique table members: 2064.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (46.02 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   118                      0.0000      392         
      0     0        0.0000   118                Cliques: 167     1171         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1239    0.00%
Elapsed time = 3.55 sec. (3039.80 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  97

Root node processing (before b&c):
  Real time             =    3.55 sec. (3040.00 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.55 sec. (3040.00 ticks)
3.547000 
TRUE-modelo
camion 2, TRUE-heu 0.000182
Salgo lazy
*     0+    0                         6506.0000     5219.0830            19.78%
      0     0  -1.00000e+75     0     6506.0000     5219.0830     2547   19.78%
      0     0     5560.2361   100     6506.0000      Cuts: 52     2816   14.54%
      0     0     5578.5987   100     6506.0000      Cuts: 95     3012   14.25%
      0     0     5596.3405   100     6506.0000      Cuts: 82     3594   13.98%
      0     0     5601.2751   100     6506.0000      Cuts: 73     3742   13.91%
      0     0     5606.0571   100     6506.0000      Cuts: 46     3901   13.83%
      0     0     5621.2921   100     6506.0000      Cuts: 74     4171   13.60%
      0     0     5626.1150   100     6506.0000      Cuts: 83     4323   13.52%
      0     0     5641.7710   100     6506.0000      Cuts: 73     4473   13.28%
      0     0     5681.7201   100     6506.0000      Cuts: 66     4698   12.67%
      0     0     5729.8389   100     6506.0000      Cuts: 68     4937   11.93%
      0     0     5909.3478   100     6506.0000      Cuts: 56     5734    9.17%
      0     0     5942.1328   100     6506.0000     Cuts: 106     6528    8.67%
      0     0     5945.5356   100     6506.0000      Cuts: 62     6688    8.61%
      0     0     5951.2270   100     6506.0000      Cuts: 79     6870    8.53%
      0     0     5951.5395   100     6506.0000      Cuts: 72     6999    8.52%
      0     0     5953.9315   100     6506.0000      Cuts: 30     7077    8.49%
      0     0     5961.1660   100     6506.0000      Cuts: 86     7159    8.37%
      0     0     5982.8563   100     6506.0000      Cuts: 72     7309    8.04%
      0     0     5983.3135   100     6506.0000      Cuts: 54     7391    8.03%
      0     2     5983.3135   154     6506.0000     5983.3135     7391    8.03%                        0             0
Elapsed time = 7.09 sec. (2504.01 ticks, tree = 0.02 MB, solutions = 2)
      5     5     6234.8331   176     6506.0000     6058.1068     8361    6.88%         X_0_7_0 N     32      7      2
     24    16     6222.4450   153     6506.0000     6058.1068    13065    6.88%        X_0_10_5 D     13      5      4
     70    26     6398.0345   102     6506.0000     6058.1068    23064    6.88%         X_1_7_8 D    168    160     17
    184    69     6390.9552   117     6506.0000     6131.5933    32703    5.75%        X_2_10_4 D    121    113     23
    320    62     6386.4217   127     6506.0000     6131.5933    50380    5.75%        X_2_4_10 D    178    170     16
    578    49     6484.1555    69     6506.0000     6307.0531    64034    3.06%         X_0_6_9 D    548    540     33

GUB cover cuts applied:  42
Cover cuts applied:  92
Flow cuts applied:  27
Mixed integer rounding cuts applied:  15
Zero-half cuts applied:  62
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    6.77 sec. (2451.76 ticks)
Parallel b&c, 8 threads:
  Real time             =    4.77 sec. (1631.75 ticks)
  Sync time (average)   =    2.40 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   11.53 sec. (4083.51 ticks)
11.531000 

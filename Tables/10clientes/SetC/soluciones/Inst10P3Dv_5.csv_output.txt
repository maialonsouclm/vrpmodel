Instances\Inst10P3Dv_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000063
camion 1, TRUE-heu 0.000088
camion 2, TRUE-heu 0.000111
camion 3, TRUE-heu 0.000143
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 9596.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 532 rows and 232 columns.
MIP Presolve modified 11892 coefficients.
Reduced MIP has 882 rows, 12212 columns, and 106372 nonzeros.
Reduced MIP has 11812 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (81.88 ticks)
Clique table members: 302.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.23 sec. (257.69 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         9596.0000        0.0000           100.00%
      0     0     4098.1327   251     9596.0000     4098.1327       16   57.29%
      0     0     5200.1140   251     9596.0000      Cuts: 96     2757   45.81%
      0     0     5337.0711   251     9596.0000     Cuts: 135     4655   44.38%
      0     0     5730.4573   251     9596.0000     Cuts: 143     8032   40.28%
Dentro lazy
camion 0, TRUE-heu 0.000179
camion 2, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 3432 rows and 34 columns.
MIP Presolve modified 4203 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1542 rows, 1037 columns, and 61092 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.47 sec. (523.51 ticks)
Tried aggregator 1 time.
Detecting symmetries...
MIP Presolve modified 515 coefficients.
Reduced MIP has 1542 rows, 1037 columns, and 61092 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (102.76 ticks)
Clique table members: 1478.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.06 sec. (52.15 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   145                      0.0000      436         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      436    0.00%
Elapsed time = 1.97 sec. (1786.42 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    1.97 sec. (1786.64 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.97 sec. (1786.64 ticks)
1.985000 
TRUE-modelo
camion 3, TRUE-heu 0.000247
Salgo lazy
*     0+    0                         9485.0000     5730.4573            39.58%
Dentro lazy
camion 0, TRUE-heu 0.000275
camion 2, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 3432 rows and 34 columns.
MIP Presolve modified 3819 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1542 rows, 1037 columns, and 61092 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.47 sec. (523.51 ticks)
Tried aggregator 1 time.
Detecting symmetries...
MIP Presolve modified 336 coefficients.
Reduced MIP has 1542 rows, 1037 columns, and 61092 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (102.94 ticks)
Clique table members: 1478.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (52.15 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   145                      0.0000      436         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      436    0.00%
Elapsed time = 2.17 sec. (1784.58 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    2.17 sec. (1784.80 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.17 sec. (1784.80 ticks)
2.203000 
TRUE-modelo
camion 3, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 3418 rows and 34 columns.
MIP Presolve modified 3449 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1557 rows, 1069 columns, and 60512 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.39 sec. (434.83 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1557 rows, 1069 columns, and 60512 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (85.68 ticks)
Clique table members: 1494.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (38.78 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   144                      0.0000      340         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      340    0.00%
Elapsed time = 1.59 sec. (1338.31 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    1.61 sec. (1338.52 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.61 sec. (1338.52 ticks)
1.609000 
TRUE-modelo
Salgo lazy
*     0+    0                         9018.0000     5730.4573            36.46%
      0     0  -1.00000e+75     0     9018.0000     5730.4573     8032   36.46%
      0     0     5762.6233   251     9018.0000      Cuts: 92    10233   36.10%
      0     0     5775.4555   251     9018.0000     Cuts: 124    11313   35.96%
      0     0     5792.9913   251     9018.0000      Cuts: 86    15025   35.76%
      0     0     5799.7320   251     9018.0000      Cuts: 96    16658   35.69%
      0     0     5804.1040   251     9018.0000     Cuts: 120    17431   35.64%
      0     0     5870.2684   251     9018.0000     Cuts: 121    20548   34.90%
      0     0     5928.0690   251     9018.0000     Cuts: 117    23621   34.26%
      0     0     5995.4376   251     9018.0000     Cuts: 118    26250   33.52%
      0     0     6022.6882   251     9018.0000     Cuts: 151    27548   33.21%
      0     0     6024.9407   251     9018.0000     Cuts: 124    28024   33.19%
      0     0     6031.4135   251     9018.0000     Cuts: 101    28774   33.12%
      0     0     6046.1548   251     9018.0000     Cuts: 123    29977   32.95%
      0     0     6046.7055   251     9018.0000      Cuts: 77    31808   32.95%
      0     0     6047.2927   251     9018.0000      Cuts: 65    32636   32.94%
      0     2     6047.2927   363     9018.0000     6047.2927    32636   32.94%                        0             0
Elapsed time = 23.11 sec. (19131.00 ticks, tree = 0.02 MB, solutions = 3)
      1     3     6150.0019   365     9018.0000     6073.5980    32967   32.65%         X_0_0_7 D      8      0      1
      2     4     6179.3816   387     9018.0000     6073.5980    34357   32.65%        X_3_5_10 D     16      8      2
      4     5     6181.0331   354     9018.0000     6073.5980    34586   32.65%         X_3_4_5 D     24     16      3
      5     6     6090.7581   395     9018.0000     6074.6929    34725   32.64%         X_3_4_5 D     15      7      2
      8    10     7246.7594   252     9018.0000     6074.6929    41049   32.64%         X_3_0_5 D     14      6      3
      9     8     6149.9962   385     9018.0000     6074.6929    35731   32.64%        X_0_5_10 D     23     15      3
     13    11     7293.8453   266     9018.0000     6074.6929    41868   32.64%         X_3_0_4 D     22     14      4
     15    14     7244.6122   229     9018.0000     6074.6929    50751   32.64%         X_3_6_2 D     11      3      5
     18    12     7418.9498   281     9018.0000     6074.6929    49868   32.64%         X_3_0_4 N     21     14      4
     32    21     7458.3991   262     9018.0000     6074.6929    63415   32.64%         X_0_1_7 U     37     29      6
Elapsed time = 29.36 sec. (24073.36 ticks, tree = 0.10 MB, solutions = 3)
     47    38     7436.1720   268     9018.0000     6152.3614    77759   31.78%         X_3_0_8 D     80     72      9
     71    40     6356.3043   280     9018.0000     6152.3614    83217   31.78%         X_1_5_0 D     33     25      9
    103    72     7007.5832   175     9018.0000     6152.3614   108590   31.78%        X_0_4_10 D     50     42      9
    130   101     7672.1650   156     9018.0000     6152.3614   124178   31.78%         X_3_6_0 D    158    142     18
    162   128     7709.5931   167     9018.0000     6152.3614   143808   31.78%        X_3_6_10 D    132    124     17
    210   160     8810.1745    84     9018.0000     6152.3614   155997   31.78%         X_1_6_3 D    347    331     36
    252   188     7835.6547   173     9018.0000     6152.3614   169004   31.78%         X_2_0_4 D    270    262     30
    288   223     8211.3526   206     9018.0000     6152.3614   188001   31.78%         X_1_2_3 D    332    324     41
    366   302     8030.8749   154     9018.0000     6152.3614   222507   31.78%         X_1_1_3 D    389    373     42
    397   309     8870.9587   184     9018.0000     6152.3614   225448   31.78%         X_1_4_9 D    461    453     50
Elapsed time = 45.16 sec. (34204.44 ticks, tree = 2.32 MB, solutions = 3)
    443   353     7227.0842   217     9018.0000     6152.3614   246375   31.78%         X_0_8_0 D    263    255     29
    499   414     8977.5413   193     9018.0000     6152.3614   288796   31.78%         X_3_1_0 D    594    586     25
    533   408     7330.5495   262     9018.0000     6152.3614   289803   31.78%         X_1_8_9 D    359    351     41
    571   448     7801.7042   169     9018.0000     6152.3614   305066   31.78%         X_3_0_4 D    504    496     31
    632   454     8410.8887   171     9018.0000     6152.3614   304007   31.78%         X_1_8_3 D    765    757     31
    682   536     8013.7624   194     9018.0000     6152.3614   357520   31.78%        X_1_5_10 D    820    812     24
    748   501     8017.7078   203     9018.0000     6152.3614   352939   31.78%         X_0_7_0 D    648    640     18
    845   607     7936.9738   162     9018.0000     6152.3614   392257   31.78%         X_1_2_7 D   1034   1026     39
    956   620     6881.1134   270     9018.0000     6152.3614   408198   31.78%              Y2 D   1069   1061     23
   1014   731     7456.1182   219     9018.0000     6228.5880   436066   30.93%         X_0_7_0 D   1554   1546     11
Elapsed time = 60.44 sec. (43904.54 ticks, tree = 7.30 MB, solutions = 3)
   1097   789     8619.0852   150     9018.0000     6228.5880   482217   30.93%         X_1_8_9 D   1008   1000     40
   1160   886     6586.9832   269     9018.0000     6228.5880   517912   30.93%         X_0_0_8 N   1826    201     29
   1220   956     7307.7637   258     9018.0000     6228.5880   554685   30.93%        X_1_5_10 D   1128   1120     27
   1285   973     8233.5585   182     9018.0000     6228.5880   566281   30.93%         X_1_8_0 D   1380   1372     29
   1382  1030     8800.4943   191     9018.0000     6228.5880   598650   30.93%        X_0_10_4 D   1946   1938     44
   1454  1100     8718.2891   172     9018.0000     6505.6444   629661   27.86%         X_1_6_9 D   1344   1336     54
   1577  1204     8341.3786    68     9018.0000     6505.6444   665137   27.86%         X_3_1_2 D   2402   2394     71
   1682  1269     7987.4207   166     9018.0000     6535.4671   706901   27.53%        X_0_4_10 D   1472   1464     31
   1755  1341     8437.9714   172     9018.0000     6535.4671   732541   27.53%         X_1_2_9 D   1502   1494     29
   1849  1419     8258.4106   244     9018.0000     6535.4671   758507   27.53%        X_1_5_10 D   1609   1601     51
Elapsed time = 75.67 sec. (53552.10 ticks, tree = 16.30 MB, solutions = 3)
   1926  1559     8706.2350    77     9018.0000     6543.1004   812079   27.44%         X_3_8_7 D   1989   1981     61
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2993 rows and 34 columns.
MIP Presolve modified 3841 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1980 rows, 1037 columns, and 67189 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.42 sec. (450.43 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1980 rows, 1037 columns, and 67189 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (96.44 ticks)
Clique table members: 1916.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.06 sec. (47.74 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   151                      0.0000      365         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      639    0.00%
Elapsed time = 1.83 sec. (1465.71 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  27

Root node processing (before b&c):
  Real time             =    1.83 sec. (1465.95 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.83 sec. (1465.95 ticks)
1.844000 
TRUE-modelo
camion 1, TRUE-heu 0.000408
camion 3, TRUE-heu 0.000445
Salgo lazy
   1996  1591     7714.0675   226     9018.0000     6555.6137   839531   27.31%         X_2_9_4 D   1900   1892     29
*  2001+ 1563                         8988.0000     6555.6137            27.06%
   2071  1638     8051.7161   203     8988.0000     6555.6137   870151   27.06%        X_0_10_5 D   2213   2205     39
   2170  1725     8693.1604   169     8988.0000     6571.8192   911426   26.88%        X_1_5_10 D   2155   2147     60
   2226  1723     7418.8015   263     8988.0000     6585.0560   910610   26.74%              Y2 U   2898   2666     46
   2302  1754     7360.4647   242     8988.0000     6680.1158   941586   25.68%        X_1_10_5 D   2080   2072     24
   2498  1912     8054.0663   185     8988.0000     6714.7469  1029303   25.29%        X_1_5_10 D   2789   2781     19
   2570  1931     8816.8863   210     8988.0000     6714.7469  1049422   25.29%         X_2_9_4 D   2884   2876     59
   2644  1900     8792.8618   152     8988.0000     6719.1879  1047981   25.24%         X_1_7_0 D   2031   2023     66
   2751  2080     7191.3792   193     8988.0000     6728.7815  1083089   25.14%        X_1_10_5 U   2507   1096     24
Elapsed time = 92.95 sec. (63265.27 ticks, tree = 25.10 MB, solutions = 4)
   2818  2134     8827.4008   147     8988.0000     6728.7815  1126989   25.14%         X_0_0_5 D   2207   2199     28
   2888  2254     8151.9728   326     8988.0000     6776.3077  1196401   24.61%         X_3_0_4 U   2406    593     71
   2990  2354     8715.9352   122     8988.0000     6776.3077  1239314   24.61%         X_1_1_3 D   3373   3365     40
   3094  2402     8638.6100   129     8988.0000     6841.0338  1265390   23.89%        X_3_4_10 D   2631   2615     45
   3157  2428        cutoff           8988.0000     6878.5791  1302038   23.47%         X_0_7_0 U   2705   2489     12
   3304  2447     8706.4635   189     8988.0000     6878.5791  1316127   23.47%         X_1_7_0 U   2939   2931     26
   3366  2569     8089.6481   296     8988.0000     6884.5257  1361390   23.40%         X_1_1_7 U   4914    449     57
   3425  2564     7679.8303   146     8988.0000     6900.2638  1376003   23.23%         X_1_0_8 D   3580   3572     12
   3549  2758     8570.8203   144     8988.0000     6944.0417  1451302   22.74%         X_1_8_2 D   3844   3836     45
   3723  2783     8516.1220   205     8988.0000     6944.0417  1478721   22.74%         X_2_0_7 U   2976   2960     27
Elapsed time = 108.06 sec. (72991.57 ticks, tree = 34.72 MB, solutions = 4)
   3824  2965     8435.9129   129     8988.0000     6944.0417  1525912   22.74%        X_0_4_10 D   3662   3654     45
   3971  3061     8367.0231   179     8988.0000     6945.8084  1565144   22.72%         X_3_0_7 D   3359   3351     15
   4058  3146     8336.5254   169     8988.0000     7002.5575  1600262   22.09%         X_1_3_9 D   5762   5754     52
   4133  3203     7702.9536   194     8988.0000     7015.6973  1614513   21.94%         X_0_7_0 D   4334   4326     12
   4230  3350     8857.3786   111     8988.0000     7018.0951  1677578   21.92%      Z_2_8_0_15 U   4742   4726     57
   4313  3368     8834.9963   226     8988.0000     7018.0951  1682206   21.92%        X_1_10_5 D   4966   4958     78
   4418  3405     8573.8910    92     8988.0000     7018.0951  1722414   21.92%         X_3_7_0 D   3504   3496     24
   4544  3425     8914.9653   176     8988.0000     7018.0951  1733226   21.92%         X_2_9_6 D   3823   3815    107
   4666  3517     8618.0558   207     8988.0000     7076.8350  1771212   21.26%        X_0_10_4 D   5174   5166     17
   4774  3697     8698.3098   195     8988.0000     7076.8350  1836248   21.26%         X_3_0_2 D   5334   5326     27
Elapsed time = 122.91 sec. (82599.04 ticks, tree = 47.29 MB, solutions = 4)
   4892  3713     8487.2237   293     8988.0000     7122.6716  1840585   20.75%        X_3_10_8 U   5494    697     82
   5014  3932     8608.0000    27     8988.0000     7141.6596  1917323   20.54%         X_0_2_3 D   6554   6546     38
   5107  3947     8313.4523   215     8988.0000     7161.0008  1941778   20.33%         X_0_7_0 D   4153   4145     26
   5235  3968     8562.1765   181     8988.0000     7161.0008  1945273   20.33%         X_0_1_7 D   4321   4313     47
   5361  4091     8256.1718   102     8988.0000     7161.0008  2006426   20.33%         X_2_6_2 U   7066   7058     25
   5493  4052     7935.0367   150     8988.0000     7194.7933  2000273   19.95%         X_1_7_6 D   4383   4375     20
   5599  4333     8561.7031   123     8988.0000     7194.7933  2070784   19.95%         X_1_0_9 D   4567   4559     43
   5692  4320     8940.2203   165     8988.0000     7240.7500  2067801   19.44%         X_3_3_0 D   5941   5933     28
   5809  4418     7900.1069   142     8988.0000     7245.7239  2115258   19.38%         X_1_2_9 D   5009   5001     26
   5954  4560     8851.0724   122     8988.0000     7249.1926  2171640   19.35%         X_1_3_6 D   7810   7802     47
Elapsed time = 138.17 sec. (92261.89 ticks, tree = 59.71 MB, solutions = 4)
   6086  4629     8933.7451   176     8988.0000     7249.1926  2207756   19.35%        X_0_4_10 D   6092   6084     18
   6273  4808     8290.3945   122     8988.0000     7251.8445  2270008   19.32%         X_0_8_0 D   5665   5657     36
   6388  4774     8211.9088   131     8988.0000     7288.6618  2252598   18.91%         X_1_8_6 D   8498   8490     17
   6548  4769        cutoff           8988.0000     7294.3870  2269671   18.84%        X_1_10_7 D   6438   6430     24
   6683  5123     8895.3000   102     8988.0000     7313.6343  2356769   18.63%         X_1_2_1 D   6137   6129     37
Dentro lazy
camion 0, TRUE-heu 0.000477
camion 1, TRUE-heu 0.000508
camion 3, TRUE-heu 0.000535
Salgo lazy
   6787  5178     7938.7976   178     8988.0000     7315.1447  2371515   18.61%        X_1_5_10 D   5911   5903     20
*  6792  4972      integral     0     8712.0000     7315.1447  2317647   16.03%
   6937  5241        cutoff           8712.0000     7315.1447  2395730   16.03%              Y3 D   7547   7283     12
   7027  4339        cutoff           8712.0000     7333.3687  2401668   15.82%        X_1_5_10 U   6312   6296     32
   7185  4522     8632.2213   115     8712.0000     7342.0366  2491115   15.73%         X_1_0_3 D   7093   7085     56
   7316  4647     8201.3088   108     8712.0000     7348.5269  2536947   15.65%         X_3_1_3 D   6689   6681     27
Elapsed time = 153.59 sec. (101854.62 ticks, tree = 61.63 MB, solutions = 5)
Dentro lazy
camion 0, TRUE-heu 0.000569
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 1527 rows and 34 columns.
MIP Presolve modified 4394 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1527 rows, 1037 columns, and 53146 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.22 sec. (262.34 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1527 rows, 1037 columns, and 53146 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (61.77 ticks)
Clique table members: 1463.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (39.91 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   155                      0.0000      370         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      370    0.00%
Elapsed time = 1.42 sec. (1017.71 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    1.42 sec. (1017.91 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.42 sec. (1017.91 ticks)
1.437000 
TRUE-modelo
camion 2, TRUE-heu 0.000628
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000657
camion 1, TRUE-heu 0.000687
camion 3, TRUE-heu 0.000726
Salgo lazy
   7457  4677     7945.5344   153     8712.0000     7349.5834  2541023   15.64%         X_0_0_8 D   7033   7025      8
*  7493  4619      integral     0     8567.0000     7352.0575  2528095   14.18%
   7572  4741        cutoff           8567.0000     7363.0191  2572903   14.05%         X_1_8_2 D   6848   6840     56
   7690  3932        cutoff           8567.0000     7374.9002  2631473   13.92%        X_1_10_7 U   8091   8075     22
   7817  3953     7847.8573   146     8567.0000     7376.2420  2631608   13.90%        X_0_10_5 D   7673   7665     25
   7929  4144     8389.8333    21     8567.0000     7393.6296  2738008   13.70%         X_2_6_2 U   7900   7892     26
   8036  4136     7828.1408   156     8567.0000     7393.6296  2731766   13.70%         X_1_2_7 D   8041   8033     15
   8124  4164     7697.5403   185     8567.0000     7397.8621  2750788   13.65%         X_1_4_5 D   7400   7392     18
   8184  4174     8213.2044    89     8567.0000     7417.2503  2754967   13.42%         X_2_6_7 D   7480   7472     28
   8317  4191     7855.9310   141     8567.0000     7417.2503  2758480   13.42%         X_1_7_0 U   7696   7688     10
   8885  4544     8517.9253   131     8567.0000     7463.5313  2934149   12.88%         X_1_9_6 D   8232   8224     34
Elapsed time = 175.80 sec. (114390.03 ticks, tree = 61.51 MB, solutions = 7)
   9301  4996     7506.0793   252     8567.0000     7476.3257  3105827   12.73%         X_0_7_0 N   9636   6701     10
   9677  5096     8014.5993   151     8567.0000     7519.9352  3194146   12.22%         X_1_0_6 D   9947   9939     25
  10172  5391     8461.2764   217     8567.0000     7569.9462  3385894   11.64%         X_1_4_5 D  10715  10707     27
  10624  5787     8198.0916    72     8567.0000     7586.9921  3540665   11.44%         X_2_2_8 D  10406  10398     29
  11290  6007     8256.6700   102     8567.0000     7617.5524  3636437   11.08%         X_1_2_0 D  11748  11740     26
  11782  6444     7938.4372   187     8567.0000     7639.3927  3807985   10.83%         X_2_0_7 U  12691  12683     20
  12261  6769     8309.6926   137     8567.0000     7659.2342  3932542   10.60%         X_1_4_5 D  11103  11095     37
  12660  7053     8285.9511   266     8567.0000     7677.3096  4063312   10.39%         X_1_3_7 D  12433  12425     23
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 541 rows and 34 columns.
MIP Presolve modified 2935 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1074 rows, 1037 columns, and 39103 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (150.58 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1074 rows, 1037 columns, and 39103 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (39.72 ticks)
Clique table members: 1010.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (22.48 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   122                      0.0000      278         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      278    0.00%
Elapsed time = 0.72 sec. (620.53 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    0.72 sec. (620.69 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.72 sec. (620.69 ticks)
0.718000 
TRUE-modelo
camion 1, TRUE-heu 0.000791
camion 2, TRUE-heu 0.000821
Salgo lazy
  13095  7256     7952.8433   163     8567.0000     7696.6980  4192738   10.16%              Y2 U  13356   6492      9
* 13150+ 7153                         8473.0000     7696.6980             9.16%
  13581  6655     8380.2424   188     8473.0000     7710.5904  4330524    9.00%         X_1_7_9 D  13294  13286     50
Elapsed time = 238.50 sec. (152640.38 ticks, tree = 102.43 MB, solutions = 8)
  14126  6908     8268.7431   118     8473.0000     7724.9778  4468596    8.83%         X_1_2_3 D  16298  16290     48
  14657  7193        cutoff           8473.0000     7740.8877  4578212    8.64%         X_1_4_6 D  14470  14462     32
  15181  7428     7990.5432   183     8473.0000     7760.4011  4699412    8.41%         X_3_7_0 D  14901  14893     12
  15802  7757     7883.6612   164     8473.0000     7777.3015  4848044    8.21%         X_1_8_0 N  14824  14899     11
  16431  8093        cutoff           8473.0000     7788.2258  4964756    8.08%         X_2_6_3 U  18186  18178     40
  17119  8592     8028.1762   158     8473.0000     7800.3717  5105578    7.94%        X_1_4_10 D  15992  15984     23
  17702  8830     8341.4000    50     8473.0000     7813.7688  5207161    7.78%         X_1_8_9 U  18118  11941     34
  18290  9079     8107.7080   136     8473.0000     7824.0535  5295109    7.66%         X_2_7_6 D  17265  17257     14
  18874  9370     7968.1729   182     8473.0000     7840.3197  5440849    7.47%         X_3_0_4 N  17857   9523      7
  19409  9739     8080.6036   170     8473.0000     7851.6210  5581627    7.33%         X_1_6_9 D  18605  18597     21
Elapsed time = 304.22 sec. (190881.78 ticks, tree = 174.95 MB, solutions = 8)
  19921 10140     8311.0371   100     8473.0000     7864.0433  5754543    7.19%         X_1_8_0 U  18632  11560     16
  20573 10301     8318.2200   123     8473.0000     7876.0282  5835215    7.05%         X_1_0_7 D  21707  21699     24
  21113 10403     8344.2449   175     8473.0000     7883.0000  5935957    6.96%         X_1_8_3 D  23642  23634     32
  21694 10800     8209.2792   159     8473.0000     7890.9891  6140097    6.87%         X_1_3_6 D  22739  22731     29
  22215 11056     7959.6161   121     8473.0000     7897.9042  6286821    6.79%         X_1_7_6 N  23163   2407     22
  22757 11255     8361.6305   100     8473.0000     7908.5661  6439300    6.66%         X_1_8_9 D  23803  23795     20
  23300 11377     8400.0560   215     8473.0000     7919.9992  6537157    6.53%         X_1_4_5 U  25794  11472     28
  23891 11688     8078.5725   195     8473.0000     7929.4855  6709582    6.41%         X_2_7_6 D  23575  23567     48
  24468 11771     8307.9705   136     8473.0000     7940.8345  6777160    6.28%         X_2_7_3 D  24454  24446     18
  24973 11960     8261.2120   163     8473.0000     7948.0848  6920733    6.20%         X_1_2_3 D  26227  26219     27
Elapsed time = 368.80 sec. (229131.29 ticks, tree = 238.37 MB, solutions = 8)
  25522 12045        cutoff           8473.0000     7952.4247  7059819    6.14%         X_1_7_3 U  25057  13553     16
  26057 12181     8051.6306   213     8473.0000     7963.3581  7146708    6.01%         X_1_8_2 U  25468  21050     17
  26622 12200     8352.4972   153     8473.0000     7970.4898  7216307    5.93%         X_2_1_3 D  25857  25849     23
  27161 12358    infeasible           8473.0000     7982.2146  7372497    5.79%         X_2_8_0 U  26385  17408     26
  27682 12416     8189.4645   185     8473.0000     7988.0105  7436906    5.72%         X_2_7_8 D  27926  27918     14
  28223 12514     8276.5416   211     8473.0000     7998.5520  7683344    5.60%         X_2_6_0 D  30642  30634     23
  28747 12501     8352.8752   188     8473.0000     8006.8585  7766229    5.50%         X_1_8_9 U  29166  23873     32
  29216 12505        cutoff           8473.0000     8014.2075  7877795    5.41%         X_2_6_8 U  28201   9402     19
  29854 12521     8454.0962    87     8473.0000     8024.1112  8090977    5.30%         X_2_3_7 D  31515  31507     25
  30492 12594        cutoff           8473.0000     8032.1434  8137661    5.20%        X_1_5_10 U  29765  29749     22
Elapsed time = 434.98 sec. (267338.57 ticks, tree = 270.38 MB, solutions = 8)
  31107 12773     8322.0194   247     8473.0000     8040.9729  8227990    5.10%         X_2_7_3 D  29897  29889     32
  31767 12834     8309.6282   218     8473.0000     8052.3009  8485118    4.97%         X_1_2_0 U  30957    221     24
  32453 12789     8214.5694   202     8473.0000     8064.6516  8564221    4.82%         X_3_0_2 U  34251  10154     24
  33126 12824     8362.2003   235     8473.0000     8068.9471  8597815    4.77%         X_0_0_8 D  36434  36426     36
  33903 12793     8250.7410   224     8473.0000     8078.7895  8698208    4.65%         X_1_3_6 D  32297  32289     33
  34722 12977    infeasible           8473.0000     8093.5982  8914317    4.48%         X_2_9_0 U  37842  16116     30
  35594 13040     8216.7281   149     8473.0000     8106.9114  9003582    4.32%         X_2_6_3 D  34241  34233     53
  36467 13061        cutoff           8473.0000     8113.3381  9090884    4.24%         X_1_0_8 U  33176  14675     14
  37279 13180     8208.2656   131     8473.0000     8124.4405  9376949    4.11%         X_2_8_0 D  37711  37703     40
  38297 13145        cutoff           8473.0000     8136.3726  9464039    3.97%         X_1_0_7 U  39558  27293     24
Elapsed time = 503.67 sec. (305515.57 ticks, tree = 308.51 MB, solutions = 8)
  39202 13216     8337.3757    89     8473.0000     8143.8259  9547961    3.88%         X_3_4_8 U  39311  14482     31
  40182 13425     8364.2319   141     8473.0000     8155.0926  9683931    3.75%         X_1_2_0 D  42570  42562     41
  41195 13467        cutoff           8473.0000     8165.3442  9793075    3.63%         X_1_3_7 U  42126  30286     44
  42315 13322     8215.2923   110     8473.0000     8172.9072  9897448    3.54%         X_2_7_0 D  44450  44442     54
  43431 13334        cutoff           8473.0000     8181.6152 10096411    3.44%         X_2_2_6 U  42397  14928     25
  44562 13314        cutoff           8473.0000     8191.7830 10223653    3.32%         X_1_0_7 D  45982  45974     38
  45915 13280     8450.3136   127     8473.0000     8203.9446 10347816    3.18%              Y3 D  47547  47539     34
  47082 13164     8338.7677   105     8473.0000     8214.5651 10484395    3.05%         X_2_8_0 D  46665  46657     37
  48253 13065     8291.4044   117     8473.0000     8222.6178 10617780    2.96%         X_2_6_2 D  48772  48764     28
  49471 12942        cutoff           8473.0000     8236.0468 10749550    2.80%         X_1_0_4 U  46464  46456     33
Elapsed time = 574.47 sec. (343700.32 ticks, tree = 326.03 MB, solutions = 8)
  50804 12908     8446.4532   214     8473.0000     8248.4248 10840147    2.65%         X_2_7_3 D  52382  52374     31
  52232 12740        cutoff           8473.0000     8258.1957 10986117    2.54%         X_3_0_3 U  50509   1653     38
  53467 12480        cutoff           8473.0000     8268.2478 11142558    2.42%         X_3_3_0 U  54252  33812     31
  54943 12262        cutoff           8473.0000     8279.0537 11246315    2.29%        X_1_5_10 D  55524  55516     39
  56428 11634        cutoff           8473.0000     8293.2359 11454007    2.12%         X_2_8_0 U  55305  29338     34
  58014 11209     8380.7546   180     8473.0000     8303.2156 11554499    2.00%         X_1_8_2 U  60707  50498     26
  59379 11016        cutoff           8473.0000     8313.5949 11627743    1.88%         X_2_1_3 U  61875  43256     24
  60890 10213     8395.2838   119     8473.0000     8327.6910 11838233    1.71%         X_2_0_6 D  59633  59625     28
  62406  9740     8394.5769   203     8473.0000     8334.8281 11941884    1.63%         X_1_2_6 U  60881  42149     40
  63982  8895        cutoff           8473.0000     8350.2721 12112367    1.45%         X_2_7_0 U  64215  53067     31
Elapsed time = 649.06 sec. (381889.24 ticks, tree = 227.40 MB, solutions = 8)
  65462  8200        cutoff           8473.0000     8358.7287 12237277    1.35%         X_2_4_5 U  67627  59715     30
  67067  7389     8445.6284    41     8473.0000     8373.0117 12374784    1.18%         X_2_8_0 U  68814  38613     36
  68636  6481     8423.3746    56     8473.0000     8384.9425 12515686    1.04%         X_3_2_0 D  66593  66585     29
  70081  5753     8469.0241    66     8473.0000     8395.2710 12629312    0.92%         X_2_5_4 U  72238  39486     48
  71631  4793     8436.1403    98     8473.0000     8409.6885 12755857    0.75%         X_2_0_2 D  69829  69821     51
  72224  4268        cutoff           8473.0000     8411.3183 12823998    0.73%         X_2_6_0 U  70677  21142     41
  72910  3946     8445.6284    67     8473.0000     8419.6311 12854006    0.63%     Z_2_10_5_15 N  72503  72495     46
  73798  3036        cutoff           8473.0000     8427.1720 12924485    0.54%         X_1_8_9 N  74604  26036     18
  74790  2254     8471.4710   170     8473.0000     8440.6157 12960683    0.38%         X_1_6_9 D  76618  76610     20
  75034  2262     8447.0994    59     8473.0000     8440.6157 12961676    0.38%         X_0_3_0 D  73077  73069     60
Elapsed time = 729.91 sec. (420159.99 ticks, tree = 40.41 MB, solutions = 8)
  75739  2102     8447.2398    84     8473.0000     8442.0066 12996237    0.37%     Z_3_10_6_12 U  78374  78294     28
  78040  1336     8454.6033    35     8473.0000     8451.2292 13078468    0.26%       Z_2_5_6_2 U  74973  78194     46
  81941   306        cutoff           8473.0000     8468.4392 13128103    0.05%         X_0_8_0 D  85782  85774     43

GUB cover cuts applied:  166
Cover cuts applied:  2316
Flow cuts applied:  53
Mixed integer rounding cuts applied:  94
Zero-half cuts applied:  54
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =   22.66 sec. (18823.60 ticks)
Parallel b&c, 8 threads:
  Real time             =  735.67 sec. (415253.41 ticks)
  Sync time (average)   =   66.56 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  758.33 sec. (434077.01 ticks)
758.344000 

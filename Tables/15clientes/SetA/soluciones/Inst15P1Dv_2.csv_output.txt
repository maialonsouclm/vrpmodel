Instances\Inst15P1Dv_2.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000050
camion 1, TRUE-heu 0.000087
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7530.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 627 rows and 123 columns.
MIP Presolve modified 357 coefficients.
Reduced MIP has 641 rows, 1829 columns, and 11375 nonzeros.
Reduced MIP has 1408 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (10.00 ticks)
Clique table members: 125.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.03 sec. (17.19 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7530.0000        0.0000           100.00%
      0     0     5026.0483   104     7530.0000     5026.0483      594   33.25%
      0     0     5800.2250   104     7530.0000      Cuts: 37      909   22.97%
      0     0     5936.4927   104     7530.0000      Cuts: 50     1233   21.16%
      0     0     5952.5012   104     7530.0000      Cuts: 40     1295   20.95%
      0     0     6076.6258   104     7530.0000      Cuts: 52     1412   19.30%
      0     0     6434.8803   104     7530.0000      Cuts: 51     1606   14.54%
      0     0     6485.8110   104     7530.0000      Cuts: 49     1753   13.87%
      0     0     6497.6616   104     7530.0000      Cuts: 40     1849   13.71%
      0     0     6505.3821   104     7530.0000      Cuts: 61     1942   13.61%
      0     0     6510.9007   104     7530.0000      Cuts: 55     2047   13.53%
      0     0     6515.0357   104     7530.0000      Cuts: 38     2100   13.48%
      0     0     6515.7960   104     7530.0000      Cuts: 12     2131   13.47%
      0     0     6520.1931   104     7530.0000      Cuts: 30     2223   13.41%
Dentro lazy
camion 0, TRUE-heu 0.000137
camion 1, TRUE-heu 0.000174
Salgo lazy
*     0+    0                         7389.0000     6520.1931            11.76%
      0     0  -1.00000e+75     0     7389.0000     6520.1931     2223   11.76%
      0     0     6524.5210   104     7389.0000      Cuts: 34     2336   11.70%
      0     0     6525.2071   104     7389.0000      Cuts: 28     2459   11.69%
      0     0     6525.3663   104     7389.0000       Cuts: 7     2493   11.69%
Dentro lazy
camion 0, TRUE-heu 0.000221
camion 1, TRUE-heu 0.000258
Salgo lazy
*     0+    0                         7344.0000     6525.3663            11.15%
      0     0  -1.00000e+75     0     7344.0000     6525.3663     2493   11.15%
      0     2     6606.3465    75     7344.0000     6525.3663     2493   11.15%                        0             0
Elapsed time = 2.27 sec. (708.53 ticks, tree = 0.02 MB, solutions = 3)
     42    17     7140.9333    29     7344.0000     6696.8427     4673    8.81%         X_1_9_2 D    128    120     14
Dentro lazy
camion 0, TRUE-heu 0.000299
camion 1, TRUE-heu 0.000331
Salgo lazy
*    66    21      integral     0     7256.0000     6696.8427     5619    7.71%
Dentro lazy
camion 0, TRUE-heu 0.000376
camion 1, TRUE-heu 0.000409
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000446
camion 1, TRUE-heu 0.000477
Salgo lazy
*   160+   36                         7188.0000     6843.5783             4.79%
*   192    40      integral     0     7147.0000     6843.5783    13016    4.25%

Cover cuts applied:  11
Flow cuts applied:  53
Mixed integer rounding cuts applied:  22
Zero-half cuts applied:  31
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    2.00 sec. (707.93 ticks)
Parallel b&c, 8 threads:
  Real time             =    2.95 sec. (474.43 ticks)
  Sync time (average)   =    1.93 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.95 sec. (1182.36 ticks)
4.968000 

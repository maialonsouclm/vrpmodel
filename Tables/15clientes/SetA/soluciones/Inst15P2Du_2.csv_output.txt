Instances\Inst15P2Du_2.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000089
camion 1, TRUE-heu 0.000129
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7506.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 547 rows and 62 columns.
MIP Presolve modified 2069 coefficients.
Reduced MIP has 735 rows, 2850 columns, and 21893 nonzeros.
Reduced MIP has 2400 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (18.95 ticks)
Clique table members: 156.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.08 sec. (48.40 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7506.0000        0.0000           100.00%
      0     0     4852.4883   130     7506.0000     4852.4883     1128   35.35%
      0     0     5762.5218   130     7506.0000     Cuts: 124     1929   23.23%
      0     0     5769.8188   130     7506.0000      Cuts: 74     2168   23.13%
      0     0     5772.6140   130     7506.0000      Cuts: 61     2319   23.09%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 17530 rows and 34 columns.
MIP Presolve modified 5402 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 4232 rows, 685 columns, and 87405 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.81 sec. (857.13 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 4232 rows, 685 columns, and 87405 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (123.18 ticks)
Clique table members: 4170.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.20 sec. (70.73 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   148                      0.0000      433         
      0     0        0.0000   148                    Cuts: 37      855         
Detecting symmetries...
      0     2        0.0000     6                      0.0000      855         
Elapsed time = 6.69 sec. (3833.23 ticks, tree = 0.02 MB, solutions = 0)
    202    51    infeasible                            0.0000    11245         
    384     9        0.0000    14                      0.0000    25206         
    631    20    infeasible                            0.0000    42992         
    928    32    infeasible                            0.0000    63489         
   1173    21        0.0000    71                      0.0000    87755         
   1461    56        0.0000    68                      0.0000   105590         
   1685    47        0.0000    59                      0.0000   139455         
   1770    10        0.0000    34                      0.0000   155585         
   1783    32    infeasible                            0.0000   146817         

Clique cuts applied:  105
Zero-half cuts applied:  1

Root node processing (before b&c):
  Real time             =    4.88 sec. (3831.24 ticks)
Parallel b&c, 12 threads:
  Real time             =   19.20 sec. (2156.71 ticks)
  Sync time (average)   =   13.87 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   24.08 sec. (5987.96 ticks)
24.093000 
FALSE-modelo
 Salgo lazy
      0     0     5779.6802   130     7506.0000      Cuts: 91     2471   23.00%
      0     0     5793.1999   130     7506.0000      Cuts: 63     2594   22.82%
      0     0     5830.3548   130     7506.0000      Cuts: 64     2749   22.32%
      0     0     5879.5855   130     7506.0000      Cuts: 74     2943   21.67%
      0     0     5897.4073   130     7506.0000      Cuts: 88     3137   21.43%
      0     0     5907.3964   130     7506.0000      Cuts: 77     3369   21.30%
      0     0     5989.4872   130     7506.0000      Cuts: 92     3552   20.20%
      0     0     6046.1452   130     7506.0000      Cuts: 98     3652   19.45%
      0     0     6111.9709   130     7506.0000      Cuts: 84     3753   18.57%
      0     0     6229.0647   130     7506.0000      Cuts: 95     3910   17.01%
      0     0     6344.1515   130     7506.0000      Cuts: 83     4237   15.48%
      0     0     6371.5871   130     7506.0000      Cuts: 85     4420   15.11%
      0     0     6384.4532   130     7506.0000      Cuts: 68     4500   14.94%
      0     0     6386.0795   130     7506.0000      Cuts: 45     4682   14.92%
      0     0     6386.7456   130     7506.0000      Cuts: 45     4731   14.91%
      0     0     6396.5633   130     7506.0000      Cuts: 46     4859   14.78%
      0     0     6422.8204   130     7506.0000      Cuts: 74     5072   14.43%
      0     0     6428.5290   130     7506.0000      Cuts: 54     5243   14.35%
      0     0     6428.6366   130     7506.0000      Cuts: 81     5334   14.35%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 22114 rows and 34 columns.
MIP Presolve modified 5020 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 4449 rows, 717 columns, and 101547 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 1.59 sec. (1586.87 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 4449 rows, 717 columns, and 101547 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (142.62 ticks)
Clique table members: 4387.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.23 sec. (113.91 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   202                      0.0000      662         
      0     0        0.0000   202                Cliques: 134     2404         
      0     0        cutoff                                       4646         
Elapsed time = 9.31 sec. (9480.52 ticks, tree = 0.01 MB, solutions = 0)

Clique cuts applied:  142

Root node processing (before b&c):
  Real time             =    9.31 sec. (9480.84 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    9.31 sec. (9480.84 ticks)
9.328000 
FALSE-modelo
 Salgo lazy
      0     2     6568.5905   101     7506.0000     6428.6366     5334   14.35%                        0             0
Elapsed time = 36.97 sec. (1485.52 ticks, tree = 0.02 MB, solutions = 1)
      4     6     6595.0605   104     7506.0000     6568.5905     5890   12.49%        X_1_10_5 D     32     24      4
Dentro lazy
camion 0, TRUE-heu 0.000265
camion 1, TRUE-heu 0.000297
Salgo lazy
     67    53     6920.0690    82     7506.0000     6583.8222     9552   12.29%        X_1_6_15 D    160    152     19
*   109+    6                         7194.0000     6595.8273             8.31%
    312   234     7101.9079    68     7194.0000     6631.6102    19078    7.82%        X_1_7_11 D    616    608     40
    656   496     6889.5457    68     7194.0000     6631.6102    34075    7.82%         X_0_0_6 D   1254   1246     25
   1019   649     6935.0980    75     7194.0000     6674.1900    44883    7.23%       X_1_13_12 D   1012   1004     11
   1354   934     6932.6800    52     7194.0000     6720.6614    71602    6.58%       X_1_13_12 D   1613   1605     17
Dentro lazy
camion 0, TRUE-heu 0.000333
camion 1, TRUE-heu 0.000364
Salgo lazy
   1664  1091     6979.1603   102     7194.0000     6760.4143    90973    6.03%        X_1_15_7 D   1861   1853     15
Dentro lazy
camion 0, TRUE-heu 0.000398
camion 1, TRUE-heu 0.000426
Salgo lazy
*  1700  1068      integral     0     7153.0000     6761.4315    89152    5.47%
   1938  1156     6976.8471    74     7153.0000     6776.3809   112478    5.27%         X_1_7_0 U   1833   1817     13
   2260  1224     6923.5783    70     7153.0000     6815.8122   131689    4.71%         X_1_3_7 D   2704   2696     18
   3637  1481     6999.3792    76     7153.0000     6906.6936   226264    3.44%         X_1_7_0 N   4310    627     19
Elapsed time = 43.72 sec. (4612.87 ticks, tree = 8.01 MB, solutions = 4)
   5423   676        cutoff           7153.0000     7022.2485   305861    1.83%        X_0_7_15 U   5471   1100     17
Dentro lazy
camion 0, TRUE-heu 0.000460
camion 1, TRUE-heu 0.000488
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000529
camion 1, TRUE-heu 0.000558
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000592
camion 1, TRUE-heu 0.000620
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000654
camion 1, TRUE-heu 0.000694
Salgo lazy
*  6105   104      integral     0     7147.0000     7124.6667   324001    0.31%

GUB cover cuts applied:  30
Cover cuts applied:  240
Flow cuts applied:  125
Mixed integer rounding cuts applied:  49
Zero-half cuts applied:  51
Gomory fractional cuts applied:  1
User cuts applied:  2

Root node processing (before b&c):
  Real time             =   36.59 sec. (1463.05 ticks)
Parallel b&c, 8 threads:
  Real time             =   11.33 sec. (4414.62 ticks)
  Sync time (average)   =    4.35 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   47.92 sec. (5877.67 ticks)
47.921000 

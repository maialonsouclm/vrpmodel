Instances\Inst15P2Du_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000100
camion 1, TRUE-heu 0.000146
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7506.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 547 rows and 62 columns.
MIP Presolve modified 2069 coefficients.
Reduced MIP has 735 rows, 2850 columns, and 21893 nonzeros.
Reduced MIP has 2400 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (18.95 ticks)
Clique table members: 156.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.08 sec. (64.13 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7506.0000        0.0000           100.00%
      0     0     4850.8668   141     7506.0000     4850.8668     1441   35.37%
      0     0     5690.7537   141     7506.0000     Cuts: 119     2364   24.18%
      0     0     5717.1735   141     7506.0000     Cuts: 112     2521   23.83%
      0     0     5929.4941   141     7506.0000      Cuts: 79     3480   21.00%
      0     0     5961.0082   141     7506.0000     Cuts: 127     3784   20.58%
      0     0     5966.0576   141     7506.0000      Cuts: 81     4096   20.52%
      0     0     5970.2819   141     7506.0000      Cuts: 78     4232   20.46%
      0     0     6003.4764   141     7506.0000      Cuts: 69     4447   20.02%
      0     0     6008.2808   141     7506.0000      Cuts: 57     4539   19.95%
      0     0     6035.2852   141     7506.0000      Cuts: 81     4693   19.59%
      0     0     6101.9222   141     7506.0000      Cuts: 64     5497   18.71%
      0     0     6108.1073   141     7506.0000      Cuts: 82     5570   18.62%
      0     0     6120.3301   141     7506.0000      Cuts: 65     5696   18.46%
      0     0     6183.8598   141     7506.0000      Cuts: 65     5928   17.61%
      0     0     6198.8639   141     7506.0000      Cuts: 72     6040   17.41%
      0     0     6208.1078   141     7506.0000      Cuts: 47     6099   17.29%
      0     0     6233.9374   141     7506.0000      Cuts: 58     6227   16.95%
      0     0     6259.8939   141     7506.0000      Cuts: 85     6355   16.60%
      0     0     6304.3927   141     7506.0000     Cuts: 109     6708   16.01%
      0     0     6318.1850   141     7506.0000     Cuts: 110     6985   15.82%
      0     0     6324.4503   141     7506.0000      Cuts: 91     7218   15.74%
      0     0     6328.5874   141     7506.0000      Cuts: 90     7487   15.69%
      0     0     6345.8840   141     7506.0000      Cuts: 65     7691   15.46%
      0     0     6349.5815   141     7506.0000      Cuts: 63     7890   15.41%
      0     0     6352.2103   141     7506.0000      Cuts: 52     8095   15.37%
      0     0     6369.5900   141     7506.0000      Cuts: 73     8377   15.14%
      0     0     6370.0952   141     7506.0000      Cuts: 55     8456   15.13%
      0     0     6370.1781   141     7506.0000      Cuts: 29     8509   15.13%
      0     2     6370.1781   117     7506.0000     6370.1781     8509   15.13%                        0             0
Elapsed time = 5.23 sec. (1668.59 ticks, tree = 0.02 MB, solutions = 1)
      3     3     6546.3201   110     7506.0000     6545.5833    10233   12.80%        X_1_10_5 D     16      8      2
Dentro lazy
camion 0, TRUE-heu 0.000183
camion 1, TRUE-heu 0.000218
Salgo lazy
*    19+    2                         7455.0000     6546.3201            12.19%
     19     4     6617.2008    69     7455.0000     6546.3201    10714   12.19%       X_0_10_14 N      6     16      3
    125    79     6679.6853    93     7455.0000     6569.6877    25275   11.88%         X_0_9_4 D    204    196      9
Dentro lazy
camion 0, TRUE-heu 0.000257
camion 1, TRUE-heu 0.000303
Salgo lazy
    274   188        cutoff           7455.0000     6599.5826    42835   11.47%         X_1_3_7 U    376    360     37
*   341   187      integral     0     7371.0000     6599.5826    43016   10.47%
    537   323     7068.8574    49     7371.0000     6634.2086    64379   10.00%        X_1_9_15 D    701    693     29
Dentro lazy
camion 0, TRUE-heu 0.000338
camion 1, TRUE-heu 0.000369
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000405
camion 1, TRUE-heu 0.000433
Salgo lazy
*   628   362      integral     0     7253.0000     6650.0893    72989    8.31%
    766   417     6968.9490    60     7253.0000     6686.2814    87193    7.81%         X_0_8_4 D    643    635     13
   1018   543        cutoff           7253.0000     6727.3354   113161    7.25%        X_1_0_15 U   1221   1213     10
Dentro lazy
camion 0, TRUE-heu 0.000467
camion 1, TRUE-heu 0.000499
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000539
camion 1, TRUE-heu 0.000570
Salgo lazy
*  1196   615      integral     0     7188.0000     6751.2587   134666    6.08%
   1248   589     7153.9700    55     7188.0000     6761.7900   146249    5.93%        X_1_12_9 D   1124   1116     16
   1500   617     6926.8597    88     7188.0000     6805.2571   168551    5.32%         X_1_7_0 N   1789    861     13
Dentro lazy
camion 0, TRUE-heu 0.000604
camion 1, TRUE-heu 0.000640
Salgo lazy
*  1699   677      integral     0     7185.0000     6844.6307   196718    4.74%
Dentro lazy
camion 0, TRUE-heu 0.000678
camion 1, TRUE-heu 0.000708
Salgo lazy
*  2010   698      integral     0     7153.0000     6913.2731   229615    3.35%         X_1_0_7 D   2093   2085     15
Elapsed time = 11.16 sec. (4384.32 ticks, tree = 0.86 MB, solutions = 8)
Dentro lazy
camion 0, TRUE-heu 0.000756
camion 1, TRUE-heu 0.000787
Salgo lazy
*  2061   628      integral     0     7147.0000     6925.3074   244528    3.10%
   3061   293        cutoff           7147.0000     7074.9606   343236    1.01%       X_1_12_13 U   3246   2105     25

GUB cover cuts applied:  23
Cover cuts applied:  247
Flow cuts applied:  138
Mixed integer rounding cuts applied:  52
Zero-half cuts applied:  51
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    4.81 sec. (1641.39 ticks)
Parallel b&c, 8 threads:
  Real time             =    9.72 sec. (3825.40 ticks)
  Sync time (average)   =    3.88 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   14.53 sec. (5466.78 ticks)
14.531000 

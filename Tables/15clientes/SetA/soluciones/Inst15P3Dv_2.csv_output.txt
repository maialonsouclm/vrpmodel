Instances\Inst15P3Dv_2.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000036
camion 1, TRUE-heu 0.000074
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7894.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 546 rows and 62 columns.
MIP Presolve modified 3989 coefficients.
Reduced MIP has 766 rows, 4770 columns, and 39220 nonzeros.
Reduced MIP has 4320 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (29.53 ticks)
Clique table members: 186.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.14 sec. (151.99 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7894.0000        0.0000           100.00%
      0     0     4745.2446   147     7894.0000     4745.2446     2621   39.89%
      0     0     5654.8256   147     7894.0000      Cuts: 79     4855   28.37%
      0     0     5698.1222   147     7894.0000      Cuts: 84     5268   27.82%
      0     0     5731.8106   147     7894.0000     Cuts: 102     5681   27.39%
      0     0     5931.6493   147     7894.0000     Cuts: 100     6502   24.86%
      0     0     5948.4676   147     7894.0000     Cuts: 107     7067   24.65%
      0     0     5974.9321   147     7894.0000      Cuts: 82     7631   24.31%
      0     0     5988.3923   147     7894.0000      Cuts: 97     7976   24.14%
      0     0     5999.9584   147     7894.0000      Cuts: 70     8252   23.99%
      0     0     6009.6967   147     7894.0000      Cuts: 61     8658   23.87%
      0     0     6020.8653   147     7894.0000      Cuts: 86     9026   23.73%
      0     0     6029.3049   147     7894.0000      Cuts: 81     9265   23.62%
      0     0     6045.4610   147     7894.0000      Cuts: 79     9498   23.42%
      0     0     6052.9147   147     7894.0000      Cuts: 64     9725   23.32%
      0     0     6086.0836   147     7894.0000      Cuts: 99    10072   22.90%
      0     0     6088.6980   147     7894.0000     Cuts: 105    10365   22.87%
      0     0     6090.0460   147     7894.0000      Cuts: 68    10627   22.85%
      0     0     6105.4283   147     7894.0000      Cuts: 95    10956   22.66%
      0     0     6113.7600   147     7894.0000      Cuts: 75    11303   22.55%
      0     0     6113.9636   147     7894.0000      Cuts: 71    11478   22.55%
      0     0     6116.1041   147     7894.0000      Cuts: 49    11668   22.52%
      0     0     6137.5243   147     7894.0000      Cuts: 71    12030   22.25%
      0     0     6146.2935   147     7894.0000      Cuts: 58    12352   22.14%
      0     0     6158.0198   147     7894.0000      Cuts: 56    12529   21.99%
      0     0     6163.5426   147     7894.0000      Cuts: 49    12640   21.92%
      0     0     6176.8799   147     7894.0000      Cuts: 74    12994   21.75%
      0     0     6203.4758   147     7894.0000      Cuts: 87    13559   21.42%
      0     0     6207.4108   147     7894.0000      Cuts: 91    13898   21.37%
      0     0     6272.2128   147     7894.0000      Cuts: 49    14676   20.54%
      0     0     6321.9581   147     7894.0000      Cuts: 83    14905   19.91%
      0     0     6337.0089   147     7894.0000      Cuts: 50    15393   19.72%
      0     0     6338.6694   147     7894.0000      Cuts: 68    15601   19.70%
Dentro lazy
camion 0, TRUE-heu 0.000111
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7372 rows and 34 columns.
MIP Presolve modified 4619 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.69 sec. (786.75 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (124.15 ticks)
Clique table members: 2820.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.20 sec. (122.48 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   228                      0.0000      793         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      950    0.00%
Elapsed time = 4.63 sec. (3326.73 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  18

Root node processing (before b&c):
  Real time             =    4.63 sec. (3327.03 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.63 sec. (3327.03 ticks)
4.641000 
TRUE-modelo
Salgo lazy
*     0+    0                         7754.0000     6338.6694            18.25%
Dentro lazy
camion 0, TRUE-heu 0.000180
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7372 rows and 34 columns.
MIP Presolve modified 4619 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.70 sec. (762.19 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (124.36 ticks)
Clique table members: 2820.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.23 sec. (116.96 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   211                      0.0000      676         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1076    0.00%
Elapsed time = 4.17 sec. (3853.49 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  20

Root node processing (before b&c):
  Real time             =    4.19 sec. (3853.78 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.19 sec. (3853.78 ticks)
4.187000 
TRUE-modelo
Salgo lazy
*     0+    0                         7565.0000     6338.6694            16.21%
      0     0  -1.00000e+75     0     7565.0000     6338.6694    15601   16.21%
      0     2     6338.6694   172     7565.0000     6338.6694    15601   16.21%                        0             0
Elapsed time = 20.58 sec. (5010.06 ticks, tree = 0.02 MB, solutions = 3)
      2     3     6544.6857   157     7565.0000     6547.6359    17196   13.45%              Y1 U      8      0      1
      3     3     6595.3383   140     7565.0000     6547.6359    18846   13.45%        X_0_11_3 N     15      8      2
      7     5     6686.7289   116     7565.0000     6595.9555    21027   12.81%         X_0_0_7 U     24     16      3
Dentro lazy
camion 0, TRUE-heu 0.000261
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7370 rows and 34 columns.
MIP Presolve modified 5667 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2885 rows, 1005 columns, and 92205 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.72 sec. (806.81 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2885 rows, 1005 columns, and 92205 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (132.94 ticks)
Clique table members: 2821.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    1.23 sec. (1159.30 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.23 sec. (1159.30 ticks)
1.547000 
TRUE-modelo
Salgo lazy
     14     9     6897.5786   132     7565.0000     6598.6148    23524   12.77%         X_1_0_7 D     47     13      5
Dentro lazy
camion 0, TRUE-heu 0.000339
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7370 rows and 34 columns.
MIP Presolve modified 5585 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2884 rows, 973 columns, and 89135 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.72 sec. (771.92 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2884 rows, 973 columns, and 89135 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (128.10 ticks)
Clique table members: 2820.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.22 sec. (94.48 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   207                      0.0000      547         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      549    0.00%
Elapsed time = 4.30 sec. (2978.72 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  3

Root node processing (before b&c):
  Real time             =    4.30 sec. (2979.01 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.30 sec. (2979.01 ticks)
4.297000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000414
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7372 rows and 34 columns.
MIP Presolve modified 4619 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.70 sec. (748.51 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (124.38 ticks)
Clique table members: 2820.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.22 sec. (103.49 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   220                      0.0000      585         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      630    0.00%
Elapsed time = 3.31 sec. (2782.48 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  7

Root node processing (before b&c):
  Real time             =    3.31 sec. (2782.77 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.31 sec. (2782.77 ticks)
3.328000 
TRUE-modelo
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000531
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7372 rows and 34 columns.
MIP Presolve modified 4619 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.67 sec. (764.35 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (124.21 ticks)
Clique table members: 2820.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    1.19 sec. (1125.69 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.19 sec. (1125.69 ticks)
1.188000 
TRUE-modelo
Salgo lazy
*    23+    6                         7418.0000     6639.6190            10.49%
     24    16     6740.4689   107     7418.0000     6639.6190    26604   10.49%        X_0_1_11 D     45     37      8
     36    30     7016.6714    94     7418.0000     6639.6190    35156   10.49%        X_0_9_13 D     96     88     11
     55    38     6896.3549   108     7418.0000     6639.6190    41573   10.49%        X_0_5_10 D     54     46      8
     96    52     6916.2762    84     7418.0000     6639.6190    50412   10.49%        X_0_9_13 D     86     78     11
    141    88     6934.7349   129     7418.0000     6639.6190    62914   10.49%         X_0_7_1 D     58     50     10
    353   251     7071.1791    77     7418.0000     6697.8916   120380    9.71%        X_0_10_8 D    557    549     35
Elapsed time = 36.30 sec. (8346.05 ticks, tree = 0.40 MB, solutions = 7)
    887   493     7077.5000    31     7418.0000     6756.0626   155282    8.92%        X_0_12_8 D    992    984     41
   1345  1032     7399.8846    89     7418.0000     6756.0626   223028    8.92%         X_1_7_9 D   1549   1541     48
   1913  1353     6954.5698   107     7418.0000     6818.7724   265111    8.08%        X_1_7_13 D   2086   2078     16
   2479  1915     7203.7996    76     7418.0000     6835.5013   351894    7.85%         X_1_1_0 D   2924   2916     37
   2974  2195     7263.2064    69     7418.0000     6864.1429   409149    7.47%         X_1_7_3 D   2905   2897     21
   3533  2514     7110.9000    78     7418.0000     6885.1551   473074    7.18%        X_1_7_12 D   3272   3264     14
   4102  2925     7226.8442    71     7418.0000     6899.2997   538617    6.99%         X_1_7_3 D   3825   3817     20
   4546  3126     7149.7959    36     7418.0000     6915.9881   606198    6.77%        X_0_13_3 D   4485   4477     25
   5036  3347     7369.6329   126     7418.0000     6927.0799   674343    6.62%         X_1_7_6 U   4608   3956     11
Dentro lazy
camion 0, TRUE-heu 0.000609
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7372 rows and 34 columns.
MIP Presolve modified 4619 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.64 sec. (753.14 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2882 rows, 973 columns, and 87279 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (124.05 ticks)
Clique table members: 2820.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.22 sec. (108.98 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   213                      0.0000      652         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      674    0.00%
Elapsed time = 4.83 sec. (3062.42 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  5

Root node processing (before b&c):
  Real time             =    4.84 sec. (3062.71 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.84 sec. (3062.71 ticks)
4.843000 
TRUE-modelo
Salgo lazy
*  5187  3351      integral     0     7380.0000     6931.6948   679047    6.07%
   5533  3257     7244.6774    33     7380.0000     6938.5039   724566    5.98%         X_1_2_6 U   5072   5064     45
Elapsed time = 58.50 sec. (17900.73 ticks, tree = 20.46 MB, solutions = 8)
   5934  3485     7155.8450   107     7380.0000     6948.5797   788988    5.85%        X_0_0_15 U   5584    934     43
   6434  3649     7321.9361   107     7380.0000     6960.1161   864802    5.69%        X_0_5_10 D   6177   6169     35
   6866  3880     7290.2561    93     7380.0000     6972.3893   909233    5.52%        X_0_15_0 U   6778   6770     11
   7290  4141     7193.1590    83     7380.0000     6982.2964   995209    5.39%        X_0_0_15 D   7389   7381     26
   7726  4266     7265.5698    51     7380.0000     6992.0877  1036682    5.26%        X_0_7_13 U   7682     80     10
Dentro lazy
camion 0, TRUE-heu 0.000684
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7370 rows and 34 columns.
MIP Presolve modified 5671 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2885 rows, 1005 columns, and 92205 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.70 sec. (811.44 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2885 rows, 1005 columns, and 92205 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (132.43 ticks)
Clique table members: 2821.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    1.17 sec. (1186.33 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.17 sec. (1186.33 ticks)
1.171000 
TRUE-modelo
Salgo lazy
*  7930+ 4387                         7343.0000     6997.0804             4.71%
   8106  3964        cutoff           7343.0000     6999.9857  1105067    4.67%        X_0_15_0 D   7970   7954     11
   8549  4095     7101.6465   120     7343.0000     7015.2486  1156111    4.46%        X_0_15_0 D   8257   8249     17
   8907  4240     7232.3263   130     7343.0000     7028.8799  1229583    4.28%         X_0_7_6 D   8609   8601     31
   9189  4237        cutoff           7343.0000     7038.0833  1263529    4.15%        X_1_15_7 U   9436   9420     18
   9531  4273     7136.9083   136     7343.0000     7050.0951  1327880    3.99%         X_0_9_1 D  10112  10104     23
Elapsed time = 77.48 sec. (27475.55 ticks, tree = 27.87 MB, solutions = 9)
   9824  4350        cutoff           7343.0000     7058.6111  1386267    3.87%        X_1_0_15 U   9927   9919     14
Dentro lazy
camion 0, TRUE-heu 0.000759
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7734 rows and 34 columns.
MIP Presolve modified 3639 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2521 rows, 973 columns, and 81634 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 1.17 sec. (1306.18 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2521 rows, 973 columns, and 81634 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (115.40 ticks)
Clique table members: 2459.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.14 sec. (80.93 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   190                      0.0000      495         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1338    0.00%
Elapsed time = 3.83 sec. (3248.82 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  325
Zero-half cuts applied:  1

Root node processing (before b&c):
  Real time             =    3.83 sec. (3249.13 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.83 sec. (3249.13 ticks)
3.843000 
TRUE-modelo
Salgo lazy
*  9985  4355      integral     0     7300.0000     7064.7333  1414298    3.22%
  10175  3609     7217.3190   180     7300.0000     7066.5528  1457219    3.20%        X_1_7_14 D  10412  10404     26
  10496  3590        cutoff           7300.0000     7076.9661  1508227    3.06%        X_1_0_15 U  10057   8481     16
  10818  3557     7147.3784   108     7300.0000     7089.7491  1551003    2.88%         X_1_0_7 N  11528   2862     14
  11189  3518     7140.5864   142     7300.0000     7100.0000  1600124    2.74%         X_0_4_0 N  11840   2881     19
  11612  3476        cutoff           7300.0000     7115.5439  1678842    2.53%        X_1_15_0 U  11097   7711     11
  12065  3406     7268.3262    49     7300.0000     7128.5368  1722202    2.35%         X_1_6_0 D  12084  12076     30
  12551  3324        cutoff           7300.0000     7141.4050  1778668    2.17%         X_0_9_8 U  12629   4517     28
  13079  3206     7212.1293    96     7300.0000     7157.5506  1826394    1.95%        X_0_12_6 D  13261  13253     36
  13647  3132        cutoff           7300.0000     7164.7163  1894794    1.85%         X_1_7_2 D  13644  13636     25
Elapsed time = 99.91 sec. (37044.38 ticks, tree = 22.27 MB, solutions = 10)
  14251  3021        cutoff           7300.0000     7180.6372  1950913    1.64%         X_0_6_4 U  15008  12503     39
  14828  2750        cutoff           7300.0000     7191.8223  2011622    1.48%        X_0_12_8 U  13843  10251     54
  15408  2558        cutoff           7300.0000     7203.6470  2052029    1.32%         X_0_4_6 U  14855   5468     29
  16183  2153     7258.6447    50     7300.0000     7222.4815  2111150    1.06%        X_1_1_11 D  15599  15591     25
  16826  1655        cutoff           7300.0000     7237.7648  2159309    0.85%       X_0_12_15 U  17712   8266     39
  17676   932        cutoff           7300.0000     7259.7867  2208045    0.55%         X_1_7_2 U  17308   2390     21

GUB cover cuts applied:  68
Cover cuts applied:  1304
Flow cuts applied:  165
Mixed integer rounding cuts applied:  120
Zero-half cuts applied:  52
Lift and project cuts applied:  1
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =   20.33 sec. (4946.71 ticks)
Parallel b&c, 8 threads:
  Real time             =   94.97 sec. (38530.30 ticks)
  Sync time (average)   =   17.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  115.30 sec. (43477.01 ticks)
115.297000 

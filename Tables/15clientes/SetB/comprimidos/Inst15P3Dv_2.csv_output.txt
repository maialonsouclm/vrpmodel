Instances\Inst15P3Dv_2.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000033
camion 1, TRUE-heu 0.000066
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7728.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 549 rows and 62 columns.
MIP Presolve modified 3925 coefficients.
Reduced MIP has 764 rows, 4770 columns, and 39126 nonzeros.
Reduced MIP has 4320 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (29.17 ticks)
Clique table members: 186.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.16 sec. (149.70 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7728.0000        0.0000           100.00%
      0     0     4803.3579   142     7728.0000     4803.3579     2502   37.84%
      0     0     5732.6246   142     7728.0000     Cuts: 105     4050   25.82%
      0     0     5802.2489   142     7728.0000      Cuts: 71     4962   24.92%
      0     0     5825.2948   142     7728.0000      Cuts: 90     5596   24.62%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 10908 rows and 34 columns.
MIP Presolve modified 3813 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2707 rows, 973 columns, and 92532 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 1.50 sec. (1503.12 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2707 rows, 973 columns, and 92532 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (132.74 ticks)
Clique table members: 2643.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.20 sec. (103.49 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   194                      0.0000      663         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      695    0.00%
Elapsed time = 3.80 sec. (3008.82 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  7
Zero-half cuts applied:  1

Root node processing (before b&c):
  Real time             =    3.81 sec. (3009.13 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.81 sec. (3009.13 ticks)
3.812000 
TRUE-modelo
camion 1, TRUE-heu 0.000151
Salgo lazy
*     0+    0                         7724.0000     5825.2948            24.58%
      0     0  -1.00000e+75     0     7724.0000     5825.2948     5596   24.58%
      0     0     5832.6131   142     7724.0000      Cuts: 71     5870   24.49%
      0     0     5844.7676   142     7724.0000      Cuts: 98     6300   24.33%
      0     0     5862.0880   142     7724.0000     Cuts: 116     6954   24.11%
      0     0     6037.6503   142     7724.0000      Cuts: 91     8141   21.83%
      0     0     6075.7720   142     7724.0000      Cuts: 95     9068   21.34%
      0     0     6087.2365   142     7724.0000      Cuts: 84     9250   21.19%
      0     0     6095.3455   142     7724.0000      Cuts: 46     9421   21.09%
      0     0     6109.1443   142     7724.0000      Cuts: 29     9574   20.91%
      0     0     6125.2723   142     7724.0000      Cuts: 50     9756   20.70%
      0     0     6135.5972   142     7724.0000      Cuts: 54     9994   20.56%
      0     0     6148.3870   142     7724.0000      Cuts: 49    10237   20.40%
      0     0     6158.8314   142     7724.0000      Cuts: 46    10378   20.26%
      0     0     6162.8319   142     7724.0000      Cuts: 46    10485   20.21%
      0     0     6165.2071   142     7724.0000      Cuts: 43    10589   20.18%
      0     0     6172.3502   142     7724.0000      Cuts: 36    10773   20.09%
      0     0     6277.1836   142     7724.0000      Cuts: 26    10945   18.73%
      0     0     6297.9993   142     7724.0000      Cuts: 63    11110   18.46%
      0     0     6339.2353   142     7724.0000      Cuts: 40    11290   17.93%
      0     0     6356.1306   142     7724.0000      Cuts: 37    11471   17.71%
      0     0     6358.4220   142     7724.0000      Cuts: 46    11544   17.68%
      0     0     6359.9350   142     7724.0000      Cuts: 31    11671   17.66%
      0     0     6360.8322   142     7724.0000      Cuts: 13    11761   17.65%
Dentro lazy
camion 0, TRUE-heu 0.000192
camion 1, TRUE-heu 0.000226
Salgo lazy
*     0+    0                         7612.0000     6360.8322            16.44%
      0     0  -1.00000e+75     0     7612.0000     6360.8322    11761   16.44%
      0     2     6360.8322   238     7612.0000     6360.8322    11761   16.44%                        0             0
Elapsed time = 8.95 sec. (3459.34 ticks, tree = 0.02 MB, solutions = 3)
      1     3     6363.4349   236     7612.0000     6364.0209    11955   16.39%        X_0_5_14 D      8      0      1
      3     5     6396.4825   229     7612.0000     6365.9192    12669   16.37%         X_0_5_9 D     24     16      3
      5     7     6438.3840   220     7612.0000     6365.9192    13206   16.37%         X_0_5_4 D     40     32      5
      7     9     6910.0266   171     7612.0000     6365.9192    13868   16.37%         X_0_8_0 U     12      4      5
     18    14     6501.2332   202     7612.0000     6365.9192    15756   16.37%        X_0_10_4 D     18     10      8
Dentro lazy
camion 0, TRUE-heu 0.000264
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7421 rows and 34 columns.
MIP Presolve modified 3779 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2837 rows, 1069 columns, and 92435 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 1.28 sec. (1414.92 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2837 rows, 1069 columns, and 92435 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (132.16 ticks)
Clique table members: 2774.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    1.75 sec. (1767.41 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.75 sec. (1767.41 ticks)
1.765000 
TRUE-modelo
Salgo lazy
     30    22     6871.5481   138     7612.0000     6365.9192    18750   16.37%        X_1_11_3 U     42     26     10
Dentro lazy
camion 0, TRUE-heu 0.000345
camion 1, TRUE-heu 0.000388
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000425
camion 1, TRUE-heu 0.000457
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000496
camion 1, TRUE-heu 0.000554
Salgo lazy
     44    43     6946.6626   142     7612.0000     6365.9192    28155   16.37%        X_1_3_15 U     66     50     12
Dentro lazy
camion 0, TRUE-heu 0.000592
camion 1, TRUE-heu 0.000632
Salgo lazy
*    69+    2                         7586.0000     6365.9192            16.08%
     75    56     7000.0650   166     7586.0000     6365.9192    32967   16.08%       X_0_10_12 D    114     98     17
    110    83     7267.5609   145     7586.0000     6365.9192    36588   16.08%         X_1_7_3 D    173    165     19
*   114+    2                         7394.0000     6365.9192            13.90%
    414   269     7285.8045   115     7394.0000     6424.7460    67368   13.11%        X_1_12_7 D    447    439     43
Elapsed time = 16.81 sec. (6795.30 ticks, tree = 1.25 MB, solutions = 5)
Dentro lazy
camion 0, TRUE-heu 0.000669
camion 1, TRUE-heu 0.000703
Salgo lazy
*   580   406      integral     0     7184.0000     6424.7460    84042   10.57%
    758   319     6670.4526   155     7184.0000     6536.5986    93541    9.01%         X_1_9_4 N    945     80     11
   1099   606     7125.7195   170     7184.0000     6574.3966   137051    8.49%         X_1_4_5 D    670    662     33
   1542   713     6911.7491   189     7184.0000     6722.4054   186748    6.43%        X_1_3_11 D   1328   1312      8
   2213  1051     7121.7338   114     7184.0000     6804.5184   269815    5.28%         X_0_9_3 D   1939   1931     35
   3048  1299     7095.6080    90     7184.0000     6907.4304   340793    3.85%        X_1_13_7 D   3186   3178     30
   3861  1386        cutoff           7184.0000     6963.6123   408749    3.07%        X_0_12_3 U   3582    748     36
Dentro lazy
camion 0, TRUE-heu 0.000742
camion 1, TRUE-heu 0.000777
Salgo lazy
*  4688  1268      integral     0     7147.0000     7018.6866   461226    1.80%        X_1_15_2 U   5133   5125     22

GUB cover cuts applied:  62
Cover cuts applied:  163
Flow cuts applied:  80
Mixed integer rounding cuts applied:  28
Zero-half cuts applied:  49
Lift and project cuts applied:  1
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    8.50 sec. (3395.17 ticks)
Parallel b&c, 8 threads:
  Real time             =   24.09 sec. (10727.41 ticks)
  Sync time (average)   =    6.69 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   32.59 sec. (14122.58 ticks)
32.609000 

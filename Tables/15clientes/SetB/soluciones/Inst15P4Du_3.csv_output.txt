Instances\Inst15P4Du_3.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000032
camion 1, TRUE-heu 0.000079
camion 2, TRUE-heu 0.000099
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 8690.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 819 rows and 141 columns.
MIP Presolve modified 4102 coefficients.
Reduced MIP has 1099 rows, 5187 columns, and 41150 nonzeros.
Reduced MIP has 4512 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (32.32 ticks)
Clique table members: 229.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.13 sec. (147.07 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         8690.0000        0.0000           100.00%
      0     0     4800.2888   146     8690.0000     4800.2888       18   44.76%
      0     0     5353.1718   146     8690.0000     Cuts: 102      303   38.40%
      0     0     5647.5884   146     8690.0000     Cuts: 192      770   35.01%
      0     0     5704.3738   146     8690.0000      Cuts: 99     1145   34.36%
      0     0     5788.0585   146     8690.0000      Cuts: 61     1301   33.39%
      0     0     5864.4688   146     8690.0000      Cuts: 82     1517   32.51%
      0     0     5915.5779   146     8690.0000     Cuts: 195     1968   31.93%
      0     0     5990.1897   146     8690.0000     Cuts: 103     2539   31.07%
      0     0     6029.2451   146     8690.0000     Cuts: 170     2963   30.62%
      0     0     6041.1063   146     8690.0000     Cuts: 126     3171   30.48%
      0     0     6064.2483   146     8690.0000     Cuts: 141     3610   30.22%
      0     0     6082.6382   146     8690.0000      Cuts: 59     3957   30.00%
      0     0     6089.7817   146     8690.0000      Cuts: 77     4084   29.92%
      0     0     6109.1524   146     8690.0000      Cuts: 39     4228   29.70%
      0     0     6121.5139   146     8690.0000      Cuts: 58     4627   29.56%
      0     0     6150.1440   146     8690.0000      Cuts: 78     5219   29.23%
      0     0     6165.3260   146     8690.0000      Cuts: 65     5387   29.05%
      0     0     6223.0901   146     8690.0000      Cuts: 78     5742   28.39%
      0     0     6236.5323   146     8690.0000     Cuts: 121     6011   28.23%
      0     0     6292.3831   146     8690.0000      Cuts: 89     6813   27.59%
      0     0     6303.4183   146     8690.0000     Cuts: 129     7165   27.46%
      0     0     6303.9841   146     8690.0000     Cuts: 100     7289   27.46%
      0     0     6307.1109   146     8690.0000      Cuts: 19     7373   27.42%
      0     0     6307.8978   146     8690.0000      Cuts: 51     7499   27.41%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 23632 rows and 34 columns.
MIP Presolve modified 4039 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2934 rows, 717 columns, and 77306 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.67 sec. (870.37 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2934 rows, 717 columns, and 77306 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (109.03 ticks)
Clique table members: 2872.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.27 sec. (109.68 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   176                      0.0000      802         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1914    0.00%
Elapsed time = 2.81 sec. (3043.36 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  163

Root node processing (before b&c):
  Real time             =    2.81 sec. (3043.63 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.81 sec. (3043.63 ticks)
2.828000 
TRUE-modelo
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 10732 rows and 34 columns.
MIP Presolve modified 3947 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2872 rows, 653 columns, and 64644 nonzeros.
Reduced MIP has 653 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.53 sec. (664.73 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2872 rows, 653 columns, and 64644 nonzeros.
Reduced MIP has 653 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (92.27 ticks)
Clique table members: 2810.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (48.96 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   150                      0.0000      377         
      0     0        0.0000   150                 Cliques: 91     1284         
Detecting symmetries...
      0     2        0.0000     4                      0.0000     1284         
Elapsed time = 5.13 sec. (3060.94 ticks, tree = 0.02 MB, solutions = 0)
    304    88    infeasible                            0.0000    15472         
*   352    98      integral     0        0.0000        0.0000    18476    0.00%
    379    12        0.0000   171        0.0000        0.0000     5864    0.00%

Clique cuts applied:  108
Cover cuts applied:  3

Root node processing (before b&c):
  Real time             =    3.30 sec. (3047.15 ticks)
Parallel b&c, 12 threads:
  Real time             =    8.14 sec. (984.17 ticks)
  Sync time (average)   =    6.07 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   11.44 sec. (4031.32 ticks)
11.453000 
TRUE-modelo
Salgo lazy
*     0+    0                         8559.0000     6307.8978            26.30%
      0     0  -1.00000e+75     0     8559.0000     6307.8978     7499   26.30%
      0     2     6307.8978   253     8559.0000     6307.8978     7499   26.30%                        0             0
Elapsed time = 18.56 sec. (3676.82 ticks, tree = 0.02 MB, solutions = 2)
      1     3     6369.2728   225     8559.0000     6337.4735     8091   25.96%         X_0_0_7 U      8      0      1
      3     5     6422.9504   219     8559.0000     6337.4735     8587   25.96%         X_0_4_9 D     24     16      3
      5     6     6433.1906   229     8559.0000     6337.4735     8859   25.96%       X_1_10_14 D     32     24      4
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 18848 rows and 34 columns.
MIP Presolve modified 3991 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2917 rows, 685 columns, and 73965 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.63 sec. (827.18 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2917 rows, 685 columns, and 73965 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (104.77 ticks)
Clique table members: 2855.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.13 sec. (92.78 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   145                      0.0000      713         
      0     0        0.0000   145                   Cuts: 106     1850         
      0     0        0.0000   145                 Cliques: 19     2089         
      0     0        0.0000   145                   Cuts: 102     2331         
Detecting symmetries...
      0     2        0.0000    65                      0.0000     2331         
Elapsed time = 8.45 sec. (5045.11 ticks, tree = 0.02 MB, solutions = 0)
      4     4        0.0000    95                      0.0000     3483         
     11     4        0.0000    76                      0.0000     8554         
     24     5        0.0000   182                      0.0000     6227         
     26     4        0.0000    85                      0.0000    13284         
     47     5        0.0000   164                      0.0000    16110         
*    90    20      integral     0        0.0000        0.0000    19117    0.00%

Clique cuts applied:  197
Zero-half cuts applied:  3
Lift and project cuts applied:  1

Root node processing (before b&c):
  Real time             =    6.23 sec. (5020.42 ticks)
Parallel b&c, 12 threads:
  Real time             =   22.80 sec. (1858.39 ticks)
  Sync time (average)   =   20.50 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   29.03 sec. (6878.81 ticks)
29.250000 
TRUE-modelo
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 14088 rows and 34 columns.
MIP Presolve modified 4494 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 3356 rows, 685 columns, and 75634 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.55 sec. (721.32 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 3356 rows, 685 columns, and 75634 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (106.89 ticks)
Clique table members: 3294.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (59.21 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   146                      0.0000      442         
      0     0        0.0000   146                   Cuts: 157     2351         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     4178    0.00%
Elapsed time = 4.33 sec. (4183.76 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  139

Root node processing (before b&c):
  Real time             =    4.33 sec. (4184.02 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.33 sec. (4184.02 ticks)
4.328000 
TRUE-modelo
Salgo lazy
      8     8     6447.5604   233     8559.0000     6337.4735    10658   25.96%        X_0_7_11 D     40     32      5
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 13436 rows and 34 columns.
MIP Presolve modified 3174 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 4009 rows, 653 columns, and 69859 nonzeros.
Reduced MIP has 653 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.86 sec. (1128.02 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 4009 rows, 653 columns, and 69859 nonzeros.
Reduced MIP has 653 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (99.29 ticks)
Clique table members: 3947.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (72.87 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   130                      0.0000      554         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1733    0.00%
Elapsed time = 3.63 sec. (3314.26 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  159

Root node processing (before b&c):
  Real time             =    3.64 sec. (3314.51 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    3.64 sec. (3314.51 ticks)
3.656000 
TRUE-modelo
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 17180 rows and 34 columns.
MIP Presolve modified 3459 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 4584 rows, 717 columns, and 78731 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 1.02 sec. (1380.80 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 4584 rows, 717 columns, and 78731 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (111.36 ticks)
Clique table members: 4522.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.22 sec. (107.77 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   205                      0.0000      662         
      0     0        0.0000   205                   Cuts: 258     3072         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     5994    0.00%
Elapsed time = 7.50 sec. (6912.45 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  354
Zero-half cuts applied:  1

Root node processing (before b&c):
  Real time             =    7.50 sec. (6912.76 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    7.50 sec. (6912.76 ticks)
7.516000 
TRUE-modelo
Salgo lazy
     14    14     7031.2178    64     8559.0000     6337.4735    14488   25.96%        X_1_0_15 D     36     28      8
*    16+    2                         8550.0000     6337.4735            25.88%
*    20+    5                         8230.0000     6337.4735            23.00%
     23    19     7249.3205    51     8230.0000     6337.4735    16454   23.00%        X_1_8_15 U     68     52     11
     34    26     6429.8925   222     8230.0000     6349.9203    23306   22.84%         X_0_4_9 D     23     15      3
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 13441 rows and 34 columns.
MIP Presolve modified 3243 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 4005 rows, 685 columns, and 72238 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.84 sec. (1181.20 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 4005 rows, 685 columns, and 72238 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (102.29 ticks)
Clique table members: 3943.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.16 sec. (107.21 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   184                      0.0000      758         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1503    0.00%
Elapsed time = 4.27 sec. (4241.58 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  223

Root node processing (before b&c):
  Real time             =    4.27 sec. (4241.84 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.27 sec. (4241.84 ticks)
4.281000 
TRUE-modelo
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 13664 rows and 34 columns.
MIP Presolve modified 4949 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 3779 rows, 685 columns, and 78345 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.55 sec. (693.38 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 3779 rows, 685 columns, and 78345 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (110.91 ticks)
Clique table members: 3717.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.09 sec. (63.43 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   145                      0.0000      396         
      0     0        0.0000   145                 Cliques: 56      478         
      0     0        0.0000   145                   Cuts: 119      709         
      0     0        0.0000   145                     Cuts: 8      889         
Detecting symmetries...
      0     2        0.0000    38                      0.0000      889         
Elapsed time = 7.63 sec. (4361.72 ticks, tree = 0.02 MB, solutions = 0)
     20     8        0.0000    24                      0.0000     2153         
    781    45    infeasible                            0.0000    23089         
   1286    11        0.0000    37                      0.0000    48134         
   1622    42        0.0000    49                      0.0000    71605         
   1854    24        0.0000    66                      0.0000    94094         
   2073    17        0.0000    62                      0.0000   121504         
   2152    11        0.0000   209                      0.0000    48911         
   2153     3        0.0000   168                      0.0000   143126         
   2169    10        0.0000    60                      0.0000   149677         
   2961   160        0.0000    82                      0.0000   222546         
Elapsed time = 39.94 sec. (7774.64 ticks, tree = 0.26 MB, solutions = 0)
   3572   115    infeasible                            0.0000   317593         

Clique cuts applied:  141
Zero-half cuts applied:  2

Root node processing (before b&c):
  Real time             =    5.70 sec. (4359.91 ticks)
Parallel b&c, 12 threads:
  Real time             =   48.73 sec. (5154.05 ticks)
  Sync time (average)   =   38.41 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   54.44 sec. (9513.96 ticks)
54.453000 
FALSE-modelo
 Salgo lazy
     71    43     6438.6705   232     8230.0000     6349.9203    33820   22.84%       X_1_10_14 D     47     39      6
     92    69     6600.2627   207     8230.0000     6349.9203    41409   22.84%        X_0_11_0 D     94     86     12
    254   145     7625.9675    39     8230.0000     6349.9203    67840   22.84%         X_2_5_6 U    556    540     21
Elapsed time = 126.66 sec. (7181.40 ticks, tree = 0.43 MB, solutions = 3)
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 10678 rows and 34 columns.
MIP Presolve modified 4065 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2927 rows, 685 columns, and 75512 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.77 sec. (989.58 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2927 rows, 685 columns, and 75512 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (106.48 ticks)
Clique table members: 2865.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (64.74 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   151                      0.0000      485         
      0     0        0.0000   151                Cliques: 158     2122         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     3749    0.00%
Elapsed time = 4.28 sec. (4199.10 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  111

Root node processing (before b&c):
  Real time             =    4.28 sec. (4199.36 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.28 sec. (4199.36 ticks)
4.281000 
TRUE-modelo
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 13623 rows and 34 columns.
MIP Presolve modified 3285 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 3821 rows, 685 columns, and 69948 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.84 sec. (1186.44 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 3821 rows, 685 columns, and 69948 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (99.45 ticks)
Clique table members: 3759.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.11 sec. (65.23 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   152                      0.0000      439         
      0     0        0.0000   152                Cliques: 121     1340         
      0     0        0.0000   152                Cliques: 104     2719         
Detecting symmetries...
      0     2        0.0000    66                      0.0000     2727         
Elapsed time = 11.11 sec. (6590.15 ticks, tree = 0.02 MB, solutions = 0)
    449   138        0.0000    27                      0.0000    14808         
   1498   195    infeasible                            0.0000    38790         
   2658   210        0.0000    32                      0.0000    63598         
   3918    50        0.0000    56                      0.0000    93276         
   4859    13        0.0000    49                      0.0000   122634         
   5179    17        0.0000    53                      0.0000   135931         
   5365    63        0.0000    72                      0.0000   157439         
   5586   106    infeasible                            0.0000   171327         
   5751   202        0.0000    60                      0.0000   207582         
   6587   298        0.0000    81                      0.0000   296611         
Elapsed time = 29.83 sec. (9695.28 ticks, tree = 2.54 MB, solutions = 0)
   7208   170    infeasible                            0.0000   378224         
   7740   112        0.0000    86                      0.0000   475611         
   8199    73        0.0000    54                      0.0000   559175         

Clique cuts applied:  229

Root node processing (before b&c):
  Real time             =    9.23 sec. (6562.69 ticks)
Parallel b&c, 12 threads:
  Real time             =   37.72 sec. (6465.56 ticks)
  Sync time (average)   =   23.57 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   46.95 sec. (13028.25 ticks)
46.984000 
FALSE-modelo
 Salgo lazy
    635   356     7251.1340    92     8230.0000     6349.9203    98167   22.84%         X_1_9_4 D    957    941     13
   1059   756     7961.8680    30     8230.0000     6412.0899   142391   22.09%         X_0_3_0 D    691    683     33
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 7372 rows and 34 columns.
MIP Presolve modified 3915 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 2871 rows, 621 columns, and 54213 nonzeros.
Reduced MIP has 621 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.33 sec. (370.00 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 2871 rows, 621 columns, and 54213 nonzeros.
Reduced MIP has 621 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (78.10 ticks)
Clique table members: 2809.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.14 sec. (39.57 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   115                      0.0000      311         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1104    0.00%
Elapsed time = 2.39 sec. (1733.45 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  46

Root node processing (before b&c):
  Real time             =    2.39 sec. (1733.63 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.39 sec. (1733.63 ticks)
2.390000 
TRUE-modelo
camion 1, TRUE-heu 0.000510
Salgo lazy
*  1227+  829                         7623.0000     6412.0899            15.88%
   1564   735     7277.7946   129     7623.0000     6430.2937   195313   15.65%       X_0_13_15 D   2636   2628     91
   2122  1053     7506.1814   152     7623.0000     6501.9274   233905   14.71%        X_2_15_0 D   2286   2278     27
   2612  1513     7011.4784   206     7623.0000     6625.2337   283771   13.09%        X_2_3_11 D   2321   2313     19
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 10278 rows and 34 columns.
MIP Presolve modified 4560 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 3327 rows, 717 columns, and 72744 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.47 sec. (600.19 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 3327 rows, 717 columns, and 72744 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (103.39 ticks)
Clique table members: 3265.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.16 sec. (97.73 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   186                      0.0000      671         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     1691    0.00%
Elapsed time = 2.97 sec. (2757.37 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  71

Root node processing (before b&c):
  Real time             =    2.97 sec. (2757.62 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    2.97 sec. (2757.62 ticks)
2.984000 
TRUE-modelo
camion 1, TRUE-heu 0.000574
Salgo lazy
*  2815+ 1563                         7285.0000     6661.0878             8.56%
   3102   919        cutoff           7285.0000     6690.6073   341577    8.16%        X_2_4_10 U   2272   2015     12
   3466  1157     7064.3512   138     7285.0000     6741.7543   392502    7.46%         X_1_7_0 D   3925   3917      8
   3834  1329     7210.8797   117     7285.0000     6799.6552   446562    6.66%         X_2_0_7 U   3818   2635     11
   4229  1499     6982.2204   156     7285.0000     6848.3941   506183    5.99%         X_1_4_8 D   4250   4242     19
   4585  1664        cutoff           7285.0000     6877.2509   559156    5.60%        X_2_3_11 U   5900   3415     17
Elapsed time = 197.09 sec. (16763.25 ticks, tree = 11.17 MB, solutions = 6)
   5074  1832     7218.3302    45     7285.0000     6914.9141   627417    5.08%        X_0_10_5 U   5654   5646     32
   5522  1964        cutoff           7285.0000     6947.0359   689694    4.64%         X_1_8_7 U   4424   3525     25
   6030  2076        cutoff           7285.0000     6971.8448   749855    4.30%        X_0_1_14 U   5243   5235     22
   6457  2133        cutoff           7285.0000     6999.0842   797949    3.92%         X_1_8_0 U   5839   3986     24
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 10278 rows and 34 columns.
MIP Presolve modified 4560 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 3327 rows, 717 columns, and 72744 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.44 sec. (594.64 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 3327 rows, 717 columns, and 72744 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (103.22 ticks)
Clique table members: 3265.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.17 sec. (66.78 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   149                      0.0000      483         
      0     0        0.0000   149                Cliques: 134     1989         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000     2147    0.00%
Elapsed time = 4.73 sec. (4977.48 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  155

Root node processing (before b&c):
  Real time             =    4.73 sec. (4977.74 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    4.73 sec. (4977.74 ticks)
4.750000 
TRUE-modelo
camion 1, TRUE-heu 0.000635
Salgo lazy
*  6828+ 2142                         7275.0000     7018.2577             3.53%
   6901  2158     7076.2718   182     7275.0000     7020.2495   850758    3.50%         X_0_7_1 U   6737   1509     12
   7390  2084        cutoff           7275.0000     7058.0686   905651    2.98%         X_1_6_2 D   6539   6531     27
   7875  1995     7122.8659   134     7275.0000     7093.2508   951251    2.50%        X_2_3_11 D   7906   6753     15
   8398  1834        cutoff           7275.0000     7120.7250  1014112    2.12%         X_1_9_5 U   8322   5502     29
   9028  1541        cutoff           7275.0000     7154.3953  1074695    1.66%       X_1_15_12 U   8914   3824     35
   9862   979        cutoff           7275.0000     7202.6046  1125752    1.00%        X_1_3_11 U   9216   9200     35
Elapsed time = 216.92 sec. (26311.42 ticks, tree = 5.37 MB, solutions = 7)

GUB cover cuts applied:  60
Cover cuts applied:  498
Flow cuts applied:  171
Mixed integer rounding cuts applied:  70
Zero-half cuts applied:  83
Gomory fractional cuts applied:  3
User cuts applied:  2

Root node processing (before b&c):
  Real time             =   18.14 sec. (3606.17 ticks)
Parallel b&c, 8 threads:
  Real time             =  201.14 sec. (23404.10 ticks)
  Sync time (average)   =   38.79 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  219.28 sec. (27010.27 ticks)
219.282000 

Instances\Inst5P1Dv_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000030
camion 1, TRUE-heu 0.000057
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 6001.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 86 rows and 42 columns.
MIP Presolve modified 1079 coefficients.
Reduced MIP has 201 rows, 1050 columns, and 8860 nonzeros.
Reduced MIP has 1000 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (6.66 ticks)
Clique table members: 101.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.02 sec. (9.74 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         6001.0000        0.0000           100.00%
      0     0     4008.6913    53     6001.0000     4008.6913      462   33.20%
      0     0     5453.7000    53     6001.0000      Cuts: 40      562    9.12%
      0     0     5590.0000    53     6001.0000      Cuts: 31      610    6.85%
      0     0     5960.1053    53     6001.0000      Cuts: 38      641    0.68%
      0     0        cutoff           6001.0000                    646    0.00%                        0             0
Elapsed time = 0.13 sec. (64.39 ticks, tree = 0.01 MB, solutions = 1)

GUB cover cuts applied:  9
Flow cuts applied:  6
Mixed integer rounding cuts applied:  16
Zero-half cuts applied:  25
Gomory fractional cuts applied:  2

Root node processing (before b&c):
  Real time             =    0.13 sec. (64.45 ticks)
Parallel b&c, 8 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.13 sec. (64.45 ticks)
0.141000 

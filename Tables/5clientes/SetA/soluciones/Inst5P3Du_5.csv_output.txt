Instances\Inst5P3Du_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000070
camion 1, TRUE-heu 0.000091
camion 2, TRUE-heu 0.000109
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7663.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 129 rows and 111 columns.
MIP Presolve modified 5962 coefficients.
Reduced MIP has 339 rows, 5847 columns, and 51770 nonzeros.
Reduced MIP has 5772 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (39.26 ticks)
Clique table members: 189.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.14 sec. (149.98 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7663.0000        0.0000           100.00%
      0     0     3844.9944    80     7663.0000     3844.9944       12   49.82%
      0     0     4933.4763    80     7663.0000      Cuts: 38     1957   35.62%
      0     0     5213.0028    80     7663.0000      Cuts: 68     2629   31.97%
      0     0     5361.8639    80     7663.0000      Cuts: 41     3131   30.03%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 536 rows and 34 columns.
MIP Presolve modified 2807 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1072 rows, 973 columns, and 36587 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (144.20 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1072 rows, 973 columns, and 36587 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (38.31 ticks)
Clique table members: 1008.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (21.68 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   124                      0.0000      250         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      250    0.00%
Elapsed time = 0.75 sec. (498.27 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    0.75 sec. (498.42 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.75 sec. (498.42 ticks)
0.750000 
TRUE-modelo
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 538 rows and 34 columns.
MIP Presolve modified 2935 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1072 rows, 1037 columns, and 37119 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.11 sec. (148.89 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1072 rows, 1037 columns, and 37119 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (37.20 ticks)
Clique table members: 1010.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.27 sec. (275.78 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.27 sec. (275.78 ticks)
0.297000 
TRUE-modelo
Salgo lazy
*     0+    0                         6606.0000     5361.8639            18.83%
      0     0  -1.00000e+75     0     6606.0000     5361.8639     3131   18.83%
      0     0     5443.5068    80     6606.0000      Cuts: 54     3600   17.60%
      0     0     5547.0722    80     6606.0000      Cuts: 35     4286   16.03%
      0     0     5548.9907    80     6606.0000      Cuts: 27     4445   16.00%
      0     0     5548.9907    80     6606.0000      Cuts: 36     4542   16.00%
      0     0     5548.9907    80     6606.0000      Cuts: 25     4680   16.00%
      0     0     5548.9907    80     6606.0000      Cuts: 26     4809   16.00%
      0     0     5548.9907    80     6606.0000      Cuts: 38     5005   16.00%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 536 rows and 34 columns.
MIP Presolve modified 2807 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1072 rows, 973 columns, and 36587 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (134.80 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1072 rows, 973 columns, and 36587 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (36.96 ticks)
Clique table members: 1008.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.28 sec. (253.48 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.28 sec. (253.48 ticks)
0.296000 
TRUE-modelo
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 537 rows and 34 columns.
MIP Presolve modified 6469 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1073 rows, 1037 columns, and 37615 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (153.81 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1073 rows, 1037 columns, and 37615 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (37.21 ticks)
Clique table members: 1010.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.27 sec. (276.77 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.27 sec. (276.77 ticks)
0.266000 
TRUE-modelo
Salgo lazy
      0     2     5548.9907    71     6606.0000     5548.9907     5005   16.00%                        0             0
Elapsed time = 5.36 sec. (2282.67 ticks, tree = 0.02 MB, solutions = 2)
*     2+    1                         6323.0000     5573.2680            11.86%
      2     3     6207.0000    15     6323.0000     5573.2680     5720   11.86%         X_1_2_4 U      7      0      1
     19    11     6194.7801   105     6323.0000     5662.7307     8223   10.44%         X_0_3_4 D     79     71      8
     32     8     5791.9364    92     6323.0000     5670.3406     7604   10.32%         X_0_3_2 N      2     47      5
     54    17        cutoff           6323.0000     5679.3201    17489   10.18%         X_0_0_5 D     72     64      7
     81    18        cutoff           6323.0000     5714.3051    25688    9.63%         X_0_5_4 U     91     34     10
    147    18     5731.7449    92     6323.0000     5732.7445    26846    9.34%         X_1_2_3 D     97     89     12
    190    15     6220.6000    61     6323.0000     5740.5960    36997    9.21%         X_2_4_0 D    144    136     19
    268    12        cutoff           6323.0000     5822.7123    44339    7.91%         X_1_2_0 U    216    192     19

GUB cover cuts applied:  23
Cover cuts applied:  60
Flow cuts applied:  14
Mixed integer rounding cuts applied:  17
Zero-half cuts applied:  13
Gomory fractional cuts applied:  2

Root node processing (before b&c):
  Real time             =    4.14 sec. (2243.77 ticks)
Parallel b&c, 8 threads:
  Real time             =    5.11 sec. (2098.61 ticks)
  Sync time (average)   =    2.67 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    9.25 sec. (4342.38 ticks)
9.250000 

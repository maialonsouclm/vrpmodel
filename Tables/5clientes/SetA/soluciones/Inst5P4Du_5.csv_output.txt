Instances\Inst5P4Du_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000036
camion 1, TRUE-heu 0.000054
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 6001.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 86 rows and 42 columns.
MIP Presolve modified 2039 coefficients.
Reduced MIP has 216 rows, 2010 columns, and 17500 nonzeros.
Reduced MIP has 1960 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (12.70 ticks)
Clique table members: 116.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.02 sec. (25.43 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         6001.0000        0.0000           100.00%
      0     0     3835.4394    68     6001.0000     3835.4394      676   36.09%
      0     0     5443.6000    68     6001.0000      Cuts: 11     1256    9.29%
      0     0     5507.5000    68     6001.0000      Cuts: 34     1479    8.22%
      0     0     5909.3333    68     6001.0000      Cuts: 37     1572    1.53%
      0     0        cutoff           6001.0000                   1619    0.00%                        0             0
Elapsed time = 0.19 sec. (145.37 ticks, tree = 0.01 MB, solutions = 1)

GUB cover cuts applied:  9
Flow cuts applied:  10
Mixed integer rounding cuts applied:  8
Zero-half cuts applied:  16
Lift and project cuts applied:  1

Root node processing (before b&c):
  Real time             =    0.19 sec. (145.47 ticks)
Parallel b&c, 8 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.19 sec. (145.47 ticks)
0.188000 

Instances\Inst5P2Du_5.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000030
camion 1, TRUE-heu 0.000086
camion 2, TRUE-heu 0.000106
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7663.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 129 rows and 111 columns.
MIP Presolve modified 4042 coefficients.
Reduced MIP has 319 rows, 3927 columns, and 34490 nonzeros.
Reduced MIP has 3852 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (26.85 ticks)
Clique table members: 169.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.08 sec. (69.66 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7663.0000        0.0000           100.00%
      0     0     3879.5866    73     7663.0000     3879.5866     1410   49.37%
      0     0     4854.1175    73     7663.0000      Cuts: 46     1745   36.66%
      0     0     5137.6822    73     7663.0000      Cuts: 67     2038   32.95%
      0     0     5496.1019    73     7663.0000      Cuts: 67     2370   28.28%
      0     0     5542.2011    73     7663.0000      Cuts: 60     2484   27.68%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 1524 rows and 34 columns.
MIP Presolve modified 2748 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1515 rows, 717 columns, and 34692 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (150.29 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1515 rows, 717 columns, and 34692 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (41.97 ticks)
Clique table members: 1453.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.05 sec. (21.95 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    98                      0.0000      260         
Detecting symmetries...
      0     2        0.0000    10                      0.0000      260         
Elapsed time = 2.41 sec. (681.90 ticks, tree = 0.02 MB, solutions = 0)
   1000   349        0.0000    22                      0.0000    37822         
   2102   838        0.0000    37                      0.0000    91697         
   3030  1086        0.0000    16                      0.0000   139148         
   3975  1247        0.0000    38                      0.0000   190197         
   4722  1317        0.0000    31                      0.0000   230474         
   5370  1456        0.0000    51                      0.0000   278791         

Performing restart 1

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 2 rows and 0 columns.
MIP Presolve modified 217 coefficients.
Reduced MIP has 1513 rows, 717 columns, and 34624 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (48.49 ticks)
Represolve time = 0.06 sec. (58.13 ticks)
   5669     0        0.0000   120                     Cuts: 6   325283         
   5669     0        0.0000   120                   Cuts: 109   325352         
   5669     0        0.0000   120                   Cuts: 100   325467         
*  5669+    0                            0.0000        0.0000             0.00%
   5669     0        cutoff              0.0000        0.0000   325467    0.00%

Clique cuts applied:  22
Cover cuts applied:  2
Zero-half cuts applied:  3
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    0.80 sec. (681.14 ticks)
Parallel b&c, 12 threads:
  Real time             =   13.74 sec. (2945.74 ticks)
  Sync time (average)   =    7.05 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   14.53 sec. (3626.88 ticks)
14.531000 
TRUE-modelo
camion 1, TRUE-heu 0.000192
Salgo lazy
*     0+    0                         7396.0000     5542.2011            25.06%
      0     0  -1.00000e+75     0     7396.0000     5542.2011     2484   25.06%
      0     0     5576.9031    73     7396.0000      Cuts: 42     2559   24.60%
      0     0     5606.6270    73     7396.0000      Cuts: 82     2677   24.19%
      0     0     5618.3762    73     7396.0000      Cuts: 43     2822   24.03%
      0     0     5667.1969    73     7396.0000      Cuts: 63     2987   23.37%
      0     0     5676.3068    73     7396.0000      Cuts: 58     3195   23.25%
      0     0     5689.0488    73     7396.0000      Cuts: 26     3265   23.08%
      0     0     5694.2764    73     7396.0000      Cuts: 64     3352   23.01%
      0     0     5694.5538    73     7396.0000      Cuts: 61     3420   23.00%
      0     0     5694.6835    73     7396.0000      Cuts: 29     3488   23.00%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 538 rows and 34 columns.
MIP Presolve modified 2295 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1062 rows, 717 columns, and 25179 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (83.86 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1062 rows, 717 columns, and 25179 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (25.37 ticks)
Clique table members: 1000.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.05 sec. (12.79 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    91                      0.0000      176         
      0     0        0.0000    91                 Cliques: 63      313         
      0     0        0.0000    91                   Cuts: 113      545         
      0     0        0.0000    91                  Cliques: 1      602         
Detecting symmetries...
      0     2        0.0000    37                      0.0000      602         
Elapsed time = 2.81 sec. (674.80 ticks, tree = 0.02 MB, solutions = 0)
   1650   515        0.0000    10                      0.0000    47373         
   3774  1439        0.0000    20                      0.0000   120286         
   5668  2060        0.0000    20                      0.0000   201904         

Performing restart 1

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 2 rows and 0 columns.
MIP Presolve modified 217 coefficients.
Reduced MIP has 1060 rows, 717 columns, and 25111 nonzeros.
Reduced MIP has 717 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (30.49 ticks)
Represolve time = 0.08 sec. (45.05 ticks)
   5853     0        0.0000   101                 ZeroHalf: 1   217158         
   5853     0        0.0000   101                    Cuts: 49   217358         
   5853     0        0.0000   101                     Cuts: 4   217405         
   5853     2        0.0000    10                      0.0000   217405         
   6649   273    infeasible                            0.0000   263384         
   7533   587        0.0000    29                      0.0000   318496         
   8278   962        0.0000    16                      0.0000   367474         
   9030  1290        0.0000    36                      0.0000   419802         
   9944  1746        0.0000    53                      0.0000   469631         
  12666  3014        0.0000    41                      0.0000   683697         
Elapsed time = 25.72 sec. (4260.75 ticks, tree = 1.83 MB, solutions = 0)
  15494  3730        0.0000    49                      0.0000   881735         

Performing restart 2

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 201 rows and 58 columns.
MIP Presolve modified 584 coefficients.
Reduced MIP has 831 rows, 659 columns, and 18993 nonzeros.
Reduced MIP has 659 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (25.43 ticks)
Tried aggregator 1 time.
Reduced MIP has 831 rows, 659 columns, and 18993 nonzeros.
Reduced MIP has 659 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (18.53 ticks)
Represolve time = 0.14 sec. (88.81 ticks)
  17850     2        0.0000    26                      0.0000  1047082         
  21877  1568        0.0000    29                      0.0000  1253616         
  25594  2610        0.0000    14                      0.0000  1491725         
  29032  3322    infeasible                            0.0000  1708185         
  32082  3683        0.0000    66                      0.0000  1914123         
  35022  4266        0.0000    30                      0.0000  2125441         
  37633  4667        0.0000    41                      0.0000  2351763         
  39293  5243        0.0000    52                      0.0000  2522734         
  40977  5410        0.0000    25                      0.0000  2716263         
Elapsed time = 64.59 sec. (14146.66 ticks, tree = 12.77 MB, solutions = 0)
  42700  5577        0.0000    57                      0.0000  2893883         
* 43730  5469      integral     0        0.0000        0.0000  2973689    0.00%

Clique cuts applied:  184
Cover cuts applied:  30
Mixed integer rounding cuts applied:  45
Zero-half cuts applied:  18
Gomory fractional cuts applied:  2

Root node processing (before b&c):
  Real time             =    1.48 sec. (674.09 ticks)
Parallel b&c, 12 threads:
  Real time             =   68.36 sec. (15243.28 ticks)
  Sync time (average)   =   32.08 sec.
  Wait time (average)   =    0.08 sec.
                          ------------
Total (root+branch&cut) =   69.84 sec. (15917.37 ticks)
69.843000 
TRUE-modelo
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 538 rows and 34 columns.
MIP Presolve modified 2167 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1060 rows, 653 columns, and 22791 nonzeros.
Reduced MIP has 653 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (71.88 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1060 rows, 653 columns, and 22791 nonzeros.
Reduced MIP has 653 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (22.92 ticks)
Clique table members: 998.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (12.26 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    76                      0.0000      178         
      0     0        0.0000    76                  Cliques: 1      203         
      0     0        0.0000    76                    Cuts: 49      329         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      329    0.00%
Elapsed time = 0.72 sec. (519.91 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  3
Zero-half cuts applied:  1

Root node processing (before b&c):
  Real time             =    0.72 sec. (520.00 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.72 sec. (520.00 ticks)
0.718000 
TRUE-modelo
Salgo lazy
*     0+    0                         6323.0000     5694.6835             9.94%
      0     0  -1.00000e+75     0     6323.0000     5694.6835     3488    9.94%
      0     2     5694.6835   169     6323.0000     5694.6835     3488    9.94%                        0             0
Elapsed time = 89.13 sec. (1742.22 ticks, tree = 0.02 MB, solutions = 3)
     12     5     5731.7721   145     6323.0000     5732.3055     4448    9.34%         X_1_2_1 D     48     40      6
     50     4     5877.0942   157     6323.0000     5870.5379     8030    7.16%         X_2_0_4 D     47     39      6

GUB cover cuts applied:  54
Cover cuts applied:  12
Flow cuts applied:  13
Mixed integer rounding cuts applied:  17
Zero-half cuts applied:  34
Gomory fractional cuts applied:  6

Root node processing (before b&c):
  Real time             =   88.75 sec. (1724.89 ticks)
Parallel b&c, 8 threads:
  Real time             =    4.84 sec. (644.76 ticks)
  Sync time (average)   =    4.10 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   93.59 sec. (2369.65 ticks)
93.594000 

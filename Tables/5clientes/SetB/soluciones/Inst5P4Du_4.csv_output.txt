Instances\Inst5P4Du_4.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000023
camion 1, TRUE-heu 0.000043
camion 2, TRUE-heu 0.000064
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 7663.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 129 rows and 111 columns.
MIP Presolve modified 4042 coefficients.
Reduced MIP has 319 rows, 3927 columns, and 34490 nonzeros.
Reduced MIP has 3852 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (26.18 ticks)
Clique table members: 169.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.08 sec. (112.85 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         7663.0000        0.0000           100.00%
      0     0     3841.9528    70     7663.0000     3841.9528     1963   49.86%
      0     0     4625.7208    70     7663.0000      Cuts: 46     2212   39.64%
      0     0     4795.5673    70     7663.0000      Cuts: 61     2388   37.42%
      0     0     5134.2808    70     7663.0000      Cuts: 87     2712   33.00%
      0     0     5463.3612    70     7663.0000      Cuts: 65     3073   28.70%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 538 rows and 34 columns.
MIP Presolve modified 2231 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1061 rows, 685 columns, and 23985 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (78.95 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1061 rows, 685 columns, and 23985 nonzeros.
Reduced MIP has 685 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (24.39 ticks)
Clique table members: 999.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (16.68 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    85                      0.0000      229         
      0     0        0.0000    85                 Cliques: 39      380         
      0     0        0.0000    85                    Cuts: 24      461         
      0     0        0.0000    85                    Cuts: 60      544         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      544    0.00%
Elapsed time = 1.19 sec. (744.69 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  25
Zero-half cuts applied:  2
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    1.19 sec. (744.79 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.19 sec. (744.79 ticks)
1.203000 
TRUE-modelo
camion 2, TRUE-heu 0.000122
Salgo lazy
*     0+    0                         6323.0000     5463.3612            13.60%
      0     0  -1.00000e+75     0     6323.0000     5463.3612     3073   13.60%
      0     0     5544.4518    70     6323.0000      Cuts: 54     3421   12.31%
      0     0     5596.2439    70     6323.0000      Cuts: 49     3534   11.49%
      0     0     5608.7759    70     6323.0000      Cuts: 67     3664   11.30%
      0     0     5635.1483    70     6323.0000      Cuts: 45     3785   10.88%
      0     0     5726.9329    70     6323.0000      Cuts: 57     3945    9.43%
      0     0     5730.6385    70     6323.0000      Cuts: 35     4015    9.37%
      0     0     5741.9805    70     6323.0000      Cuts: 30     4071    9.19%
      0     0     5750.5663    70     6323.0000      Cuts: 47     4162    9.05%
      0     0     5750.5663    70     6323.0000      Cuts: 18     4203    9.05%
      0     0     5750.5663    70     6323.0000      Cuts: 15     4224    9.05%
      0     0     5750.5663    70     6323.0000      Cuts: 24     4280    9.05%
      0     2     5750.5663   137     6323.0000     5750.5663     4280    9.05%                        0             0
Elapsed time = 4.03 sec. (1392.92 ticks, tree = 0.02 MB, solutions = 2)
     51     7        cutoff           6323.0000     6151.9845     6534    2.70%         X_0_2_3 U     80     53      9

GUB cover cuts applied:  56
Cover cuts applied:  55
Flow cuts applied:  3
Mixed integer rounding cuts applied:  9
Zero-half cuts applied:  51
Gomory fractional cuts applied:  2

Root node processing (before b&c):
  Real time             =    3.77 sec. (1373.71 ticks)
Parallel b&c, 8 threads:
  Real time             =    3.19 sec. (401.72 ticks)
  Sync time (average)   =    2.21 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    6.95 sec. (1775.43 ticks)
6.953000 

Instances\Inst5P1Dv_4.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000021
camion 1, TRUE-heu 0.000037
camion 2, TRUE-heu 0.000058
camion 3, TRUE-heu 0.000078
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 9600.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 172 rows and 212 columns.
MIP Presolve modified 4162 coefficients.
Reduced MIP has 402 rows, 3892 columns, and 33912 nonzeros.
Reduced MIP has 3792 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (30.33 ticks)
Clique table members: 202.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.11 sec. (111.58 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                         9600.0000        0.0000           100.00%
      0     0     3959.3482    82     9600.0000     3959.3482      858   58.76%
      0     0     5529.5853    82     9600.0000      Cuts: 66     1617   42.40%
      0     0     5970.6470    82     9600.0000      Cuts: 86     2130   37.81%
      0     0     6298.2577    82     9600.0000      Cuts: 87     2364   34.39%
Dentro lazy
camion 0, TRUE-heu 0.000101
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 645 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.01 sec. (11.72 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (7.80 ticks)
Clique table members: 523.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (3.16 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    45                      0.0000       95         
      0     0        0.0000    45                     Cuts: 4      139         
      0     0        0.0000    45                    Cuts: 45      214         
      0     2        0.0000    41                      0.0000      214         
Elapsed time = 15.88 sec. (190.61 ticks, tree = 0.02 MB, solutions = 0)
   2269   263    infeasible                            0.0000    84365         
   5011   274        0.0000    25                      0.0000   173837         
   7598   112        0.0000    13                      0.0000   265542         

Clique cuts applied:  20
Cover cuts applied:  160
Zero-half cuts applied:  20
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =   13.90 sec. (190.31 ticks)
Parallel b&c, 12 threads:
  Real time             =   34.03 sec. (786.73 ticks)
  Sync time (average)   =   28.91 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   47.94 sec. (977.04 ticks)
47.953000 
FALSE-modelo
 Salgo lazy
      0     0     6349.1291    82     9600.0000      Cuts: 81     2569   33.86%
      0     0     6359.7184    82     9600.0000      Cuts: 50     2636   33.75%
      0     0     6371.0832    82     9600.0000      Cuts: 68     2744   33.63%
      0     0     6381.2570    82     9600.0000      Cuts: 70     2818   33.53%
      0     0     6387.4694    82     9600.0000      Cuts: 73     2895   33.46%
      0     0     6395.7958    82     9600.0000      Cuts: 43     2998   33.38%
      0     0     6396.5201    82     9600.0000      Cuts: 47     3064   33.37%
      0     0     6397.9468    82     9600.0000      Cuts: 43     3133   33.35%
      0     0     6398.4003    82     9600.0000      Cuts: 27     3176   33.35%
      0     0     6398.4003    82     9600.0000      Cuts: 10     3195   33.35%
Dentro lazy
camion 0, TRUE-heu 0.000146
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 645 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (11.29 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (7.38 ticks)
Clique table members: 523.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (2.67 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    44                      0.0000       80         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000       81    0.00%
Elapsed time = 5.48 sec. (98.71 ticks, tree = 0.01 MB, solutions = 1)

Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    5.48 sec. (98.75 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    5.48 sec. (98.75 ticks)
5.500000 
TRUE-modelo
camion 2, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 1004 rows and 34 columns.
MIP Presolve modified 1143 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 586 rows, 365 columns, and 12638 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (78.27 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 586 rows, 365 columns, and 12638 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (19.66 ticks)
Clique table members: 593.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (3.65 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    46                      0.0000       85         
      0     0        0.0000    46                     Cuts: 2      116         
      0     0        0.0000    46                    Cuts: 62      178         
Detecting symmetries...
      0     2        0.0000    34                      0.0000      178         
Elapsed time = 2.41 sec. (293.04 ticks, tree = 0.02 MB, solutions = 0)
*   280    16      integral     0        0.0000        0.0000    12995    0.00%

Clique cuts applied:  6
Zero-half cuts applied:  14
Lift and project cuts applied:  1
Gomory fractional cuts applied:  2

Root node processing (before b&c):
  Real time             =    0.59 sec. (292.72 ticks)
Parallel b&c, 12 threads:
  Real time             =   31.73 sec. (87.72 ticks)
  Sync time (average)   =   30.45 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   32.33 sec. (380.45 ticks)
32.328000 
TRUE-modelo
Salgo lazy
*     0+    0                         8740.0000     6398.4003            26.79%
      0     0  -1.00000e+75     0     8740.0000     6398.4003     3195   26.79%
      0     2     6398.4003   178     8740.0000     6398.4003     3195   26.79%                        0             0
Elapsed time = 94.02 sec. (2006.65 ticks, tree = 0.02 MB, solutions = 2)
Dentro lazy
camion 0, TRUE-heu 0.000214
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 645 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (11.57 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (7.65 ticks)
Clique table members: 523.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (2.54 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    44                      0.0000       80         
      0     0        0.0000    44                    Cuts: 12      135         
      0     0        0.0000    44                     Cuts: 9      194         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      194    0.00%
Elapsed time = 9.11 sec. (199.23 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  1
Zero-half cuts applied:  3

Root node processing (before b&c):
  Real time             =    9.13 sec. (199.26 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    9.13 sec. (199.26 ticks)
9.125000 
TRUE-modelo
camion 2, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 1004 rows and 34 columns.
MIP Presolve modified 1143 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 586 rows, 365 columns, and 12638 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (78.27 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 586 rows, 365 columns, and 12638 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (19.66 ticks)
Clique table members: 593.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (3.65 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    46                      0.0000       85         
      0     0        0.0000    46                     Cuts: 2      116         
      0     0        0.0000    46                    Cuts: 62      178         
Detecting symmetries...
      0     2        0.0000    34                      0.0000      178         
Elapsed time = 2.42 sec. (293.04 ticks, tree = 0.02 MB, solutions = 0)
*   280    16      integral     0        0.0000        0.0000    12995    0.00%

Clique cuts applied:  6
Zero-half cuts applied:  14
Lift and project cuts applied:  1
Gomory fractional cuts applied:  2

Root node processing (before b&c):
  Real time             =    0.64 sec. (292.72 ticks)
Parallel b&c, 12 threads:
  Real time             =   28.17 sec. (87.72 ticks)
  Sync time (average)   =   26.80 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   28.81 sec. (380.45 ticks)
28.813000 
TRUE-modelo
Salgo lazy
      3     3     7113.8717   143     8740.0000     6683.9974     3604   23.52%              Y2 U      7      0      1
*     4+    2                         8595.0000     6683.9974            22.23%
Dentro lazy
camion 0, TRUE-heu 0.000290
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 645 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (11.29 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (7.38 ticks)
Clique table members: 523.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (3.24 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    35                      0.0000       97         
      0     0        0.0000    35                    Cuts: 64      189         
      0     0        0.0000    35                  Cliques: 4      239         
      0     0        0.0000    35                    Cuts: 35      297         
      0     2        0.0000    41                      0.0000      297         
Elapsed time = 12.34 sec. (204.07 ticks, tree = 0.02 MB, solutions = 0)
   2797   172        0.0000    19                      0.0000    91437         
   5160    52    infeasible                            0.0000   205969         

Clique cuts applied:  23
Cover cuts applied:  42
Zero-half cuts applied:  13

Root node processing (before b&c):
  Real time             =   10.73 sec. (203.76 ticks)
Parallel b&c, 12 threads:
  Real time             =   18.38 sec. (537.62 ticks)
  Sync time (average)   =   14.56 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   29.11 sec. (741.38 ticks)
29.141000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 645 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (11.58 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (7.81 ticks)
Clique table members: 523.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (3.22 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    52                      0.0000       99         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      130    0.00%
Elapsed time = 8.66 sec. (143.91 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  1
Zero-half cuts applied:  5
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    8.66 sec. (143.94 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    8.66 sec. (143.94 ticks)
8.672000 
TRUE-modelo
camion 1, TRUE-heu 0.000390
camion 3, TRUE-heu 0.000415
Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000444
camion 1, TRUE-heu 0.000470
camion 2, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 1003 rows and 34 columns.
MIP Presolve modified 1123 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 587 rows, 365 columns, and 12365 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (77.99 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 587 rows, 365 columns, and 12365 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (20.03 ticks)
Clique table members: 525.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (3.54 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    46                      0.0000       88         
      0     0        0.0000    46                     Cuts: 9      145         
      0     0        0.0000    46                     Cuts: 3      181         
      0     0        0.0000    46                    Cuts: 48      260         
      0     2        0.0000    44                      0.0000      260         
Elapsed time = 3.33 sec. (366.14 ticks, tree = 0.02 MB, solutions = 0)

Clique cuts applied:  22
Cover cuts applied:  1
Zero-half cuts applied:  18
Gomory fractional cuts applied:  3

Root node processing (before b&c):
  Real time             =    0.89 sec. (365.80 ticks)
Parallel b&c, 12 threads:
  Real time             =   35.95 sec. (215.15 ticks)
  Sync time (average)   =   31.37 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   36.84 sec. (580.94 ticks)
36.844000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000549
camion 1, TRUE-heu 0.000573
camion 3, TRUE-heu 0.000594
Salgo lazy
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 645 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (11.58 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (7.81 ticks)
Clique table members: 523.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (3.22 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    52                      0.0000       99         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      130    0.00%
Elapsed time = 9.67 sec. (143.91 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  1
Zero-half cuts applied:  5
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    9.67 sec. (143.94 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    9.67 sec. (143.94 ticks)
9.672000 
TRUE-modelo
camion 1, TRUE-heu 0.000646
camion 3, TRUE-heu 0.000668
Salgo lazy
     45    16        cutoff           8595.0000     7181.3663     6986   16.45%         X_1_3_5 U    200     30      6
*    46+    6                         8402.0000     7181.3663            14.53%
Dentro lazy
camion 0, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 645 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (11.58 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (7.81 ticks)
Clique table members: 523.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (3.22 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    52                      0.0000       99         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      130    0.00%
Elapsed time = 8.84 sec. (143.91 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  1
Zero-half cuts applied:  5
Gomory fractional cuts applied:  1

Root node processing (before b&c):
  Real time             =    8.84 sec. (143.94 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    8.84 sec. (143.94 ticks)
8.844000 
TRUE-modelo
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 645 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (11.44 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (7.54 ticks)
Clique table members: 523.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (3.02 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    44                      0.0000       94         
      0     0        0.0000    44                    Cuts: 17      148         
      0     0        0.0000    44                    Cuts: 53      226         
      0     2        0.0000    37                      0.0000      226         
Elapsed time = 34.13 sec. (246.27 ticks, tree = 0.02 MB, solutions = 0)
   2956   276        0.0000    43                      0.0000    74157         
   6214   131    infeasible                            0.0000   184109         
   8271     0    infeasible                                     258522         

Clique cuts applied:  8
Cover cuts applied:  73
Zero-half cuts applied:  17

Root node processing (before b&c):
  Real time             =   32.50 sec. (245.97 ticks)
Parallel b&c, 12 threads:
  Real time             =   41.14 sec. (717.11 ticks)
  Sync time (average)   =   36.56 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   73.64 sec. (963.08 ticks)
73.657000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000752
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 645 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (11.44 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 585 rows, 365 columns, and 7385 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.00 sec. (7.54 ticks)
Clique table members: 523.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.00 sec. (2.88 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    46                      0.0000       90         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      122    0.00%
Elapsed time = 8.36 sec. (116.63 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  1

Root node processing (before b&c):
  Real time             =    8.36 sec. (116.66 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    8.36 sec. (116.66 ticks)
8.375000 
TRUE-modelo
camion 2, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 1003 rows and 34 columns.
MIP Presolve modified 1123 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 587 rows, 365 columns, and 12365 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.08 sec. (77.99 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 587 rows, 365 columns, and 12365 nonzeros.
Reduced MIP has 365 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (20.03 ticks)
Clique table members: 525.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.01 sec. (3.54 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    46                      0.0000       88         
      0     0        0.0000    46                     Cuts: 9      145         
      0     0        0.0000    46                     Cuts: 3      181         
      0     0        0.0000    46                    Cuts: 48      260         
      0     2        0.0000    44                      0.0000      260         
Elapsed time = 3.00 sec. (366.14 ticks, tree = 0.02 MB, solutions = 0)

Clique cuts applied:  22
Cover cuts applied:  1
Zero-half cuts applied:  18
Gomory fractional cuts applied:  3

Root node processing (before b&c):
  Real time             =    1.36 sec. (365.80 ticks)
Parallel b&c, 12 threads:
  Real time             =   36.22 sec. (215.15 ticks)
  Sync time (average)   =   32.45 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =   37.58 sec. (580.94 ticks)
37.594000 
FALSE-modelo
 Salgo lazy
*   128+    5                         8297.0000     7455.0454            10.15%
    178    44     8155.7640    36     8297.0000     7455.0454    14190   10.15%         X_1_0_4 U    154    146      8
    555    63        cutoff           8297.0000     7985.3964    27483    3.76%         X_3_5_3 U    822    806     15

GUB cover cuts applied:  58
Cover cuts applied:  133
Flow cuts applied:  7
Mixed integer rounding cuts applied:  18
Zero-half cuts applied:  40
Gomory fractional cuts applied:  2
User cuts applied:  5

Root node processing (before b&c):
  Real time             =   93.67 sec. (1987.59 ticks)
Parallel b&c, 8 threads:
  Real time             =  254.53 sec. (1212.05 ticks)
  Sync time (average)   =   90.25 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  348.20 sec. (3199.64 ticks)
348.218000 

Instances\Inst5P3Du_4.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000022
camion 1, TRUE-heu 0.000043
camion 2, TRUE-heu 0.000062
camion 3, TRUE-heu 0.000081
camion 4, TRUE-heu 0.000103
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 11974.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 215 rows and 345 columns.
MIP Presolve modified 14879 coefficients.
Reduced MIP has 555 rows, 14385 columns, and 128086 nonzeros.
Reduced MIP has 14260 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.16 sec. (133.25 ticks)
Clique table members: 305.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.27 sec. (277.36 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                        11974.0000        0.0000           100.00%
      0     0     3840.3942   155    11974.0000     3840.3942       24   67.93%
      0     0     5387.0456   155    11974.0000      Cuts: 48     4339   55.01%
      0     0     5483.6692   155    11974.0000      Cuts: 51     5375   54.20%
      0     0     5750.2922   155    11974.0000      Cuts: 55     5876   51.98%
Dentro lazy
camion 0, TRUE-heu 0.000126
camion 2, TRUE-heu 0.000162
camion 4, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 2989 rows and 34 columns.
MIP Presolve modified 7021 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1977 rows, 973 columns, and 61469 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.38 sec. (421.00 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1977 rows, 973 columns, and 61469 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.09 sec. (89.44 ticks)
Clique table members: 1914.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.08 sec. (54.86 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   173                      0.0000      414         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      494    0.00%
Elapsed time = 1.95 sec. (1674.51 ticks, tree = 0.01 MB, solutions = 1)

Clique cuts applied:  36

Root node processing (before b&c):
  Real time             =    1.95 sec. (1674.73 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.95 sec. (1674.73 ticks)
1.969000 
TRUE-modelo
Salgo lazy
*     0+    0                        10578.0000     5750.2922            45.64%
Dentro lazy
camion 0, TRUE-heu 0.000212
camion 2, TRUE-heu 0.000239
camion 4, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 1524 rows and 34 columns.
MIP Presolve modified 3196 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1522 rows, 941 columns, and 46221 nonzeros.
Reduced MIP has 941 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.22 sec. (234.53 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1522 rows, 941 columns, and 46221 nonzeros.
Reduced MIP has 941 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (56.83 ticks)
Clique table members: 1460.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.06 sec. (32.25 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   123                      0.0000      308         
Detecting symmetries...
      0     2        0.0000    16                      0.0000      308         
Elapsed time = 2.98 sec. (953.80 ticks, tree = 0.02 MB, solutions = 0)
*    63+   20                            0.0000        0.0000             0.00%

Clique cuts applied:  3

Root node processing (before b&c):
  Real time             =    1.20 sec. (952.75 ticks)
Parallel b&c, 12 threads:
  Real time             =    7.78 sec. (74.35 ticks)
  Sync time (average)   =    6.46 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    8.98 sec. (1027.11 ticks)
9.016000 
TRUE-modelo
Salgo lazy
*     0+    0                        10246.0000     5750.2922            43.88%
Dentro lazy
camion 0, TRUE-heu 0.000294
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 536 rows and 34 columns.
MIP Presolve modified 3479 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1074 rows, 1037 columns, and 39103 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.13 sec. (153.17 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1074 rows, 1037 columns, and 39103 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (40.21 ticks)
Clique table members: 1010.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.30 sec. (283.98 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.30 sec. (283.98 ticks)
0.313000 
TRUE-modelo
camion 2, TRUE-heu 0.000351
Salgo lazy
*     0+    0                         9101.0000     5750.2922            36.82%
      0     0  -1.00000e+75     0     9101.0000     5750.2922     5876   36.82%
      0     0     6007.7232   155     9101.0000      Cuts: 46     6705   33.99%
      0     0     6018.5326   155     9101.0000      Cuts: 55     7022   33.87%
      0     0     6036.3204   155     9101.0000      Cuts: 50     7260   33.67%
      0     0     6073.4266   155     9101.0000      Cuts: 44     7558   33.27%
      0     0     6079.6016   155     9101.0000      Cuts: 38     7882   33.20%
      0     0     6082.7567   155     9101.0000      Cuts: 42     8209   33.16%
      0     0     6082.7567   155     9101.0000      Cuts: 28     8443   33.16%
      0     0     6082.7567   155     9101.0000      Cuts: 31     8732   33.16%
      0     0     6082.7567   155     9101.0000      Cuts: 33     8997   33.16%
      0     0     6084.5819   155     9101.0000      Cuts: 47     9240   33.14%
      0     0     6221.9558   155     9101.0000      Cuts: 42     9568   31.63%
      0     0     6221.9558   155     9101.0000      Cuts: 41     9763   31.63%
      0     2     6221.9558    62     9101.0000     6221.9558     9763   31.63%                        0             0
Elapsed time = 21.48 sec. (11497.80 ticks, tree = 0.02 MB, solutions = 4)
      2     4     7928.1193   108     9101.0000     6229.6973    11510   31.55%              Y4 U      6      7      2
      7     9     7997.4918    56     9101.0000     6507.7492    15823   28.49%         X_4_4_5 N      1     22      5
     20    17     8453.9817    77     9101.0000     6865.2248    23175   24.57%         X_2_3_2 D     38     30      6
     29    15     7564.8561   113     9101.0000     6922.5807    22769   23.94%         X_1_5_4 U     24     16      3
     41    27     8319.3163    64     9101.0000     6922.5807    24954   23.94%         X_2_1_4 D     76     60     10
     54    39     8250.3849    77     9101.0000     6922.5807    30018   23.94%         X_2_4_0 D     61     53      9
     77    51     8767.8667    67     9101.0000     6922.5807    31002   23.94%         X_1_0_2 D    118    110     14
    122    84     8433.1179    50     9101.0000     6922.5807    37002   23.94%         X_3_4_5 D    139    131     18
    156    95     8829.2479    30     9101.0000     6922.5807    39311   23.94%         X_3_0_1 D    176    160     21
    287   167     7307.6080    96     9101.0000     7035.1095    59038   22.70%         X_3_4_5 U    289    240      5
Elapsed time = 26.44 sec. (15131.18 ticks, tree = 0.87 MB, solutions = 4)
    404   230     8231.3746   115     9101.0000     7035.1095    72440   22.70%         X_1_2_5 D    539    531     15
    543   336     8083.0115    68     9101.0000     7035.1095    99166   22.70%         X_1_0_2 D    585    577     10
    641   449        cutoff           9101.0000     7035.1095   123379   22.70%              Y3 U    901    885     27
    759   490        cutoff           9101.0000     7350.0568   141516   19.24%         X_3_3_5 U    674    739     13
    915   541        cutoff           9101.0000     7535.9821   168517   17.20%         X_1_4_5 U    898    738      8
   1096   594     8551.9706   110     9101.0000     7742.0276   198576   14.93%         X_0_2_5 D    982    974     12
   1276   681     8240.3180    65     9101.0000     7872.4800   237364   13.50%         X_4_3_1 D   1191   1183     10
   1465   808     8269.0092    62     9101.0000     7999.5327   275347   12.10%         X_0_1_5 D   1442   1434     22
Dentro lazy
camion 0, TRUE-heu 0.000387
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 536 rows and 34 columns.
MIP Presolve modified 3879 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1073 rows, 1005 columns, and 37845 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (137.88 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1073 rows, 1005 columns, and 37845 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (37.14 ticks)
Clique table members: 1009.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (24.83 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   124                      0.0000      303         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      303    0.00%
Elapsed time = 1.42 sec. (634.95 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    1.42 sec. (635.10 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    1.42 sec. (635.10 ticks)
1.438000 
TRUE-modelo
camion 2, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 1925 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 605 rows, 1005 columns, and 21945 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (35.48 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 605 rows, 1005 columns, and 21945 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (22.08 ticks)
Clique table members: 543.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.16 sec. (121.25 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.16 sec. (121.25 ticks)
0.172000 
TRUE-modelo
Salgo lazy
   1640   870     8871.0199    38     9101.0000     8019.2603   303672   11.89%         X_0_0_4 D   1963   1955     17
Dentro lazy
camion 0, TRUE-heu 0.000470
camion 1, TRUE-heu 0.000494
camion 4, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 1797 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 603 rows, 941 columns, and 20489 nonzeros.
Reduced MIP has 941 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (33.35 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 603 rows, 941 columns, and 20489 nonzeros.
Reduced MIP has 941 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (20.84 ticks)
Clique table members: 541.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (7.79 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    83                      0.0000      154         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      154    0.00%
Elapsed time = 0.38 sec. (236.81 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    0.38 sec. (236.91 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.38 sec. (236.91 ticks)
0.391000 
TRUE-modelo
Salgo lazy
*  1770   822      integral     0     8941.0000     8019.2603   278429   10.31%
Dentro lazy
camion 0, TRUE-heu 0.000586
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 538 rows and 34 columns.
MIP Presolve modified 2998 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1073 rows, 1069 columns, and 38313 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (147.98 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1073 rows, 1069 columns, and 38313 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (37.10 ticks)
Clique table members: 1011.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.06 sec. (21.17 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    98                      0.0000      271         
      0     0        0.0000    98                    Cuts: 12      471         
Detecting symmetries...
      0     2        0.0000    29                      0.0000      471         
Elapsed time = 3.67 sec. (762.21 ticks, tree = 0.02 MB, solutions = 0)
    562   182    infeasible                            0.0000    31862         
   1820   407    infeasible                            0.0000    69805         
   3207   386    infeasible                            0.0000   110257         
   4347   409        0.0000    41                      0.0000   148762         
   5283   484    infeasible                            0.0000   197658         
   6382   433        0.0000    79                      0.0000   237552         
   7511   447    infeasible                            0.0000   270973         

Performing restart 1

Repeating presolve.
Tried aggregator 1 time.
MIP Presolve eliminated 2 rows and 0 columns.
MIP Presolve modified 283 coefficients.
Reduced MIP has 1071 rows, 1069 columns, and 38157 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (44.94 ticks)
Represolve time = 0.09 sec. (61.52 ticks)
   7835     0        0.0000   143                 Cliques: 15   298600         
   7835     0        0.0000   143                  Cliques: 1   298601         
   7835     0        0.0000   143                    Cuts: 59   298943         
   7835     2        0.0000    30                      0.0000   298943         
   7961    10    infeasible                            0.0000   308744         
   9749     7    infeasible                            0.0000   403166         
Elapsed time = 56.94 sec. (4452.43 ticks, tree = 0.04 MB, solutions = 0)

Clique cuts applied:  60

Root node processing (before b&c):
  Real time             =    1.36 sec. (761.42 ticks)
Parallel b&c, 12 threads:
  Real time             =   74.64 sec. (4026.47 ticks)
  Sync time (average)   =   63.83 sec.
  Wait time (average)   =    0.03 sec.
                          ------------
Total (root+branch&cut) =   76.00 sec. (4787.89 ticks)
76.000000 
FALSE-modelo
 Salgo lazy
Dentro lazy
camion 0, TRUE-heu 0.000643
camion 1, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 44 rows and 34 columns.
MIP Presolve modified 2052 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 608 rows, 1069 columns, and 24425 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (34.50 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 608 rows, 1069 columns, and 24425 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.01 sec. (24.47 ticks)
Clique table members: 545.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.14 sec. (117.44 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.14 sec. (117.44 ticks)
0.141000 
TRUE-modelo
camion 4, TRUE-heu 0.000699
Salgo lazy
   1860   927     8977.2974    12     8941.0000     8050.0617   318870    9.96%         X_2_4_0 U   1400   1392     11
Elapsed time = 120.86 sec. (24733.64 ticks, tree = 8.07 MB, solutions = 5)
   2083   930     8303.9287    58     8941.0000     8118.0741   350074    9.20%         X_2_4_0 D   2146   2138      8
   2313  1050     8876.6601    58     8941.0000     8172.9504   387861    8.59%         X_1_2_3 U   2453    420     21
   2529  1119     8551.2611    46     8941.0000     8208.9563   446030    8.19%         X_3_4_0 D   2934   2926     23
   2797  1092        cutoff           8941.0000     8252.4579   475383    7.70%         X_4_0_1 U   2987   2979     25
   3027  1056     8894.4774    72     8941.0000     8303.6111   527452    7.13%         X_4_1_3 D   3252   3244     12
   3297  1030     8819.1090    61     8941.0000     8342.3685   559736    6.70%         X_2_3_0 D   3282   3274     18
   3532   947        cutoff           8941.0000     8399.0075   627782    6.06%         X_0_2_5 U   3578   2784     12
   3774   850        cutoff           8941.0000     8460.2898   682340    5.38%              Y1 D   4092   4084     26
   4055   694     8686.8222    37     8941.0000     8561.0741   731354    4.25%         X_2_2_0 U   4262   4254      8
   4350   515     8684.5728    79     8941.0000     8614.7761   778402    3.65%         X_2_4_0 N   4413   3348     18
Elapsed time = 136.42 sec. (34352.93 ticks, tree = 3.51 MB, solutions = 7)
   4697   245        cutoff           8941.0000     8782.1566   828497    1.78%         X_0_3_5 U   5215   2759     20

Cover cuts applied:  541
Flow cuts applied:  12
Mixed integer rounding cuts applied:  26
Zero-half cuts applied:  23
Gomory fractional cuts applied:  3
User cuts applied:  1

Root node processing (before b&c):
  Real time             =   21.14 sec. (11361.46 ticks)
Parallel b&c, 8 threads:
  Real time             =  119.41 sec. (24626.86 ticks)
  Sync time (average)   =   18.81 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  140.55 sec. (35988.32 ticks)
140.547000 

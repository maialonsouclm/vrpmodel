Instances\Inst5P3Du_2.csvVersion identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 8
CPXPARAM_MIP_Strategy_File                       3
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_TimeLimit                               3600
Lazy constraint(s) or lazy constraint/branch callback is present.
    Disabling dual reductions (CPX_PARAM_REDUCE) in presolve.
    Disabling non-linear reductions (CPX_PARAM_PRELINEAR) in presolve.
    Disabling presolve reductions that prevent crushing forms.
         Disabling repeat represolve because of lazy constraint/incumbent callback.
Dentro lazy
camion 0, TRUE-heu 0.000026
camion 1, TRUE-heu 0.000049
camion 2, TRUE-heu 0.000070
camion 3, TRUE-heu 0.000090
camion 4, TRUE-heu 0.000108
Salgo lazy
1 of 1 MIP starts provided solutions.
MIP start 'm1' defined initial solution with objective 11974.0000.
Tried aggregator 1 time.
MIP Presolve eliminated 215 rows and 345 columns.
MIP Presolve modified 19679 coefficients.
Reduced MIP has 585 rows, 19185 columns, and 171286 nonzeros.
Reduced MIP has 19060 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.22 sec. (178.40 ticks)
Clique table members: 335.
MIP emphasis: balance optimality and feasibility.
MIP search method: traditional branch-and-cut.
Parallel mode: deterministic, using up to 8 threads.
Root relaxation solution time = 0.36 sec. (366.47 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap         Variable B NodeID Parent  Depth

*     0+    0                        11974.0000        0.0000           100.00%
      0     0     3828.9143   217    11974.0000     3828.9143       19   68.02%
      0     0     5859.5207   217    11974.0000      Cuts: 58     3432   51.06%
      0     0     6491.0899   217    11974.0000      Cuts: 63     4848   45.79%
      0     0     6576.4921   217    11974.0000      Cuts: 62     5859   45.08%
Dentro lazy
camion 0, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 1001 rows and 34 columns.
MIP Presolve modified 2467 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 610 rows, 1037 columns, and 29679 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.17 sec. (176.20 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 610 rows, 1037 columns, and 29679 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (39.51 ticks)
Clique table members: 546.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.34 sec. (294.60 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.34 sec. (294.60 ticks)
0.343000 
TRUE-modelo
camion 1, TRUE-heu 0.000162
camion 3, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 43 rows and 34 columns.
MIP Presolve modified 2533 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 607 rows, 1005 columns, and 23865 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (35.04 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 607 rows, 1005 columns, and 23865 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (22.06 ticks)
Clique table members: 543.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.14 sec. (115.33 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.14 sec. (115.33 ticks)
0.157000 
TRUE-modelo
camion 4, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 538 rows and 34 columns.
MIP Presolve modified 2998 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 1073 rows, 1069 columns, and 38313 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.14 sec. (146.76 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 1073 rows, 1069 columns, and 38313 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (37.61 ticks)
Clique table members: 1011.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.03 sec. (19.05 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000   117                      0.0000      249         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      249    0.00%
Elapsed time = 0.69 sec. (572.17 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    0.69 sec. (572.32 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.69 sec. (572.32 ticks)
0.703000 
TRUE-modelo
Salgo lazy
*     0+    0                        11579.0000     6576.4921            43.20%
Dentro lazy
camion 0, TRUE-heu 0.000243
camion 1, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 43 rows and 34 columns.
MIP Presolve modified 2899 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 607 rows, 1005 columns, and 23865 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (36.95 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 607 rows, 1005 columns, and 23865 nonzeros.
Reduced MIP has 1005 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (23.09 ticks)
Clique table members: 543.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.17 sec. (120.42 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.17 sec. (120.42 ticks)
0.172000 
TRUE-modelo
camion 3, TRUE-heu 0.000291
camion 4, TRUE-heu 0.000334
Salgo lazy
*     0+    0                        11542.0000     6576.4921            43.02%
      0     0  -1.00000e+75     0    11542.0000     6576.4921     5859   43.02%
      0     0     6592.5511   217    11542.0000      Cuts: 81     6473   42.88%
      0     0     6599.5073   217    11542.0000      Cuts: 43     7323   42.82%
      0     0     6615.7164   217    11542.0000      Cuts: 74     7968   42.68%
      0     0     6616.2642   217    11542.0000      Cuts: 43     8477   42.68%
      0     0     6620.5645   217    11542.0000      Cuts: 55     8954   42.64%
      0     0     6629.1598   217    11542.0000      Cuts: 62     9418   42.56%
      0     0     6639.8256   217    11542.0000      Cuts: 64    10246   42.47%
      0     0     6639.9693   217    11542.0000      Cuts: 46    10738   42.47%
      0     0     6640.0178   217    11542.0000      Cuts: 31    11280   42.47%
      0     0     6640.5490   217    11542.0000      Cuts: 46    12385   42.47%
      0     2     6640.5490   105    11542.0000     6640.5490    12385   42.47%                        0             0
Elapsed time = 14.55 sec. (17123.27 ticks, tree = 0.02 MB, solutions = 3)
      1     3     7608.6945   138    11542.0000     6673.1648    14090   42.18%              Y3 N      7      0      1
      4     5     7954.6746   101    11542.0000     7421.8509    18473   35.70%              Y4 U     16      8      2
      6     4     8364.7926   118    11542.0000     7421.8509    18207   35.70%         X_4_3_0 U      6      7      2
     10     7     9442.8642   121    11542.0000     7685.4430    25016   33.41%         X_1_0_2 U      3      6      3
     17     9     9653.9027   139    11542.0000     7685.4430    25123   33.41%         X_1_2_0 D     11      3      4
     21    10     8239.6632    94    11542.0000     8202.5436    26208   28.93%         X_2_4_5 D     40     32      5
     25    12     8406.6024   132    11542.0000     8202.5436    28237   28.93%         X_1_2_0 D     30     22      5
     30    15     9783.1098   104    11542.0000     8202.5436    31995   28.93%         X_1_5_4 D     19     11      5
     34    20     8854.8819   123    11542.0000     8210.9565    32934   28.86%         X_2_0_5 D     53     45      8
     58    37     8599.1393   118    11542.0000     8210.9565    39493   28.86%         X_1_2_0 D     68     60     11
Elapsed time = 20.11 sec. (22003.06 ticks, tree = 0.23 MB, solutions = 3)
     82    59     8384.1066   122    11542.0000     8210.9565    44996   28.86%         X_1_0_1 D     95     87     12
    104    71     9461.4301   118    11542.0000     8210.9565    54122   28.86%         X_1_0_3 U    128    112     15
    129    98    infeasible          11542.0000     8210.9565    78734   28.86%              Y2 D     99     83     12
    150   109     9970.3131    98    11542.0000     8210.9565    86591   28.86%         X_3_2_0 U    134    126     17
    176   122    10063.5292   152    11542.0000     8210.9565   105323   28.86%         X_2_2_5 D    212    204     26
    196   143    10142.2061   119    11542.0000     8210.9565   115336   28.86%         X_2_2_1 D    244    236     30
    229   160    10646.3007    81    11542.0000     8210.9565   128940   28.86%         X_2_0_1 U    268    252     32
    248   168     8933.5921   103    11542.0000     8210.9565   130897   28.86%         X_1_0_2 D    296    288      9
    288   203    10487.1977    75    11542.0000     8210.9565   157446   28.86%         X_2_3_5 D    185    177     16
    333   231    10180.4405   117    11542.0000     8210.9565   167201   28.86%         X_4_0_1 D    271    255     29
Elapsed time = 35.69 sec. (31925.96 ticks, tree = 1.46 MB, solutions = 3)
    358   234     9006.5951   123    11542.0000     8210.9565   170826   28.86%         X_1_0_2 D    436    428      9
Dentro lazy
camion 0, TRUE-heu 0.000374
camion 1, TRUE-heu 0.000397
camion 2, FALSE-heu 0.000
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 43 rows and 34 columns.
MIP Presolve modified 3011 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 608 rows, 1037 columns, and 24657 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.06 sec. (40.41 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 608 rows, 1037 columns, and 24657 nonzeros.
Reduced MIP has 1037 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.03 sec. (25.86 ticks)
Clique table members: 544.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.16 sec. (129.14 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.16 sec. (129.14 ticks)
0.156000 
TRUE-modelo
camion 4, TRUE-heu 0.000448
Salgo lazy
    421   265     9970.3006   159    11542.0000     8210.9565   188468   28.86%         X_1_4_0 D    453    445     10
    448   270    10630.8999   130    11542.0000     8370.5976   189737   27.48%         X_0_4_0 D    493    485     15
    481   319    10561.1484   136    11542.0000     8370.5976   223978   27.48%         X_4_2_4 D    550    542     17
*   491   252      integral     0    11396.0000     8370.5976   180703   26.55%
    514   321     9446.9293    82    11396.0000     8397.0983   228988   26.32%         X_4_5_0 D    489    481     11
    545   343    10109.7462   107    11396.0000     8397.0983   246955   26.32%         X_1_5_4 D    636    628     15
    595   341    10255.5838    63    11396.0000     8412.1157   244001   26.18%         X_1_5_4 D    613    605     14
    637   391    10348.6741   111    11396.0000     8415.6722   273112   26.15%         X_4_3_4 D    669    661     21
    682   410     9738.3472   156    11396.0000     8415.6722   290890   26.15%         X_3_0_2 D    726    718     10
    739   432    11072.3990    79    11396.0000     8436.5717   296722   25.97%         X_2_2_3 D    643    635     42
Elapsed time = 50.22 sec. (41734.31 ticks, tree = 3.36 MB, solutions = 4)
    777   493    10352.6878   107    11396.0000     8436.5717   323021   25.97%         X_1_4_0 D    756    748     14
    825   504    11085.5652    75    11396.0000     8470.4378   318054   25.67%         X_1_1_0 D    910    902     33
    870   518    11006.0954    98    11396.0000     8479.1551   334642   25.60%         X_3_0_4 D    880    872     22
    913   563    10504.8043   100    11396.0000     8482.4259   364771   25.57%         X_2_4_3 D    989    981     29
    958   592    10349.2870    96    11396.0000     8484.3559   377602   25.55%         X_1_0_4 U    984    968     12
   1018   674    10821.3525   113    11396.0000     8484.3559   421931   25.55%         X_2_2_3 D   1126   1118     30
   1085   681    10437.4119   115    11396.0000     8576.8584   424509   24.74%         X_1_5_0 D   1214   1206     13
   1138   692        cutoff          11396.0000     8585.8604   441648   24.66%         X_1_0_1 D   1052   1044     12
   1187   811    10448.2362   146    11396.0000     8600.0981   490998   24.53%         X_2_4_3 D   1296   1288     29
   1250   848    11125.0563   122    11396.0000     8679.2716   520048   23.84%         X_4_2_1 D   1249   1241     20
Elapsed time = 63.44 sec. (51483.14 ticks, tree = 7.90 MB, solutions = 4)
   1304   837    11068.4546    69    11396.0000     8715.3689   513240   23.52%         X_0_0_4 D   1148   1140     23
   1375   916    10376.7686   105    11396.0000     8724.0451   566126   23.45%         X_2_2_5 D   1448   1440     20
   1456   959    10062.5464    82    11396.0000     8724.0451   602284   23.45%         X_3_5_4 D   1530   1522     19
   1503   968    10271.7134   151    11396.0000     8724.0451   605094   23.45%         X_0_4_0 D   1602   1594     28
   1560   970     9930.3453   104    11396.0000     8789.5050   609221   22.87%         X_1_0_3 U   1650    298      7
   1624  1120    10401.1601    81    11396.0000     8812.0332   695709   22.67%         X_2_0_2 U   1742   1734     16
   1679  1140    10323.3479   114    11396.0000     8812.0332   715529   22.67%         X_4_2_4 D   1428   1420     18
   1747  1206    10083.9167   149    11396.0000     8812.0665   752995   22.67%         X_1_5_0 D   1850   1842     13
   1825  1241    11256.5569   103    11396.0000     8845.0018   784895   22.39%         X_2_0_2 D   1950   1934     18
   1878  1283    10691.0678    79    11396.0000     8873.9426   813723   22.13%         X_1_4_0 D   1851   1843     12
Elapsed time = 77.41 sec. (61185.15 ticks, tree = 12.93 MB, solutions = 4)
Dentro lazy
camion 0, TRUE-heu 0.000479
camion 1, TRUE-heu 0.000508
camion 2, TRUE-heu 0.000529
camion 4, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 45 rows and 34 columns.
MIP Presolve modified 1861 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 604 rows, 973 columns, and 21217 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (33.19 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 604 rows, 973 columns, and 21217 nonzeros.
Reduced MIP has 973 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (20.47 ticks)
Clique table members: 542.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.
Root relaxation solution time = 0.02 sec. (9.04 ticks)

        Nodes                                         Cuts/
   Node  Left     Objective  IInf  Best Integer    Best Bound    ItCnt     Gap

      0     0        0.0000    87                      0.0000      164         
*     0+    0                            0.0000        0.0000             0.00%
      0     0        cutoff              0.0000        0.0000      164    0.00%
Elapsed time = 0.53 sec. (256.32 ticks, tree = 0.01 MB, solutions = 1)

Root node processing (before b&c):
  Real time             =    0.53 sec. (256.42 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.53 sec. (256.42 ticks)
0.531000 
TRUE-modelo
Salgo lazy
   1943  1320        cutoff          11396.0000     8879.0366   832677   22.09%         X_4_5_0 U   2042   2026     22
   2004  1342    10450.4145    97    11396.0000     8889.0767   846366   22.00%         X_4_0_1 D   1887   1879     21
   2052  1395    10104.9880   107    11396.0000     8902.1757   884684   21.88%         X_1_5_3 D   2017   2009     17
*  2061+ 1300                        10777.0000     8902.1757            17.40%
   2112  1002    10010.4862   124    10777.0000     8902.1757   904869   17.40%         X_4_4_5 D   2112   2104      9
   2171  1026    10452.4916    97    10777.0000     8903.6107   925787   17.38%         X_0_5_4 D   2390   2382     20
   2236  1031    10650.0000    62    10777.0000     8925.5823   920626   17.18%         X_2_0_5 D   2191   2183     16
   2293  1040    10213.5573   119    10777.0000     8940.0357   977400   17.05%         X_4_3_0 D   2305   2297     14
   2356  1107        cutoff          10777.0000     8948.0644  1024903   16.97%         X_1_2_3 D   2517   2509     20
   2416  1124    10433.1771    47    10777.0000     8997.0971  1074802   16.52%         X_1_0_4 U   2662    346     13
   2484  1128     9813.6465   138    10777.0000     9017.9844  1110277   16.32%         X_4_4_5 U   2435    127     16
Elapsed time = 92.63 sec. (70906.84 ticks, tree = 11.14 MB, solutions = 5)
   2548  1151        cutoff          10777.0000     9053.3167  1131584   15.99%         X_1_5_4 U   2148   2132     13
   2609  1172     9736.3597   145    10777.0000     9062.3285  1163735   15.91%         X_4_4_5 D   2846    127     16
   2672  1181    10569.4872    65    10777.0000     9133.9570  1166768   15.25%         X_3_0_2 D   2918   2910     25
   2739  1245     9554.6790   106    10777.0000     9182.3544  1240940   14.80%         X_1_0_1 D   2802   2794      8
   2797  1252    10677.8534   115    10777.0000     9331.3217  1245436   13.41%         X_3_0_3 D   2866   2858     16
   2851  1277    10592.2338   129    10777.0000     9389.8775  1298878   12.87%              Y2 U   3118   3110     14
   2939  1295    10543.8236    94    10777.0000     9415.7687  1309915   12.63%         X_4_4_1 D   3032   3024     35
   2994  1307    10442.2246   121    10777.0000     9504.0323  1358827   11.81%         X_4_0_3 D   2965   2957     14
   3151  1425    10748.0000    32    10777.0000     9543.4034  1400553   11.45%       Z_2_4_2_8 U   3729   3721     95
Dentro lazy
camion 0, TRUE-heu 0.000602
camion 1, TRUE-heu 0.000638
camion 2, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 44 rows and 34 columns.
MIP Presolve modified 2052 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 608 rows, 1069 columns, and 24425 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (32.86 ticks)
Found incumbent of value 0.000000 after 0.08 sec. (63.78 ticks)

Root node processing (before b&c):
  Real time             =    0.08 sec. (63.90 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.08 sec. (63.90 ticks)
0.078000 
TRUE-modelo
camion 3, FALSE-heu 0.001
Version identifier: 12.10.0.0 | 2019-11-26 | 843d4de2ae
CPXPARAM_Threads                                 12
CPXPARAM_MIP_Strategy_Probe                      -1
CPXPARAM_Emphasis_MIP                            1
Tried aggregator 2 times.
MIP Presolve eliminated 44 rows and 34 columns.
MIP Presolve modified 2052 coefficients.
Aggregator did 2 substitutions.
Reduced MIP has 608 rows, 1069 columns, and 24425 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.05 sec. (34.27 ticks)
Tried aggregator 1 time.
Detecting symmetries...
Reduced MIP has 608 rows, 1069 columns, and 24425 nonzeros.
Reduced MIP has 1069 binaries, 0 generals, 0 SOSs, and 0 indicators.
Presolve time = 0.02 sec. (24.16 ticks)
Clique table members: 545.
MIP emphasis: integer feasibility.
MIP search method: dynamic search.
Parallel mode: deterministic, using up to 12 threads.

Root node processing (before b&c):
  Real time             =    0.16 sec. (127.75 ticks)
Parallel b&c, 12 threads:
  Real time             =    0.00 sec. (0.00 ticks)
  Sync time (average)   =    0.00 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =    0.16 sec. (127.75 ticks)
0.157000 
TRUE-modelo
Salgo lazy
   3246  1340     9975.4373   123    10777.0000     9590.0297  1396451   11.01%         X_4_4_5 N   3152   1130      9
Elapsed time = 107.61 sec. (80631.60 ticks, tree = 13.52 MB, solutions = 5)
   3396  1384    10227.2771   118    10777.0000     9590.0297  1438575   11.01%         X_4_2_4 D   3510   3502     19
*  3477  1343      integral     0    10748.0000     9590.0297  1400819   10.77%      Z_3_2_7_26 D   4033   4017    129
   3591  1412     9976.6684    90    10748.0000     9717.0946  1460714    9.59%         X_0_1_0 N   3036   1244     15
   3660  1479    10735.5046    73    10748.0000     9742.5989  1494288    9.35%              Y2 U   3442   3434     15
   3724  1420    10365.9084   119    10748.0000     9813.0738  1571549    8.70%         X_3_5_4 D   3451   3443     20
   3794  1451    10106.4488    66    10748.0000     9830.0142  1601886    8.54%         X_0_5_4 D   3790   3782     24
   3866  1474        cutoff          10748.0000     9852.7899  1622134    8.33%         X_1_5_0 U   3260   1203     11
   3950  1484        cutoff          10748.0000     9904.9687  1656522    7.84%         X_0_2_1 U   3778   3770     24
   4008  1467    10741.5417    45    10748.0000     9914.8403  1651113    7.75%         X_4_3_0 D   4489   4481     10
   4086  1486        cutoff          10748.0000     9933.4645  1700215    7.58%         X_1_5_4 U   5272   3160     11
Elapsed time = 121.02 sec. (89805.13 ticks, tree = 15.33 MB, solutions = 6)
   4147  1504    10032.9796    99    10748.0000     9957.1293  1774008    7.36%         X_3_4_0 N   4078   3605     14
   4233  1524    10516.5368    72    10748.0000     9964.9060  1798787    7.29%         X_3_0_2 D   4745   4737     26
   4315  1543    10569.1458   120    10748.0000     9969.5729  1833977    7.24%         X_1_5_4 D   4154    184     21
   4391  1520    10144.0095   129    10748.0000     9995.5545  1807604    7.00%         X_3_0_2 N   4905   1491     14
   4470  1571    10151.3791   109    10748.0000    10015.8227  1861704    6.81%         X_0_1_0 N   4470   1388     14
   4544  1568    10641.0137   123    10748.0000    10016.8326  1894917    6.80%         X_3_2_0 D   5041   5033     21
   4638  1588    10125.0313   123    10748.0000    10030.3381  1936820    6.68%         X_0_1_0 N   4582    883     20
   4716  1595        cutoff          10748.0000    10056.8418  1941433    6.43%         X_2_4_3 U   4686   4670     31
   4804  1606    10657.3768    65    10748.0000    10057.1803  1968423    6.43%         X_1_4_0 U   5880   4102     17
   4878  1666    10570.3696    94    10748.0000    10066.7903  2022080    6.34%         X_4_2_1 D   4830   4822     34
Elapsed time = 134.92 sec. (99441.11 ticks, tree = 17.81 MB, solutions = 6)
   4957  1685    10571.4565    79    10748.0000    10084.2595  2036647    6.18%         X_1_5_2 D   4575   4567     19
   5021  1695    10716.7783    72    10748.0000    10084.2595  2066651    6.18%         X_2_2_4 U   4771   4755     31
   5086  1698    10586.7640    80    10748.0000    10099.9638  2123352    6.03%         X_4_5_4 D   6184   6176     14
   5164  1740    10552.9176    97    10748.0000    10108.9024  2182508    5.95%         X_4_4_5 D   4946   4938     12
   5238  1744    10294.5792   117    10748.0000    10113.2736  2185304    5.91%         X_4_5_4 D   5010   5002     21
   5331  1738        cutoff          10748.0000    10132.7681  2210079    5.72%         X_2_3_4 U   6424   6408     17
   5413  1772    10247.0876    99    10748.0000    10134.3709  2260484    5.71%         X_3_2_0 U   5341   3282     20
   5488  1773    10567.2569   110    10748.0000    10148.8885  2277979    5.57%         X_4_0_1 D   5183   5175     17
   5560  1775    10544.3953    98    10748.0000    10150.6033  2274266    5.56%         X_2_4_0 U   5314   4271     22
   5774  1778    10580.3251    84    10748.0000    10186.2034  2397604    5.23%         X_0_5_4 N   6744   6736     17
Elapsed time = 154.38 sec. (111928.59 ticks, tree = 19.75 MB, solutions = 6)
   5931  1839        cutoff          10748.0000    10206.9883  2536786    5.03%         X_0_0_4 D   5751   5743     28
   6238  1858    10565.1024    84    10748.0000    10241.8261  2621048    4.71%         X_4_4_0 D   7144   7128     14
   6577  1846    infeasible          10748.0000    10262.8625  2683501    4.51%         X_1_5_0 U   7480   7472     21
   6864  1825    infeasible          10748.0000    10289.5151  2799223    4.27%         X_0_2_3 D   5940   5916     21
   7163  1721    10568.7256   101    10748.0000    10326.2915  2994862    3.92%         X_2_0_2 U   6955   6947     11
   7446  1677    10666.2969   103    10748.0000    10358.2447  3121224    3.63%         X_2_4_0 U   8185   8177     24
   7703  1678        cutoff          10748.0000    10380.6667  3172211    3.42%         X_0_5_4 U   7518    808     14
   8021  1599        cutoff          10748.0000    10407.5067  3328431    3.17%         X_0_4_0 U   7132    772     17
   8299  1525    10707.9677   108    10748.0000    10430.9583  3427495    2.95%         X_3_0_2 U   9184   6260     15
   8624  1271        cutoff          10748.0000    10467.6304  3640005    2.61%         X_3_4_5 U   8411   4734     23
Elapsed time = 213.67 sec. (150188.67 ticks, tree = 14.74 MB, solutions = 6)
   8998  1181        cutoff          10748.0000    10513.6352  3715092    2.18%         X_2_2_3 D   9122   9114     17
   9403   914    10692.0417   129    10748.0000    10549.4379  3881541    1.85%         X_2_0_2 U   8548   8622     21
   9850   642    10741.5417    41    10748.0000    10593.7717  3990886    1.43%         X_4_3_0 U   9895   9879     15

Cover cuts applied:  1743
Flow cuts applied:  24
Mixed integer rounding cuts applied:  28
Zero-half cuts applied:  12
Gomory fractional cuts applied:  4

Root node processing (before b&c):
  Real time             =   14.16 sec. (16896.22 ticks)
Parallel b&c, 8 threads:
  Real time             =  226.48 sec. (148487.80 ticks)
  Sync time (average)   =   27.79 sec.
  Wait time (average)   =    0.00 sec.
                          ------------
Total (root+branch&cut) =  240.64 sec. (165384.01 ticks)
240.641000 

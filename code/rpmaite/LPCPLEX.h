#ifndef _LPCPLEX_H_
#define _LPCPLEX_H_
#include "stdafx.h"

#include "ilcplex/ilocplex.h"
ILOSTLBEGIN

#pragma warning(disable:4786)
#pragma warning(disable:4996)


using namespace std;
/*
	IloIntVarArray  variable entera
	IloNumVarArray  variable real
*/
typedef IloIntVarArray NumVector;
typedef IloArray<IloNumVarArray> NumVarMatrixF;
typedef IloArray<IloIntVarArray> NumVarMatrix;
typedef IloArray<IloArray<IloNumVarArray> > NumVarMatrix3F;
typedef IloArray<IloArray<IloIntVarArray> > NumVarMatrix3;
typedef IloArray<IloArray<IloArray<IloIntVarArray> > > NumVarMatrix4;
typedef IloArray<IloBoolVarArray> BoolVarMatrix;



class LPCPLEX
{

	char m_nombre[200];

	IloEnv env;
	IloModel model;
	IloCplex cplex;
	IloEnv env2;
	IloModel model2;
	IloCplex cplex2;
	IloEnv env3;
	
public:

	double m_split_and_delivery(vector<cliente> l_cli, camion &cam, int maxcam, int multi, int esta,char* nom, datos datcallback,int maxtime, int modelo, list<camion>& solm);
	void add_restricciones(IloRangeArray &restr, IloRangeArray &restrlazy, int maxcam, int maxclientes, camion cam, vector<cliente> l_cli, NumVarMatrix3F &x_kij, NumVarMatrix4 & z_kitp, NumVector & y_k,NumVarMatrix3F &f_kij, int multi, bool es_lazy);
	void add_rest_estabilidad(IloRangeArray &restr, IloRangeArray &restrlazy, int maxcam,int maxclientes,camion cam, vector<cliente> l_cli, NumVarMatrix4& z_kitp, NumVarMatrix& l_kp,NumVarMatrix& r_kp,NumVarMatrix& s_kp, bool es_lazy);
	void escribe_csv(double VFO,double gap,IloCplex cplex,double secs,int maxcam, int maxclientes, camion cam,vector<cliente> l_cli, NumVarMatrix3F x_kij, NumVarMatrix4  z_kitp, NumVector  y_k,NumVarMatrix3F &f_kij,char* nom,datos datcallback, list<camion>& solm);
	void sol_to_draw(IloCplex cplex,int maxcam, int maxclientes, camion cam,vector<cliente> l_cli, NumVarMatrix3F x_kij, NumVarMatrix4  z_kitp, NumVector  y_k,NumVarMatrix s_kp ,char* nom,datos datcallback, list<camion>& solm, int esta, int multidrop);

	void add_sol_cw(IloRangeArray& restr2, int maxcam, int maxclientes, camion& cam, vector<cliente> l_cli, vector<datos> solcw, NumVarMatrix3F& x_kij, NumVarMatrix4& z_kitp, NumVector& y_k, NumVarMatrix3F& f_kij, NumVarMatrix& l_kp, NumVarMatrix& r_kp, NumVarMatrix& s_kp, IloCplex& cplex2);

	//modelo 1 cam
	bool modelo1cam(datos &dat,vector<int> ruta,vector<vector<double> > pesos, vector<vector<int> > p_t);
	void add_restri_1cam(IloRangeArray &restr2, int maxclientes, datos dat,NumVarMatrix& l_kp, NumVarMatrix& s_kp, NumVarMatrix& r_kp,NumVarMatrix4& m_kitp,NumVector& c_k, vector<vector<double> > pesos);

	//add sol de c&w
	void add_sol_cw(int maxcam, int maxclientes, camion & cam, vector<cliente> l_cli, vector<datos> solcw, NumVarMatrix3F& x_kij, NumVarMatrix4& z_kitp, NumVector& y_k, NumVarMatrix3F& f_kij, NumVarMatrix& l_kp, NumVarMatrix& r_kp, NumVarMatrix& s_kp, IloCplex& cplex2);
};


#endif
#include "stdafx.h"

bool intercambiar(datos & dat, int tok1, int tok2, int cli, int &contp)
{
	bool enc=false;
	int mitad=dat.posiciones/2;

	while(!enc)
	{
		if(tok2==tok1)
		{
			break;
		}
		else
		{
			if(dat.f1_cli.at(tok2)==cli)
			{
				int tmp=dat.f1_cli.at(tok2);
				dat.f1_cli.at(tok2)=dat.f1_cli.at(tok1);
				dat.f1_cli.at(tok1)=tmp;
				double tmp2=dat.f1_peso.at(tok2);
				dat.f1_peso.at(tok2)=dat.f1_peso.at(tok1);
				dat.f1_peso.at(tok1)=tmp2;
				contp++;
				enc=true;
			}
			else
			{
				if(dat.f1_cli.at(tok2+mitad)==cli)
				{
					int tmp=dat.f1_cli.at(tok2+mitad);
					dat.f1_cli.at(tok2+mitad)=dat.f1_cli.at(tok1);
					dat.f1_cli.at(tok1)=tmp;
					double tmp2=dat.f1_peso.at(tok2+mitad);
					dat.f1_peso.at(tok2+mitad)=dat.f1_peso.at(tok1);
					dat.f1_peso.at(tok1)=tmp2;
					contp++;
					enc=true;
				}
				else tok2++;
			}
		}
	}
	return enc;
}

void comp_pal_cli(datos dat, int &pos,int &cli, int &contp)
{
	if(contp==dat.cuantos.at(cli)) 
	{
		pos=cli;
		cli=dat.ruta.at(pos);
		contp=0;
	}
}


bool comprobar_peso(datos &dat)
{
	double pesototal=0;
	double pesopos1=0;
	double pesopos2=0;
	int mitad=dat.posiciones/2;
	for(int j=0; j < dat.posiciones/2; j++)
	{
		pesototal+=dat.f1_peso.at(j)+dat.f1_peso.at(j+mitad);
		double pos=(j*dat.largo_pallet)+(dat.largo_pallet/2);
		pesopos1+=dat.f1_peso.at(j)*(dat.pos_eje2-pos);
		pesopos1+=dat.f1_peso.at(j+mitad)*(dat.pos_eje2-pos);
		pesopos2+=dat.f1_peso.at(j)*(pos-dat.pos_eje1);
		pesopos2+=dat.f1_peso.at(j+mitad)*(pos-dat.pos_eje1);
	}
	double eje1=pesopos1/(dat.pos_eje2-dat.pos_eje1);
	double eje2=pesopos2/(dat.pos_eje2-dat.pos_eje1);
	if(eje1 > dat.max_eje1)
	{
		/*if(!mover_derecha(dat))
		{
			/*FILE *f=fopen(dat.fich,"a+");
			fprintf(f," FALSE ");
			fclose(f);*/
			dat.p_eje1=eje1;
			dat.p_eje2=eje2;
			dat.peso_ca=pesototal;
			return false;
	//	}
	}
	else
	{
		if(eje2 > dat.max_eje2)
		{
			/*if(!mover_izquierda(dat))
			{
				/*FILE *f=fopen(dat.fich,"a+");
				fprintf(f," FALSE ");
				fclose(f);*/
				dat.p_eje1=eje1;
				dat.p_eje2=eje2;
				dat.peso_ca=pesototal;
				return false;
			/*}
			else return true;*/
		}
		else 
		{
			dat.p_eje1=eje1;
			dat.p_eje2=eje2;
			dat.peso_ca=pesototal;
			return true;
		}
	}

}

bool mover_derecha(datos dat)
{
	double pesototal=0, pesopos1=0, pesopos2=0;
	//Intentar moverlo a la derecha, si hay posiciones libres.
	if(dat.f1_cli.at((dat.posiciones/2)-1)!=0 && dat.f1_cli.at(dat.posiciones-1)!=0)
		return false;
	else
	{
		bool fin=false;
		bool movido=false;
		while(!fin)
		{
			if(dat.f1_cli.at((dat.posiciones/2)-1)==0 && dat.f1_cli.at(dat.posiciones-1)==0)
			{
				//
				int mitad=dat.posiciones/2;
				for(int j=0; j < (dat.posiciones/2)-1; j++)
				{
					pesototal+=dat.f1_peso.at(j)+dat.f1_peso.at(j+mitad);
					double pos=((j+1)*dat.largo_pallet)+(dat.largo_pallet/2);
					pesopos1+=dat.f1_peso.at(j)*(dat.pos_eje2-pos);
					pesopos1+=dat.f1_peso.at(j+mitad)*(dat.pos_eje2-pos);
					pesopos2+=dat.f1_peso.at(j)*(pos-dat.pos_eje1);
					pesopos2+=dat.f1_peso.at(j+mitad)*(pos-dat.pos_eje1);
				}
				double eje1=pesopos1/(dat.pos_eje2-dat.pos_eje1);
				double eje2=pesopos2/(dat.pos_eje2-dat.pos_eje1);
				if((eje1 < dat.max_eje1) && (eje2 < dat.max_eje2))
				{
					movido=true;
					fin=true;
				}
				else
				{
					movido=false;
					fin=true;
				}
			}
			else
			{
				if(dat.f1_cli.at((dat.posiciones/2)-1)==0)
				{
					//
					int mitad=dat.posiciones/2;
					for(int j=0; j < (dat.posiciones/2)-1; j++)
					{
						pesototal+=dat.f1_peso.at(j)+dat.f1_peso.at(j+mitad);
						double pos=((j+1)*dat.largo_pallet)+(dat.largo_pallet/2);
						double posf2=((j)*dat.largo_pallet)+(dat.largo_pallet/2);
						pesopos1+=dat.f1_peso.at(j)*(dat.pos_eje2-pos);
						pesopos1+=dat.f1_peso.at(j+mitad)*(dat.pos_eje2-posf2);
						pesopos2+=dat.f1_peso.at(j)*(pos-dat.pos_eje1);
						pesopos2+=dat.f1_peso.at(j+mitad)*(posf2-dat.pos_eje1);
					}
					double eje1=pesopos1/(dat.pos_eje2-dat.pos_eje1);
					double eje2=pesopos2/(dat.pos_eje2-dat.pos_eje1);
					if((eje1 < dat.max_eje1) && (eje2 < dat.max_eje2))
					{
						movido=true;
						fin=true;
					}
					else
					{
						movido=false;
						fin=true;
					}
				}
				else
				{
					if(dat.f1_cli.at(dat.posiciones-1)==0)
					{
						//
						int mitad=dat.posiciones/2;
						for(int j=0; j < (dat.posiciones/2)-1; j++)
						{
							pesototal+=dat.f1_peso.at(j)+dat.f1_peso.at(j+mitad);
							double pos=((j+1)*dat.largo_pallet)+(dat.largo_pallet/2);
							double posf1=((j)*dat.largo_pallet)+(dat.largo_pallet/2);
							pesopos1+=dat.f1_peso.at(j)*(dat.pos_eje2-posf1);
							pesopos1+=dat.f1_peso.at(j+mitad)*(dat.pos_eje2-pos);
							pesopos2+=dat.f1_peso.at(j)*(posf1-dat.pos_eje1);
							pesopos2+=dat.f1_peso.at(j+mitad)*(pos-dat.pos_eje1);
						}
						double eje1=pesopos1/(dat.pos_eje2-dat.pos_eje1);
						double eje2=pesopos2/(dat.pos_eje2-dat.pos_eje1);
						if((eje1 < dat.max_eje1) && (eje2 < dat.max_eje2))
						{
							movido=true;
							fin=true;
						}
						else
						{
							movido=false;
							fin=true;
						}
					}
				}
			}
		}
			return movido;
	}

}

bool mover_izquierda(datos dat)
{
	double pesototal=0, pesopos1=0, pesopos2=0;
	//Intentar moverlo a la derecha, si hay posiciones libres.
	if(dat.f1_cli.at(0)!=0 && dat.f1_cli.at(dat.posiciones/2)!=0)
		return false;
	else
	{
		bool fin=false;
		bool movido=false;
		while(!fin)
		{
			if(dat.f1_cli.at(0)==0  && dat.f1_cli.at(dat.posiciones/2)==0)
			{
				//
				int mitad=dat.posiciones/2;
				for(int j=(dat.posiciones/2)-1; j > 0; j--)
				{
					pesototal+=dat.f1_peso.at(j)+dat.f1_peso.at(j+mitad);
					double pos=((j-1)*dat.largo_pallet)+(dat.largo_pallet/2);
					pesopos1+=dat.f1_peso.at(j)*(dat.pos_eje2-pos);
					pesopos1+=dat.f1_peso.at(j+mitad)*(dat.pos_eje2-pos);
					pesopos2+=dat.f1_peso.at(j)*(pos-dat.pos_eje1);
					pesopos2+=dat.f1_peso.at(j+mitad)*(pos-dat.pos_eje1);
				}
				double eje1=pesopos1/(dat.pos_eje2-dat.pos_eje1);
				double eje2=pesopos2/(dat.pos_eje2-dat.pos_eje1);
				if((eje1 < dat.max_eje1) && (eje2 < dat.max_eje2))
				{
					movido=true;
					fin=true;
				}
				else
				{
					movido=false;
					fin=true;
				}
			}
		}
			return movido;
	}

}
void heuristico (datos& dat)
{
//si No es posible el multidrop, a�adir el corte
	int tok1=(dat.posiciones/2)-1;
	int mitad=dat.posiciones/2;
	int tok2=0;
	bool fin=false;
	int pos=0;
	int cli=dat.ruta.at(pos);
	int contp=0;
	bool enc=false;
	while(!fin)
	{
		//si las posiciones est�n vacias, busco la primera ocupada
		while(!fin && (dat.f1_cli.at(tok1)==0) && (dat.f1_cli.at(tok1+mitad)==0))
		{
			tok1--;
			if(tok1==-1)fin=true;
		}
		if(!fin)
		{
			// A y b son del mismo cli
			if((dat.f1_cli.at(tok1)==cli) && (dat.f1_cli.at(tok1+mitad)==cli))
			{
				contp=contp+2;
				comp_pal_cli(dat, pos,cli,contp);
			}
			else
			{
				//si A es del cli
				if((dat.f1_cli.at(tok1)!=0) && (dat.f1_cli.at(tok1)==cli))
				{
					contp++;
					comp_pal_cli(dat, pos,cli,contp);
					if((dat.f1_cli.at(tok1+mitad)!=0) && (dat.f1_cli.at(tok1+mitad)==cli))
					{
						contp++;
						comp_pal_cli(dat, pos,cli,contp);
					}
					else
					{
						//intercambiar B
						if(dat.f1_cli.at(tok1+mitad)!=0)
						{
							intercambiar(dat,tok1+mitad,tok2,cli,contp);
							comp_pal_cli(dat, pos,cli,contp);
						}
					}
				}
				else
				{
			
					//si b es del cli
					if((dat.f1_cli.at(tok1+mitad)!=0) && (dat.f1_cli.at(tok1+mitad)==cli))
					{
						contp++;
						if(contp==dat.cuantos.at(cli)) 
						{
							pos=cli;
							cli=dat.ruta.at(pos);
							contp=0;
							//si cambia el cliente, puede que A sea del nuevo
							if((dat.f1_cli.at(tok1)!=0) && (dat.f1_cli.at(tok1)==cli))
							{
								contp++;
								comp_pal_cli(dat, pos,cli,contp);
							}else
							{
								//intercambiar A
								if(dat.f1_cli.at(tok1)!=0)
								{
									intercambiar(dat,tok1,tok2,cli,contp);
									comp_pal_cli(dat, pos,cli,contp);
								}
							}
						}
						else
						{
							if((dat.f1_cli.at(tok1)!=0))
							{
								//intercambiar A
								intercambiar(dat,tok1,tok2,cli,contp);
								comp_pal_cli(dat, pos,cli,contp);
							}
						}
					}
					else
					{
						//intercambiar A Y B
						if(dat.f1_cli.at(tok1)!=0)
						{
							intercambiar(dat,tok1,tok2,cli,contp);
							if(contp==dat.cuantos.at(cli)) 
							{
								pos=cli;
								cli=dat.ruta.at(pos);
								contp=0;
								if((dat.f1_cli.at(tok1+mitad)!=0) && (dat.f1_cli.at(tok1+mitad)==cli))
								{
									contp++;
									comp_pal_cli(dat, pos,cli,contp);
								}
								else
								{
									if(dat.f1_cli.at(tok1+mitad)!=0)
									{
										intercambiar(dat,tok1+mitad,tok2,cli,contp);		
										comp_pal_cli(dat, pos,cli,contp);
									}
								}
							}
							else
							{
								if(dat.f1_cli.at(tok1+mitad)!=0)
								{
									intercambiar(dat,tok1+mitad,tok2,cli,contp);		
									comp_pal_cli(dat, pos,cli,contp);
								}
							}
						}
					}
				}
			}
			tok1--;
			if(tok1==-1)fin=true;
			}
		}
}

bool heuristico2 (datos& dat, vector<vector<double> > pesosp, vector<vector<int> > p_t)
{
	int mitad=dat.posiciones/2;
	int pos1=mitad-1;
	int pos2=pos1+mitad;
	int cli;
	//vector<vector<double> > pesosp;
	vector<double> aux;
	vector<int> auxint;
	//vector<vector<int> > p_t;
	double pesopos1=0,pesopos2=0;
	vector<bool> visitado;
	bool espacio_f1=false, espacio_f2=false;
	int prim_pos=-1;
	

	
	for(int i=0; i <= dat.maxcli; i++)
	{
		//pesosp.push_back(aux);
		visitado.push_back(false);
		//p_t.push_back(auxint);
	}

	//for(int p=0; p < dat.posiciones; p++)
	//{
	//	if (dat.f1_cli.at(p) != 0)
	//	{
	//		pesosp.at(dat.f1_cli.at(p)).push_back(dat.f1_peso.at(p));
	//		p_t.at(dat.f1_cli.at(p)).push_back(dat.f1_t.at(p));
	//	}
	//}*/
	
	vector<double>::iterator itp;
	vector<int>::iterator itt;
	bool fin=false;
	double pesototal=0;
	

	//Busco el primero a descargar
	cli = dat.ruta.at(0);
	visitado.at(cli)=true;
	while (!fin)
	{
		itp=pesosp.at(cli).begin();
		itt = p_t.at(cli).begin();
		while(!fin && itp != pesosp.at(cli).end())
		{
			//Pongo el pallet en la primera posicion libre desde la puerta, fila1
			dat.f1_cli[pos1]=cli;
			dat.f1_peso[pos1]=(*itp);
			dat.f1_t[pos1] = (*itt);
			pesototal+=(*itp);
			//Calculos para el peso en los ejes
			double pos=(pos1*dat.largo_pallet)+(dat.largo_pallet/2);
			pesopos1+=dat.f1_peso.at(pos1)*(dat.pos_eje2-pos);
			pesopos2+=dat.f1_peso.at(pos1)*(pos-dat.pos_eje1);
			
			pos1--;
			itp++;
			itt++;
			if(itp!=pesosp.at(cli).end())
			{
				//Pongo el pallet en la primera posicion libre desde la puerta, fila2
				dat.f1_cli[pos2]=cli;
				dat.f1_peso[pos2]=(*itp);
				dat.f1_t[pos2] = (*itt);
				//PONER EL T
				pesototal+=(*itp);
				pesopos1+=dat.f1_peso.at(pos2)*(dat.pos_eje2-pos);
				pesopos2+=dat.f1_peso.at(pos2)*(pos-dat.pos_eje1);

				pos2--;
				itp++;
				itt++;
				if(itp==pesosp.at(cli).end()) //si ya no hay mas pllates de ese cliente busco el siguiente
				{
					cli=dat.ruta.at(cli);
					if(cli==0 || visitado.at(cli)==true) fin=true;
					else 
					{
						itp=pesosp.at(cli).begin();
						itt = p_t.at(cli).begin();
						visitado.at(cli)=true;
					}
				}
			}
			else
			{
				//siguiente cliente de la ruta
				cli=dat.ruta.at(cli);
				if(cli==0 || visitado.at(cli)==true) fin=true;
				else
				{
					visitado.at(cli)=true;
					itp=pesosp.at(cli).begin();
					itt = p_t.at(cli).begin();

					dat.f1_cli[pos2]=cli;
					dat.f1_peso[pos2]=(*itp);
					dat.f1_t[pos2] = (*itt);
					pesototal+=(*itp);
					pesopos1+=dat.f1_peso.at(pos2)*(dat.pos_eje2-pos);
					pesopos2+=dat.f1_peso.at(pos2)*(pos-dat.pos_eje1);

					pos2--;
					itp++;
					itt++;
					if(itp==pesosp.at(cli).end()) //si ya no hay mas pllates de ese cliente busco el siguiente
					{
						cli=dat.ruta.at(cli);
						if(cli==0 || visitado.at(cli)==true) fin=true;
						else 
						{
							itp=pesosp.at(cli).begin();
							itt = p_t.at(cli).begin();
							visitado.at(cli)=true;
						}
					}
				}
			}
		}
		//Relleno de 0 en las posiciones que se han quedado libres
		if(pos1 > 0)
		{
			prim_pos=pos1+1;
			for(int p=pos1; p >= 0; p--)
			{
				dat.f1_cli[p]=0;
				dat.f1_peso[p]=0;
				dat.f1_t[p] = -1;
				espacio_f1=true;
			}
		}
		if (pos2 > (mitad - 1))
		{
			if (pos2 >= (prim_pos + mitad)) prim_pos = pos2 + 1;
			for (int p = pos2; p >= mitad; p--)
			{
				dat.f1_cli[p] = 0;
				dat.f1_peso[p] = 0;
				dat.f1_t[p] = -1;
				espacio_f2 = true;
			}
		}
		prim_pos = min(pos1+1,pos2-(dat.posiciones/2)+1); //primera posicion donde hay un pallet

		//Calculo el peso de los ejes
		double eje1=pesopos1/(dat.pos_eje2-dat.pos_eje1);
		double eje2=pesopos2/(dat.pos_eje2-dat.pos_eje1);

		//Si el eje 1 se ha sobrepasado, no puedo mover nada para quitarle peso.
		if(eje1 > dat.max_eje1) return false;
		else
		{
			//Si el eje 2 se ha sobrepasado,intento mover la carga hacia la cabina
			if(eje2 > dat.max_eje2) 
			{
				//Si hay espacio
				if(espacio_f1==false || espacio_f2==false) return false;
				else
				{
					bool conseguido=mov_izq(dat,prim_pos);
					if(conseguido)return true;
					else return false;
				}
			}
			else 
			{
				//Devuelvo los valores
				dat.p_eje1=eje1;
				dat.p_eje2=eje2;
				dat.peso_ca=pesototal;
				return true;
			}
		}
	}
}

bool mov_der(datos &dat)
{
	bool conseguido=false;
	bool fin=false;

	while (!fin)
	{
		int mid=dat.posiciones/2;
		if(dat.f1_cli.at(dat.posiciones-1)==0 && dat.f1_cli.at((dat.posiciones/2)-1)==0)
		{
			for(int p=(dat.posiciones/2)-2; p >0; p--)
			{
				dat.f1_cli.at(p+1)=dat.f1_cli.at(p);
				dat.f1_peso.at(p+1)=dat.f1_cli.at(p);
				dat.f1_cli.at(p+1+mid)=dat.f1_cli.at(p+mid);
				dat.f1_peso.at(p+1+mid)=dat.f1_cli.at(p+mid);
			}
			dat.f1_cli.at(0)=0;
			dat.f1_peso.at(0)=0;
			dat.f1_cli.at(mid)=0;
			dat.f1_peso.at(mid)=0;
		}
		else
		{
			if(dat.f1_cli.at(dat.posiciones-1)==0)
			{
				for(int p=(dat.posiciones/2)-2; p >0; p--)
				{
					dat.f1_cli.at(p+1)=dat.f1_cli.at(p);
					dat.f1_peso.at(p+1)=dat.f1_cli.at(p);
				}
				dat.f1_cli.at(0)=0;
				dat.f1_peso.at(0)=0;
			}
			else
			{
				for(int p=(dat.posiciones/2)-2; p >0; p--)
				{
					dat.f1_cli.at(p+1+mid)=dat.f1_cli.at(p+mid);
					dat.f1_peso.at(p+1+mid)=dat.f1_cli.at(p+mid);
				}
				dat.f1_cli.at(mid)=0;
				dat.f1_peso.at(mid)=0;
			}
		}
		
		if(comprobar_peso(dat)) 
		{
			fin=true;
			conseguido=true;
		}
		else
		{
			if(dat.f1_cli.at(dat.posiciones-1)!=0 || dat.f1_cli.at((dat.posiciones/2)-1)!=0)
				fin=true;
		}
	}
	return conseguido;
}

bool mov_izq(datos &dat, int prim_pos)
{
	bool conseguido=false;
	bool fin=false;
	int primer=-1;


	while (!fin)
	{
		int mid=dat.posiciones/2;
		if (prim_pos < mid)
		{
			for (int p = prim_pos; p < dat.posiciones / 2; p++)
			{
				dat.f1_cli.at(p - 1) = dat.f1_cli.at(p);
				dat.f1_peso.at(p - 1) = dat.f1_peso.at(p);
				dat.f1_t.at(p - 1) = dat.f1_t.at(p);
				dat.f1_cli.at(p + mid - 1) = dat.f1_cli.at(p + mid);
				dat.f1_peso.at(p + mid - 1) = dat.f1_peso.at(p + mid);
				dat.f1_t.at(p + mid - 1) = dat.f1_t.at(p + mid);
			}
			dat.f1_cli.at(mid - 1) = 0;
			dat.f1_peso.at(mid - 1) = 0;
			dat.f1_t.at(mid - 1) = -1;
			dat.f1_cli.at(dat.posiciones - 1) = 0;
			dat.f1_peso.at(dat.posiciones - 1) = 0;
			dat.f1_t.at(dat.posiciones - 1) = -1;
		}
		else
		{
			for (int p = prim_pos; p < dat.posiciones ; p++)
			{
				dat.f1_cli.at(p - 1) = dat.f1_cli.at(p);
				dat.f1_peso.at(p - 1) = dat.f1_peso.at(p);
				dat.f1_t.at(p - 1) = dat.f1_t.at(p);
				dat.f1_cli.at(p - mid - 1) = dat.f1_cli.at(p - mid);
				dat.f1_peso.at(p - mid - 1) = dat.f1_peso.at(p - mid);
				dat.f1_t.at(p - mid - 1) = dat.f1_t.at(p - mid);
			}
			dat.f1_cli.at(mid - 1) = 0;
			dat.f1_peso.at(mid - 1) = 0;
			dat.f1_t.at(mid - 1) = -1;
			dat.f1_cli.at(dat.posiciones - 1) = 0;
			dat.f1_peso.at(dat.posiciones - 1) = 0;
			dat.f1_t.at(dat.posiciones - 1) = -1;
		}

		if(comprobar_peso(dat)) 
		{
			fin=true;
			conseguido=true;
		}
		else
		{
			if(dat.f1_cli.at(0)!=0 && dat.f1_cli.at((dat.posiciones/2))!=0) 
				fin=true;
			if(dat.p_eje1 > dat.max_eje1) 
				fin=true;

			if(fin!=true)
			{
				prim_pos--;
			}
		}
	}
	return conseguido;
}
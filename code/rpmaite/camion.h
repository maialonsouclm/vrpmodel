

#ifndef CAMION_H
#define CAMION_H

class camion
{
public:
	int id;
	double largo;
	double ancho;
	double alto;
	double pos_eje1;
	double pos_eje2;
	double max_peso_eje1;
	double max_peso_eje2;
	double max_total_peso;
	double vol;
	double CGx;
	double CGy;
	double peso_eje1;
	double peso_eje2;
	double peso_total;
	//list<bloque> solucion; //lista de bloques de cajas colocados
	int posiciones;
	double ancho_pallet;
	double largo_pallet;
	double alto_pallet;
	double peso_pallet;
	double peso_vacio;
	vector<pallet> carga;  //posiciones de los pallets en la carga de este camion
	vector<int> pos_carga;
	vector<double> peso_pos;
	vector<int> ruta;
	vector<double> peso_por_cli;
	double suma1;
	double suma2;
	double media_peso;
	bool compartido;

	camion(void){};
	void add_datos(int idc, double w, double h, double l, double peso, double d1,double p1,double d2,double p2, double ap, double lp, double hp, double pp, int posi)
	{
		id=idc;
		ancho=w;
		largo=l;
		alto=h;
		max_total_peso=peso;
		pos_eje1=d1;
		pos_eje2=d2;
		max_peso_eje1=p1;
		max_peso_eje2=p2;
		vol=0;
		CGx=0;
		CGy=0;
		peso_eje1=0;
		peso_eje2=0;
		peso_total=0;
		
		posiciones=posi;
		ancho_pallet=ap;
	    largo_pallet=lp;
		alto_pallet=hp;
		peso_pallet=pp;
		peso_vacio=2000;
		for(int i=0; i < posi;i++)
		{
			pos_carga.push_back(0);
			peso_pos.push_back(0);
			ruta.push_back(-1);
			peso_por_cli.push_back(0);
		}
		suma1=0;
		suma2=0;
		compartido = false;
	};

};
#endif
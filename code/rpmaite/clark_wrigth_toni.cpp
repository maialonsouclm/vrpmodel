#include "stdafx.h"
#include "LPCPLEX.h"
#include <assert.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <ilconcert/ilolinear.h>
extern  int DibujarOpenGL();

/* Rellena en tantos camiones como quepan los pallets de un solo cliente */
void rellenar_camiones_un_cliente(vector<datos>& sol,int posmedio, vector<cliente> lpc, int cliente,camion truck)
{
	

	//Variables
	bool fin = false;
	bool finder = false;
	bool finizq = false;
	double totalpes = 0;
	double pesopos1 = 0;
	double pesopos2 = 0;
	int izq = posmedio;
	int der = posmedio + 1;
	int cont = 0;
	bool solouno_izq = false;
	bool solouno_der = false;
	list<pallet>::iterator itdem, itsig;

	itdem = lpc.at(cliente).pedidos.begin();
	itsig = itdem;
	itsig++;
	int num_pal = (int)lpc.at(cliente).pedidos.size();

	//hasta que no ponga todos los pallets del cliente
	
	//while (itdem != lpc.at(cliente).pedidos.end())
	while (num_pal > 0)
	{
		
		fin = false;
		finder = false;
		finizq = false;
		totalpes = 0;
		pesopos1 = 0;
		pesopos2 = 0;
		izq = posmedio;
		der = posmedio + 1;
		cont = 0;
		solouno_izq = false;
		solouno_der = false;

		//abro cami�n
		datos dat_new;
		rellena_callback(dat_new, lpc, truck, 10, (int)lpc.size(), "  ", -1);

		//ponemos su ruta deposito - cliente - deposito
		dat_new.ruta[0] = cliente;
		dat_new.ruta[cliente] = 0;
		dat_new.fo_heu = lpc.at(0).distancias.at(cliente);
		dat_new.fo_heu += lpc.at(cliente).distancias.at(0);
		dat_new.num_pallets = 0;
		
		
		
		while (!fin)
		{
			if (num_pal >1)  //si queda numero par de pallets
			{
				if (!finizq && izq >=0) //si puedo poner pallet en la parte izquierda
				{
					//intento colocar dos, uno arriba y otro abajo
					if (colocar2((*itdem), (*itsig), izq, dat_new, totalpes, pesopos1, pesopos2, cliente))
					{
						//muevo los iteradores a los siguientes pallets y descuento los colocados
						lpc.at(cliente).pedidos.erase(itdem);
						lpc.at(cliente).pedidos.erase(itsig);
						itdem= lpc.at(cliente).pedidos.begin();
						if (itdem != lpc.at(cliente).pedidos.end())
						{
							itsig = itdem;
							itsig++;
						}
						num_pal--;
						num_pal--;
						izq--;
						if (izq < 0)finizq = true;
					}
					else
					{
						//si no consigo poner 2 pallets intento poner solo 1
						if (num_pal >=1 && solouno_der == false && colocar((*itdem), izq, dat_new, totalpes, pesopos1, pesopos2, cliente))
						{
							//Indico qu en este lado solo hay un pallet arriba y muevo los iteradores y descuento el pallet
							solouno_izq = true;
							lpc.at(cliente).pedidos.erase(itdem);
							itdem = lpc.at(cliente).pedidos.begin();
							if (itdem != lpc.at(cliente).pedidos.end())
							{
								itsig = itdem;
								itsig++;
							}
							num_pal--;
							izq--;
							finizq = true;
						}
						else
						{
							if (num_pal >= 1 && solouno_der == false) 
							{
								list<pallet>::iterator itaux;
								itaux = itdem;
								itaux++;
								bool enc = false;
								while (itaux != lpc.at(cliente).pedidos.end() && !enc)
								{
									if (colocar((*itaux), izq, dat_new, totalpes, pesopos1, pesopos2, cliente))
									{
										enc = true;
										solouno_izq = true;
										lpc.at(cliente).pedidos.erase(itaux);
										itdem = lpc.at(cliente).pedidos.begin();
										if (itdem != lpc.at(cliente).pedidos.end())
										{
											itsig = itdem;
											itsig++;
										}
										num_pal--;
										izq--;
									}
									else itaux++;
								}
							}
							finizq = true;  // si no puedo poner nada m�s en este lado, lo indico
						}
					}
				} //no finizq;

				if (!finder && der<=15) //sipuedo paner pallets a la derecha
				{
					//intento poner dos pallets uno arriba y otro abajo.
					if (num_pal >=2 && colocar2((*itdem), (*itsig), der, dat_new, totalpes, pesopos1, pesopos2, cliente))
					{
						//muevo los iteradores a los siguientes pallets y descuento los colocados
						lpc.at(cliente).pedidos.erase(itdem);
						lpc.at(cliente).pedidos.erase(itsig);
						itdem = lpc.at(cliente).pedidos.begin();
						if (itdem != lpc.at(cliente).pedidos.end())
						{
							itsig = itdem;
							itsig++;
						}
						num_pal--;
						num_pal--;
						der++;
						if (der > 15) finder = true;
					}
					else
					{
						//si no consigo poner 2 pallets intento poner solo 1
						if (num_pal >=1 && solouno_izq == false && colocar((*itdem), der, dat_new, totalpes, pesopos1, pesopos2, cliente))
						{
							//Indico qu en este lado solo hay un pallet arriba y muevo los iteradores y descuento el pallet
							solouno_der = true;
							lpc.at(cliente).pedidos.erase(itdem);
							itdem = lpc.at(cliente).pedidos.begin();
							if (itdem != lpc.at(cliente).pedidos.end())
							{
								itsig = itdem;
								itsig++;
							}
							num_pal--;
							der++;
							finder = true;
						}
						else
						{
							if (num_pal >= 1 && solouno_izq == false)
							{
								list<pallet>::iterator itaux;
								itaux = itdem;
								itaux++;
								bool enc = false;
								while (itaux != lpc.at(cliente).pedidos.end() && !enc)
								{
									if (colocar((*itaux), der, dat_new, totalpes, pesopos1, pesopos2, cliente))
									{
										enc = true;
										solouno_der = true;
										lpc.at(cliente).pedidos.erase(itaux);
										itdem = lpc.at(cliente).pedidos.begin();
										if (itdem != lpc.at(cliente).pedidos.end())
										{
											itsig = itdem;
											itsig++;
										}
										num_pal--;
										der++;
									}
									else itaux++;
								}
							}
							finder = true;
						}
					}
				}//nofinder
				if (finizq && finder)
				{
					fin = true;
					dat_new.p_eje1 = pesopos1 / (truck.pos_eje2 - truck.pos_eje1);
					dat_new.p_eje2 = pesopos2 / (truck.pos_eje2 - truck.pos_eje1);
					dat_new.peso_ca = totalpes;
					if (dat_new.num_pallets == truck.posiciones) dat_new.lleno = true;
					sol.push_back(dat_new);
				}
			}
			else //solo queda un pallet
			{
				if (num_pal > 0)
				{
					//si lo puedo poner tanto a la izquierda como a la derecha
					if (!solouno_der && !solouno_izq)
					{
						//lo intento poner a la izquierda, primero
						if (izq >=0 && colocar((*itdem), izq, dat_new, totalpes, pesopos1, pesopos2, cliente))
						{
							itdem++;
							num_pal--;
							fin = true;
							dat_new.p_eje1 = pesopos1 / (truck.pos_eje2 - truck.pos_eje1);
							dat_new.p_eje2 = pesopos2 / (truck.pos_eje2 - truck.pos_eje1);
							dat_new.peso_ca = totalpes;
							if (dat_new.num_pallets == truck.posiciones) dat_new.lleno = true;
							sol.push_back(dat_new);
						}
						else
						{
							//si no lo consigo, intento ponerlo a la derecha
							if ( der <=15 && colocar((*itdem), der, dat_new, totalpes, pesopos1, pesopos2, cliente))
							{
								itdem++;
								num_pal--;
								fin = true;
								dat_new.p_eje1 = pesopos1 / (truck.pos_eje2 - truck.pos_eje1);
								dat_new.p_eje2 = pesopos2 / (truck.pos_eje2 - truck.pos_eje1);
								dat_new.peso_ca = totalpes;
								if (dat_new.num_pallets == truck.posiciones) dat_new.lleno = true;
								sol.push_back(dat_new);
							}
							else //si tampoco lo consigo, cierro el cami�n
							{
								fin = true;
								dat_new.p_eje1 = pesopos1 / (truck.pos_eje2 - truck.pos_eje1);
								dat_new.p_eje2 = pesopos2 / (truck.pos_eje2 - truck.pos_eje1);
								dat_new.peso_ca = totalpes;
								if (dat_new.num_pallets == truck.posiciones) dat_new.lleno = true;
								sol.push_back(dat_new);
							}
						}
					}
					else
					{
						if (solouno_der == false) //si solo lo puedo poner a la izquierda
						{
							if (colocar((*itdem), izq, dat_new, totalpes, pesopos1, pesopos2, cliente))
							{
								itdem++;
								num_pal--;
								fin = true;
								dat_new.p_eje1 = pesopos1 / (truck.pos_eje2 - truck.pos_eje1);
								dat_new.p_eje2 = pesopos2 / (truck.pos_eje2 - truck.pos_eje1);
								dat_new.peso_ca = totalpes;
								if (dat_new.num_pallets == truck.posiciones) dat_new.lleno = true;
								sol.push_back(dat_new);
							}
							else //si tampoco lo consigo, cierro el cami�n
							{
								fin = true;
								dat_new.p_eje1 = pesopos1 / (truck.pos_eje2 - truck.pos_eje1);
								dat_new.p_eje2 = pesopos2 / (truck.pos_eje2 - truck.pos_eje1);
								dat_new.peso_ca = totalpes;
								if (dat_new.num_pallets == truck.posiciones) dat_new.lleno = true;
								sol.push_back(dat_new);
							}
						}
						else
						{
							if (solouno_izq == false) //si solo lo puedo colocar en la derecha
							{
								if (colocar((*itdem), der, dat_new, totalpes, pesopos1, pesopos2, cliente))
								{
									itdem++;
									num_pal--;
									fin = true;
									dat_new.p_eje1 = pesopos1 / (truck.pos_eje2 - truck.pos_eje1);
									dat_new.p_eje2 = pesopos2 / (truck.pos_eje2 - truck.pos_eje1);
									dat_new.peso_ca = totalpes;
									if (dat_new.num_pallets == truck.posiciones) dat_new.lleno = true;
									sol.push_back(dat_new);
								}
								else//si tampoco lo consigo, cierro el cami�n
								{
									fin = true;
									dat_new.p_eje1 = pesopos1 / (truck.pos_eje2 - truck.pos_eje1);
									dat_new.p_eje2 = pesopos2 / (truck.pos_eje2 - truck.pos_eje1);
									dat_new.peso_ca = totalpes;
									if (dat_new.num_pallets == truck.posiciones) dat_new.lleno = true;
									sol.push_back(dat_new);
								}
							}
						}
					}
				}
				else
				{
					fin = true;
					dat_new.p_eje1 = pesopos1 / (truck.pos_eje2 - truck.pos_eje1);
					dat_new.p_eje2 = pesopos2 / (truck.pos_eje2 - truck.pos_eje1);
					dat_new.peso_ca = totalpes;
					if (dat_new.num_pallets == truck.posiciones) dat_new.lleno = true;
					sol.push_back(dat_new);
				}
			}
		}

	}
}

vector<datos> clarkwrigth_toni(list<camion>& solucion, camion truck, vector<cliente> l_clientes, datos dat)
{
	vector<datos> sol;
	int cont = 0;
	//int num_ruta;
	vector<ped_cli> l_sol;
	list<pallet>::iterator itdem;
	list<vector<int> > rutas;
	list<vector<int> >::iterator it_ru;
	vector<int> cliente_ruta;
	vector<int> ruta2;
	double totalpes = 0;
	double pesopos1 = 0;
	double pesopos2 = 0;
	double p = 0;
	bool fin = false, finder = false, finizq = false;
	//int izq;
	//int der;
	
	//Calculo cual es la posici�n del medio, en funci�n de los pesos de los ejes.
	double midp = ((truck.max_peso_eje2*truck.pos_eje2) + (truck.max_peso_eje1*truck.pos_eje1)) / (truck.max_peso_eje1 + truck.max_peso_eje2);
	int posmed =(int) floor((midp / truck.largo_pallet) + 0.5);
	int frente = truck.posiciones / 2;

	

	// inicializar rutas a un solo cliente
	//coloco todos los pallets del cliente i en tantos camiones como necesite
	for (int i = 1; i < l_clientes.size(); i++)
	{
		rellenar_camiones_un_cliente(sol, posmed, l_clientes, i, truck);
	}

	/*****************************************************************/
	// CALCULO LOS AHORROS de todas las rutas que he generado
	/*****************************************************************/
	bool posible = true;
	while (posible)
	{
		list<ahorro_toni> ahorros;
		list<ahorro_toni>::iterator it_sa;
		posible = false;

		for (int i = 0;i < sol.size() - 1;i++)
		{
			if (sol.at(i).lleno)  continue;  //si el cami�n va completo, no voy a poder juntarlo con ning�n otro.

			for (int ii = i + 1;ii < sol.size();ii++)
			{
				if (sol.at(ii).lleno) continue;
				
				//si no cabe porque se excede el numero de posiciones pasa al siguiente
				if ((sol[i].num_pallets + sol[ii].num_pallets) > truck.posiciones) continue;
				
				//si el peso excede el m�ximo no se puede juntar.
				if ((sol[i].peso_ca + sol[ii].peso_ca) > truck.max_total_peso) continue;
				
				ahorro_toni ah;
				// Calcular ahorros de unir el cami�n i, con el ii
				int next = sol[i].ruta[0];
				while (sol[i].ruta[next] != 0)
					next = sol[i].ruta[next];

				//calculo el ahorro d(i,o)+d(0,ii)-d(i,ii)
				double aux = l_clientes.at(next).distancias.at(0) + l_clientes.at(0).distancias.at(sol[ii].ruta[0]) - l_clientes.at(next).distancias.at(sol[ii].ruta[0]);
				if (aux > 0)
				{
					ah.first_route = i;
					ah.second_route = ii;
					ah.sav = aux;
					ahorros.push_back(ah);
				}
				// Calcular ahorros opcion de unir el cami�n ii y luego el i
				next = sol[ii].ruta[0];
				while (sol[ii].ruta[next] != 0)
					next = sol[ii].ruta[next];

				aux = l_clientes.at(next).distancias.at(0) + l_clientes.at(0).distancias.at(sol[i].ruta[0]) - l_clientes.at(next).distancias.at(sol[i].ruta[0]);
				if (aux > 0)
				{
					ah.first_route = ii;
					ah.second_route = i;
					ah.sav = aux;
					ahorros.push_back(ah);
				}
			}
		}


		//ordeno la lista de mayor a menor ahorro.
		ahorros.sort(ahorroComparator());

		/*****************************************************************/
		// chequear packing y coger mejor opcion
		/*****************************************************************/
		for (it_sa = ahorros.begin(); it_sa != ahorros.end(); it_sa++)
		{
			int pallets_ruta = 0;
			datos dat_new; // copio de la estructura dat pasada por argumento
			rellena_callback(dat_new, l_clientes, truck, dat.maxcam, dat.maxcli, dat.nominsta, dat.modelo);

			
			// Genero la ruta unificada de los dos camiones.
			vector<int> visitados;
			vector<int>::iterator itv;
			bool visit = false;
			int next = sol[(*it_sa).first_route].ruta[0];
			int prev = 0;
			while (next != 0)
			{
				visit = false;
				for (itv = visitados.begin(); itv != visitados.end(); itv++)
				{
					if ((*itv) == next)
					{
						visit = true;
						break;
					}
				}
				if (!visit)
				{
					dat_new.ruta[prev] = next;
					visitados.push_back(next);
					prev = next;
					next = sol[(*it_sa).first_route].ruta[next];
				}
				else next = sol[(*it_sa).first_route].ruta[next];
			}
			next = sol[(*it_sa).second_route].ruta[0];
			while (next != 0)
			{
				visit = false;
				for (itv = visitados.begin(); itv != visitados.end(); itv++)
				{
					if ((*itv) == next)
					{
						visit = true;
						break;
					}
				}
				if (!visit)
				{
					dat_new.ruta[prev] = next;
					visitados.push_back(next);
					prev = next;
					next = sol[(*it_sa).second_route].ruta[next];
				}else next = sol[(*it_sa).second_route].ruta[next];
			}
			// return to depot
			dat_new.ruta[prev] = 0;
		
			
			//si no cabe porque se excede el numero de posiciones pasa al siguiente
			if ((sol[(*it_sa).first_route].num_pallets + sol[(*it_sa).second_route].num_pallets) > truck.posiciones) continue;
			//si no cabe porque se excede el peso pasa al siguiente
			if((sol[(*it_sa).first_route].peso_ca + sol[(*it_sa).second_route].peso_ca) > truck.max_total_peso) continue;

			/*****************************************************************/
			//RELLENAMOS LOS PALLETS EN EL ORDEN DE LA RUTA
			// los pallets no tienen porque estar ordenados segun la ruta, eso lo har� el heuristico
			//pongo en la puerta la primera ruta y luego la segunda
			/*****************************************************************/
			vector<vector<double> > pesosp;
			vector<double> aux;
			vector<int> auxint;
			vector<vector<int> > p_t;

			for (int i = 0; i <= dat.maxcli; i++)
			{
				pesosp.push_back(aux);
				p_t.push_back(auxint);
			}


			//Relleno los pallets por cliente
			for (int p = 0; p < (dat.posiciones/2); p++)
			{
				if (sol[(*it_sa).first_route].f1_cli[p] != 0)
				{
					pesosp.at(sol[(*it_sa).first_route].f1_cli[p]).push_back(sol[(*it_sa).first_route].f1_peso[p]);
					p_t.at(sol[(*it_sa).first_route].f1_cli[p]).push_back(sol[(*it_sa).first_route].f1_t[p]);
				}
				if (sol[(*it_sa).first_route].f1_cli[p+frente] != 0)
				{
					pesosp.at(sol[(*it_sa).first_route].f1_cli[p+frente]).push_back(sol[(*it_sa).first_route].f1_peso[p+frente]);
					p_t.at(sol[(*it_sa).first_route].f1_cli[p+frente]).push_back(sol[(*it_sa).first_route].f1_t[p+frente]);
				}
			}
			for (int p = 0; p < (dat.posiciones/2); p++)
			{
				if (sol[(*it_sa).second_route].f1_cli[p] != 0)
				{
					pesosp.at(sol[(*it_sa).second_route].f1_cli[p]).push_back(sol[(*it_sa).second_route].f1_peso[p]);
					p_t.at(sol[(*it_sa).second_route].f1_cli[p]).push_back(sol[(*it_sa).second_route].f1_t[p]);
				}
				if (sol[(*it_sa).second_route].f1_cli[p+frente] != 0)
				{
					pesosp.at(sol[(*it_sa).second_route].f1_cli[p+frente]).push_back(sol[(*it_sa).second_route].f1_peso[p+frente]);
					p_t.at(sol[(*it_sa).second_route].f1_cli[p+frente]).push_back(sol[(*it_sa).second_route].f1_t[p+frente]);
				}

			}

			////primera posici�n en la puerta
			//int sig1= (truck.posiciones / 2) - 1;
			//			
			////primero los de la ruta 1, que es la primera que descarga
			//for (int j = (truck.posiciones / 2)-1; j >= 0; j--)
			//{
			//	//Busco en el primer cami�n, el primer pallet
			//	if (sol[(*it_sa).first_route].f1_cli[j] == 0 && sol[(*it_sa).first_route].f1_cli[j + frente] == 0) continue;
			//	//pon1 = false; pon2 = false;
			//	if (sol[(*it_sa).first_route].f1_cli[j] > 0.1)
			//	{
			//		//a�ado el pallet a la primera posici�n libre desde la puerta (sig1).
			//		int cliente = sol[(*it_sa).first_route].f1_cli[j];
			//		dat_new.f1_cli[sig1] = sol[(*it_sa).first_route].f1_cli[j];
			//		dat_new.f1_peso[sig1] = sol[(*it_sa).first_route].f1_peso[j];
			//		dat_new.f1_t[sig1] = sol[(*it_sa).first_route].f1_t[j];

			//		
			//		//siguiente posici�n donde colocar
			//		if (sig1 < (truck.posiciones / 2)) sig1 = sig1 + frente;
			//		else sig1 = sig1 - frente-1;
			//	}
			//	
			//	if (sol[(*it_sa).first_route].f1_cli[j + frente] > 0.1)
			//	{
			//		int cliente = sol[(*it_sa).first_route].f1_cli[j + frente];
			//		dat_new.f1_cli[sig1] = sol[(*it_sa).first_route].f1_cli[j + frente];
			//		dat_new.f1_peso[sig1] = sol[(*it_sa).first_route].f1_peso[j+frente];
			//		dat_new.f1_t[sig1] = sol[(*it_sa).first_route].f1_t[j + frente];
			//		

			//		//siguiente posici�n donde colocar
			//		if (sig1 < (truck.posiciones / 2)) sig1 = sig1 + frente;
			//		else sig1 = sig1 - frente - 1;
			//	}
			//}
			////ahora pongo los pallets de la ruta 2, que es la segunda que descarga
			//for (int j = (dat.posiciones / 2)-1; j >= 0; j--)
			//{
			//	if (sol[(*it_sa).second_route].f1_cli[j] == 0 && sol[(*it_sa).second_route].f1_cli[j + frente] == 0) continue;
			//	
			//	if (sol[(*it_sa).second_route].f1_cli[j] > 0.1)
			//	{
			//		//add pallet
			//		int cliente = sol[(*it_sa).second_route].f1_cli[j];
			//		dat_new.f1_cli[sig1] = sol[(*it_sa).second_route].f1_cli[j];
			//		dat_new.f1_peso[sig1] = sol[(*it_sa).second_route].f1_peso[j];
			//		dat_new.f1_t[sig1] = sol[(*it_sa).second_route].f1_t[j];
			//		

			//		if (sig1 < (truck.posiciones / 2)) sig1 = sig1 + frente;
			//		else sig1 = sig1 - frente - 1;
			//	}
			//
			//	if (sol[(*it_sa).second_route].f1_cli[j + frente] > 0.1)
			//	{
			//		int cliente = sol[(*it_sa).second_route].f1_cli[j + frente];
			//		dat_new.f1_cli[sig1] = sol[(*it_sa).second_route].f1_cli[j + frente];
			//		dat_new.f1_peso[sig1] = sol[(*it_sa).second_route].f1_peso[j + frente];
			//		dat_new.f1_t[sig1] = sol[(*it_sa).second_route].f1_t[j + frente];
			//		

			//		if (sig1 < (truck.posiciones / 2)) sig1 = sig1 + frente;
			//		else sig1 = sig1 - frente - 1;
			//	}
			//	
			//}
			//Actualizo el n�mero de pallets
			dat_new.num_pallets = sol[(*it_sa).first_route].num_pallets + sol[(*it_sa).second_route].num_pallets;
			if (dat_new.num_pallets == truck.posiciones)dat.lleno = true;
			//printf("%d - %d ", (*it_sa).first_route, (*it_sa).second_route);

			if (heuristico2(dat_new,pesosp, p_t))
			{
				//printf("SI\n");
				posible = true;
				// actualizar sol
				
				//Calculo fo de la ruta
				dat_new.fo_heu = 0;
				int cont_cli = 0;
				for (int j = 0; j < dat_new.ruta.size(); j++)
				{
					if (dat_new.ruta.at(j) != -1)
					{
						dat_new.fo_heu += l_clientes.at(j).distancias.at(dat_new.ruta.at(j));
						cont_cli++;
					}
				}
				if(cont_cli >2) dat_new.compartido = true;
				else dat_new.compartido = false;
				//Actualizo la lista de camiones
				sol[(*it_sa).first_route] = dat_new;
				vector<datos>::iterator it_d;
				it_d = sol.begin();
				std::advance(it_d, (*it_sa).second_route);
				sol.erase(it_d);
			}
			//else printf("NO\n");
			
			if (posible)
				break; // calcular los ahorros otra vez
		} //end for (it_sa = ahorros.begin(); it_sa != ahorros.end(); it_sa++)
	}// end while(posible)

	/*****************************************************************/
	//REORDENAR LOS CAMIONES para que cumpla la restricci�n de simetria
	/*****************************************************************/
	vector<datos>::iterator itsol;
	vector<datos> sol2;
	for (int i = 1; (i <= dat.maxcli && !sol.empty()); i++)
	{
		itsol = sol.begin();
		bool fin = false;
		while(!fin)
		{
			for (int p = 0; p < dat.posiciones; p++)
			{
				if (!fin && (*itsol).f1_cli.at(p)==i && (*itsol).f1_t.at(p)==0)
				{
					sol2.push_back((*itsol));
					sol.erase(itsol);
					if (!sol.empty())
					{
						itsol = sol.begin();
						fin = true;
						break;
					}
					else
					{
						fin = true; 
						break;
					}
				}
			}
			if (fin)break;
			else itsol++;
			if (itsol == sol.end()) fin = true;
		}
	}
	while (!sol.empty())
	{
		itsol = sol.begin();
		sol2.push_back((*itsol));
		sol.erase(itsol);		
	}
	sol.clear();
	
	for (itsol = sol2.begin(); itsol != sol2.end(); itsol++)
	{
		sol.push_back((*itsol));
	}
	//Imrimir la soluci�n

	//FILE *f = fopen("./solcw.txt","w+");
	//vector<datos>::iterator itd;
	//vector<int>::iterator itr;
	//int k = 0;
	//for (itd = sol.begin(); itd != sol.end(); itd++)
	//{
	//	//Ruta
	//	int ini = 0;
	//	int dest = (*itd).ruta.at(ini);
	//	bool fin = false;
	//	while(!fin)
	//	{
	//		fprintf(f,"X_%d_%d\n",ini,dest);
	//		if (dest == 0) fin = true;
	//		ini = dest;
	//		dest = (*itd).ruta.at(ini);
	//	}

	//	//Packing
	//	for (int p = 0; p < dat.posiciones; p++)
	//	{
	//		if ((*itd).f1_cli.at(p) > 0)
	//		{
	//			fprintf(f,"Z_%d_%d_%d_%d\n",k, (int)(*itd).f1_cli.at(p), (*itd).f1_t.at(p),p);
	//		}
	//	}
	//	k++;
	//}
	//fclose(f);
	
	//escribirsol(sol);
	//DibujarOpenGL();

	return sol;
}

bool colocar( pallet  pal, int pos,datos &dat, double &totp, double &pee1, double &pee2, int cli)
{
	double p,aux1,aux2;

	totp += pal.peso;
	if(pos < (dat.posiciones/2))
		p= (pos *dat.largo_pallet) + (dat.largo_pallet / 2);
	else	p = ((pos - (dat.posiciones / 2))*dat.largo_pallet) + (dat.largo_pallet / 2);
	aux1= pal.peso*(dat.pos_eje2 - p);
	aux2= pal.peso*(p - dat.pos_eje1);
	double mp1 = (pee1+aux1) / (dat.pos_eje2 - dat.pos_eje1);
	double mp2 = (pee2 + aux2) / (dat.pos_eje2 - dat.pos_eje1);
	if (totp <= dat.peso_total && mp1 <= dat.max_eje1 && mp2 <= dat.max_eje2)
	{

		dat.f1_peso.at(pos) = pal.peso; // VIP
		dat.f1_cli.at(pos) = cli;				 // VIP
		dat.f1_t.at(pos) = pal.id;			 // VIP
		dat.num_pallets++;
		dat.peso_ca = totp;
		dat.p_eje1 = mp1;
		dat.p_eje2 = mp2;
		pee1 += aux1;
		pee2 += aux2;
		return true;
	}
	else
	{
		totp -= pal.peso;
		return false;
	}
}

bool colocar2(pallet  pal, pallet pal2, int pos, datos& dat, double& totp, double& pee1, double& pee2, int cli)
{
	double p1,aux1,aux2;
	int frente = (dat.posiciones / 2);
	//peso total
	totp += pal.peso;
	totp += pal2.peso;

	//peso ejes
	if (pos < (dat.posiciones / 2))
		p1 = (pos * dat.largo_pallet) + (dat.largo_pallet / 2);
	else	p1 = ((pos - (dat.posiciones / 2)) * dat.largo_pallet) + (dat.largo_pallet / 2);
	//p1 = (pos * dat.largo_pallet) + (dat.largo_pallet / 2);
	//p2 = ((pos - (dat.posiciones / 2)) * dat.largo_pallet) + (dat.largo_pallet / 2);
	aux1= pal.peso * (dat.pos_eje2 - p1);
	aux2= pal.peso * (p1 - dat.pos_eje1);
	aux1 += pal2.peso * (dat.pos_eje2 - p1);
	aux2 += pal2.peso * (p1 - dat.pos_eje1);
	double mp1=(pee1 +aux1) / (dat.pos_eje2 - dat.pos_eje1);
	double mp2 =(pee2 + aux2) / (dat.pos_eje2 - dat.pos_eje1);

	if (totp <= dat.peso_total && mp1<= dat.max_eje1 && mp2 <= dat.max_eje2)
	{
		dat.f1_peso.at(pos) = pal.peso; // VIP
		dat.f1_cli.at(pos) = cli;				 // VIP
		dat.f1_t.at(pos) = pal.id;			 // VIP
		dat.num_pallets++;
		dat.f1_peso.at(pos+frente) = pal2.peso; // VIP
		dat.f1_cli.at(pos+frente) = cli;				 // VIP
		dat.f1_t.at(pos+frente) = pal2.id;			 // VIP
		dat.num_pallets++;
		dat.peso_ca = totp;
		dat.p_eje1 = mp1;
		dat.p_eje2 = mp2;
		pee1 += aux1;
		pee2 += aux2;
		return true;
	}
	else
	{
		totp -= pal.peso;
		totp -= pal2.peso;
		return false;
	}
}
void escribo_sol_cw(vector<datos> aa, vector<cliente> l_clientes, char* nom_inst, int cinf)
{
	char nom[500];
	strcpy(nom,".\\Results\\");
	strcat(nom, nom_inst);
	strcat(nom, "_solcw.txt");
	int comp = 0;

	FILE* f = fopen(nom,"w");
	int fo = 0;
	for (int i = 0; i < aa.size(); i++)
	{
		if (aa.at(i).compartido) comp++;
		for (int j = 0; j < aa.at(i).ruta.size(); j++)
		{
			if (aa.at(i).ruta.at(j) != -1)
				fo += (int)l_clientes.at(j).distancias.at(aa.at(i).ruta.at(j));
		}
	}
	fprintf(f,"F.O: %d\n",fo );
	fprintf(f, "ROUTING\n");
	for (int i = 0; i < aa.size(); i++)
	{
		for (int j = 0; j < aa.at(i).ruta.size(); j++)
		{
			if (aa.at(i).ruta.at(j) != -1)
				fprintf(f, "X%d;%d;%d;%d\n", i,j, (int)aa.at(i).ruta.at(j), (int)l_clientes.at(j).distancias.at(aa.at(i).ruta.at(j)));
		}
	}
	fprintf(f, "PACKING\n");

	double pt = 0;
	double pe1 = 0;
	double pe2 = 0;
	double pp = 0;

	for (int i = 0; i < aa.size(); i++)
	{
		for (int j = 0; j < aa.at(0).posiciones; j++)
		{
			if (aa.at(i).f1_cli.at(j) > 0.1)
			{
				pt += aa.at(i).f1_peso.at(j);
				if (j < aa.at(0).posiciones / 2)
					pp = (j * 800) + (800 / 2);
				else	pp = ((j - (32 / 2)) * 800) + (800 / 2);
				pe1 += aa.at(i).f1_peso.at(j) * (12716 - pp);
				pe2 += aa.at(i).f1_peso.at(j) * (pp - 914);
				fprintf(f, "Z%d %d %d %d\n", i, (int)(aa.at(i).f1_cli.at(j)), aa.at(i).f1_t.at(j), j);
			}
		}
		double mp1 = pe1 / (12716 - 914);
		double mp2 = pe2 / (12716 - 914);
		fprintf(f,"truck weigth: %.2f axle1: %.2f axle2: %.2f\n",aa.at(i).peso_ca,aa.at(i).p_eje1, aa.at(i).p_eje2);
	}
	fclose(f);
	int csup = (int) aa.size();
	FILE* f2 = fopen(".\\Results\\c&w.txt", "a+");
	fprintf(f2, "%s;%d;%d;%d;%d;%d\n", nom_inst, cinf, csup, comp,csup-comp, fo);
	fclose(f2);
}
void escribirsol(vector<datos> dat)
{
	vector<datos>::iterator itd;
	int cont = 0;
	FILE* f;
	f = fopen(".\\Results\\temp.txt", "w+");
	itd = dat.begin();
	fprintf(f, "%d %d %d %d %d %d\n", (int)(*itd).largo, (int)(*itd).ancho, (int)(*itd).posiciones,(int)(*itd).peso_total, (int)(*itd).p_eje1, (int)(*itd).p_eje2);
	for (itd = dat.begin(); itd != dat.end(); itd++)
	{
		cont++;
		
		for (int p = 0; p < (*itd).posiciones; p++)
		{
			if ((*itd).f1_peso.at(p) > 0)
				fprintf(f, "%d %d %d %d %d\n",cont, (int)(*itd).f1_cli.at(p), (*itd).f1_t.at(p), p, (int)(*itd).f1_peso.at(p));
		}
	}
	fprintf(f, "9999 9999 9999 9999");
	fclose(f);
}
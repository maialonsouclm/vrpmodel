

#ifndef CLIENTE_H
#define CLIENTE_H
class cliente
{
public:
	int id;
	double coord_x;
	double coord_y;
	double coord_z;
	//lista de cajas a servir
	list<pallet> pedidos;
	int quedan;
	vector<float> distancias;

	cliente(int i)
	{
		id=i;
		quedan=0;
	};

	int cuantas_quedan(){return quedan;};
	void inicializa_distancias(int total_cli)
	{
		for (int i=0; i <= total_cli;i++)
		{
			distancias.push_back(0);
		}
	}

	void add_dist(int cli, float i)
	{
		distancias.at(cli)=i;
	}

};

class ped_cli{

public:
	int id;
	vector<int> id_pallet;
	vector<double> peso_pallet;

};
#endif
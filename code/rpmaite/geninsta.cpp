#include "stdafx.h"

void genera_instancias_TSP(int num_cli,int numpal,char* demanda,int cant_ins)
{
	int n_cli[5] = { 5,15,25,40,50 };  //N�mero de clientes
	int num_pal[5] = { 40,80,120,80,80 };  //cantidad de pallets seg�n la opci�n
	/*int num_cli = 5;
	int numpal = 1;
	char* demanda = "v";*/

	vector<int> pallets;
	char nom[500];
	char num[5];

	
	//Cantidad de instancias por tipo
	for (int i = 1; i <= cant_ins; i++)
	{
		//Compongo el nombre del archivo
		strcpy(nom, "Inst");
		itoa(num_cli, num, 10);
		strcat(nom, num);
		strcat(nom, "P");
		itoa(numpal, num, 10);
		strcat(nom, num);
		strcat(nom, "D");
		strcat(nom, demanda);
		strcat(nom, "_");
		itoa(i, num, 10);
		strcat(nom, num);
		strcat(nom, ".csv");

		//Pongo el truck y el pallet
		FILE* f2 = fopen(nom, "a+");
		fprintf(f2, "#truck 1\n");
		fprintf(f2, "1 13500 2450 2450 20411 10432 914 9979 12716 32\n");
		fprintf(f2, "#pallet\n");
		fprintf(f2, "1 1200 800 145 25\n");
		fclose(f2);

		//Genero los pallets
		pallets.clear();
		switch (numpal)
		{
			case 1: generar_pallets(num_pal[0], 1700, 2300, pallets);
				break;
			case 2: generar_pallets(num_pal[1], 700, 1300, pallets);
				break;
			case 3: generar_pallets(num_pal[2], 300, 1000, pallets);
				break;
			case 4: generar_pallets(num_pal[3], 1000, 1000, pallets);
				break;
			case 5: generar_pallets(num_pal[4], 100, 1900, pallets);
				break;
			default:
				break;
		}
		
		//Reparto los pallets entre clientes.
		if (strcmp(demanda,"u")==0)	reparte_pal(1, pallets, nom, num_cli);
		else reparte_pal(2, pallets, nom, num_cli);

		//Leo las distancias de los clientes
		leer_dist(num_cli, nom);
	}
		
}
void reparte_pal(int tipodem, vector<int> pallets, char* nomarchivo,int numcli)
{
	int pal_por_cli;
	vector<int>::iterator itp;
	vector<int> dem_x_cli;

	FILE* f2 = fopen(nomarchivo, "a+");
	fprintf(f2, "#demand %d\n", pallets.size());
	if (tipodem == 1)
	{
		pal_por_cli = (int)(pallets.size() / numcli);

		for (int i = 0; i < (int)pallets.size(); i++)
		{
			fprintf(f2, "%d %d %d\n", i+1, (i % numcli)+1, pallets[i]);
		}
		fclose(f2);
	}
	else
	{
		
		int contpal = 0;
		for (int j = 1; j <= numcli; j++)
		{
			int minval= ceil(0.333*floor(pallets.size() / numcli));
			int maxval = ceil(1.66*floor(pallets.size() / numcli));
			int ran = get_random(minval, maxval);
			//printf("%d\n", ran);
			pal_por_cli = ran;
			dem_x_cli.push_back(pal_por_cli);
			contpal += pal_por_cli;
		}
		if (contpal > pallets.size())
		{
			vector<int>::iterator itpal;
			itpal = dem_x_cli.begin();

			while (contpal > pallets.size())
			{
				if ((*itpal) == 1) itpal++;
				else
				{
					(*itpal)--;
					contpal--;
					itpal++;
				}
				if(itpal==dem_x_cli.end())itpal= dem_x_cli.begin();
			}
		}
		else
		{
			if (contpal < pallets.size())
			{
				vector<int>::iterator itpal;
				itpal = dem_x_cli.begin();

				while (contpal < pallets.size())
				{
					(*itpal)++;
					contpal++;
					itpal++;
					if (itpal == dem_x_cli.end())itpal = dem_x_cli.begin();
				}
			}
		}

		itp = pallets.begin();
		int cont = 1;
		for (int j = 1; j <= numcli; j++)
		{
			for (int i = 1; i <= dem_x_cli.at(j-1); i++)
			{
				fprintf(f2, "%d %d %d\n", cont, j, (*itp));
				cont++;
				itp++;

			}
		}
		
		fclose(f2);
	}
}
void leer_dist(int ncli, char* nomarchivo)
{
	int d1, d2, d3, d4, d5, d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,d20,d21,d22,d23,d24,d25;
	int d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49,d50,d51;
	FILE *f;

	if (ncli == 5) f = fopen("./tsp/tsp/Test_6.txt", "r");
	else if (ncli == 10) f = fopen("./tsp/tsp/Test_11.txt", "r");
	else if(ncli==15) f=fopen("./tsp/tsp/Test_16.txt","r");
	else if (ncli == 20) f = fopen("./tsp/tsp/Test_21.txt", "r");
	else if (ncli == 25) f = fopen("./tsp/tsp/Test_26.txt", "r");
	else if (ncli == 30) f = fopen("./tsp/tsp/Test_31.txt", "r");
	else if (ncli == 50) f = fopen("./tsp/tsp/Test_51.txt", "r");
	FILE* f2 = fopen(nomarchivo,"a+");
	fprintf(f2,"#distances %d %d\n",ncli,ncli);

	for (int i = 0; i <= ncli; i++)
	{
		if (ncli == 5)
		{
			fscanf(f, "%d\t%d\t%d\t%d\t%d\t%d\n", &d1, &d2, &d3, &d4, &d5, &d6);
			fprintf(f2, "%d\t%d\t%d\t%d\t%d\t%d\n", d1, d2, d3, d4, d5, d6);
		}
		if (ncli == 10)
		{
			fscanf(f, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &d1, &d2, &d3, &d4, &d5, &d6, &d7, &d8, &d9, &d10, &d11);
			fprintf(f2, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11);
		}
		if (ncli == 15)
		{
			fscanf(f, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &d1, &d2, &d3, &d4, &d5, &d6, &d7, &d8, &d9, &d10, &d11, &d12, &d13, &d14, &d15, &d16);
			fprintf(f2, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16);
		}
		if (ncli == 20)
		{
			fscanf(f, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &d1, &d2, &d3, &d4, &d5, &d6, &d7, &d8, &d9, &d10, &d11, &d12, &d13, &d14, &d15, &d16, &d17, &d18, &d19, &d20, &d21);
		    fprintf(f2, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21);
		}
		if (ncli == 25)
		{
			fscanf(f, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &d1, &d2, &d3, &d4, &d5, &d6, &d7, &d8, &d9, &d10, &d11, &d12, &d13, &d14, &d15, &d16, &d17, &d18, &d19, &d20, &d21, &d22, &d23, &d24, &d25, &d26 );
			fprintf(f2, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26);
		}
		if (ncli == 40)
		{
			fscanf(f, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &d1, &d2, &d3, &d4, &d5, &d6, &d7, &d8, &d9, &d10, &d11, &d12, &d13, &d14, &d15, &d16, &d17, &d18, &d19, &d20, &d21, &d22, &d23, &d24, &d25, &d26, &d27, &d28, &d29, &d30, &d31, &d32, &d33, &d34, &d35, &d36, &d37, &d38, &d39, &d40, &d41);
			fprintf(f2, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41);
		}
		if (ncli == 50)
		{
			fscanf(f, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &d1, &d2, &d3, &d4, &d5, &d6, &d7, &d8, &d9, &d10, &d11, &d12, &d13, &d14, &d15, &d16, &d17, &d18, &d19, &d20, &d21, &d22, &d23, &d24, &d25, &d26, &d27, &d28, &d29, &d30, &d31, &d32, &d33, &d34, &d35, &d36, &d37, &d38, &d39, &d40, &d41,&d42,&d43,&d44,&d45,&d46,&d47,&d48,&d49,&d50,&d51);
			fprintf(f2, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51);
		}
	}
	fclose(f2);
	fclose(f);
}
void generar_pallets(int cant_pal,int min,int max, vector<int> &pallets)
{
	
	for (int i = 1; i <= (cant_pal/2); i++)
	{
		int peso = get_random(min, max);
		pallets.push_back(peso);
		int dif1 = peso - min;
		int dif2 = max - peso;
		if (dif1 > dif2) pallets.push_back(min + dif2);
		else pallets.push_back(max - dif1);
	}
	if (cant_pal % 2 != 0)
	{
		int peso = get_random(min, max);
		pallets.push_back(peso);
	}
}
void genera_instancias()
{
	//Par�metros
	int clientes=3;  //n�mero de clientes
	
	//routing
	int cerca=1;  //km para cerca
	int lejos=500; //km para lejos

	//packing
	int pesado=1500;	//kg pallets pesados
	int ligero=100;		//kg pallets ligeros

	//instancias
	int cant=1;			//cantidad de instancias por tipo

	//tipo de pallet (Europallet) en mm
	int lp=1200;
	int wp=800;
	int hp=145;
	int pp=25;

	//tipo camion
	int lc=13500; //L
	int wc=2450; //W
	int hc=2450; //H
	int qm=20411; //peso m�ximo
	int q1=10432; //peso max eje 1
	int d1=914; //distancia al eje 1
	int q2=9979;  //peso maximo eje 2
	int d2=12716; //distancia al eje 2
	int posiciones=2*(lc/wp);


	//VARIABLES
	int cap=0;
	int contp=0;
	double mat[16][16];
	char nom[500];
	char num[5];

	
	
	int peso;
	int dist;

	list<string> lpal;
	list<string>::iterator itpal;
	char cad[15];
	char* cadaux;
	vector<int> dx;
	vector<int> dy;
	FILE f;

	//capacidad
	for(int c=1; c<=4; c++)
	{
		for(int r=1; r<=2; r++)
		{
			for(int p=1; p <=4; p++)
			{
				for(int i=1; i <=cant; i++)
				{
					strcpy(nom,"Inst");
					itoa(clientes,num,10);
					strcat(nom,num);
					strcat(nom,"C");
					itoa(c,num,10);
					strcat(nom,num);
					strcat(nom,"R");
					itoa(r,num,10);
					strcat(nom,num);
					strcat(nom,"P");
					itoa(p,num,10);
					strcat(nom,num);
					//strcat(nom,"N");
					//itoa(i,num,10);
					//strcat(nom,num);
					strcat(nom,".csv");
					/*f=fopen("ejecuta.bat","a+");
					fprintf(f,"%s %d %d %d %d\n",nom,3,1,0,1);
					fclose(f);*/
					//Para todas las instancias camion y pallet
					printf("%s\n",nom);
					FILE *f=fopen(nom,"w+");
					fprintf(f,"#camion 1\n");
					fprintf(f,"1 %d %d %d %d %d %d %d %d %d\n",lc,wc,hc,qm,q1,d1,q2,d2,posiciones);
					fprintf(f,"#pallet \n");
					fprintf(f,"1 %d %d %d %d\n", lp,wp,hp,pp);
						
					//genero la demanda en un lista
					//Generamos los pallets de cada tipo
					contp=0;
					lpal.clear();
					for(int cc=1; cc <=clientes; cc++)
					{
						if(c==1) cap=get_random(48,72);
						if(c==2) cap=get_random(28,48);
						if(c==3) cap=get_random(1,28);
						if(c==4)
						{
							int auxc=get_random(1,3);
							if(auxc==1) cap=get_random(48,72);
							else if(auxc==2) cap=get_random(28,48);
							else if(auxc==3) cap=get_random(1,28);
						}
						

						for(int pp=1; pp <=cap; pp++)
						{
							contp++;
							if(p==1)peso=get_random(1000,1500);
							if(p==2)peso=get_random(500,1000);
							if(p==3)peso=get_random(100,500);
							if(p==4)
							{
								if(pp < cap/2) peso=get_random(1000,1500);
								else peso=get_random(100,750);
							}
							//lo almaceno en una lista
							itoa(contp,cad,10);
							strcat(cad," ");
							itoa(cc,cadaux,10);
							strcat(cad,cadaux);
							itoa(peso,cadaux,10);
							strcat(cad," ");
							strcat(cad,cadaux);
							strcat(cad,"\n");
							lpal.push_back(cad);
						}
					}

					//PALLETS
					fprintf(f,"#demanda %d\n",(int)lpal.size());
					for(itpal=lpal.begin();itpal!=lpal.end();itpal++)
					{
						fprintf(f,"%s",(*itpal).c_str());
					}

					dx.clear();
					dy.clear();
					for(int ii=0; ii <= clientes; ii++)
					{
						if(r==1)
						{
							if(ii==0)
							{
								dx.push_back(0);
								dy.push_back(0);
							}
							else
							{
								dx.push_back(get_random(0,100));
								dy.push_back(get_random(0,100));
							}
						}
						else
						{
							if(r==2)
							{
								if(ii==0)
								{
									dx.push_back(50);
									dy.push_back(50);
								}
								else
								{
									dx.push_back(get_random(0,100));
									dy.push_back(get_random(0,100));
								}
							}
						}
					}
						
					//DISTANCIAS
					fprintf(f,"#distancias %d %d \n",clientes+1, clientes+1);
					
					for(int ii=0; ii <= clientes; ii++)
					{
						for(int jj=0; jj <= clientes; jj++)
						{
							/*if(ii >= jj)
							{*/
								if(ii==jj)mat[ii][jj]=0;
								else
								{
									double aa=abs((dx[ii]-dx[jj])*(dx[ii]-dx[jj]));
									double bb=abs((dy[ii]-dy[jj])*(dy[ii]-dy[jj]));
									double distancia=sqrt(aa+bb);
									mat[ii][jj]=distancia;
									mat[jj][ii]=distancia;
								}
							//}
						}
					}
					for(int ii=0; ii <= clientes; ii++)
					{
						for(int jj=0; jj <= clientes; jj++)
						{					
							fprintf(f,"%.2f ",mat[ii][jj]);
						}
						fprintf(f,"\n");
					}

					fclose(f);
				}
			}
		}
	}
	
}

void generabat()
{
	FILE * f=fopen("./ejecutar.csv","a+");
	//capacidad
	for(int c=1; c<=4; c++)
	{
		for(int r=1; r<=4; r++)
		{
			for(int p=1; p <=4; p++)
			{
				for(int i=1; i <=2; i++)
				{
					fprintf(f,"Inst7C%dR%dP%dN%d.csv;3;0;1;1;600\n",c,r,p,i);
				}
			}
		}
	}
	fclose(f);
}
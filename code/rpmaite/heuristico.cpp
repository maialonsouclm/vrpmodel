#include "stdafx.h"
#include "LPCPLEX.h"
#include <assert.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <ilconcert/ilolinear.h>

/*
* Devuelve la cota inferior, la m�xima por posiciones o por peso.
*
*/
int cota_inferior(camion truck, vector<cliente> clientes)
{
	int total_dem=0;
	double total_peso = 0;
	vector<cliente>::iterator itc;
	list<pallet>::iterator itp;

	//Calculo la demanda total de pallets y el peso total de todos los pallets
	for(itc=clientes.begin(); itc != clientes.end(); itc++)
	{
		total_dem+=(int)(*itc).pedidos.size();
		
		for(itp=(*itc).pedidos.begin(); itp!=(*itc).pedidos.end(); itp++)
		{
			total_peso+=(*itp).peso;
		}
	}
	int cot1=ceil((float)total_dem/truck.posiciones);  //cota por posiciones
	int cot2=ceil((float)total_peso/truck.max_total_peso);  //cota por peso;
	int max_cot;

	if (cot1 > cot2) max_cot = cot1;
	else max_cot=cot2;

return max_cot;
}
/*
* Devuelve la cota superior del n�mero de camiones que necesitas para esa demanda
* para ello va colocando en orden desde la posici�n 0 (cabina) hasta la puerta los pallets del cliente 1, luego del 2, etc.
* 
*/
int cota_superior(list<camion>& solucion, camion truck, vector<cliente> l_clientes, float &fun_obj)
{

	int maxcliente=(int)l_clientes.size()-1;
	list<pallet>::iterator itp,itp2;
	double pesomax=0, posicion=0, numerador=0,G1,G2,newcog,newcogy;
	vector<pallet>::iterator itc;
	int contc=0, contcli=1, contpal=0;
	double tau1y=(truck.ancho/2)-(truck.ancho/8);
	double tau2y=(truck.ancho/2)+(truck.ancho/8);
	bool fin=false;
	int pos1=-1, pos2=pos1+(truck.posiciones/2);
	bool cambio = false;  //indica si el cami�n es compartido(true) o de un solo cliente (false).
	

	//creo el camion vacio
	camion cam;
	cam.add_datos(contc,truck.ancho,truck.alto,truck.largo,truck.max_total_peso,truck.pos_eje1,truck.max_peso_eje1,truck.pos_eje2,truck.max_peso_eje2, truck.ancho_pallet,truck.largo_pallet,truck.alto_pallet,truck.peso_pallet,truck.posiciones);
	
	//Cojo los pedidos del primer cliente
	itp=l_clientes.at(contcli).pedidos.begin();
	itp2=itp;
	itp2++;
	contpal=0;
	
	while(!fin)
	{
		//posici�n 0 y 16 las dos primeras de la cabina.
		pos1++;
		pos2=pos1+(truck.posiciones/2);

		//Si es la �ltima posici�n, 
		if(pos1==(truck.posiciones/2))
		{
			solucion.push_back(cam);
			contc++;
			cambio = false;
			cam.id=contc;
			cam.carga.clear();
			cam.peso_total=0;
			cam.CGx=0;
			cam.peso_eje1=0;
			cam.peso_eje2=0;
			cam.CGy=0;
			cam.suma1=0;
			cam.suma2=0;
			for(int i=0; i < truck.posiciones; i++)
			{
				cam.pos_carga.at(i)=0;
				cam.peso_pos.at(i)=0;
			}
			pos1=0;
			pos2=pos1+(truck.posiciones/2);
			if (cambio)
			{
				fun_obj += l_clientes.at(contcli - 1).distancias.at(0);
			}
			else fun_obj += l_clientes.at(contcli).distancias.at(0);
			
		}

		//Si el pallet cabe en la posici�n por peso, lo coloco
		if (cabe_pallet(pos1,(*itp),truck,cam,newcog,newcogy,G1,G2))
		{
			//Si es el primer cliente d ela ruta, a�ado la distancia del deposito a ese cliente
			if(cam.carga.empty()) fun_obj += l_clientes.at(0).distancias.at(contcli);
			else
			{
				//Actualizo la funci�n objetivo si a�ado pallets de un nuevo cliente
				if (cambio)
				{
					if (cam.carga.size() > 0 && cambio == true) cam.compartido = true;
					fun_obj += l_clientes.at(contcli - 1).distancias.at(contcli);
					cambio = false;
				}
			}

			//A�ado el apllet al cami�n
			cam.carga.push_back((*itp));
			cam.pos_carga.at(pos1)=1;
			cam.peso_total+=(*itp).peso;
			cam.peso_pos.at(pos1)=(*itp).peso;
			cam.CGx=newcog;
			cam.peso_eje1=G1;
			cam.peso_eje2=G2;
			cam.CGy=newcogy;
			//Paso al siguiente pallet
			itp++;
			contpal++;
			if(itp==l_clientes.at(contcli).pedidos.end())
			{
				//Si es el �ltimo cliente, a�ado la distancia de ese al deposito.
				if(contcli==maxcliente) fun_obj += l_clientes.at(contcli).distancias.at(0);
				contcli++;
				contpal=0;
				//Si era el �limo pallet del cliente anterior, paso a siguiente cliente
				if (contcli <= maxcliente)
				{
					itp = l_clientes.at(contcli).pedidos.begin();
					cambio = true;
				}
				else
				{
					//Si era el �ltmo pallet de todos, a�ado el cami�n a la soluci�n y termino
					solucion.push_back(cam);
					contc++;
					fin=true;
					cambio = false;
				}
			}

			//Lo mismo para el siguiente pallet en pos2
			if (!fin)
			{
				if(cabe_pallet(pos2,(*itp),truck,cam,newcog,newcogy,G1,G2))
				{
					if (cam.carga.empty()) fun_obj += l_clientes.at(0).distancias.at(contcli);
					else
					{
						if (cambio)
						{
							if (cam.carga.size() > 0 && cambio == true) cam.compartido = true;
							fun_obj += l_clientes.at(contcli - 1).distancias.at(contcli);
							cambio = false;
						}
					}

					
					cam.carga.push_back((*itp));
					cam.pos_carga.at(pos2)=1;
					cam.peso_total+=(*itp).peso;
					cam.peso_pos.at(pos2)=(*itp).peso;
					cam.CGx=newcog;
					cam.peso_eje1=G1;
					cam.peso_eje2=G2;
					cam.CGy=newcogy;
					itp++;
					contpal++;
				
					if(itp==l_clientes.at(contcli).pedidos.end())
					{
						if (contcli == maxcliente) fun_obj += l_clientes.at(contcli).distancias.at(0);
						
						contcli++;
						contpal=0;
						if (contcli <= maxcliente)
						{
							itp = l_clientes.at(contcli).pedidos.begin();
							cambio = true;
						}
						else
						{
							solucion.push_back(cam);
							contc++;
							fin=true;
							cambio = false;
						}
					}
				}
				else 
				{
					solucion.push_back(cam);
					contc++;
					cam.id=contc;
					cam.carga.clear();
					cam.peso_total=0;
					cam.CGx=0;
					cam.peso_eje1=0;
					cam.peso_eje2=0;
					cam.CGy=0;
					cam.suma1=0;
					cam.suma2=0;
					cam.compartido = false;
					for(int i=0; i < truck.posiciones; i++)
					{
						cam.pos_carga.at(i)=0;
						cam.peso_pos.at(i)=0;
					}
					pos1=-1;
					pos2=pos1+(truck.posiciones/2);
					if (cambio)
					{
						fun_obj += l_clientes.at(contcli-1).distancias.at(0);
						cambio = false;
					}
					else fun_obj += l_clientes.at(contcli).distancias.at(0);
				}
			}
		}
		else //Si el pallet no cabe en pos1, a�ado el cami�n a la soluci�n y abro otro cami�n para continuar por donde me qued�.
		{
			solucion.push_back(cam);
			contc++;
			cam.id=contc;
			cam.carga.clear();
			cam.peso_total=0;
			cam.CGx=0;
			cam.peso_eje1=0;
			cam.peso_eje2=0;
			cam.CGy=0;
			cam.suma1=0;
			cam.suma2=0;
			cam.compartido = false;
			for(int i=0; i < truck.posiciones; i++)
			{
				cam.pos_carga.at(i)=0;
				cam.peso_pos.at(i)=0;
			}
			pos1=-1;
			pos2=pos1+(truck.posiciones/2);
			if (cambio)
			{
				fun_obj += l_clientes.at(contcli - 1).distancias.at(0);
				cambio = false;
			}
			else fun_obj += l_clientes.at(contcli).distancias.at(0);
		}
		
	}
	//comprobar_pesos_ejes(solucion);
	
	return contc;
}

/*
*  Compruebo si un pallet (itp) cabe en la posici�n(pos) del cami�n (cam)
*  La funci�n devuelve:
*    newcog: el nuevo centro de gravedad con el nuevo pallet colocado.
*   newcogy centro de gravedad en Y
*   G1 peso en el eje1
*   G2: peso en el eje 2
*  true o false si lo puede colocar o no.
*/
bool cabe_pallet(int pos,pallet itp,camion truck,camion cam,double &newcog,double &newcogy,double &G1,double &G2)
{
	double pesomax,posicion,numerador,posiciony,numeradory;


	pesomax=cam.peso_total+itp.peso;
	if(pos < (truck.posiciones/2)) posicion=(pos*truck.ancho_pallet)+(truck.ancho_pallet/2);
	else posicion=((pos%(truck.posiciones/2))*truck.ancho_pallet)+(truck.ancho_pallet/2);

	if(pos < (truck.posiciones/2)) posiciony=(truck.largo_pallet/2);
	else posiciony=3*(truck.largo_pallet/2);

	truck.suma1+=itp.peso*(truck.pos_eje2-posicion);
	truck.suma2+=itp.peso*(posicion-truck.pos_eje1);

	numerador=(cam.CGx*cam.peso_total)+(itp.peso*posicion);
	newcog=numerador/(cam.peso_total+itp.peso);

	numeradory=(cam.CGy*cam.peso_total)+(itp.peso*posiciony);
	newcogy=numeradory/(cam.peso_total+itp.peso);

	G1=((cam.peso_total+itp.peso)*(truck.pos_eje2-newcog))/(truck.pos_eje2-truck.pos_eje1);
	G2=((cam.peso_total+itp.peso)*(newcog-truck.pos_eje1))/(truck.pos_eje2-truck.pos_eje1);

	if((pesomax < cam.max_total_peso) && (newcog < truck.pos_eje2) &&(G1 < truck.max_peso_eje1) && (G2 < truck.max_peso_eje2))
	{
		double p1=truck.max_peso_eje1*(truck.pos_eje2-truck.pos_eje1);
		double p2=truck.max_peso_eje2*(truck.pos_eje2-truck.pos_eje1);
		if(truck.suma1 < p1 && truck.suma2 < p2)
			return true;
		else return false;
	}
	else return false;
}

bool comprobar_pesos_ejes(list<camion> solucion)
{
	bool cabe=false;
	double suma1=0,suma2=0;
	list<camion>::iterator itc;
	double posi=0;
	FILE* f1=fopen(".\\result\\suma.txt","w+");

	for(itc=solucion.begin(); itc!=solucion.end();itc++)
	{
		suma1=0;suma2=0;
		for(int i=0; i < (int)(*itc).peso_pos.size(); i++)
		{
			posi=((i%((*itc).posiciones/2))*(*itc).ancho_pallet)+((*itc).ancho_pallet/2);
			double aux=(*itc).peso_pos.at(i)*((*itc).pos_eje2-posi);
			fprintf(f1,"%.2f +",aux);
			suma1+=(*itc).peso_pos.at(i)*((*itc).pos_eje2-posi);
			
			suma2+=(*itc).peso_pos.at(i)*(posi-(*itc).pos_eje1);
		}
		fclose(f1);
		double p1=(*itc).max_peso_eje1*((*itc).pos_eje2-(*itc).pos_eje1);
		double p2=(*itc).max_peso_eje2*((*itc).pos_eje2-(*itc).pos_eje1);
		printf("\nsumas:%.2f\t%.2f\n",suma1,suma2);
		printf("Ejes :%.2f\t%.2f\n",p1,p2);
		
	}
	







	return cabe;
}




//int  clarkwrigth(list<camion>& solucion, camion truck, vector<cliente> l_clientes,datos dat)
//{
//	int cont = 0;
//	int num_ruta;
//	vector<ped_cli> l_sol;
//	list<pallet>::iterator itdem;
//	list<ahorro> savings;
//	list<ahorro>::iterator it_sa;
//	list<vector<int> > rutas;
//	list<vector<int> >::iterator it_ru;
//	vector<int> cliente_ruta;
//	vector<int> ruta2;
//
//	for (int i = 0; i <= l_clientes.size(); i++)
//	{
//		cliente_ruta.push_back(0);
//		vector<int>aux;
//		for (int j = 0; j <= dat.maxcli; j++)
//		{
//			aux.push_back(-1);
//		}
//		rutas.push_back(aux);
//	}
//
//
//	//1.- Calculo las rutas de 0 a i.
//	for (int i = 1; i < l_clientes.size(); i++)
//	{
//		for (int j = 1; j < l_clientes.size(); j++)
//		{
//			if (i < j)
//			{
//				int aux = l_clientes.at(i).distancias.at(0) + l_clientes.at(0).distancias.at(j)- l_clientes.at(i).distancias.at(j);
//				ahorro ahor_aux;
//				ahor_aux.ini = i;
//				ahor_aux.dest = j;
//				ahor_aux.sav = aux;
//				savings.push_back(ahor_aux);
//			}
//		}
//	}
//	//ordeno la lista de mayor a menor.
////	savings.sort([](const ahorro & a, const ahorro & b) {return a.sav > b.sav;} );
//
//	//2.- Calculo las rutas iniciales
//	it_ru = rutas.begin();
//	for (int i = 1; i < l_clientes.size(); i++)
//	{
//		(*it_ru).at(0) = i;
//		(*it_ru).at(i) = 0;
//		it_ru++;
//		cliente_ruta.at(i) = i;
//	}
//
//	//3.- Busco el primer candidato
//	int pallets_ruta;
//	for (it_sa = savings.begin(); it_sa != savings.end(); it_sa++)
//	{
//		pallets_ruta = 0;
//		int inicio = (*it_sa).ini;
//		int dest = (*it_sa).dest;
//		//Si pertenecen a la misma ruta seguir buscando
//		if (cliente_ruta.at(inicio) == cliente_ruta.at(dest)) continue;
//		else
//		{
//			for (int i = 0; i <= dat.maxcli; i++)
//			{
//				dat.ruta.at(i) = -1;
//				dat.cuantos.at(i) = 0;
//			}
//			//cargo la ruta
//			if (cliente_ruta.at(inicio) < cliente_ruta.at(dest)) num_ruta = cliente_ruta.at(inicio);
//			else  num_ruta = cliente_ruta.at(inicio);
//			it_ru = rutas.begin();
//			num_ruta--;
//			while (num_ruta > 0)
//			{
//				it_ru++;
//				num_ruta--;
//			}
//			
//			ruta2.clear();
//			for (int k = 0; k < (*it_ru).size(); k++)
//			{
//				dat.ruta.at(k) = (*it_ru).at(k);
//				if ((*it_ru).at(k) > 0)
//				{
//					ruta2.push_back((*it_ru).at(k));
//					dat.cuantos.at((*it_ru).at(k)) = l_clientes.at((*it_ru).at(k)).pedidos.size();
//					pallets_ruta += l_clientes.at((*it_ru).at(k)).pedidos.size();
//				}
//			}
//			int aux = dat.ruta.at(inicio);
//			dat.ruta.at(inicio) = dest;
//			ruta2.push_back(dest);
//			dat.ruta.at(dest) = aux;
//			dat.cuantos.at(dest) = l_clientes.at(dest).pedidos.size();
//			pallets_ruta += l_clientes.at(dest).pedidos.size();
//			
//			
//		}
//		// cabe por demanda
//		if (pallets_ruta <= 32)
//		{
//			//Relleno la estructura de datos para el heuristico
//			//inicializar ruta y peso y cli
//			for (int i = 0; i < dat.posiciones; i++)
//			{
//				dat.f1_peso.at(i) = 0;
//				dat.f1_cli.at(i) = 0;
//				dat.f1_t.at(i) = 0;
//			}
//			l_sol.clear();
//			for (int i = 0; i <= dat.maxcli; i++)
//			{
//				ped_cli pcli;
//				pcli.id = 0;
//				l_sol.push_back(pcli);
//			}
//
//			int itcli = dat.ruta.at(0);
//			itdem = l_clientes.at(itcli).pedidos.begin();
//			bool fin = false;
//			int cont2 = 0;
//			while(!fin)
//			{
//				for (int pos = 15; (pos >= 0 & !fin); pos--)
//				{
//					dat.f1_peso.at(pos) = (*itdem).peso;
//					dat.f1_cli.at(pos) = itcli;
//					dat.f1_t.at(pos) = cont2;
//					l_sol.at(itcli).id_pallet.push_back(cont2);
//					l_sol.at(itcli).peso_pallet.push_back((*itdem).peso);
//					itdem++;
//					if (itdem != l_clientes.at(itcli).pedidos.end())
//					{
//						cont2++;
//						int pos2 = pos + 16;
//						dat.f1_peso.at(pos2) = (*itdem).peso;
//						dat.f1_cli.at(pos2) = itcli;
//						dat.f1_t.at(pos2) = cont2;
//						l_sol.at(itcli).id_pallet.push_back(cont2);
//						l_sol.at(itcli).peso_pallet.push_back((*itdem).peso);
//						itdem++;
//						if (itdem == l_clientes.at(itcli).pedidos.end())
//						{
//							itcli=dat.ruta.at(itcli);
//							if (itcli <= 0)fin = true;
//							else
//							{
//								itdem = itdem = l_clientes.at(itcli).pedidos.begin();
//								cont2 = 0;
//							}
//						}
//						else cont2++;
//					}
//					else
//					{
//						itcli = dat.ruta.at(itcli);
//						if (itcli <= 0)fin = true;
//						else
//						{
//							itdem = itdem = l_clientes.at(itcli).pedidos.begin();
//							cont2 = 0;
//						}
//					}
//				}
//			}
//
//
//			if (heuristico2(dat))
//			{
//				printf("SI\n");
//				//actualizo las rutas.
//				cliente_ruta.at(dest) = inicio;
//				(*it_ru).at(inicio) = dest;
//				(*it_ru).at(dest) = 0;
//			}
//			else
//			{
//				LPCPLEX model2;
//
//				bool enc = model2.modelo1cam(dat, ruta2, l_sol);
//				if (enc == true)
//				{
//					cliente_ruta.at(dest) = inicio;
//					(*it_ru).at(inicio) = dest;
//					(*it_ru).at(dest) = 0;
//					printf("SI\n");
//				}
//				else printf("NO\n");
//			}
//		}
//		else return 0;
//	}
//}
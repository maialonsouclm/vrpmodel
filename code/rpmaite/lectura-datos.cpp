#include "stdafx.h"
#include <algorithm>

void leer_mias(char* name,vector<cliente>& list_clientes, camion &truck, int maxcli)
{
	FILE *f;
	char frase[150];
	int num,max_m;
	int ID,L,W,H,Q,Q1,D1,Q2,D2,POS;
	int cli, peso;

	//abro el archivo
	f=fopen(name,"r");
	printf("%s",name);

	if (f==NULL)printf("npo");

	fscanf(f,"%s %d",frase,&num);
	max_m=num;


	//camiones
	for(int i=1; i <= max_m;i++)
	{
		fscanf(f,"%d %d %d %d %d %d %d %d %d %d",&ID,&L,&W,&H,&Q,&Q1,&D1,&Q2,&D2,&POS);
		truck.add_datos(ID,W,H,L,Q,D1,Q1,D2,Q2,0,0,0,0,POS);
	}
	fscanf(f,"%s %d",frase,&num);
	max_m=num;
	//PALLETS
	for(int i=1; i <= max_m;i++)
	{
		fscanf(f,"%d %d %d %d",&L,&W,&H,&Q);
		truck.ancho_pallet=W;
		truck.largo_pallet=L;
		truck.alto_pallet=H;
		truck.peso_pallet=Q;
	}

	fscanf(f,"%s %d",frase,&num);
	max_m=num;
	//DEMANDA
	//Inicializo los clientes
	for(int i=0; i <=maxcli; i++)
	{
		cliente clien(i);
		clien.inicializa_distancias(maxcli);
		list_clientes.push_back(clien);
	}
	int cont = -1;
	int c_ant = 1;
	for(int i=1; i <= max_m;i++)
	{
		fscanf(f,"%d %d %d",&ID,&cli,&peso);
		cont = (int)list_clientes.at(cli).pedidos.size();
		pallet pal(truck.largo_pallet,truck.ancho_pallet,truck.alto_pallet,peso,cont,ID);		
		list_clientes.at(cli).pedidos.push_back(pal);	
		

	}

	//DISTANCIAS
	int x,y;
	float d1 = 0;
	fscanf(f,"%s %d %d",frase,&x, &y);

	for (int i = 0; i <= maxcli; i++)
	{
		for (int j = 0; j <= maxcli; j++)
		{
			fscanf(f, "%f ", &d1);
			list_clientes.at(i).add_dist(j, d1);
		}
	}
	
	
	fclose(f);
	/*list<pallet>::iterator itp;
	for (int i = 0; i <= maxcli; i++)
	{
		int cont = 0;
		for(itp = list_clientes.at(i).pedidos.begin();itp!= list_clientes.at(i).pedidos.end();itp++)
		{
			(*itp).id = cont;
			cont++;
		}

	}*/
}
//void leer_pollaris(char* name,vector<cliente>& list_clientes, camion &truck, int maxcli)
//{
//	FILE *f;
//	
//	
//	char linea[200],linea2[200],linea3[200],linea4[200];
//	int c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16;
//	int d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16;
//	int p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16;
//	double f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17;
//	
//
//	//Inicializo los clientes
//	for(int i=0; i <=maxcli; i++)
//	{
//		cliente cli(i);
//		cli.inicializa_distancias(maxcli);
//		list_clientes.push_back(cli);
//	}
//
//	//Camion
//	truck.add_datos(0,2440,2440,9120,32200,1000,11600,6500,21000,1200,800,100,10,22);
//
//	f=fopen(name,"rb");
//	printf("%s",name);
//
//	if (f==NULL)
//		printf("npo");
//
//	
//	switch(maxcli)
//	{
//		case 10: fscanf(f,";%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;",&c1,&c2,&c3,&c4,&c5,&c6,&c7,&c8,&c9,&c10,&c11);
//			for(int i=0; i <= 10;i++)
//			{
//				fscanf(f,"%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf",&f1,&f2,&f3,&f4,&f5,&f6,&f7,&f8,&f9,&f10,&f11,&f12);
//				list_clientes.at(i).add_dist(0,f2);
//				list_clientes.at(i).add_dist(1,f3);
//				list_clientes.at(i).add_dist(2,f4);
//				list_clientes.at(i).add_dist(3,f5);
//				list_clientes.at(i).add_dist(4,f6);
//				list_clientes.at(i).add_dist(5,f7);
//				list_clientes.at(i).add_dist(6,f8);
//				list_clientes.at(i).add_dist(7,f9);
//				list_clientes.at(i).add_dist(8,f10);
//				list_clientes.at(i).add_dist(9,f11);
//				list_clientes.at(i).add_dist(10,f12);
//			}
//			fgets(linea,200,f);
//			fgets(linea2,200,f);
//			fgets(linea3,200,f);
//			fgets(linea4,200,f);
//			fscanf(f,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;;",&c1,&c2,&c3,&c4,&c5,&c6,&c7,&c8,&c9,&c10);
//			fscanf(f,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;;",&d1,&d2,&d3,&d4,&d5,&d6,&d7,&d8,&d9,&d10);
//			fscanf(f,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;;",&p1,&p2,&p3,&p4,&p5,&p6,&p7,&p8,&p9,&p10);
//			for(int j=0; j< d1; j++)
//			{
//				pallet pal(1200,800,100,p1/d1,j);
//				list_clientes.at(c1-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d2; j++)
//			{
//				pallet pal(1200,800,100,p2/d2,j);
//				list_clientes.at(c2-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d3; j++)
//			{
//				pallet pal(1200,800,100,p3/d3,j);
//				list_clientes.at(c3-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d4; j++)
//			{
//				pallet pal(1200,800,100,p4/d4,j);
//				list_clientes.at(c4-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d5; j++)
//			{
//				pallet pal(1200,800,100,p5/d5,j);
//				list_clientes.at(c5-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d6; j++)
//			{
//				pallet pal(1200,800,100,p6/d6,j);
//				list_clientes.at(c6-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d7; j++)
//			{
//				pallet pal(1200,800,100,p7/d7,j);
//				list_clientes.at(c7-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d8; j++)
//			{
//				pallet pal(1200,800,100,p8/d8,j);
//				list_clientes.at(c8-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d9; j++)
//			{
//				pallet pal(1200,800,100,p9/d9,j);
//				list_clientes.at(c9-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d10; j++)
//			{
//				pallet pal(1200,800,100,p10/d10,j);
//				list_clientes.at(c10-1).pedidos.push_back(pal);
//			}
//			break;
//		case 15: fscanf(f,";%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d",&c1,&c2,&c3,&c4,&c5,&c6,&c7,&c8,&c9,&c10,&c11,&c12,&c13,&c14,&c15,&c16);
//			for(int i=0; i <= 15;i++)
//			{
//				fscanf(f,"%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf",&f1,&f2,&f3,&f4,&f5,&f6,&f7,&f8,&f9,&f10,&f11,&f12,&f13,&f14,&f15,&f16,&f17);
//				list_clientes.at(i).add_dist(0,f2);
//				list_clientes.at(i).add_dist(1,f3);
//				list_clientes.at(i).add_dist(2,f4);
//				list_clientes.at(i).add_dist(3,f5);
//				list_clientes.at(i).add_dist(4,f6);
//				list_clientes.at(i).add_dist(5,f7);
//				list_clientes.at(i).add_dist(6,f8);
//				list_clientes.at(i).add_dist(7,f9);
//				list_clientes.at(i).add_dist(8,f10);
//				list_clientes.at(i).add_dist(9,f11);
//				list_clientes.at(i).add_dist(10,f12);
//				list_clientes.at(i).add_dist(11,f13);
//				list_clientes.at(i).add_dist(12,f14);
//				list_clientes.at(i).add_dist(13,f15);
//				list_clientes.at(i).add_dist(14,f16);
//				list_clientes.at(i).add_dist(15,f17);
//			}
//			fgets(linea,200,f);
//			fgets(linea2,200,f);
//			fgets(linea3,200,f);
//			fgets(linea4,200,f);
//			fscanf(f,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;;",&c1,&c2,&c3,&c4,&c5,&c6,&c7,&c8,&c9,&c10,&c11,&c12,&c13,&c14,&c15);
//			fscanf(f,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;;",&d1,&d2,&d3,&d4,&d5,&d6,&d7,&d8,&d9,&d10,&d11,&d12,&d13,&d14,&d15);
//			fscanf(f,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;;",&p1,&p2,&p3,&p4,&p5,&p6,&p7,&p8,&p9,&p10,&p11,&p12,&p13,&p14,&p15);
//			for(int j=0; j< d1; j++)
//			{
//				pallet pal(1200,800,100,p1/d1,j);
//				list_clientes.at(c1-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d2; j++)
//			{
//				pallet pal(1200,800,100,p2/d2,j);
//				list_clientes.at(c2-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d3; j++)
//			{
//				pallet pal(1200,800,100,p3/d3,j);
//				list_clientes.at(c3-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d4; j++)
//			{
//				pallet pal(1200,800,100,p4/d4,j);
//				list_clientes.at(c4-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d5; j++)
//			{
//				pallet pal(1200,800,100,p5/d5,j);
//				list_clientes.at(c5-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d6; j++)
//			{
//				pallet pal(1200,800,100,p6/d6,j);
//				list_clientes.at(c6-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d7; j++)
//			{
//				pallet pal(1200,800,100,p7/d7,j);
//				list_clientes.at(c7-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d8; j++)
//			{
//				pallet pal(1200,800,100,p8/d8,j);
//				list_clientes.at(c8-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d9; j++)
//			{
//				pallet pal(1200,800,100,p9/d9,j);
//				list_clientes.at(c9-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d10; j++)
//			{
//				pallet pal(1200,800,100,p10/d10,j);
//				list_clientes.at(c10-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d11; j++)
//			{
//				pallet pal(1200,800,100,p11/d11,j);
//				list_clientes.at(c11-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d12; j++)
//			{
//				pallet pal(1200,800,100,p12/d12,j);
//				list_clientes.at(c12-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d13; j++)
//			{
//				pallet pal(1200,800,100,p13/d13,j);
//				list_clientes.at(c13-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d14; j++)
//			{
//				pallet pal(1200,800,100,p14/d14,j);
//				list_clientes.at(c14-1).pedidos.push_back(pal);
//			}
//			for(int j=0; j< d15; j++)
//			{
//				pallet pal(1200,800,100,p15/d15,j);
//				list_clientes.at(c15-1).pedidos.push_back(pal);
//			}
//			break;
//	}
//	fclose(f);
//	
//
//}


//Ortec: leer_csv(argv[1],clientes,truck);

//void leer_csv(char* name,vector<cliente>& list_clientes, camion &truck)
//{
//	FILE *f;
//	char * frase=new char[20];
//	int num,id,id_cli,q,apila,idl,itl,lp;
//	double w,l,h,peso,max_alt,d1,d2,p1,p2,dist;
//	double anchom,altom,largom,pesom;
//    int rx,ry,rz,arriba,abajo;
//	int cont=0;
//	
//	int max_m=0;
//	
//	vector<cliente>::iterator itc;
//	list<box> lcajas;
//	list<layer> llayers;
//	list<box>::iterator ip;
//	list<layer>::iterator il;
//	
//
//	f=fopen(name,"r");
//	printf("%s",name);
//
//	if (f==NULL)
//		printf("npo");
//
//	fscanf(f,"%s %d",frase,20,&num,sizeof(int));
//	max_m=num;
//	//creo e deposito
//	cliente cli(0);
//	list_clientes.push_back(cli);
//
//	for(int i=0; i < max_m;i++)
//	{
//		fscanf(f,"%d %d %d %lf %lf %lf %lf %d %d %d %d %lf %d %d %d",&id,&id_cli,&q,&w,&l,&h,&peso,&rx,&ry,&rz,&apila,&max_alt,&arriba,&abajo,&idl);
//		if(list_clientes.empty() || (((int)list_clientes.size())<= id_cli))
//		{
//			cliente cli(id_cli);
//			list_clientes.push_back(cli);
//		}
//		box caja(id,l,w,h,q,peso,id_cli,rx,ry,rz,idl);
//		lcajas.push_back(caja);
//		
//	}
//
//	fscanf(f,"%s %d",frase,20,&num,sizeof(int));
//	max_m=num;
//	for(int i=0; i < max_m;i++)
//	{
//		fscanf(f,"%d %lf %lf %lf %lf %d %d %d",&id,&w,&l,&h,&peso,&rx,&itl,&lp);
//		layer lay(id, rx?true:false, itl,id,w,l,h, 0,0, 0, 0, 0,lp);
//		lay.set_weight(peso);
//		llayers.push_back(lay);
//	}
//
//	fscanf(f,"%s",frase,20);
//	fscanf(f,"%d %lf %lf %lf %lf",&id,&anchom,&largom,&altom,&pesom);
//	//truck (id, w, h, l, peso, 0,0,0,0);
//	//l_t.push_back(pallet);
//
//	fscanf(f,"%s",frase,20);
//	fscanf(f,"%d %lf %lf %lf %lf %lf %lf %lf %lf",&id,&w,&l,&h,&peso,&d1,&d2,&p1,&p2);
//	int pos=2*((int)(l/anchom));
//	truck.add_datos(id, w, h, l, peso, d1,p1,d2,p2, anchom,largom,altom,pesom, pos);
//	
//
//	// distancias
//	int total_clientes=list_clientes.size();
//	for(itc=list_clientes.begin(); itc!=list_clientes.end(); itc++)
//	{
//		(*itc).inicializa_distancias(total_clientes);
//	}
//	fscanf(f,"%s",frase,20);
//	
//	for(int i=0; i < total_clientes; i++)
//	{
//		fscanf(f,"%d ",&id);
//		for(int j=0; j < total_clientes; j++)
//		{
//			fscanf(f,"%lf",&dist);
//			list_clientes.at(i).add_dist(j,dist);
//		}
//	}
//
//
//
//	//a�adir al layer, peso,id_prod, cant,apila,prio
//	for(ip=lcajas.begin(); ip!=lcajas.end(); ip++)
//	{
//		for(il=llayers.begin();il!=llayers.end();il++)
//		{
//			if((*il).get_id()==(*ip).id_layer)
//			{
//				(*il).set_id_prio((*ip).cliente);
//				(*il).set_num_items_order((*ip).cantidad);
//				(*il).set_id_item((*ip).id);
//				int n=(*ip).cantidad/(*il).get_items_layer();
//				(*il).set_num_layers((*ip).cantidad/(*il).get_items_layer());
//				if(n > (*il).get_max_layer_per_pallet())
//				{
//					int stock= n/(*il).get_max_layer_per_pallet();
//					int casel= n - (stock * (*il).get_max_layer_per_pallet());
//					if(stock >0) (*il).set_stock_pallet(stock);
//					if(casel > 0)(*il).set_case_layer(casel);
//				}
//				else
//				{
//					(*il).set_case_layer(n);
//				}
//			}
//		}
//	}
//
//	// Hacemos los pallets por cliente
//	double altoac[4];
//	double pesoac[4];
//	for(int i=1; i < 4; i++)
//	{
//		altoac[i]=altom;
//		pesoac[i]=pesom;
//	}
//	for(il=llayers.begin();il!= llayers.end(); il++)
//	{
//		//stock pallets
//		if((*il).get_num_stock_pallet()>0)
//		{
//			double alto=altom+((*il).get_height()*(*il).get_max_layer_per_pallet());
//			double peso=pesom+((*il).get_weight()*(*il).get_max_layer_per_pallet());
//			for (int i=0; i < (*il).get_num_stock_pallet(); i++)
//			{
//				pallet pal(largom,anchom,alto,peso,i);
//				list_clientes.at((*il).get_id_prio()).pedidos.push_back(pal);
//			}
//			(*il).set_stock_pallet(0);
//		}
//		//case pallets
//		if((*il).get_case_layers()>0)
//		{
//			//si al menos cabe un layer.
//			double dist=truck.alto-altoac[(*il).get_id_prio()];
//			if(dist >= (*il).get_height())
//			{
//				int caben=dist/(*il).get_height();
//				caben=std::min(caben,(*il).get_case_layers());
//				altoac[(*il).get_id_prio()]+=caben*(*il).get_height();
//				pesoac[(*il).get_id_prio()]+=caben*(*il).get_weight();
//				(*il).set_case_layer((*il).get_case_layers()-caben);
//			}
//			else
//			{
//				pallet pal(largom,anchom,altoac[(*il).get_id_prio()],pesoac[(*il).get_id_prio()], (*il).get_num_stock_pallet()+1);
//				list_clientes.at((*il).get_id_prio()).pedidos.push_back(pal);
//				altoac[(*il).get_id_prio()]=altom;
//				pesoac[(*il).get_id_prio()]=pesom;
//				//si no he podidio poner todas las de ese layer
//				if((*il).get_case_layers()>0)
//				{
//					dist=truck.alto-altoac[(*il).get_id_prio()];
//					if(dist >= (*il).get_height())
//					{
//						int caben=dist/(*il).get_height();
//						caben=std::min(caben,(*il).get_case_layers());
//						altoac[(*il).get_id_prio()]+=caben*(*il).get_height();
//						pesoac[(*il).get_id_prio()]+=caben*(*il).get_weight();
//						(*il).set_case_layer((*il).get_case_layers()-caben);
//					}
//				}
//			}
//		}
//	}
//	
//
//	//PARA LA PRUEBA ME QUEDO CON 3 PALLETS UNO DE CADA CLIENTE
//	list<pallet>::iterator itp,itp2;
//	for (int i=1; i <= 3; i++)
//	{
//		itp=list_clientes.at(i).pedidos.begin();
//		for(int j=0; j < 20; j++ )
//		{	
//			itp++;
//		}
//		/*if(i<=2)
//		{
//			itp2=itp;
//			itp2++;
//			list_clientes.at(i).pedidos.erase(itp2,list_clientes.at(i).pedidos.end());
//		}
//		else*/ list_clientes.at(i).pedidos.erase(itp,list_clientes.at(i).pedidos.end());
//	}
//
//}

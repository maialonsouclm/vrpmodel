/***********************************************/
/*        MODELO PACKING 1 CAMION              */
/***********************************************/

#pragma warning(disable:4786)

#include "LPCPLEX.h"
#include <assert.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <ilconcert/ilolinear.h>

bool LPCPLEX::modelo1cam(datos &dat, vector<int> ruta, vector<vector<double> > pesos, vector<vector<int> > p_t)
{
	int maxpos = dat.posiciones;
	int maxclientes = ruta.size() - 1;
	int maxcam = 1;
	char nombre[22];
	double pesototal = 0;
	double pesopos1 = 0, pesopos2 = 0;
	int pos;
	int frente = dat.posiciones / 2;

	model2 = IloModel(env2);
	IloRangeArray restr2(env2);
	IloNum max_time = 60; //tiempo del modelo


	//Creo las variables
	NumVector c_k(env2);   //si el camion k es usado o no
	NumVarMatrix4 m_kitp(env2, maxcam); // si el pallet t del cliente i esta en la posici�n p del cami�n k

	//variables de estabilidad
	NumVarMatrix l_kp(env2, maxcam);  //hueco izquierda de la posicion p del camion k
	NumVarMatrix r_kp(env2, maxcam); //hueco derecha de la posicion p del camion k
	NumVarMatrix s_kp(env2, maxcam);//hueco fila opuesta de la posicion p del camion k


/***************** INICIILIZO VARIABLES ************************/

	//inicializo la variable camion
	c_k = IloIntVarArray(env2, maxcam, 0, 1);
	for (int k = 0; k < maxcam; k++)
	{
		sprintf(nombre, "C%d", k);
		c_k[k].setName(nombre);
	}


	//inicializo la variable m_ktip
	for (int k = 0; k < maxcam; k++)
	{
		m_kitp[k] = NumVarMatrix3(env2, dat.maxcli + 1);
		for (int i = 1; i <= dat.maxcli; i++)
		{
			m_kitp[k][i] = NumVarMatrix(env2, dat.cuantos.at(i));
			for (int t = 0; t < dat.cuantos.at(i); t++)
			{
				m_kitp[k][i][t] = IloIntVarArray(env2, dat.posiciones, 0, 1);
				for (int p = 0; p < dat.posiciones; p++)
				{
					sprintf(nombre, "m_%d_%d_%d_%d", k, i, t, p);
					m_kitp[k][i][t][p].setName(nombre);
				}
			}
		}
	}

	/********** VARIABLES ESTABILIDAD ***************/
	//inicializo l_kp
	for (int k = 0; k < maxcam; k++)
	{
		l_kp[k] = IloIntVarArray(env2, maxpos, 0, 1);
		for (int p = 0; p < maxpos; p++)
		{
			sprintf(nombre, "L_%d_%d", k, p);
			l_kp[k][p].setName(nombre);
		}
	}

	//inicializo b_kp
	for (int k = 0; k < maxcam; k++)
	{
		r_kp[k] = IloIntVarArray(env2, maxpos, 0, 1);
		for (int p = 0; p < maxpos; p++)
		{
			sprintf(nombre, "R_%d_%d", k, p);
			r_kp[k][p].setName(nombre);
		}
	}
	//inicializo s_kp
	for (int k = 0; k < maxcam; k++)
	{
		s_kp[k] = IloIntVarArray(env2, maxpos, 0, 1);
		for (int p = 0; p < maxpos; p++)
		{
			sprintf(nombre, "S_%d_%d", k, p);
			s_kp[k][p].setName(nombre);
		}
	}
	/**********************************************************/
	/***************** RESTRICCIONES **************************/
	/**********************************************************/

	add_restri_1cam(restr2, maxclientes, dat, l_kp, s_kp, r_kp, m_kitp, c_k, pesos);

	model2.add(restr2);


	/**********************************************************/
	/***************FUNCION OBJETIVO **************************/
	/**********************************************************/

	//IloExpr Expression_fobj2(env2);

	//for (int k = 0 ;k < maxcam; k++)
	//{
	//	Expression_fobj2 += c_k[k];
	//}
	////minimizar numero camiones
	//model2.add(IloMinimize(env2,Expression_fobj2));
	//Expression_fobj2.end();	

	/***************************************************************/
/*************** EJECUTAMOS EL MODELO **************************/
/***************************************************************/
	try{

		IloCplex cplex2(model2);
	
		//No hace el probing
		cplex2.setParam(IloCplex::Probe,-1);
		//1 CORE
		cplex2.setParam(IloCplex::Threads, 12);

		
		cplex2.setParam(IloCplex::MIPEmphasis,1);

		//cplex2.exportModel("maite1CAM.lp");
		//resolver el modelo y tomamos tiempos
		IloNum inicio,fin;
		clock_t t_ini, t_fin;

		inicio=cplex2.getCplexTime();
		t_ini = clock();


		IloBool result=cplex2.solve();

		t_fin = clock();
		fin=cplex2.getCplexTime();
		
		//double secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
		double secs = (double)(fin - inicio);
		dat.tiempo_modelo += secs;
		printf("%f \n",secs);

		//Estado del algoritmo	al terminar
		IloCplex::Status estado=cplex2.getCplexStatus();	
		IloCplex::Status sub_estado=cplex2.getCplexSubStatus();

		bool opt = false;
		if (result != false)
		{
			//double value2 = cplex2.getObjValue();
			////|bestnode-bestinteger|/(1e-10+|bestinteger|)
			//double relativeGap = fabs(cplex2.getObjValue() - cplex2.getBestObjValue()) / (fabs(cplex2.getObjValue()) + 1e-10);
			//if (value2 != 1)
			//{
			//	opt = false;
			//	
			//}
			//else
			//{
				opt = true;
				dat.f1_cli.clear();
				dat.f1_peso.clear();
				dat.f1_t.clear();
				for (int i = 0; i < dat.posiciones; i++)
				{
					dat.f1_peso.push_back(0);
					dat.f1_cli.push_back(0);
					dat.f1_t.push_back(-1);
				}
				//Guardo la soluci�n
				for (int k = 0; k < maxcam; k++)
				{
					for (int p = 0; p < dat.posiciones; p++)
					{
						for (int i = 1; i <= dat.maxcli; i++)
						{
							for (int t = 0; t < dat.cuantos.at(i); t++)
							{
								if (cplex2.isExtracted(m_kitp[k][i][t][p]) && cplex2.getValue(m_kitp[k][i][t][p]) > 0.01)
								{
									//printf("%d %d \n",i,p);
									dat.f1_cli.at(p) = i;
									dat.f1_peso.at(p) =pesos.at(i).at(t) ;
									dat.f1_t.at(p) =p_t.at(i).at(t) ;
									pesototal += pesos.at(i).at(t);
									//Calculos para el peso en los ejes
									if(p < (dat.posiciones/2)) pos = (p * dat.largo_pallet) + (dat.largo_pallet / 2);
									else pos= ((p-frente) * dat.largo_pallet) + (dat.largo_pallet / 2);
									pesopos1 += dat.f1_peso.at(p) * (dat.pos_eje2 - pos);
									pesopos2 += dat.f1_peso.at(p) * (pos - dat.pos_eje1);
								}
							}
						}
					}
				}
				dat.peso_ca = pesototal;
				dat.p_eje1=pesopos1 / (dat.pos_eje2 - dat.pos_eje1);
				dat.p_eje2=pesopos2 / (dat.pos_eje2 - dat.pos_eje1);
			/*}*/
		}
		else
		{
			dat.veces_modelo_gap++;
			opt = false;

		}
	


		cplex2.end();
		model2.end();
		env2.end();
		return opt;
	}
	catch (IloException& e) 
	{
		printf("EXCEPCION");
	    std::cerr << "IloException: " << e << "Status" << e ;
		return false;
	}
}

void LPCPLEX::add_restri_1cam(IloRangeArray &restr2, int maxclientes, datos dat, NumVarMatrix& l_kp, NumVarMatrix& s_kp, NumVarMatrix& r_kp, NumVarMatrix4& m_kitp, NumVector& c_k, vector<vector<double> > pesos)
{
	double l_posicion=dat.largo_pallet;
	double medio_p;
	double Gx=dat.largo/2;
	double Gy=dat.ancho/2;
	double tau1x=dat.pos_eje2-(dat.largo/2);
	double tau2x=(dat.largo/2)-2;
	double tau1y=dat.ancho/8;
	double tau2y=dat.ancho/8;
	double posydetras=(dat.ancho)/4;
	double posydelante=(dat.ancho*3)/4;
	int maxcam = 1;
	
	int contr = 0;
	
	/************* En cada posicion un pallet o nada *************/
	for (int k = 0; k < maxcam; k++)
	{
		for (int p = 0; p < dat.posiciones; p++)
		{
			IloExpr v(env2);
			for (int i = 1; i <= dat.maxcli; i++)
			{
				for (int t = 0; t < dat.cuantos.at(i); t++)
				{
					v += m_kitp[k][i][t][p];
				}
			}
			restr2.add(IloRange(env2, -IloInfinity, v, 1));
			v.end();
			contr++;
		}
	}
	//printf("en cada posici�n un pallet o nada %d \n", contr);
	// si el camion esta en uso al menos un pallet dentro
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v(env2);
		v += c_k[k];
		for (int i = 1; i <= dat.maxcli; i++)
		{
			for (int t = 0; t < dat.cuantos.at(i); t++)
			{
				for (int p = 0; p < dat.posiciones; p++)
				{
					v -= m_kitp[k][i][t][p];
				}
			}
		}
		restr2.add(IloRange(env2, -IloInfinity, v, 0));
		v.end();
		contr++;
	}
	//printf("si el cami�n esta en uso un pallet o nada %d \n", contr);
	/************* DEMANDA, todos los pallets deben ir colocados en alguan posicion ************/
	for (int i = 1; i <= dat.maxcli; i++)
	{
		for (int t = 0; t < dat.cuantos.at(i); t++)
		{
			IloExpr v(env2);
			for (int k = 0; k < maxcam; k++)
			{
				for (int p = 0; p < dat.posiciones; p++)
				{
					v += m_kitp[k][i][t][p];
				}
			}
			restr2.add(IloRange(env2, 1, v, 1));
			v.end();
			contr++;
		}
	}
	
	//printf("demanda %d \n", contr);
	/****Atender la demanda, el total de pallets de cada cliente debe ser el demandado *****/
	for (int i = 1; i <= dat.maxcli; i++)
	{
		IloExpr v(env2);
		for (int k = 0; k < maxcam; k++)
		{
			for (int p = 0; p < dat.posiciones; p++)
			{
				for (int t = 0; t < dat.cuantos.at(i); t++)
				{
					v += m_kitp[k][i][t][p];
				}
			}
		}
		v -= dat.cuantos.at(i);
		restr2.add(IloRange(env2, 0, v, IloInfinity));
		v.end();
		contr++;
	}
	//printf("Atender la demnada, el total de pallet de cada cliente es el demandado %d \n", contr);
	/************* PESO ************/
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v(env2);
		for (int i = 1; i <= dat.maxcli; i++)
		{
			for (int t = 0; t < dat.cuantos.at(i); t++)
			{
				for (int p = 0; p < dat.posiciones; p++)
				{

					v += m_kitp[k][i][t][p] * pesos.at(i).at(t);
				}
			}
		}
		v -= (dat.peso_total*c_k[k]);
		restr2.add(IloRange(env2, -IloInfinity, v, 0));
		v.end();
		contr++;
	}
	//printf("peso %d \n", contr);


	/************* PESO EJES ************/
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v2(env2);
		for (int i = 1; i <= dat.maxcli; i++)
		{
			for (int t = 0; t < dat.cuantos.at(i); t++)
			{
				for (int p = 0; p < dat.posiciones; p++)
				{
					medio_p = (p % (dat.posiciones / 2))*l_posicion + (l_posicion / 2);
					v2 += m_kitp[k][i][t][p] * pesos.at(i).at(t) *(dat.pos_eje2 - medio_p);
				}
			}
		}
		v2 -= (dat.max_eje1*(dat.pos_eje2 - dat.pos_eje1))*c_k[k];
		restr2.add(IloRange(env2, -IloInfinity, v2, 0));
		v2.end();
		contr++;


		IloExpr v3(env2);
		for (int i = 1; i <= dat.maxcli; i++)
		{
			for (int t = 0; t < dat.cuantos.at(i); t++)
			{
				for (int p = 0; p < dat.posiciones; p++)
				{
					medio_p = (p % (dat.posiciones / 2))*l_posicion + (l_posicion / 2);
					v3 += m_kitp[k][i][t][p] * pesos.at(i).at(t) *(medio_p - dat.pos_eje1);
				}
			}
		}
		v3 -= (dat.max_eje2*(dat.pos_eje2 - dat.pos_eje1))*c_k[k];
		restr2.add(IloRange(env2, -IloInfinity, v3, 0));
		v3.end();
		contr++;
	}

	//printf("peso ejes %d \n", contr);
	/************* CENTRO GRAVEDAD ************/

	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v4(env2);
		IloExpr v5(env2);
		for (int i = 1; i <= dat.maxcli; i++)
		{
			for (int p = 0; p < dat.posiciones; p++)
			{
				medio_p = (p % (dat.posiciones / 2))*l_posicion + (l_posicion / 2);
				for (int t = 0; t < dat.cuantos.at(i); t++)
				{
					v4 += m_kitp[k][i][t][p] * pesos.at(i).at(t) *medio_p;
					v5 += m_kitp[k][i][t][p] * pesos.at(i).at(t);
				}
			}
		}
		v5 += dat.peso_ca;
		v4 = ((dat.peso_ca*Gx) + v4) - (v5*(Gx + tau1x));
		restr2.add(IloRange(env2, -IloInfinity, v4, 0));
		v4.end();
		v5.end();
		contr++;

		IloExpr v6(env2);
		IloExpr v7(env2);
		for (int j = 1; j <= dat.maxcli; j++)
		{
			for (int p = 0; p < dat.posiciones; p++)
			{
				medio_p = (p % (dat.posiciones / 2))*l_posicion + (l_posicion / 2);
				for (int t = 0; t < dat.cuantos.at(j); t++)
				{
					v6 += m_kitp[k][j][t][p] * pesos.at(j).at(t) *medio_p;
					v7 += m_kitp[k][j][t][p] * pesos.at(j).at(t);
				}
			}
		}
		v7 += dat.peso_ca;
		v6 = ((dat.peso_ca*Gx) + v6) - (v7*(Gx - tau2x));
		restr2.add(IloRange(env2, 0, v6, IloInfinity));
		v6.end();
		v7.end();
		contr++;

		IloExpr v8(env2);
		IloExpr v9(env2);
		for (int j = 1; j <= dat.maxcli; j++)
		{
			for (int p = 0; p < dat.posiciones; p++)
			{
				if (p < (dat.posiciones / 2))medio_p = posydetras;
				else medio_p = posydelante;
				for (int t = 0; t < dat.cuantos.at(j); t++)
				{
					v8 += m_kitp[k][j][t][p] * pesos.at(j).at(t) *medio_p;
					v9 += m_kitp[k][j][t][p] * pesos.at(j).at(t);
				}
			}
		}
		v9 += dat.peso_ca;
		v8 = ((dat.peso_ca*Gy) + v8) - (v9*(Gy + tau1y));
		restr2.add(IloRange(env2, -IloInfinity, v8, 0));
		v8.end();
		v9.end();
		contr++;

		IloExpr v10(env2);
		IloExpr v11(env2);
		for (int j = 1; j <= dat.maxcli; j++)
		{
			for (int p = 0; p < dat.posiciones; p++)
			{
				if (p < (dat.posiciones / 2))medio_p = posydetras;
				else medio_p = posydelante;
				for (int t = 0; t < dat.cuantos.at(j); t++)
				{
					v10 += m_kitp[k][j][t][p] * pesos.at(j).at(t) *medio_p;
					v11 += m_kitp[k][j][t][p] * pesos.at(j).at(t);
				}
			}
		}
		v11 += dat.peso_ca;
		v10 = ((dat.peso_ca*Gy) + v10) - (v11*(Gy - tau2y));
		restr2.add(IloRange(env2, 0, v10, IloInfinity));
		v10.end();
		v11.end();
		contr++;
	}
	//printf("cog %d \n", contr);
	/************* ESTABILIDAD ************/
	for (int k = 0; k < maxcam; k++)
	{
		for (int p = 1; p < dat.posiciones; p++)
		{
			if (p == (dat.posiciones / 2)) continue;
			else
			{
				IloExpr v1(env2);
				v1 += l_kp[k][p];
				for (int i = 1; i <= dat.maxcli; i++)
				{
					for (int t = 0; t < dat.cuantos.at(i); t++)
					{
						v1 += m_kitp[k][i][t][p - 1] - m_kitp[k][i][t][p];
					}
				}
				restr2.add(IloRange(env2, 0, v1, IloInfinity));
				v1.end();
				contr++;
			}
		}

		IloExpr v1(env2);
		v1 = l_kp[k][0];
		int pos = (dat.posiciones / 2);

		IloExpr v2(env2);
		v2 = l_kp[k][pos];
		for (int i = 1; i <= dat.maxcli; i++)
		{
			for (int t = 0; t < dat.cuantos.at(i); t++)
			{
				//para posicion 0		
				v1 -= m_kitp[k][i][t][0];

				//para posicion p/2 la 15		
				v2 -= m_kitp[k][i][t][pos];
			}
		}
		restr2.add(IloRange(env2, 0, v1, IloInfinity));
		v1.end();
		contr++;
		restr2.add(IloRange(env2, 0, v2, IloInfinity));
		v2.end();
		contr++;

		for (int p = 0; p < dat.posiciones - 1; p++)
		{
			if (p == (dat.posiciones / 2) - 1) continue;
			IloExpr v1(env2);
			v1 = r_kp[k][p];
			for (int i = 1; i <= dat.maxcli; i++)
			{
				for (int t = 0; t < dat.cuantos.at(i); t++)
				{
					v1 += m_kitp[k][i][t][p + 1] - m_kitp[k][i][t][p + 1];
				}
			}
			restr2.add(IloRange(env2, 0, v1, IloInfinity));
			v1.end();
			contr++;
		}


		pos = (dat.posiciones / 2) - 1;
		v1 = r_kp[k][pos];
		v2 = r_kp[k][dat.posiciones - 1];

		for (int i = 1; i <= dat.maxcli; i++)
		{
			for (int t = 0; t < dat.cuantos.at(i); t++)
			{
				//para posicion p/2-1				
				v1 -= m_kitp[k][i][t][pos];

				//para posicion p, ultima									
				v2 -= m_kitp[k][i][t][dat.posiciones - 1];
			}
		}
		restr2.add(IloRange(env2, 0, v1, IloInfinity));
		v1.end();
		contr++;
		restr2.add(IloRange(env2, 0, v2, IloInfinity));
		v2.end();
		contr++;


		for (int p = 0; p < (dat.posiciones / 2) - 1; p++)
		{
			IloExpr v1(env2);
			v1 = s_kp[k][p];
			for (int i = 1; i <= dat.maxcli; i++)
			{
				for (int t = 0; t < dat.cuantos.at(i); t++)
				{
					v1 += m_kitp[k][i][t][p + (dat.posiciones / 2)] - m_kitp[k][i][t][p];
				}
			}
			restr2.add(IloRange(env2, 0, v1, IloInfinity));
			v1.end();
			contr++;
		}

		for (int p = 0; p < (dat.posiciones / 2) - 1; p++)
		{
			IloExpr v1(env2);
			v1 = s_kp[k][p];
			for (int i = 1; i <= dat.maxcli; i++)
			{
				for (int t = 0; t < dat.cuantos.at(i); t++)
				{
					v1 += m_kitp[k][i][t][p] - m_kitp[k][i][t][p + (dat.posiciones / 2)];
				}
			}
			restr2.add(IloRange(env2, 0, v1, IloInfinity));
			v1.end();
			contr++;
		}
		
		IloExpr v12(env2);
		for (int p = 0; p < (dat.posiciones / 2); p++)
		{
			v12 += l_kp[k][p];
		}
		restr2.add(IloRange(env2, -IloInfinity, v12, 1));
		v12.end();
		contr++;
		
		IloExpr v13(env2);
		for (int p = (dat.posiciones / 2); p < dat.posiciones; p++)
		{
			v13 += l_kp[k][p];
		}
		restr2.add(IloRange(env2, -IloInfinity, v13, 1));
		v13.end();
		contr++;

		IloExpr v14(env2);
		for (int p = 0; p < (dat.posiciones / 2); p++)
		{
			v14 += r_kp[k][p];
		}
		restr2.add(IloRange(env2, -IloInfinity, v14, 1));
		v14.end();
		contr++;

		IloExpr v15(env2);
		for (int p = (dat.posiciones / 2); p < dat.posiciones; p++)
		{
			v15 += r_kp[k][p];
		}
		restr2.add(IloRange(env2, -IloInfinity, v15, 1));
		v15.end();
		contr++;
	
		IloExpr v16(env2);
		for (int p = 0; p < (dat.posiciones / 2); p++)
		{
			v16 += s_kp[k][p];
		}
		restr2.add(IloRange(env2, -IloInfinity, v16, 1));
		v16.end();
		contr++;
	}
	//printf("estabilidad %d \n", contr);
	/************* MULTIDROP ************/


	vector<int>::iterator iti;
	vector<int>::iterator itj;
	vector<int> auxr;
	for (int k = 0; k < maxcam; k++)
	{
		int sig = dat.ruta.at(0);
		while (sig != 0)
		{
			auxr.push_back(sig);
			sig = dat.ruta.at(sig);
		}
		for (iti=auxr.begin();iti!=auxr.end();iti++)
		{
			if ((*iti) <= 0) continue;
			
			for (itj = iti; itj != auxr.end(); itj++)
			{
				if ((*itj) <= 0) continue;
				if ((*iti) == (*itj)) continue;
				
				for (int p = 0; p < dat.posiciones; p++)
				{
					IloExpr v17(env2);
					for (int t = 0; t < dat.cuantos.at((*iti)); t++)
					{
						v17 += m_kitp[k][(*iti)][t][p];
					}
					if (p < (dat.posiciones / 2))
					{
						for (int pp = p + 1; pp < dat.posiciones; pp++)
						{
							if ((pp >= (dat.posiciones / 2)) && (pp <= p + (dat.posiciones / 2))) continue;

							IloExpr v18(env2);
							for (int t = 0; t < dat.cuantos.at((*itj)); t++)
							{
								v18 += m_kitp[k][(*itj)][t][pp];
							}
							v18 = v17 + v18;
							//if (dat.ruta.at((*iti)) == (*itj)) v18 = v18 + 1; //esto es x_i_j=1 si va de i a j
							restr2.add(IloRange(env2, -IloInfinity, v18, 1));
							v18.end();
						}
					}
					else
					{
						for (int pp = p - (dat.posiciones / 2); pp < dat.posiciones; pp++)
						{
							if ((pp <= (p - (dat.posiciones / 2))) || (pp > 15 && pp <= p)) continue;

							IloExpr v18(env2);
							for (int t = 0; t < dat.cuantos.at((*itj)); t++)
							{
								v18 += m_kitp[k][(*itj)][t][pp];
							}
							v18 = v17 + v18;
							//if (dat.ruta.at((*iti)) == (*itj)) v18 = v18 + 1; //esto es x_i_j=1 si va de i a j
							restr2.add(IloRange(env2, -IloInfinity, v18, 1));
							v18.end();
						}
					}
					v17.end();
				}
			}
		}
	}
	//printf("multidrop %d \n", contr);
	/*****Orden de los camiones***/
	/*IloExpr v1(env2);
	v1 += c_k[0] - c_k[1];
	restr2.add(IloRange(env2, 0, v1, IloInfinity));
	v1.end();

	IloExpr v2(env2);
	v2 += c_k[0] ;
	restr2.add(IloRange(env2, 1, v2, 1));
	v2.end();*/
}
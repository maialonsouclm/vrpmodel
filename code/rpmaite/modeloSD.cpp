/***********************************************/
/*        MODELO SPLIT AND DELIVERY            */
/***********************************************/

#pragma warning(disable:4786)

#include "LPCPLEX.h"
#include <assert.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <ilconcert/ilolinear.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>



/**********************************************************/
/********************** LAZY CALLBACK *********************/
/**********************************************************/
ILOLAZYCONSTRAINTCALLBACK6(MIFUNsd, datos&, dat,vector<cliente>, l_cli,NumVarMatrix3F, x_kij, NumVarMatrix4, z_kitp,NumVector, y_k, IloRangeArray, restr)
{

	printf("Dentro lazy\n");

	dat.veces_entra++;


	IloEnv callEnv = getEnv();
	list<pallet>::iterator itp;
	vector<int> ruta2;
	vector<int>::iterator itr;
	int total_pal=0;
	int camiones_sol=0;	
	vector<vector <double> > pesosp;
	vector<vector<int> > p_t;
	vector<ped_cli> l_sol;
	vector<int> auxint;
	vector<double> auxd;
	vector<string> sol_heu;
	bool abort =false;
	dat.fo_heu=0;
	bool enc;
	int cont_cli = 0;
	
	camion cam;
	dat.sol_1cam.clear();
	cam.add_datos(9, dat.ancho, 2400, dat.largo, dat.peso_total, dat.pos_eje1, dat.max_eje1, dat.pos_eje2, dat.max_eje2, dat.largo_pallet, dat.ancho, 30, 30, dat.posiciones);
	dat.fo_heu = 0;
		
//LEO LA SOLUCION
	for(int k=0; k < dat.maxcam && abort==false; k++)
	{
		
		if(getValue(y_k[k])<=0.0001) 
			continue;
		else dat.veces_entra_k++;

		printf("camion %d, ",k);
		camiones_sol++;
		//cam_con_estamulti.push_back(0);
		datos dat_aux;
		rellena_callback(dat_aux, l_cli, cam, dat.maxcam, dat.maxcli, dat.nominsta, dat.modelo);
		pesosp.clear();
		p_t.clear();
		
		for(int i=0; i <= dat.maxcli; i++)
		{
			pesosp.push_back(auxd);
			p_t.push_back(auxint);
		}
		
		//recojo la ruta del camion K
		ruta2.clear();
		total_pal=0;
		cont_cli = 0;
		for(int i=0; i <= dat.maxcli; i++)
		{
			for(int j=0; j <=dat.maxcli; j++)
			{
				if(i==j) continue;
				if(getValue(x_kij[k][i][j])>0.0001)
				{
					cont_cli++;
					dat_aux.ruta.at(i)=j;// guardo la ruta
					ruta2.push_back(j);
					dat_aux.fo_heu+=l_cli.at(i).distancias.at(j); //calculo la fo de la soluci�n
				}
			}
		}

		if (cont_cli > 2) dat_aux.compartido = true;
		else dat_aux.compartido = false;

		total_pal = 0;
		//para cada cami�n cojo el packing
		for(int i=1; i <= dat.maxcli; i++)
		{
			for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
			{
				for(int p=0; p < dat.posiciones; p++)
				{
					if(getValue(z_kitp[k][i][t][p])>0.0001)
					{
						itp = l_cli.at(i).pedidos.begin();
						while ((*itp).id != t) itp++;
						dat_aux.cuantos.at(i)++;
						pesosp.at(i).push_back((*itp).peso);
						p_t.at(i).push_back(t);
						total_pal++;
					}
				}
			}
		}


		

//REORDENO LA SOLUCION
		
	if(total_pal > 0)
	{
		dat_aux.num_pallets = total_pal;
		dat.fo_heu += dat_aux.fo_heu;
		
		struct timespec start, end;

		timespec_get(&start, TIME_UTC);

		
		
		if(heuristico2(dat_aux,pesosp,p_t)) 
		{
			dat.veces_bien++;
			//Para el cami�n k tengo una solucion con estabilidad y multidrop
			//me la guardo
			
			timespec_get(&end, TIME_UTC);
			long seconds = end.tv_sec - start.tv_sec;
			long nanoseconds = end.tv_nsec - start.tv_nsec;
			dat.tiempo_heu += seconds + nanoseconds * 1e-9;
			 
			

			printf("TRUE-heu %f\n", dat.tiempo_heu);
			dat.sol_1cam.push_back(dat_aux);
		}
		else
		{
			
			
			timespec_get(&end, TIME_UTC);
			long seconds = end.tv_sec - start.tv_sec;
			long nanoseconds = end.tv_nsec - start.tv_nsec;
			dat.tiempo_heu += seconds + nanoseconds * 1e-9;
			printf("FALSE-heu %.3f\n", dat.tiempo_heu);

			dat.veces_modelo++;
			//LLAMAR AL MODELO de 1 CAMION
			LPCPLEX model2;

			enc = model2.modelo1cam(dat_aux, ruta2, pesosp,p_t);
			dat.tiempo_modelo += dat_aux.tiempo_modelo;
			if (enc)
			{
				dat.veces_modelo_opt++;
				printf("TRUE-modelo\n");
				//Me gurado la soluci�n
				dat.sol_1cam.push_back(dat_aux);
			}
			else
			{
				dat.veces_modelo_noopt++;
				printf("FALSE-modelo\n ");
				abort = true;

				//si no encuentre sol CORTE
				dat.veces_corte++;
				IloExpr v1(callEnv);

				int num_pal = 0;
				int num_cli = 0;
				for (int i = 1; i <= dat.maxcli; i++)
				{
					for (int t = 0; t < ((int)l_cli.at(i).pedidos.size()); t++)
					{
						for (int p = 0; p < dat.posiciones; p++)
						{
							if (getValue(z_kitp[k][i][t][p]) > 0.0001)
							{
								v1 += z_kitp[k][i][t][p];
								num_pal++;
							}
						}
					}
				}
				for (int i = 0; i <= dat.maxcli; i++)
				{
					for (int j = 0; j <= dat.maxcli; j++)
					{
						if (i == j) continue;
						if (getValue(x_kij[k][i][j]) > 0.0001)
						{
							v1 += x_kij[k][i][j];
							num_cli++;
						}
					}
				}

				v1 -= num_pal + num_cli - 1;


				//a�ado el corte
				restr.add(IloRange(callEnv, -IloInfinity, v1, 0));
				IloInt n = restr.getSize();
				add(restr[n - 1]).end();
				v1.end();

			}
		}
	}else
	{
		printf("Total_pal=%d\n",total_pal);
	}
 }//del for
	if(abort==false)
	{
		char nom_file[1000];

		dat.veces_encuentra_sol++;
		if(dat.fo_heu < dat.fo_mejor_sol_heu || dat.fo_mejor_sol_heu==0)
		{
			dat.fo_mejor_sol_heu=dat.fo_heu;
			vector<datos>::iterator itd;
			dat.mejor_sol_1cam.clear();
			int cont = 0;
			for (itd = dat.sol_1cam.begin(); itd != dat.sol_1cam.end(); itd++)
			{
				dat.mejor_sol_1cam.push_back((*itd));
				/*FILE* f = fopen("./sol_inter.txt","a+");
				fprintf(f, "F.O: %d\n", dat.fo_heu);
				fprintf(f, "ROUTING\n");
				
				for (int i = 0; i < (*itd).ruta.size(); i++)
				{
					if ((*itd).ruta.at(i) != -1)
							fprintf(f, "X%d;%d;%d;%d\n", cont,i, (*itd).ruta.at(i), (int)l_cli.at(i).distancias.at((*itd).ruta.at(i)));
					
				}
				fprintf(f, "PACKING\n");
				double pt = 0;
				double pe1 = 0;
				double pe2 = 0;
				double pp = 0;

				
				for (int j = 0; j < (*itd).f1_cli.size(); j++)
				{
					if ((*itd).f1_cli.at(j) > 0.1)
					{
						fprintf(f, "Z%d %d %d %d\n", cont, (int)((*itd).f1_cli.at(j)), (*itd).f1_t.at(j),j,(*itd).f1_peso.at(j));
					}
				}
				fprintf(f, "truck weigth: %.2f axle1: %.2f axle2: %.2f\n", (*itd).peso_ca, (*itd).p_eje1, (*itd).p_eje2);
				
				fclose(f);
				cont++;*/
			}
		}

			
	}
printf("Salgo lazy\n");
}

double LPCPLEX::m_split_and_delivery(vector<cliente> l_cli, camion &cam, int maxcam, int multi, int esta, char* nom, datos datcallback, int maxtime, int modelo, list<camion>& solm )
{
	//int maxcam=7;
	int maxclientes=(int)l_cli.size()-1;
	

	double value2;
	FILE *f;

	char nombre[22];
	
	bool es_lazy;

	model=IloModel(env);

	IloRangeArray restr(env);
	IloRangeArray restrlazy(env); //pool de restricciones lazy.

	IloNum max_time=maxtime; //tiempo del modelo
	
	//Creo las variables
	NumVarMatrix3F x_kij(env, maxcam); //si el enlace de i a j es usado por k
	NumVarMatrix4 z_kitp(env, maxcam); // si el pallet t del cliente i esta en la posici�n p del cami�n k
	NumVector y_k(env); // si el camion k es usado.
	NumVarMatrix3F f_kij (env, maxcam);

	//variables de estabilidad
	NumVarMatrix l_kp (env,maxcam);
	NumVarMatrix r_kp (env,maxcam);
	NumVarMatrix s_kp (env,maxcam);

	//iterador de los id pallet
	
	list<pallet>::iterator itp;


	/**********************************************************/
	/***************** INICIALIZO VARIABLES *******************/
	/**********************************************************/

	//inicializo la variable Y_k
	y_k=IloIntVarArray(env,maxcam,0,1);
	for(int k=0;k < maxcam; k++)
	{
		sprintf(nombre,"Y%d",k);
		y_k[k].setName(nombre);
	}
	

	//inicializo la variable X_kij
	for(int k=0; k < maxcam;k++)
	{
		x_kij[k]=NumVarMatrixF(env,maxclientes+1);
		for(int i=0; i <= maxclientes; i++)
		{
			x_kij[k][i]=IloNumVarArray(env,maxclientes+1,0,1,ILOBOOL);
			for(int j=0; j <= maxclientes; j++)
			{
				if (i==j) continue;
				sprintf(nombre,"X_%d_%d_%d",k,i,j);
				x_kij[k][i][j].setName(nombre);
			}
		}
	}

	
	//inicializo la variable Z_ktip
	for(int k=0; k < maxcam;k++)
	{
		z_kitp[k]=NumVarMatrix3(env,maxclientes+1);
		for(int i=1; i <= maxclientes; i++)
		{
			z_kitp[k][i]=NumVarMatrix(env,l_cli.at(i).pedidos.size());			

			for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
			{
				z_kitp[k][i][t]=IloIntVarArray(env,cam.posiciones,0,1);
				for(int p=0; p < cam.posiciones; p++)
				{
					sprintf(nombre,"Z_%d_%d_%d_%d",k,i,t,p);
					z_kitp[k][i][t][p].setName(nombre);
				}
			}
		}
	}

	//inicializo la variable F_kij
	for(int k=0; k < maxcam; k++)
	{
		f_kij[k]=NumVarMatrixF(env,maxclientes+1);
		for(int i=0; i <= maxclientes; i++)
		{
			f_kij[k][i]=IloNumVarArray(env,maxclientes+1,0,maxclientes,ILOFLOAT);
			for (int j=0; j <= maxclientes; j++)
			{
				if (i==j) continue;
				sprintf(nombre,"F_%d_%d_%d",k,i,j);
				f_kij[k][i][j].setName(nombre);
			}
		}
	}


	/********** VARIABLES ESTABILIDAD ***************/
	//inicializo l_kp
	for(int k=0; k < maxcam; k++)
	{
		l_kp[k]=IloIntVarArray(env,cam.posiciones,0,1);
		for(int p=0; p < cam.posiciones; p++)
		{
			sprintf(nombre,"L_%d_%d",k,p);
			l_kp[k][p].setName(nombre);
		}
	}

	//inicializo b_kp
	for(int k=0; k < maxcam; k++)
	{
		r_kp[k]=IloIntVarArray(env,cam.posiciones,0,1);
		for(int p=0; p < cam.posiciones; p++)
		{
			sprintf(nombre,"R_%d_%d",k,p);
			r_kp[k][p].setName(nombre);
		}
	}
	//inicializo s_kp
	for(int k=0; k < maxcam; k++)
	{
		s_kp[k]=IloIntVarArray(env,cam.posiciones,0,1);
		for(int p=0; p < cam.posiciones; p++)
		{
			sprintf(nombre,"S_%d_%d",k,p);
			s_kp[k][p].setName(nombre);
		}
	}

/**********************************************************/
/***************** RESTRICCIONES **************************/
/**********************************************************/
	if (modelo == 1) es_lazy = true;
	else es_lazy = false;

	add_restricciones(restr,restrlazy, maxcam,maxclientes,cam, l_cli, x_kij, z_kitp, y_k,f_kij, multi, es_lazy);
	if(esta==1) add_rest_estabilidad(restr,restrlazy, maxcam,maxclientes,cam, l_cli, z_kitp,l_kp, r_kp, s_kp, es_lazy);
	/*
	if(modelo==3) 
		add_sol_cw(restr,maxcam, maxclientes, cam, l_cli, datcallback.solcw, x_kij, z_kitp, y_k, f_kij, l_kp, r_kp, s_kp, cplex);*/

	

//A�ADIMOS LAS RESTRICCIONES AL MODELO 
	model.add(restr);	
	


/**********************************************************/
/***************FUNCION OBJETIVO **************************/
/**********************************************************/
	
	IloExpr Expression_fobj(env); 
	
	for (int k=0;k<maxcam;k++)
	{
		for(int i=0;i<=maxclientes; i++)
		{
			for(int j=0; j<= maxclientes; j++)
			{
				if (i==j) continue;
				double cij=l_cli.at(i).distancias.at(j);
				Expression_fobj+=cij*x_kij[k][i][j];
			}
		}
	}
	
	model.add(IloMinimize(env,Expression_fobj));
	Expression_fobj.end();	


/***************************************************************/
/*************** EJECUTAMOS EL MODELO **************************/
/***************************************************************/
	try{

		IloCplex cplex(model);
		
	
		//Para a�adir las lazy
		if (modelo == 1) 
			cplex.addLazyConstraints(restrlazy);

		//No hace el probing
		cplex.setParam(IloCplex::Probe,-1);
		//1 CORE
		cplex.setParam(IloCplex::Threads, 8);
		//para que utilice memoria de disco para trasferir nodos de la mem principal a disco.
		cplex.setParam(IloCplex:: NodeFileInd,3);
		//limite de tiempo
		cplex.setParam(IloCplex::TiLim,max_time); //TIEMPO EN SEGUNDOS

		
		
		//Las LAZYCALLBACK
		if (modelo == 2) cplex.use(MIFUNsd(env,datcallback,l_cli,x_kij, z_kitp, y_k, restr));
		if (modelo == 3)
			add_sol_cw(restr,maxcam, maxclientes, cam, l_cli, datcallback.solcw, x_kij, z_kitp, y_k, f_kij, l_kp, r_kp, s_kp, cplex);
		if(modelo==4)
		{
			cplex.use(MIFUNsd(env, datcallback, l_cli, x_kij, z_kitp, y_k, restr));
			add_sol_cw(restr, maxcam, maxclientes, cam, l_cli, datcallback.solcw, x_kij, z_kitp, y_k, f_kij, l_kp, r_kp, s_kp, cplex);
		}
		//cplex.exportModel("maite-call.lp");
		
		//CPXsetintparam(env,CPX_PARAM_THREADS,1);
	
		//numero de filas y columnas del modelo
		int nr=cplex.getNrows();
		int nc=cplex.getNcols();

		//resolver el modelo y tomamos tiempos
		IloNum inicio,fin;
		clock_t t_ini, t_fin;

		inicio=cplex.getCplexTime();
		t_ini = clock();


		IloBool result=cplex.solve();

		t_fin = clock();
		fin=cplex.getCplexTime();
		
		//double secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
		double secs = (double)(fin - inicio);
		printf("%2f \n",secs);

		//Estado del algoritmo	al terminar
		IloCplex::Status estado=cplex.getCplexStatus();	
		IloCplex::Status sub_estado=cplex.getCplexSubStatus();

		
		if(result==false)
		{
				char nom_file[1000];
				char nom_model[1000];
				strcpy(nom_file,".\\Results\\M_");
				char aux1[10];
				char aux2[10];
				sprintf(aux1, "%d", datcallback.maxcli);		
				strcpy(aux2,"Inst");
				strcat(aux2,aux1);
				char * pch=strstr(datcallback.nominsta,aux2);
				strncat(nom_file,pch,strlen(pch)-3);	
				strcpy(nom_model, nom_file);
				strcat(nom_file,"txt");
				strcat(nom_model, "lp");

			FILE *f=fopen(nom_file,"a+");
			fprintf(f,"%s ",nom);
			IloAlgorithm::Status est=cplex.getStatus();
			if (cplex.getStatus() == IloAlgorithm::Infeasible)
			{
				fprintf(f, "Infeaseble\n");
				cplex.exportModel(nom_model);
			}
			else
			{
				if ( cplex.getStatus() == IloAlgorithm::Unbounded )fprintf(f,"Unbounded\n");
				fprintf(f,"nosol;%d; ; ;%.2f; ; ; ;%d;%d\n",cplex.getNnodes(),secs,nr,nc);
			}
			fclose(f);
		}
		else
		{
			//Si ha encontrado alguna solucion posible
			if (cplex.getSolnPoolNsolns()>0)
			{
				value2=cplex.getObjValue();
				double bestval = cplex.getBestObjValue();
				//|bestnode-bestinteger|/(1e-10+|bestinteger|)
				double relativeGap = fabs(value2-bestval)/(fabs(value2)+1e-10);
				
				escribe_csv(value2,relativeGap,cplex,secs,maxcam, maxclientes, cam,l_cli,x_kij, z_kitp, y_k,f_kij,nom,datcallback, solm);	

				//if(relativeGap ==0 && modelo !=1)
				//	sol_to_draw(cplex,maxcam, maxclientes, cam,l_cli,x_kij, z_kitp, y_k,s_kp,nom,datcallback, solm, esta, multi);

				
				
				
			}
		}

		cplex.end();
		model.end();
		env.end();
		return value2;
	}
	catch (IloException& e) 
	{
		printf("EXCEPCION:%s\n",e);
	    std::cerr << "IloException: " << e << "\n Status" << e ;
		return -1;
	}


	return 0;
}



/**********************************************************/
/***************** RESTRICCIONES **************************/
/**********************************************************/

void LPCPLEX::add_rest_estabilidad(IloRangeArray &restr, IloRangeArray &restrlazy,int maxcam,int maxclientes,camion cam, vector<cliente> l_cli, NumVarMatrix4& z_kitp, NumVarMatrix& l_kp,NumVarMatrix& r_kp,NumVarMatrix& s_kp, bool es_lazy)
{
	/*
	*   ECUACION 1.19
	*/
	for(int k=0; k < maxcam; k++)
	{
		for(int p=1; p < cam.posiciones; p++)
		{
			if(p==(cam.posiciones/2)) continue;
			else
			{
				IloExpr v1(env);
				v1+=l_kp[k][p];
				for(int i=1; i <= maxclientes; i++)
				{
					for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
					{			
						v1+=z_kitp[k][i][t][p-1]-z_kitp[k][i][t][p];
					}		
				}
				 restr.add(IloRange(env,0,v1,IloInfinity));
				v1.end();
			}
		}
	}


	/*
	* ECUACI�N 1.20
	*/
	int pos=(cam.posiciones/2);
	for(int k=0; k < maxcam; k++)
	{
		IloExpr v1(env);
		v1=l_kp[k][0];
		IloExpr v2(env);
		v2=l_kp[k][pos];
		for(int i=1; i <= maxclientes; i++)
		{
			for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
			{
				//para posicion 0		
				v1-=z_kitp[k][i][t][0];
				
				//para posicion p/2 la 15		
				v2-=z_kitp[k][i][t][pos];				
			}					
		}
		 restr.add(IloRange(env, 0, v1, IloInfinity));
		v1.end();
		 restr.add(IloRange(env,0,v2,IloInfinity));
		v2.end();
	}

	/*
	*   ECUACION 1.21
	*/
	for(int k=0; k < maxcam; k++)
	{
		for(int p=0; p < cam.posiciones-1; p++)
		{
			if(p==(cam.posiciones/2)-1) continue;
			IloExpr v1(env);
			v1=r_kp[k][p];
			for(int i=1; i <= maxclientes; i++)
			{
				for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
				{				
					v1+=z_kitp[k][i][t][p+1]-z_kitp[k][i][t][p+1];
				}					
			}
			 restr.add(IloRange(env,0,v1,IloInfinity));
			v1.end();
		}
	}
	
	/*
	* ECUACI�N 1.22
	*/
	pos=(cam.posiciones/2)-1;
	for(int k=0; k < maxcam; k++)
	{
		IloExpr v1(env);
		v1=r_kp[k][pos];
		IloExpr v2(env);
		v2=r_kp[k][cam.posiciones-1];

		for(int i=1; i <= maxclientes; i++)
		{
			for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
			{
				//para posicion p/2-1				
				v1-=z_kitp[k][i][t][pos];
				
				//para posicion p, ultima									
				v2-=z_kitp[k][i][t][cam.posiciones-1];
			}					
		}
		 restr.add(IloRange(env,0,v1,IloInfinity));
		v1.end();
		 restr.add(IloRange(env,0,v2,IloInfinity));
		v2.end();
	}


	/*
	* ECUACI�N 1.23
	*/
	for(int k=0; k < maxcam; k++)
	{
		for(int p=0; p < (cam.posiciones/2)-1; p++)
		{
			IloExpr v1(env);
			v1=s_kp[k][p];
			for(int i=1; i <= maxclientes; i++)
			{
				for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
				{				
					v1+=z_kitp[k][i][t][p+(cam.posiciones/2)]-z_kitp[k][i][t][p];
				}
			}
			 restr.add(IloRange(env,0,v1,IloInfinity));
			v1.end();
		}
	}

	/*
	* ECUACI�N 1.24
	*/
	for(int k=0; k < maxcam; k++)
	{
		for(int p=0; p < (cam.posiciones/2); p++)
		{
			IloExpr v1(env);
			v1=s_kp[k][p];
			for(int i=1; i <= maxclientes; i++)
			{
				for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
				{			
					v1+=z_kitp[k][i][t][p]-z_kitp[k][i][t][p+(cam.posiciones/2)];
				}
			}
			restr.add(IloRange(env,0,v1,IloInfinity));
			v1.end();
		}
	}

	/*
	* ECUACI�N 1.25
	*/
	for(int k=0; k < maxcam; k++)
	{
		IloExpr v1(env);
		for(int p=0; p < (cam.posiciones/2); p++)
		{
			v1+=l_kp[k][p];
		}
		if(es_lazy)restrlazy.add(IloRange(env, -IloInfinity, v1, 1));
		else restr.add(IloRange(env,-IloInfinity,v1,1));
		v1.end();
	}

	/*
	* ECUACI�N 1.26
	*/
	for(int k=0; k < maxcam; k++)
	{
		IloExpr v1(env);
		for(int p=(cam.posiciones/2); p < cam.posiciones; p++)
		{
			v1+=l_kp[k][p];
		}
		if(es_lazy)restrlazy.add(IloRange(env, -IloInfinity, v1, 1));
		else restr.add(IloRange(env,-IloInfinity,v1,1));
		v1.end();
	}

	/*
	* ECUACI�N 1.27
	*/
	for(int k=0; k < maxcam; k++)
	{
		IloExpr v1(env);
		for(int p=0; p < (cam.posiciones/2); p++)
		{
			v1+=r_kp[k][p];
		}
		if(es_lazy)restrlazy.add(IloRange(env, -IloInfinity, v1, 1));
			else restr.add(IloRange(env,-IloInfinity,v1,1));
		v1.end();
	}

	/*
	* ECUACI�N 1.28
	*/
	for(int k=0; k < maxcam; k++)
	{
		IloExpr v1(env);
		for(int p=(cam.posiciones/2); p < cam.posiciones; p++)
		{
			v1+=r_kp[k][p];
		}
		if(es_lazy)restrlazy.add(IloRange(env, -IloInfinity, v1, 1));
		else restr.add(IloRange(env,-IloInfinity,v1,1));
		v1.end();
	}

	/*
	* ECUACI�N 1.29
	*/
	for(int k=0; k < maxcam; k++)
	{
		IloExpr v1(env);
		for(int p=0; p < (cam.posiciones/2); p++)
		{
			v1+=s_kp[k][p];
		}
		if(es_lazy) restrlazy.add(IloRange(env,-IloInfinity,v1,1));
		else restr.add(IloRange(env, -IloInfinity, v1, 1));
		v1.end();
	}

}


void LPCPLEX::add_restricciones(IloRangeArray &restr, IloRangeArray &restrlazy, int maxcam, int maxclientes, camion cam, vector<cliente> l_cli, NumVarMatrix3F &x_kij, NumVarMatrix4 & z_kitp, NumVector & y_k,NumVarMatrix3F &f_kij, int multi, bool es_lazy)
{
	
	list<pallet>::iterator itp;
	double l_posicion=cam.ancho_pallet;
	double medio_p;
	double Gx=cam.largo/2;
	double Gy=cam.ancho/2;
	double tau1x=cam.pos_eje2-(cam.largo/2);
	double tau2x=(cam.largo/2)-2;
	double tau1y=cam.ancho/8;
	double tau2y=cam.ancho/8;
	double posydetras=(cam.ancho)/4;
	double posydelante=(cam.ancho*3)/4;
	
	
	
	/***************************************
	* ROUTING
	****************************************/

	/* ECUACION 2 CAMIONES SALEN DEL DEPOSITO
		*  Los camiones que salen del deposito son yk
		*  sum_i Xk0i - y_k = 0  para todo k en K, i en V
		*/
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v1(env);
		for (int i = 1; i <= maxclientes; i++)
		{
			v1 += x_kij[k][0][i];
		}
		v1 -= y_k[k];
		restr.add(IloRange(env, 0, v1, 0));
		v1.end();
	}


	/* ECUACION 3 si K llega a j debe dejarlo
	*  Los camiones que llegan a j son los mismos que salen de j.
	*  sum_j Xkij + sum_j Xkji <= 0  para todo k en K, i en V'
	*/
	for (int k = 0; k < maxcam; k++)
	{
		for (int i = 1; i <= maxclientes; i++)
		{
			IloExpr v1(env);
			IloExpr v2(env);
			for (int j = 0; j <= maxclientes; j++)
			{
				if (i == j) continue;
				v1 += x_kij[k][i][j] - x_kij[k][j][i];
			}
			restr.add(IloRange(env, 0, v1, 0));
			v1.end();
			v2.end();
		}
	}

	
	
	/* ECUACION 4 Si una cami�n va de i a j, el camion Yk esta en la sol.
	*  Relacion Y con X
	*  Yk >= Xkij  para todo i,j en V y para todo k en K
	*/
	for(int k=0; k < maxcam; k++)
	{
		for(int i=0; i<=maxclientes; i++)
		{
			for(int j=0; j<=maxclientes; j++)
			{
				if (i==j) continue;
				IloExpr v(env); 
				v+=y_k[k]- x_kij[k][i][j];
				restr.add(IloRange(env,0,v,IloInfinity));
				v.end();
			}
		}
	}

	/*
	* En cada ruta solo se puede visitar cada cliente una vez.
	*/

	for (int k = 0; k < maxcam; k++)
	{
		for (int i = 1; i <= maxclientes; i++)
		{
			IloExpr v1(env);
			for (int j = 0; j <= maxclientes; j++)
			{
				v1 += x_kij[k][i][j];
			}
			restr.add(IloRange(env, -IloInfinity, v1, 1));
			v1.end();
		}
	}

	/************************************************************************/
	/*                          SUBTOUR ELIMINATION                         */
	/************************************************************************/
	/* ECUACION 5
	*	subtour 1
	*/

	for (int k = 0; k < maxcam; k++)
	{
		for (int i = 1; i <= maxclientes; i++)
		{
			IloExpr v1(env);
			v1 += f_kij[k][i][0];
			restr.add(IloRange(env, 0, v1, 0));
			v1.end();
		}
	}


	/* ECUACI�N 6
	*/
	for (int k = 0; k < maxcam; k++)
	{
		for (int i = 1; i <= maxclientes; i++)
		{
			IloExpr v1(env);
			v1 += f_kij[k][0][i];
			restr.add(IloRange(env, -IloInfinity, v1, maxclientes));
			v1.end();
		}
	}
	/* ECUACION 7
	*	subtour 3
	*/

	for (int k = 0; k < maxcam; k++)
	{
		for (int i = 1; i <= maxclientes; i++)
		{
			IloExpr v1(env);
			IloExpr v2(env);
			IloExpr v3(env);
			for (int j = 0; j <= maxclientes; j++)
			{
				if (j == i) continue;
				v1 += f_kij[k][j][i];
				v2 += f_kij[k][i][j];
				v3 += x_kij[k][i][j];
			}
			
			v1 = v1 - v2 - v3;
			restr.add(IloRange(env, 0, v1, 0));
			v1.end();
			v2.end();
			v3.end();
		}
	}


	/* ECUACION 8
	*	subtour 4
	*/

	for (int k = 0; k < maxcam; k++)
	{
		for (int i = 0; i <= maxclientes; i++)
		{
			for (int j = 0; j <= maxclientes; j++)
			{
				if (j == i) continue;
				IloExpr v1(env);
				v1 += (maxclientes*x_kij[k][i][j]) - f_kij[k][i][j];
				restr.add(IloRange(env, 0, v1, IloInfinity));
				v1.end();
			}
		}
	}


/*********************************
* PACKING
**********************************/

	/* ECUACION 9
	*  En cada posii�n debe haber como mucho un pallet
	*  sum_j sum_t Zkjtp <= 1  para todo k en K y para todo p en P
	*/
	for (int k = 0; k < maxcam; k++)
	{
		for (int pp = 0; pp < cam.posiciones; pp++)
		{
			IloExpr v(env);
			for (int j = 1; j <= maxclientes; j++)
			{
				for (int t = 0; t < ((int)l_cli.at(j).pedidos.size()); t++)
				{
					v += z_kitp[k][j][t][pp];
				}
			}
			restr.add(IloRange(env, -IloInfinity, v, 1));
			v.end();
		}
	}

	/*
	*  Para que ponga un pallet de cada tipo de cada cliente. 
	* si no la pones, el mismo pallet varias veces.
	*/
	for(int i=1; i <= maxclientes; i++)
	{
		for(int t=0; t <((int)l_cli.at(i).pedidos.size()); t++)
		{
			IloExpr v(env);
			for(int k=0; k < maxcam; k++)
			{
				for(int p=0; p < cam.posiciones; p++)
				{
					v+=z_kitp[k][i][t][p];
				}
			}
			restr.add(IloRange(env,1,v,1));
			v.end();
		}
	}



	/* ECUACION 10 DEMANDA
	*  la suma de los pallets debe ser la demandada por el cliente.
	*  sum_k sum_p Zkitp >= 1  para todo i en V' y para todo t en Di
	*/

	for (int i = 1; i <= maxclientes; i++)
	{
		IloExpr v(env);
		for (int t = 0; t < ((int)l_cli.at(i).pedidos.size()); t++)
		{
			for (int k = 0; k < maxcam; k++)
			{
				for (int p = 0; p < cam.posiciones; p++)
				{
					v += z_kitp[k][i][t][p];
				}
			}
		}
		int di = (int)l_cli.at(i).pedidos.size();
		restr.add(IloRange(env, di, v, di));
		v.end();
	}

	/* ECUACION 11 PESO M�XIMO
	*  la suma de todos los pallets de un cami�n debe ser menor que el peso m�ximo Q.
	*  sum_i sum_p sum_t Zkitp * qt <= Q  para todo k en K
	*/

	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v(env);
		for (int i = 1; i <= maxclientes; i++)
		{
			for (int p = 0; p < cam.posiciones; p++)
			{
				for (int t = 0; t < ((int)l_cli.at(i).pedidos.size()); t++)
				{
					itp = l_cli.at(i).pedidos.begin();
					while ((*itp).id != t) itp++;
					v += z_kitp[k][i][t][p] * (*itp).peso;
				}
			}
		}
		v -= cam.max_total_peso;
		restr.add(IloRange(env, -IloInfinity, v, 0));
		v.end();
	}



	/* ECUACION 12 PESO EJE 1
	*  la suma depeso de todos los pallets de un cami�n debe ser menor que el peso m�ximo que puede soportar el eje 1, Q1.
	*  sum_i sum_t sum_p sum_t (Zkitp * qt)(delta2-Opx) <= Q1(delta2-delta1)  para todo k en K
	*/
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v(env);
		for (int i = 1; i <= maxclientes; i++)
		{
			for (int t = 0; t < ((int)l_cli.at(i).pedidos.size()); t++)
			{
				itp = l_cli.at(i).pedidos.begin();
				while ((*itp).id != t) itp++;
				for (int p = 0; p < cam.posiciones; p++)
				{
					medio_p = (p % (cam.posiciones / 2))*l_posicion + (l_posicion / 2);
					v += (z_kitp[k][i][t][p] * (*itp).peso)*(cam.pos_eje2 - medio_p);
				}
				itp++;
			}
		}
		v -= cam.max_peso_eje1*(cam.pos_eje2 - cam.pos_eje1);
		restr.add(IloRange(env, -IloInfinity, v, 0));
		v.end();
	}

	/* ECUACION 13 PESO EJE 2
	*  la suma depeso de todos los pallets de un cami�n debe ser menor que el peso m�ximo que puede soportar el eje 2, Q2.
	*  sum_i sum_t sum_p sum_t (Zkitp * qt)(Opx-delta1) <= Q2(delta2-delta1)  para todo k en K
	*/
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v(env);
		for (int i = 1; i <= maxclientes; i++)
		{
			for (int t = 0; t < ((int)l_cli.at(i).pedidos.size()); t++)
			{
				itp = l_cli.at(i).pedidos.begin();
				while ((*itp).id != t) itp++;
				for (int p = 0; p < cam.posiciones; p++)
				{
					medio_p = (p % (cam.posiciones / 2))*l_posicion + (l_posicion / 2);
					v += (z_kitp[k][i][t][p] * (*itp).peso)*(medio_p - cam.pos_eje1);
				}
				itp++;
			}
		}
		v -= cam.max_peso_eje2*(cam.pos_eje2 - cam.pos_eje1);
		restr.add(IloRange(env, -IloInfinity, v, 0));
		v.end();
	}

	/* ECUACION 14 CENTRO DE GRAVEDAD TAU1X
	*  El centro de gravedad de cad cami�n no debe exceder el segundo eje: Gx+ tau1x
	*  (Qe*Gx)+ sum_j sum_p sum_t (Opx * Zkitp * qt) <= (sum_j sum_p sum_t z_kjtp +Qe)(Gx + tau1x)  para todo k en K
	*/
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v1(env);
		IloExpr v2(env);
		for (int j = 1; j <= maxclientes; j++)
		{
			for (int p = 0; p < cam.posiciones; p++)
			{
				medio_p = (p % (cam.posiciones / 2))*l_posicion + (l_posicion / 2);
				for (int t = 0; t < ((int)l_cli.at(j).pedidos.size()); t++)
				{
					itp = l_cli.at(j).pedidos.begin();
					while ((*itp).id != t) itp++;
					v1 += z_kitp[k][j][t][p] * (*itp).peso*medio_p;
					v2 += z_kitp[k][j][t][p] * (*itp).peso;
					itp++;
				}
			}
		}
		v2 += cam.peso_vacio;
		v1 = ((cam.peso_vacio*Gx) + v1) - (v2*(Gx + tau1x));
		restr.add(IloRange(env, -IloInfinity, v1, 0));
		v1.end();
		v2.end();
	}
	/* ECUACION 15 CENTRO DE GRAVEDAD TAU2X
	*  El centro de gravedad de cad cami�n no debe exceder el segundo eje: Gx+ tau1x
	*  (Qe*Gx)+ sum_j sum_p sum_t (Opx * Zkitp * qt) <= (sum_j sum_p sum_t z_kjtp +Qe)(Gx - tau2x)  para todo k en K
	*/
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v1(env);
		IloExpr v2(env);
		for (int j = 1; j <= maxclientes; j++)
		{
			for (int p = 0; p < cam.posiciones; p++)
			{
				medio_p = (p % (cam.posiciones / 2))*l_posicion + (l_posicion / 2);
				for (int t = 0; t < ((int)l_cli.at(j).pedidos.size()); t++)
				{
					itp = l_cli.at(j).pedidos.begin();
					while ((*itp).id != t) itp++;
					v1 += z_kitp[k][j][t][p] * (*itp).peso*medio_p;
					v2 += z_kitp[k][j][t][p] * (*itp).peso;
					itp++;
				}
			}
		}
		v2 += cam.peso_vacio;
		v1 = ((cam.peso_vacio*Gx) + v1) - (v2*(Gx - tau2x));
		restr.add(IloRange(env, 0, v1, IloInfinity));
		v1.end();
		v2.end();
	}

	/* ECUACION 16 CENTRO DE GRAVEDAD TAU1Y
	*  El centro de gravedad de cad cami�n no debe exceder el segundo eje: Gx+ tau1x
	*  (Qe*Gy)+ sum_j sum_p sum_t (Opx * Zkitp * qt) <= (sum_j sum_p sum_t z_kjtp +Qe)(Gy + tau1y)  para todo k en K
	*/
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v1(env);
		IloExpr v2(env);
		for (int j = 1; j <= maxclientes; j++)
		{
			for (int p = 0; p < cam.posiciones; p++)
			{
				if (p < (cam.posiciones / 2))medio_p = posydetras;
				else medio_p = posydelante;
				for (int t = 0; t < ((int)l_cli.at(j).pedidos.size()); t++)
				{
					itp = l_cli.at(j).pedidos.begin();
					while ((*itp).id != t) itp++;
					v1 += z_kitp[k][j][t][p] * (*itp).peso*medio_p;
					v2 += z_kitp[k][j][t][p] * (*itp).peso;
					itp++;
				}
			}
		}
		v2 += cam.peso_vacio;
		v1 = ((cam.peso_vacio*Gy) + v1) - (v2*(Gy + tau1y));
		restr.add(IloRange(env, -IloInfinity, v1, 0));
		v1.end();
		v2.end();
	}

	/* ECUACION 17 CENTRO DE GRAVEDAD TAU2Y
	*  El centro de gravedad de cad cami�n no debe exceder el segundo eje: Gx+ tau1x
	*  (Qe*Gy)+ sum_j sum_p sum_t (Opx * Zkitp * qt) <= (sum_j sum_p sum_t z_kjtp +Qe)(Gy - tau2y)  para todo k en K
	*/
	for (int k = 0; k < maxcam; k++)
	{
		IloExpr v1(env);
		IloExpr v2(env);
		for (int j = 1; j <= maxclientes; j++)
		{
			for (int p = 0; p < cam.posiciones; p++)
			{
				if (p < (cam.posiciones / 2))medio_p = posydetras;
				else medio_p = posydelante;
				for (int t = 0; t < ((int)l_cli.at(j).pedidos.size()); t++)
				{
					itp = l_cli.at(j).pedidos.begin();
					while ((*itp).id != t) itp++;
					v1 += z_kitp[k][j][t][p] * (*itp).peso*medio_p;
					v2 += z_kitp[k][j][t][p] * (*itp).peso;
					itp++;
				}
			}
		}
		v2 += cam.peso_vacio;
		v1 = ((cam.peso_vacio*Gy) + v1) - (v2*(Gy - tau2y));
		restr.add(IloRange(env, 0, v1, IloInfinity));
		v1.end();
		v2.end();
	}





	/* ECUACION 33
	*  Relacion Z con X
	*  sum_p Zkjtp <= sum_i Xkij  para todo j en V' y para todo t en Di
	*/
	for(int k=0; k < maxcam; k++)
	{
		for(int j=1; j<=maxclientes; j++)
		{
			IloExpr v(env);
			for(int t=0; t <((int)l_cli.at(j).pedidos.size());t++)
			{
				for(int p=0; p < cam.posiciones; p++)
				{
					v+=z_kitp[k][j][t][p];
				}
			}
			int dj=(int)l_cli.at(j).pedidos.size();
			for(int i=0; i <= maxclientes; i++)
			{
					if (i==j) continue;
					v-=dj*x_kij[k][i][j];
			}
			restr.add(IloRange(env,-IloInfinity,v,0));
			v.end();
		}
	}

	/* ECUACION 32
	*  Relacion Z con X
	*  sum_tj sum_p Zkjtp <= sum_i Xkij  para todo j en V' y para todo k en K
	*/
	for(int k=0; k < maxcam; k++)
	{
		for(int j=1; j<=maxclientes; j++)
		{
			IloExpr v(env);
			for(int t=0; t <((int)l_cli.at(j).pedidos.size());t++)
			{
				for(int p=0; p < cam.posiciones; p++)
				{
					v+=z_kitp[k][j][t][p];
				}
			}
			for(int i=0; i <= maxclientes; i++)
			{
				if (i==j) continue;
				v-=x_kij[k][i][j];
			}
			restr.add(IloRange(env,0,v,IloInfinity));
			v.end();
		}
	}


	

	/* ECUACION 34 MULTIDROP
	*  Si pones los pallets con destino i, no puedes poner pallets de destinos sucesivos en posiciones sucesivas.
	*  sum_t Zkitp + sum_t Zkjtp' <= 1  para todo k en K, i,j en V' ; p, p' en P tal que p' > p 
	*/

if(multi==1)
{
	for(int k=0; k < maxcam; k++)
	{
		for(int i=1; i <=maxclientes; i++)
		{
			for(int j=1; j <=maxclientes; j++)
			{
				if (i==j) continue;
				for(int p=0; p < cam.posiciones; p++)
				{
					IloExpr v1(env);
					for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
					{
						v1+=z_kitp[k][i][t][p];
					}
					if(p <(cam.posiciones/2))
					{
						for(int pp=p+1; pp < cam.posiciones;pp++)
						{
							if((pp >=(cam.posiciones/2)) &&(pp <= p+(cam.posiciones/2))) continue;
							
							IloExpr v2(env);
							for(int t=0; t < ((int)l_cli.at(j).pedidos.size()); t++)
							{
								v2+=z_kitp[k][j][t][pp];
							}
							v2=v1+v2+x_kij[k][i][j];
							if(es_lazy)restrlazy.add(IloRange(env,-IloInfinity,v2,2));
							else restr.add(IloRange(env, -IloInfinity, v2, 2));
							v2.end();
						}
					}
					else
					{
						for(int pp=p-(cam.posiciones/2); pp < cam.posiciones;pp++)
						{
							if((pp <= (p-(cam.posiciones/2))) || (pp >=15 && pp<=p)) continue;
							
							IloExpr v2(env);
							for(int t=0; t < ((int)l_cli.at(j).pedidos.size()); t++)
							{
								v2+=z_kitp[k][j][t][pp];
							}
							v2=v1+v2+x_kij[k][i][j];
							if (es_lazy) restrlazy.add(IloRange(env,-IloInfinity,v2,2));
							else restr.add(IloRange(env, -IloInfinity, v2, 2));
							v2.end();
						}
					}
					v1.end();
				}
			}
		}
	}
}

	

	
	

	/* ECUACION 31 SIMETRIAS
	*  Para evitar simetrias.
	*  sum_i Xk0i - y_k = 0  para todo k en K, i en V
	*/

	/*for(int k=0; k < maxcam; k++)
	{*/
		for(int i=1; i <= maxclientes; i++)
		{
			if(i <= maxcam)
			{
				IloExpr v1(env);
				for(int kp=i-1; kp >= 0; kp--)
				{
					for(int p=0; p < cam.posiciones; p++)
					{
						v1+=z_kitp[kp][i][0][p];
					}
				}
				restr.add(IloRange(env,1,v1,1));
				v1.end();
			}
		}
	//}*/



		
}


void LPCPLEX::escribe_csv(double VFO,double gap, IloCplex cplex,double secs,int maxcam, int maxclientes, camion cam,vector<cliente> l_cli, NumVarMatrix3F x_kij, NumVarMatrix4  z_kitp, NumVector  y_k,NumVarMatrix3F &f_kij, char* nom, datos datcallback, list<camion>& solm )
{
	int sol_camion[10][32];
	int rutas[10][32];
	int contc=0;
	list<pallet>::iterator itp;
	int conttruck=0;

	for (int k = 0; k < maxcam; k++)
	{
		for (int i = 0; i < cam.posiciones; i++)
		{
			sol_camion[k][i] = 0;
			rutas[k][i] = 0;
		}
	}


	/*********************** DATOS DE LA SOLUCION DEL MODELO *******************/
	char nom_file[1000];
	char nom_model[1000];
	strcpy(nom_file,".\\Results\\M_");
	char aux1[10];
	char aux2[10];
	sprintf(aux1, "%d", datcallback.maxcli);		
	strcpy(aux2,"Inst");
	strcat(aux2,aux1);
	char * pch=strstr(datcallback.nominsta,aux2);
	strncat(nom_file,pch,strlen(pch)-3);
	strcpy(nom_model, nom_file);
	strcat(nom_file,"txt");
	strcat(nom_model, "lp");

	//cplex.exportModel(nom_model);

/*********************************************************************************/


	//
	char name[10000];
	strcpy(name,nom_file);
	//strcat(name,nom);
	strcat(name,"_sol.txt");
	
	FILE *f2=fopen(name,"a+");
	fprintf(f2,"GAP:; %.2f;  F.O:; %.2f; Tiempo:;%.2f; \n",gap, VFO, secs);
	

	//ruta
	fprintf(f2,"ROUTING \n");
	bool compartido = false;
	int contcomp = 0, contsolo=0;
	float total_ru = 0;
	if (datcallback.modelo != 2 && datcallback.modelo != 4)
	{
		for (int k = 0; k < maxcam; k++)
		{
			if (cplex.isExtracted(y_k[k]) && cplex.getValue(y_k[k]) > 0.0001)
			{
				total_ru = 0;
				contc++;
				compartido = false;
				for (int i = 0; i <= maxclientes; i++)
				{
					for (int j = 0; j <= maxclientes; j++)
					{
						if (i == j) continue;
						if (cplex.isExtracted(x_kij[k][i][j]) && cplex.getValue(x_kij[k][i][j]) > 0.0001)
						{
							fprintf(f2, "X%d;%d;%d;%.2f \n", k, i, j, l_cli.at(i).distancias.at(j));
							rutas[k][i] = j;
							if (i != 0 && j != 0) compartido = true;
							total_ru += l_cli.at(i).distancias.at(j);
							//fprintf(f3,"%lf + ",l_cli.at(i).distancias.at(j));
						}
					}
				}
				if (compartido == true)contcomp++;
				else contsolo++;
				fprintf(f2, "ruta %d;%.2f \n", k, total_ru);
			}
		}
	}
	else
	{
		vector<datos>::iterator itd;
		int k = -1;
		float total_ru = 0;
		contc = (int)datcallback.mejor_sol_1cam.size();
		for (itd = datcallback.mejor_sol_1cam.begin(); itd != datcallback.mejor_sol_1cam.end(); itd++)
		{
			if ((*itd).compartido == true)contcomp++;
			else contsolo++;
			k++;
			int i = 0;
			int j=-1;
			while (j != 0)
			{
				j = (*itd).ruta.at(i);
				total_ru += l_cli.at(i).distancias.at(j);
				fprintf(f2, "X%d;%d;%d;%.2f \n", k, i, j, l_cli.at(i).distancias.at(j));
				i = j;
			}
			fprintf(f2, "ruta %d;%.2f \n", k, total_ru);
		}
	}
	
	FILE *f = fopen(nom_file, "a+");
	fprintf(f, "%s;", nom);
	double cinf = cplex.getObjValue();
	double csup = cplex.getBestObjValue();
	int nc = cplex.getNcols();
	int nr = cplex.getNrows();
	if(datcallback.veces_entra==0) fprintf(f, "%.2f;%.2f;%.2f;%.2f; %.2f; %d; %d; %d; %d; %d\n", gap, VFO, cinf,csup,secs, contc, contsolo, contcomp,nr,nc);
	else fprintf(f, "%.2f; %.2f; %.2f; %.2f; %.2f; %d; %d; %d; %d; %d; %d; %.2f; %d; %d; %d; %d; %d; %d; %d; %d; %f; %f\n", gap, VFO, cinf, csup, secs, contc, contsolo, contcomp, datcallback.veces_entra, datcallback.veces_entra_k, datcallback.veces_bien, datcallback.fo_mejor_sol_heu,datcallback.veces_modelo, datcallback.veces_modelo_opt, datcallback.veces_modelo_noopt, datcallback.veces_modelo_gap, datcallback.veces_corte, datcallback.veces_encuentra_sol, nr,nc,datcallback.tiempo_heu,datcallback.tiempo_modelo);
	//fprintf(f,"%.2f %.2f %.2f %d \n",gap, VFO, secs, contc);
	fclose(f);


double peso_cam=0;
double pesopos1=0;
double pesopos2=0;
double pos=0;
double media_peso=0;
double peso_por_cli=0;

	//packing�
	fprintf(f2,"PACKING \n");
	if (datcallback.modelo != 2 && datcallback.modelo!=4)
	{
		list<camion>::iterator  itsol = solm.begin();
		for (int k = 0; k < maxcam; k++)
		{
			peso_cam = 0;
			pesopos1 = 0;
			pesopos2 = 0;
			pos = 0;
			if (cplex.isExtracted(y_k[k]) && cplex.getValue(y_k[k]) > 0.0001)
			{
				//La ruta
				for (int i = 0; i <= maxclientes; i++)
				{
					for (int j = 0; j <= maxclientes; j++)
					{
						if (i == j) continue;
						if (cplex.isExtracted(x_kij[k][i][j]) && cplex.getValue(x_kij[k][i][j]) > 0.0001)
						{
							(*itsol).ruta.at(i) = j;
							//fprintf(f3,"%lf + ",l_cli.at(i).distancias.at(j));
						}
					}
				}
				//el packing
				peso_cam = 0;
				for (int p = 0; p < cam.posiciones; p++)
				{
					for (int i = 1; i <= maxclientes; i++)
					{
						itp = l_cli.at(i).pedidos.begin();
						for (int t = 0; t < ((int)l_cli.at(i).pedidos.size()); t++)
						{
							if (cplex.isExtracted(z_kitp[k][i][t][p]) && cplex.getValue(z_kitp[k][i][t][p]) > 0.00001)
							{
								fprintf(f2, "Z%d %d %d %d \t %.2f \n", k, i, t, p, (*itp).peso);
								peso_cam += (*itp).peso;
								(*itsol).peso_pos.at(p) = (*itp).peso;
								(*itsol).pos_carga.at(p) = i;
								(*itsol).peso_por_cli.at(i) += (*itp).peso;
								if (p < cam.posiciones / 2)	pos = (p * datcallback.largo_pallet) + (datcallback.largo_pallet / 2);
								else pos = ((p - (cam.posiciones / 2)) * datcallback.largo_pallet) + (datcallback.largo_pallet / 2);
								pesopos1 += (*itp).peso * (datcallback.pos_eje2 - pos);
								pesopos2 += (*itp).peso * (pos - datcallback.pos_eje1);
							}
							itp++;
						}
					}
				}
				double eje1 = pesopos1 / (datcallback.pos_eje2 - datcallback.pos_eje1);
				double eje2 = pesopos2 / (datcallback.pos_eje2 - datcallback.pos_eje1);

				(*itsol).peso_eje1 = eje1;
				(*itsol).peso_eje2 = eje2;
				(*itsol).peso_total = peso_cam;

				int ini = 0;
				int num = 0;
				int dest = (*itsol).ruta.at(0);
				double aux = (*itsol).peso_total;
				media_peso += (*itsol).peso_total / l_cli.at(0).distancias.at(dest);
				bool fin = false;
				num++;
				while (!fin)
				{
					aux -= (*itsol).peso_por_cli.at(dest);
					ini = dest;
					dest = (*itsol).ruta.at(ini);
					media_peso += aux / l_cli.at(ini).distancias.at(dest);
					if (dest == 0) fin = true;
					else num++;
				}


				(*itsol).media_peso = (media_peso / num);
				fprintf(f2, "truck weigth %d: %.2f axle 1: %.2f axle2: %.2f media kg/km:%.2f\n", k, peso_cam, eje1, eje2, (*itsol).media_peso);
			}
			else itsol++;
		}
	}
	else
	{
		vector<datos>::iterator itd;
		int k = -1;
		for (itd = datcallback.mejor_sol_1cam.begin(); itd != datcallback.mejor_sol_1cam.end(); itd++)
		{
			k++;
			for (int p = 0; p < datcallback.posiciones;p++)
			{
				if ((*itd).f1_cli.at(p) != 0)
				{
					fprintf(f2, "Z%d %d %d %d \t %.2f \n", k, (int)(*itd).f1_cli.at(p), (*itd).f1_t.at(p), p, (*itd).f1_peso.at(p));
				}
			}
			fprintf(f2, "truck weigth %d: %.2f axle 1: %.2f axle2: %.2f media kg/km:%.2f\n", k, (*itd).peso_ca,(*itd).p_eje1, (*itd).p_eje2, (*itd).peso_ca/(*itd).fo_heu);
		}
	}

	/*double aux=0;
	for(itsol=solm.begin();itsol != solm.end(); itsol++)
	{
		aux+=(*itsol).media_peso;
	}
	aux=aux/contc;*/

	
fclose(f2);


}

void LPCPLEX::sol_to_draw(IloCplex cplex,int maxcam, int maxclientes, camion cam,vector<cliente> l_cli, NumVarMatrix3F x_kij, NumVarMatrix4  z_kitp, NumVector  y_k,NumVarMatrix s_kp ,char* nom,datos datcallback, list<camion>& solm, int esta, int multidrop)
{

	char nom_file[50];
	char nom_file2[50];
	list<pallet>::iterator itp;
	int pallets_cada_cliente[10];
	for(int j=0; j < 10; j++) pallets_cada_cliente[j]=0;
	int demanda_total=0;
	double peso_total=0;
	double pesopos1=0;
	double pesopos2=0;
	double peso_cogx=0;
	double peso_cogy=0;
	double posydetras=(cam.ancho)/4;
	double posydelante=(cam.ancho*3)/4;
	double pos=0, posy=0,pos2=0;
	int demanda_camion=0;
	int rutas[16][7];
	int cli_por_ruta[16];
	int cli_per_ruta=0;
	int pos_pal_cli[32];
	bool multi=true;
	int contc=0;
	bool estabilidad=true;
	

	//compongo el nombre del fichero
	strcpy(nom_file,".\\Results\\Dibujo_");
	strcpy(nom_file2,".\\Results\\Compro_");
	char aux[10];
	char aux2[10];
	sprintf(aux, "%d", maxclientes);
	//itoa(maxclientes,aux,10);
	strcpy(aux2,"Inst");
	strcat(aux2,aux);
	char * pch=strstr(nom,aux2);
	strncat(nom_file,pch,strlen(pch)-3);
	strncat(nom_file2,pch,strlen(pch)-3);
	strcat(nom_file,"txt");
	strcat(nom_file2,"txt");
	FILE *f=fopen(nom_file,"a+");
	FILE *f2=fopen(nom_file2,"a+");
	char nom_instancia[50];
	strcpy(nom_instancia,"Inst");
	strncat(nom_instancia,pch,12);
	

	//solucion
	for(int p=0;p < cam.posiciones; p++)
	{
		pos_pal_cli[p]=0;
	}

	//RUTA
	for(int k=0; k < maxcam; k++)
	{
		cli_por_ruta[k]=0;
		for(int i=0;i <= maxclientes;i++)
		{
			rutas[k][i]=0;
		}
	}
	for(int k=0; k < maxcam; k++)
	{
		if(cplex.isExtracted(y_k[k])&& cplex.getValue(y_k[k])>0.0001)
		{
			contc++;
			for(int i=0; i <= maxclientes; i++)
			{
				for(int j=0; j <= maxclientes; j++)
				{
					if(i==j) continue;
					if(cplex.isExtracted(x_kij[k][i][j])&& cplex.getValue(x_kij[k][i][j])>0.00001)
					{
						rutas[k][i]=j;
						if (j!=0) cli_por_ruta[k]++;
					}
				}
			}
		}
	}


	for (int k=0; k <maxcam; k++)
	{
		if(cli_por_ruta[k]==0) continue;

		for(int p=0; p<cam.posiciones; p++)
		{
			pos_pal_cli[p]=0;
			for(int i=1; i <= maxclientes; i++)
			{
				for(int t=0; t < ((int)l_cli.at(i).pedidos.size()); t++)
				{
					itp = l_cli.at(i).pedidos.begin();
					while ((*itp).id != t)itp++;
					if(cplex.isExtracted(z_kitp[k][i][t][p])&& cplex.getValue(z_kitp[k][i][t][p])>0.01)
					{
						fprintf(f,"%d;%d;%d;%d;%.2f\n",k,i,t,p,(*itp).peso);
						//printf("Z_%d_%d_%d_%d\n",k,i,t,p);
						//printf("%d, %d, %d, %d\n",k,i,t,p);
						pos_pal_cli[p]=i;
						pallets_cada_cliente[i]++;
						demanda_total++;
						demanda_camion++;
						peso_total+=(*itp).peso;
						
						pos=(p%(cam.posiciones/2))*cam.ancho_pallet+(cam.ancho_pallet/2);
						pesopos1+=(*itp).peso*(cam.pos_eje2-pos);
						pesopos2+=(*itp).peso*(pos-cam.pos_eje1);
						peso_cogx+=(*itp).peso*pos;
						if(p < cam.posiciones/2) posy=posydetras; else pos=posydelante;
						peso_cogy+=(*itp).peso*posy;
					}
					itp++;
				}
			}
		}
		//
		double max_peso_eje1=pesopos1/(cam.pos_eje2-cam.pos_eje1);
		double max_peso_eje2=pesopos2/(cam.pos_eje2-cam.pos_eje1);
		double cogx=peso_cogx/peso_total;
		double cogy=peso_cogy/peso_total;
		fprintf(f2,"%d;%.2f;%.2f;%.2f;%.2f;%.2f;%d\n",k,peso_total,max_peso_eje1,max_peso_eje2,cogx,cogy,demanda_camion);

		//Comprobaciones
		if(peso_total > cam.max_total_peso) 
		{
			FILE *fi=fopen(".\\Results\\comprobaciones.txt","a+");
			fprintf(fi,"%s,El cami�n %d sobrepasa peso maximo.\n",nom_instancia,k);
			fclose(fi);
		}
		else
		{
			if(max_peso_eje1 > cam.max_peso_eje1)
			{
				FILE *fi=fopen(".\\Results\\comprobaciones.txt","a+");
				fprintf(fi,"%s, El cami�n %d sobrepasa peso maximo del eje 1.\n",nom_instancia,k);
				fclose(fi);
			}
			else
			{
				if(max_peso_eje2 > cam.max_peso_eje2)
				{
					FILE *fi=fopen(".\\Results\\comprobaciones.txt","a+");
					fprintf(fi,"%s, El cami�n %d sobrepasa peso maximo del eje 2.\n",nom_instancia,k);
					fclose(fi);
				}
				else
				{
					if(cogx > cam.pos_eje2 || cogx < 0)
					{
						FILE *fi=fopen(".\\Results\\comprobaciones.txt","a+");
						fprintf(fi,"%s, El cami�n %d no tiene el centro de gravedad en X dentro de su umbral.\n",nom_instancia,k);
						fclose(fi);
					}
					else
					{
						if(cogy > posydelante || cogy < posydetras)
						{
							FILE *fi=fopen(".\\Results\\comprobaciones.txt","a+");
							fprintf(fi,"%s, El cami�n %d no tiene el centro de gravedad en Y dentro de su umbral.\n",nom_instancia,k);
							fclose(fi);
						}
						else
						{
							if(multidrop==1)
							{
								multi=true;
								//multidrop  
								int tok1=(cam.posiciones/2)-1;
								while(pos_pal_cli[tok1]==0 && pos_pal_cli[tok1+(cam.posiciones/2)]==0)
								{
									tok1--;
								}
								int tok2=0;
								while(pos_pal_cli[tok2]==0 && pos_pal_cli[tok2+(cam.posiciones/2)]==0)
								{
									tok2++;
								}
								int cli=rutas[k][0];
								cli_per_ruta=cli_por_ruta[k];
								while (cli_per_ruta > 0)
								{
									//todo mismo cliente
									if(pos_pal_cli[tok1]==cli && pos_pal_cli[tok2]==cli)
									{
										for(int aux=tok2; aux<tok1; aux++)
										{
											if(pos_pal_cli[aux]!=cli)
											{
												multi=false;
												break;
											}
										}
									}
									else
									{
										int aux2=tok2;
										while(pos_pal_cli[aux2]!=cli && pos_pal_cli[aux2+(cam.posiciones/2)]!=cli)
										{
											aux2++;
										}
										if((pos_pal_cli[aux2]!=cli && pos_pal_cli[aux2+(cam.posiciones/2)]==cli)||
											(pos_pal_cli[aux2]==cli && pos_pal_cli[aux2+(cam.posiciones/2)]!=cli))
										{
											aux2++;
										}
										if(tok1 > aux2)
										{
											for(int aux=aux2; aux<tok1; aux++)
											{
												if(pos_pal_cli[aux]!=cli)
												{
													multi=false;
													break;
												}
											}
											if(multi==true) tok1=aux2;
										}
									}
									
									cli=rutas[k][cli];
									cli_per_ruta--;
								}
							}
							if (multi==false && multidrop==1)
							{
								FILE *fi=fopen(".\\Results\\comprobaciones.txt","a+");
								fprintf(fi,"%s, El cami�n %d no el multidrop.\n",nom_instancia,k);
								fclose(fi);
							}
							else
							{
								if(esta==1) //comprobamos que no hay huecos en medio.
								{
									int tok1=-1,tok2=-1;
									for(int p1=0; p1< (cam.posiciones/2); p1++) //donde empieza los pallets por la cabina
									{
										if(pos_pal_cli[p1]!=0||pos_pal_cli[p1+(cam.posiciones/2)]!=0)
										{
											tok1=p1;
											break;
										}
									}
									for(int p2=(cam.posiciones/2)-1; p2 >=0; p2--) //donde empieza los pallets desde la puerta
									{
										if(pos_pal_cli[p2]!=0||pos_pal_cli[p2+(cam.posiciones/2)]!=0)
										{
											tok2=p2;
											break;
										}
									}
									//no debe haber hueco entre medias
									for(int p1=tok1+1; p1 <tok2; p1++)
									{
										if(pos_pal_cli[p1]==0||pos_pal_cli[p1+(cam.posiciones/2)]==0)
										{
											estabilidad=false;
											break;
										}
									}
									if(estabilidad==false)
									{
										FILE *fi=fopen(".\\Results\\comprobaciones.txt","a+");
										fprintf(fi,"%s, El cami�n %d tiene huecos en medio.\n",nom_instancia,k);
										fclose(fi);
									}
								}
							}
						}
					}
				}
			}
		}
		demanda_camion=0;
		peso_total=0;
		pesopos1=0;
		pesopos2=0;
		peso_cogx=0;
		peso_cogy=0;
	}


	//Compruebo que la demnada esta satisfecha.
	int totalpallets=0;
	
	for(int ci=1; ci <= maxclientes; ci++)
	{
		totalpallets+=l_cli.at(ci).pedidos.size();
		
		if(pallets_cada_cliente[ci]!=l_cli.at(ci).pedidos.size()) 
		{
			FILE *fi=fopen(".\\Results\\comprobaciones.txt","a+");
			fprintf(fi,"%s, La demnada del cliente %d no est� satisfecha,%d, %d\n",nom_instancia,ci,pallets_cada_cliente[ci],l_cli.at(ci).pedidos.size());
			fclose(fi);
			break;
		}
	}
	if(demanda_total > totalpallets || demanda_total < totalpallets)
	{
		FILE *fi=fopen(".\\Results\\comprobaciones.txt","a+");
		fprintf(fi,"%s, La demnada total no est� satisfecha, %d, %d\n", nom_instancia,demanda_total,totalpallets);
		fclose(fi);
	}


	fclose(f);
	fclose(f2);

}

void LPCPLEX::add_sol_cw(IloRangeArray& restr2, int maxcam, int maxclientes,camion & cam,vector<cliente> l_cli, vector<datos> solcw, NumVarMatrix3F& x_kij, NumVarMatrix4& z_kitp, NumVector& y_k, NumVarMatrix3F& f_kij, NumVarMatrix& l_kp, NumVarMatrix& r_kp, NumVarMatrix& s_kp,IloCplex& cplex2)
{
	
	IloNumArray startVal(env);
	IloNumVarArray startVar(env);



	

	int cont = 0;

	for (int k = 0; k < maxcam; k++)
	{
		//Y_k
		if (k < solcw.size())
		{
			startVar.add(y_k[k]);
			startVal.add(1);
			/*IloExpr v(env);
			v += y_k[k];
			restr2.add(IloRange(env, 1, v, 1));
			v.end();*/
		}
		//else startVal.add(0);
		//cont++;



		for (int i = 0; i <= maxclientes; i++)
		{
			
			for (int j = 0; j <= maxclientes; j++)
			{
				//X_k_i_j
				if (i == j) continue;				
				if (solcw.at(k).ruta.at(i) == j)
				{
					startVar.add(x_kij[k][i][j]);
					startVal.add(1);
					/*IloExpr v(env);
					v += x_kij[k][i][j];
					restr2.add(IloRange(env, 1, v, 1));
					v.end();*/
					//printf("X_%d_%d_%d \n",k,i,j);
				}
				//else startVal.add(0);
				cont++;
				//F_k_i_j
				//startVar.add(f_kij[k][i][j]);
				//startVal.add(-1);	
				//cont++;
			}
			
			
			for (int t = 0; t < ((int)l_cli.at(i).pedidos.size()); t++)
			{
				for (int p = 0; p < cam.posiciones; p++)
				{
					//Z_k_i_t_p					
					if (solcw.at(k).f1_peso.at(p) != 0 && solcw.at(k).f1_cli.at(p) == i && solcw.at(k).f1_t.at(p)==t)
					{
						startVar.add(z_kitp[k][i][t][p]);
						startVal.add(1);

						/*IloExpr v(env);
						v += z_kitp[k][i][t][p];
						restr2.add(IloRange(env, 1, v, 1));
						v.end();*/
						//printf("Z_%d_%d_%d_%d\n",k,i,t,p);
					}
					//else startVal.add(0);
					cont++;
				}
			}
		}
	
		//for (int p = 0; p < cam.posiciones; p++)
		//{
		//	//L_k_p			
		//	if ((p == 0 || p == (cam.posiciones / 2)) && solcw.at(k).f1_cli.at(p) != 0)
		//	{
		//		startVar.add(l_kp[k][p]);
		//		startVal.add(1);
		//	}
		//	else
		//	{
		//		if (solcw.at(k).f1_cli.at(p) != 0 && solcw.at(k).f1_cli.at(p - 1) == 0)
		//		{
		//			startVar.add(l_kp[k][p]);
		//			startVal.add(1);
		//		}
		//		//else startVal.add(0);
		//	}
		//	cont++;

		//	//R_k_p			
		//	if ((p == cam.posiciones - 1 || p == (cam.posiciones / 2) - 1) && solcw.at(k).f1_cli.at(p) != 0)
		//	{
		//		startVar.add(r_kp[k][p]);
		//		startVal.add(1);
		//	}
		//	else
		//	{
		//		if (solcw.at(k).f1_cli.at(p) != 0 && solcw.at(k).f1_cli.at(p + 1) == 0)
		//		{
		//			startVar.add(r_kp[k][p]);
		//			startVal.add(1);
		//		}
		//		//else startVal.add(0);
		//	}
		//	cont++;
		//}
		//for (int p = 0; p < cam.posiciones / 2; p++)
		//{
		//	//S_k_p			
		//	int frente = p + (cam.posiciones / 2);
		//	if (solcw.at(k).f1_cli.at(p) != 0 && solcw.at(k).f1_cli.at(p + (cam.posiciones / 2)) == 0)
		//	{
		//		startVar.add(s_kp[k][p]);
		//		startVal.add(1);
		//	}
		////	else startVal.add(0);
		//	cont++;
		//}
		
	}


//int nvl = startVal.getSize();
//int nvr = startVar.getSize();
cplex2.addMIPStart(startVar, startVal);
startVal.end();
startVar.end();


}
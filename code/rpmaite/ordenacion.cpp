#include "stdafx.h"

void ordena(vector<cliente>& list_clientes)
{
	list<pallet>::iterator it1, it2;

	for(int i=0;i < (int)list_clientes.size(); i++)
	{
		if((int)list_clientes.at(i).pedidos.size() > 0)
		{
			ord_lenta(list_clientes.at(i).pedidos);
		}
	}
}


void ord_lenta(list<pallet>& lp)
{
	list<pallet>::iterator itp=lp.begin();
	list<pallet>::iterator itord=lp.begin();
	list<pallet>::iterator itpesado;

	//busco el m�s pesado de entre los desordenados
	while(itord!=lp.end())
	{
		itp=itord;
		itpesado=itp;
		for(itp; itp!=lp.end(); itp++)
		{
			if((*itp).peso > (*itpesado).peso)
			{
				itpesado=itp;
			}
		}

		if (itord!=itpesado)
		{
			lp.insert(itord,(*itpesado));
			lp.erase(itpesado);
			//itord++;
		}else itord++;
	}
	

	
}

/***********************************
Ordenaci�n Quicksort
************************************/

void ord_rapida(vector<int>& l_l,int izq_p, int der_p)
{

	int piv; 
	int i=izq_p;
	int j=der_p;

	if (der_p - izq_p >= 1) 
	{
		piv=izq_p;
		while(j > i)
		{
			while((i <= der_p) && (j > i) && (compara(l_l,i,piv)>=0))i++;
			while((j>=izq_p) && (j>=i) && (compara(l_l,j,piv)<0))j--;
			if(j > i)
			{
				intercambiar(l_l,j,i);
			}
		}
		intercambiar(l_l,izq_p,j);

		ord_rapida(l_l,izq_p,j-1);
		ord_rapida(l_l,j+1,der_p);
	}
}

void intercambiar(vector<int> &lp, int i, int j)
{
	int tmp = lp.at(i);
	lp.at(i) = lp.at(j);
	lp.at(j) = tmp;
}

/*
* return 0 if is equal
* return 1 if lp(i) > lp(j)
* return -1 if lp(i) < lp(j)
*/
int compara(vector<int> ll, int i, int j)
{
	//by day
	/*
	if(ll.at(i).get_id_prio() > ll.at(j).get_id_prio()) return -1;
	else
	{
		if(ll.at(i).get_id_prio() < ll.at(j).get_id_prio()) return 1;
		else // by density
		{
			if(ll.at(i).get_density() > ll.at(j).get_density()) return 1;
			else
			{
				if(ll.at(i).get_density() < ll.at(j).get_density()) return -1;
				else //by layer dimension X
				{
					if(ll.at(i).get_lenght() > ll.at(j).get_lenght()) return 1;
					else
					{
						if(ll.at(i).get_lenght() < ll.at(j).get_lenght()) return -1;
						else //by layer dimension Y
						{
							if(ll.at(i).get_width() > ll.at(j).get_width()) return 1;
							else
							{
								if(ll.at(i).get_width() < ll.at(j).get_width()) return -1;
								else return 0;
							}
						}
					}
				}
			}
		}
	}*/
	return 1;
}
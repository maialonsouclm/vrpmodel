#ifndef PALLET_H
#define PALLET_H
class pallet
{
public:
	double largo;
	double ancho;
	double alto;
	double peso;
	int id;
	int id_instancia;
	

	pallet()
	{
		largo=800;
		ancho=1200;
		alto=145;
		peso=25;
	};
	pallet(double larg, double anch,double alt, double pes,int ident)
	{
		largo=larg;
		ancho=anch;
		alto=alt;
		peso=pes;
		id = ident;
		id_instancia = 0;
	};
	pallet(double larg, double anch, double alt, double pes, int ident, int id_inst)
	{
		largo = larg;
		ancho = anch;
		alto = alt;
		peso = pes;
		id = ident;
		id_instancia = id_inst;
	};
	
};
#endif
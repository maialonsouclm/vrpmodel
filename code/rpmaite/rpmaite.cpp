// rpmaite.cpp: define el punto de entrada de la aplicación de consola.
//
extern  int DibujarOpenGL();
#include "stdafx.h"
#include <algorithm>



int main(int argc, char *argv[])
{
	vector<cliente> clientes;
	camion truck;
	list<camion> solucion;
	datos datcallback;
	list<camion> sol_model;
	float fo_heuristico = 0;

	//PARÁMETROS
	bool log = false;

	//genera_instancias_TSP(15, 1, "v", 5);
	//genera_instancias_TSP(15, 2, "v", 5);
	//genera_instancias_TSP(15, 3, "v", 5);
	////genera_instancias_TSP(5, 4, "v", 5);
	//genera_instancias_TSP(15, 5, "v", 5);
	//genera_instancias_TSP(15, 1, "u", 5);
	//genera_instancias_TSP(15, 2, "u", 5);
	//genera_instancias_TSP(15, 3, "u", 5);
	////genera_instancias_TSP(5, 4, "u", 5);
	//genera_instancias_TSP(15, 5, "u", 5);
	////genera_instancias();
	//generabat();

// RECOGEMOS LOS PARÁMETROS
	//Numero de clientes
	int num_clientes=atoi(argv[2]);
	//split and delivery or vrp
	int split=atoi(argv[3]);
	//con o sin multidrop
	int multi=atoi(argv[4]);
	//con o sin estabilidad
	int esta=atoi(argv[5]);
	//tiempo del modelo
	int maxtime=atoi(argv[6]);
	//tipo de modelo
	int modelo=atoi(argv[7]);


	//LEO LOS DATOS DE LA INSTANCIA
		//Ortec: leer_csv(argv[1],clientes,truck);
		//Pollaris:leer_pollaris(argv[1],clientes,truck,num_clientes);
	//Mias:
	leer_mias(argv[1],clientes,truck,num_clientes);
	   
	//ordeno los pallets de los clientes por peso de mayor a menor
	ordena(clientes);

	//COTA INFERIOR
	int c_inferior = cota_inferior(truck, clientes);

	//rellena datos para el callbak.
	rellena_callback(datcallback, clientes, truck, 11, num_clientes, argv[1], modelo);
	
	//Cota superior de C&W
	datcallback.solcw = clarkwrigth_toni(solucion, truck, clientes, datcallback);

	//Actualizo las cotas superiores de c&W
	datcallback.maxcam = datcallback.solcw.size();
	int cs_cam = datcallback.solcw.size();

	if (log)
	{
		//Escribo la solución del c&w
		escribo_sol_cw(datcallback.solcw,clientes,argv[1],c_inferior);
	}
	//
	//relleno la solución del modelo
	for(int k=0; k < cs_cam; k++)
	{
		camion cam;
		cam.add_datos(k,truck.ancho,truck.alto,truck.largo,truck.peso_vacio,truck.pos_eje1,truck.max_peso_eje1,truck.pos_eje2,truck.max_peso_eje2,0,0,0,0,truck.posiciones);
		sol_model.push_back(cam);
	}
	//
	//////LLAMO A LOS MODELOS
	//
	if(split==1)split_and_delivery(clientes, truck,cs_cam, multi, esta, argv[1], datcallback,maxtime,modelo,sol_model);
	//
	//
	//
	//DibujarOpenGL(truck);
	
	
	return 0;
}

void rellena_callback(datos &datcallback,vector<cliente>clientes, camion truck, int cs_cam, int num_clientes, char* nom, int modelo)
{
	datcallback.maxcam=cs_cam;
	datcallback.maxcli=(int)clientes.size()-1;
	datcallback.posiciones=truck.posiciones;
	datcallback.max_eje1=truck.max_peso_eje1;
	datcallback.max_eje2=truck.max_peso_eje2;
	datcallback.largo_pallet=truck.ancho_pallet;
	datcallback.pos_eje1=truck.pos_eje1;
	datcallback.pos_eje2=truck.pos_eje2;
	datcallback.veces_bien=0;
	datcallback.veces_corte=0;
	datcallback.veces_entra=0;
	datcallback.veces_entra_k=0;
	datcallback.fo_heu=0;
	datcallback.fo_mejor_sol_heu=0;
	datcallback.ancho=truck.ancho;
	datcallback.largo=truck.largo;
	datcallback.veces_encuentra_sol=0;
	datcallback.modelo=modelo;
	datcallback.veces_modelo = 0;
	strcpy(datcallback.nominsta,nom);
	datcallback.peso_total=truck.max_total_peso;
	datcallback.veces_modelo_noopt = 0;
	datcallback.veces_modelo_opt = 0;
	datcallback.lleno = false;
	datcallback.compartido = false;
	datcallback.tiempo_heu = 0;
	datcallback.tiempo_modelo = 0;


	for(int i=0; i < truck.posiciones; i++)
	{
		datcallback.f1_peso.push_back(0);
		datcallback.f1_cli.push_back(0);
		datcallback.f1_t.push_back(-1);
	}
	for(int i=0; i <= num_clientes; i++)
	{
		datcallback.ruta.push_back(-1);
		datcallback.cuantos.push_back(0);
	}
	strcpy(datcallback.fich,"./ModeloSAD.txt");
}

// stdafx.h: archivo de inclusi�n de los archivos de inclusi�n est�ndar del sistema
// o archivos de inclusi�n espec�ficos de un proyecto utilizados frecuentemente,
// pero rara vez modificados

#pragma once

#include "targetver.h"

#include <stdio.h>

// TODO: mencionar aqu� los encabezados adicionales que el programa necesita
#include <stdlib.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <vector>
#include <list>
#include <iostream>
#include <time.h>
#include <algorithm>



#define _CRTDBG_MAP_ALLOC
#pragma warning(disable: 4786)

using namespace std;
//#include "bloque.h"
//#include "espacio.h"
//#include "espacios.h"
//#include "box.h"
//#include "layer.h"
#include "pallet.h"
#include "camion.h"
#include "cliente.h"


#define get_random(min,max) ((rand() % (int)(((max)+1)-(min)))+(min))


#ifdef _DEBUG
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif  // _DEBUG


struct datos{
	int maxcam;
	int maxcli;
	int posiciones;
	int num_pallets;
	vector<int> ruta;
	vector<double> f1_peso;
	vector<double> f2_peso;
	vector<double> f1_cli;
	vector<double> f2_cli;
	vector<int> f1_t;
	vector<int> f2_t;
	vector<int> cuantos;
	double largo_pallet;
	double peso_total;
	double max_eje1;
	double max_eje2;
	double pos_eje1;
	double pos_eje2;
	double largo;
	double ancho;
	char fich[100];
	int veces_entra;
	int veces_bien;
	int veces_corte;
	int veces_entra_k;
	int veces_encuentra_sol;
	int veces_modelo;
	int veces_modelo_opt;
	int veces_modelo_noopt;
	int veces_modelo_gap;
	double fo_heu;
	double fo_mejor_sol_heu;
	double p_eje1;
	double p_eje2;
	double peso_ca;
	char nominsta[1000];
	int modelo;
	bool lleno;
	bool compartido;
	vector<datos> solcw;
	vector<datos> sol_1cam;
	vector<datos> mejor_sol_1cam;
	double tiempo_heu;
	double tiempo_modelo;
	

};
struct ahorro {
	int ini;
	int dest;
	int sav;
};

struct ahorro_toni {
	int first_route;
	int second_route;
	int sav;
};
struct ahorroComparator
{
	// Compare 2 ahorros
	bool operator ()(const ahorro_toni & a, const ahorro_toni & b)
	{
		return a.sav > b.sav;

	}
};

//void rellenar_datos_callback(datos &dcall,camion truck,vector<cliente> cli );
void rellena_callback(datos &datcallback,vector<cliente>clientes, camion truck, int cs_cam, int num_clientes, char* nom, int modelo);
//LECTURA DE DATOS
void leer_csv(char* name,vector<cliente>& list_clientes, camion &truck);
void leer_pollaris(char* name,vector<cliente>& list_clientes, camion &truck,int maxcli);
//void Constructivo(vector<cliente>& list_clientes, camion &truck);
//bool Crear_bloque(cliente *itcli,espacio hueco,bloque &bloc, pallet eupal);
//bool Buscar_hueco(espacio &esp,camion &truck, pallet euro_pallet);
void genera_instancias();
void generabat();
void leer_mias(char* name,vector<cliente>& list_clientes, camion &truck, int maxcli);
void generar_pallets(int cant_pal, int min, int max, vector<int> &pallets);
void genera_instancias_TSP(int num_cli, int numpal, char* demanda, int cant_ins);
void reparte_pal(int tipodem, vector<int> pallets, char* nomarchivo, int numcli);
void leer_dist(int ncli, char* nomarchivo);

//ORDENACION
void ordena(vector<cliente>& list_clientes);
void ord_lenta(list<pallet>& lp);
int compara(vector<int> ll, int i, int j);
void ord_rapida(vector<int>& l_l,int izq_p, int der_p);
void intercambiar(vector<int> &lp, int i, int j);


//HEURISTICO
int cota_superior(list<camion>& solucion, camion truck, vector<cliente> l_clientes,float &fun_obj);
int cota_inferior(camion truck, vector<cliente> clientes);
bool cabe_pallet(int pos,pallet itp,camion truck,camion cam,double &newcogx,double &newcogy,double &G1,double &G2);
bool comprobar_pesos_ejes(list<camion> solucion);
int  clarkwrigth(list<camion>& solucion, camion truck, vector<cliente> l_clientes, datos dat);
// Clark&Wright - Toni
vector<datos> clarkwrigth_toni(list<camion>& solucion, camion truck, vector<cliente> l_clientes, datos dat);
void rellenar_camiones_un_cliente(vector<datos>& sol, int posmedio, vector<cliente> lpc, int cliente, camion truck);
bool colocar(pallet  pal, int pos, datos &dat, double &totp, double &pee1, double &pee2, int cli);
bool colocar2(pallet  pal, pallet pal2, int pos, datos& dat, double& totp, double& pee1, double& pee2, int cli);
void escribo_sol_cw(vector<datos> aa, vector<cliente> l_clientes,char* nom_inst,int cinf);
void escribirsol(vector<datos> dat);



//Dibujar
int DibujarOpenGL();
/* int DibujarOpenGL(camion &cam);
void EscribirMejorSolucionOpenGL(camion &cam);
static void ejes(float o_x, float o_y, float o_z, float x,float y, float z);
static void cubo(GLfloat o_x, GLfloat o_y, GLfloat o_z, GLfloat x, GLfloat y, GLfloat z,int Id ,int cant);
void DibujaGL( );
void Ventana(GLsizei ancho,GLsizei alto);
static void letra (unsigned char k, int x, int y);
static void especial(int k, int x, int y);
void seleccionMenu( int opcion );
void clickRaton( int boton, int estado, int x, int y );
static void RatonMovido( int x, int y );
void CreaMenu();
int DibujarOpenGL2(vector<int> cc); */

/**********************/
//MODELOS
void split_and_delivery(vector<cliente> l_cli, camion &cam, int cs, int multi, int esta, char* nom, datos datcallback,int maxtime, int modelo, list<camion>& solm);
void vrp(vector<cliente> l_cli, camion &truc, int cs, int multi, int esta, char* nom, datos datcallback, int maxtime,list<camion>& solm);

//CALLBACK
bool intercambiar(datos & dat, int tok1, int tok2, int cli, int &contp);
void comp_pal_cli(datos dat, int &pos,int &cli, int &contp);
bool comprobar_peso(datos &dat);
bool mover_derecha(datos dat);
bool mover_izquierda(datos dat);
void heuristico (datos& dat);
bool heuristico2(datos& dat, vector<vector<double> > pesosp, vector<vector<int> > p_t);
bool mov_der(datos &dat);
bool mov_izq(datos &dat, int prim_pos);

//soluciones
void escribir_sol_heuristico(datos& dat, char* nom,double VFO,double gap, double secs,int maxcam);



